package org.jproggy.motivity.test.entities;

import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.ForeignKeySchema.oIntegerPkStore;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.adapter.MssqlAdapter;
import org.jproggy.motivity.test.Bookstore;
import org.jproggy.motivity.test.Db;
import org.jproggy.motivity.test.ForeignKeySchemaData;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.util.List;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Tests whether the save methods work in the db object classes.
 *
 * @version $Id: SaveTest.java 1436782 2013-01-22 07:51:31Z tfischer $
 */
public class SaveTest {
  private static Log log = LogFactory.getLog(SaveTest.class);

  @BeforeEach
  void setUp() throws Exception {
    Db.setUp();
  }

  /**
   * Tests the save method for a simple object.
   */
  @Test
  void testInsert() throws Exception {
    // prepare
    Bookstore.clean();
    List<Author> bookstoreContent = Bookstore.insertData();
    Author author = new Author();
    author.setName("Author");

    // execute
    author.save();

    // verify
    assertNotNull(author.getAuthorId());
    assertEquals("Author", author.getName());

    bookstoreContent.add(author);
    Bookstore.verify(bookstoreContent);
  }

  /**
   * Check that an id can be set manually.
   */
  @Test
  void testInsertWithManualId() throws Exception {
    if (Db.defaultAdapter instanceof MssqlAdapter) {
      log.error("manual id insertion is not possible for MSSQL");
      return;
    }
    ForeignKeySchemaData.clearTablesInDatabase();

    OIntegerPk oIntegerPk = new OIntegerPk();
    oIntegerPk.setId(3001);

    oIntegerPk.save();

    assertEquals(3001, oIntegerPk.getId());

    List<OIntegerPk> integerObjectPkList = oIntegerPkStore.filter(oIntegerPkStore.id.equalTo(3001)).list();
    assertEquals(1, integerObjectPkList.size());

    assertEquals(1, oIntegerPkStore.all().count());
  }

  /**
   * Tests the save method for a simple object.
   *
   * @throws Exception if a database error occurs.
   */
  @Test
  void testUpdate() throws Exception {
    // prepare
    Bookstore.clean();
    List<Author> bookstoreContent = Bookstore.insertData();
    Author author = new Author();
    author.setName("Author");
    author.save();
    author.setName("nameModified"); // modify after saving

    // execute
    author.save();

    // verify
    assertNotNull(author.getAuthorId());
    assertEquals("nameModified", author.getName());
    bookstoreContent.add(author);
    Bookstore.verify(bookstoreContent);
  }

  /**
   * Tests that the save method propagates "down" for a foreign key,
   * i.e. if save is called on an object which contains another
   * referencing object, the referencing object must also be saved.
   *
   * @throws Exception if a database error occurs.
   */
  @Test
  void testInsertPropagationDown() throws Exception {
    // prepare
    Bookstore.clean();
    List<Author> bookstoreContent = Bookstore.insertData();
    Author author = new Author();
    author.setName("Author");
    Book book = new Book();
    author.addBook(book);
    book.setTitle("Book title");
    book.setIsbn("ISBN");

    // execute
    author.save();

    // verify
    assertNotNull(author.getAuthorId());
    assertEquals("Author", author.getName());
    assertNotNull(book.getBookId());
    assertEquals("Book title", book.getTitle());
    assertEquals("ISBN", book.getIsbn());

    bookstoreContent.add(author);
    Bookstore.verify(bookstoreContent);
  }

  /**
   * Tests that the save method does not propagate "up" for a foreign key,
   * i.e. if save is called on an object referencing another object,
   * the referenced object must not be saved.
   *
   * @throws Exception if a database error occurs.
   */
  @Test
  void testNoPropagationUp() throws Exception {
    // prepare
    Bookstore.clean();
    Author author = new Author();
    author.setName("Author");
    author.save();
    author.setName("nameModified"); // modify after saving

    Book book = new Book();
    author.addBook(book);
    book.setTitle("Book title");
    book.setIsbn("ISBN");

    // execute
    book.save();

    // verify
    // propagation would have been possible, reference is there
    assertNotNull(book.getAuthor());

    // Author in db should still have old name
    List<Author> authorList = authorStore.filter(
            authorStore.name.equalTo("Author")
    ).list();

    assertEquals(1, authorList.size());
  }
}
