package org.jproggy.motivity.test.datatypes;

import static org.jproggy.motivity.test.stores.TypesSchema.blobTypeStore;

import org.apache.torque.adapter.PostgresAdapter;

import org.jproggy.motivity.test.Db;
import org.jproggy.motivity.test.entities.BlobType;

class BlobTest extends ByteTypeTest<BlobType> {

  BlobTest() {
    super(blobTypeStore);
  }

  @Override
  void setBytes(byte[] bytes, BlobType toFill) {
    toFill.setBlobValue(bytes);
    toFill.setBlobObjectValue(bytes);
  }

  @Override
  byte[] getBytes(BlobType toRead) {
    return toRead.getBlobValue();
  }

  @Override
  boolean canWriteNullBytes() {
    // the problem with postgresql is that a setNull(Types.BLOB)
    // is executed by motivity which is not accepted for BYTEA columns
    return Db.defaultAdapter instanceof PostgresAdapter;
  }
}
