package org.jproggy.motivity.test.entities;

import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.BookstoreSchema.bookStore;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.jproggy.motivity.test.Bookstore;
import org.jproggy.motivity.test.Db;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * Tests whether the getRelatedObjects methods works
 */
public class GetRelatedObjectsTest {
  private List<Author> authorList;

  @BeforeEach
  public void setUp() throws Exception {
    Db.setUp();
    Bookstore.clean();
    authorList = Bookstore.insertData();
  }

  /**
   * Tests getters of related objects.
   */
  @Test
  public void testGetRelatedObjects() {
    int authorId = authorList.get(0).getAuthorId();
    Author author = authorStore.filter(authorStore.authorId.equalTo(authorId)).one();

    List<Book> books = author.getBooks();

    assertEquals(10, books.size());
    assertTrue(books.get(0).getTitle().endsWith("- Author 1"));
    checkBackreferenceExists(author, books);
  }

  /**
   * Tests getters of related objects.
   */
  @Test
  public void testGetRelatedObject() {
    int bookId = authorList.get(0).getBooks().get(0).getBookId();
    Book book = bookStore.filter(bookStore.bookId.equalTo(bookId)).one();

    Author author = book.getAuthor();
    assertEquals(authorList.get(0).getAuthorId(), author.getAuthorId());
  }

  /** Tests getters of related objects, after the collection was initialized. */
  @Test
  public void testGetRelatedObjectsAfterInit() throws Exception {
    int authorId = authorList.get(0).getAuthorId();
    Author author = authorStore.filter(authorStore.authorId.equalTo(authorId)).one();

    author.addBook(new Book());
    List<Book> books = author.getBooks();

    // Books should not be loaded from db but cache should be used
    assertEquals(11, books.size());
  }

  private void checkBackreferenceExists(final Author author, final List<Book> books) {
    for (Book book: books) {
      assertEquals(author, book.getAuthor());
      // check that adding the backreference did not lead to changed objects
      assertFalse(book.isModified());
    }
  }

}
