package org.jproggy.motivity.test;

import static org.jproggy.motivity.Motivity.and;
import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.BookstoreSchema.bookStore;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.test.entities.Author;
import org.jproggy.motivity.test.entities.Book;
import org.jproggy.motivity.transaction.Transaction;

public class Bookstore {

  /**
   * Deletes all authors and books in the bookstore tables.
   */
  public static void clean() {
    bookStore.all().delete();
    authorStore.all().delete();
  }

  /**
   * Inserts test data into the bookstore tables.
   *
   * @return the list of added authors, which in turn contain the added books.
   */
  public static List<Author> insertData() {
    List<Author> result = new ArrayList<>();
    for (int i = 1; i <= 10; i++) {
      Author author = new Author();
      author.setName("Author " + i);
      author.save();
      result.add(author);
      assertNotEquals(0, author.getAuthorId(), "authorId should not be 0 after insert");

      for (int j = 1; j <= 10; j++) {
        Book book = new Book();
        book.setAuthor(author);
        book.setTitle("Book " + j + " - Author " + i);
        if (j < 10) {
          book.setIsbn("ISBN " + j + " - " + i);
        } else {
          book.setIsbn(null);
        }
        book.save();
      }
    }
    return result;
  }

  /**
   * Checks that the bookstore tables contain exactly the records in the
   * passed list. The books in the authors are also checked.
   *
   * @param toVerify the list of authors to check.
   */
  public static void verify(final List<Author> toVerify) {
    int numBooks = 0;

    for (Author author: toVerify) {
      Author found = authorStore.filter(
              and(
                      authorStore.authorId.equalTo(author.getAuthorId()),
                      authorStore.name.equalTo(author.getName())
              )
      ).first().orElse(null);
      assertNotNull(found, "Not found: " + author);
      assertEquals(author.toString(), found.toString());

      numBooks += author.getBooks().size();

      for (Book book: author.getBooks()) {
        Condition booksFilter = bookStore.title.equalTo(book.getTitle());
        booksFilter.and(bookStore.bookId.equalTo(book.getBookId()));
        booksFilter.and(bookStore.authorId.equalTo(book.getAuthorId()));
        booksFilter.and(bookStore.isbn.equalTo(book.getIsbn()));
        Book foundBook = bookStore.filter(booksFilter).first().orElse(null);
        assertNotNull(foundBook, "Not found: " + book);
        assertEquals(book.toString(), foundBook.toString());
      }
    }

    assertEquals(toVerify.size(), authorStore.all().count());
    assertEquals(numBooks, bookStore.all().count());
  }

  public static Author author(String s) {
    Author author = new Author();
    author.setName(s);
    author.save();
    return author;
  }

  public static Book book(Author author, String title) {
    try (Transaction tra = Transactions.ensureOpen()) {
      Book book = new Book();
      book.setAuthor(author);
      book.setTitle(title);
      book.setIsbn("unknown");
      book.save();
      tra.commit();
      return book;
    }
  }

  public static Map<String, String> likeResults = new LinkedHashMap<>();
  static {
    likeResults.put("a\\_c", "a_c");
    likeResults.put("a\\_%", "a_c");
    likeResults.put("%\\_c", "a_c");

    likeResults.put("a\\%c", "a%c");
    likeResults.put("a\\%%", "a%c");
    likeResults.put("%\\%c", "a%c");

    likeResults.put("a\\\\c", "a\\c");
    likeResults.put("a\\\\%", "a\\c");
    likeResults.put("%\\\\c", "a\\c");

    likeResults.put("a*c", "a*c");
    likeResults.put("a*%", "a*c");
    likeResults.put("%*c", "a*c");

    likeResults.put("a?c", "a?c");
    likeResults.put("a?%", "a?c");
    likeResults.put("%?c", "a?c");

    likeResults.put("a\"c", "a\"c");
    likeResults.put("a\"%", "a\"c");
    likeResults.put("%\"c", "a\"c");

    likeResults.put("a'c", "a'c");
    likeResults.put("a'%", "a'c");
    likeResults.put("%'c", "a'c");
  }
}
