package org.jproggy.motivity.test;

import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Properties;
import org.apache.torque.Torque;
import org.apache.torque.adapter.Adapter;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.generator.TemplateSet;
import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.torque.transaction.TorqueTransactionHandler;
import org.jproggy.motivity.transaction.Transaction;

public class Db {
  /** The system property containing the path to the configuration file. */
  private static final String CONFIG_FILE_SYSTEM_PROPERTY = "motivity.profile";
  public static Adapter defaultAdapter;

  private static boolean isInitialized(Connection con) throws Exception {
    try (ResultSet rs = con.getMetaData().getTables(null, "%", "%", null)) {
      while (rs.next()) {
        if ("book".equalsIgnoreCase(rs.getString("TABLE_NAME"))) return true;
      }
    }
    return false;
  }
  private static void init(Connection con) throws Exception {
    Properties config = new Properties();
    config.put("templates.path", "org/jproggy/motivity/templates/ddl");
    config.put("database.variant", "derby");
    File schemesDir = new File("src/main/schema");
    ConfigParser parser = ConfigParser.of(config);
    File[] files = schemesDir.listFiles((d, n) -> n.endsWith("-schema.xml"));
    for (File schemeFile : files) {
        Configuration scheme = parser.load(schemeFile.toURI().toURL());
        TemplateSet.of(scheme).updateDb(con);
    }
  }
  public static void setUp() throws Exception {
    synchronized (Db.class) {
      if (!Torque.isInit()) {
        Torque.init("./src/test/profile/" + System.getProperty(CONFIG_FILE_SYSTEM_PROPERTY, "derbyEmbedded") + "/Torque.properties");
        TorqueTransactionHandler.onDatabases("bookstore");
      }
      try (Transaction tra = Transactions.ensureOpen();
           Connection con = Torque.getConnection()) {
        if (!Db.isInitialized(con)) {
          Db.init(con);
          tra.commit();
        }
      }
    }
    defaultAdapter = Torque.getDatabase(Torque.getDefaultDB()).getAdapter();
  }
}
