package org.jproggy.motivity.test;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.jproggy.motivity.Motivity.and;
import static org.jproggy.motivity.test.stores.QualifiedTableNameSchema.qualifiedNameStore;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.jproggy.motivity.test.entities.QualifiedName;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests that accessing the database also works if the table name is qualified
 * by a schema name.
 *
 * @version $Id: QualifiedTableNameTest.java 1839288 2018-08-27 09:48:33Z tv $
 */
class QualifiedTableNameTest {
    QualifiedName qualifiedName1;
    QualifiedName qualifiedName2;
    QualifiedName qualifiedName3;

    @BeforeEach
    void setUp() throws Exception {
        Db.setUp();
        qualifiedNameStore.all().delete();
        qualifiedName1 = new QualifiedName();
        qualifiedName1.setPayload("qualifiedName1");
        qualifiedName1.save();
        qualifiedName2 = new QualifiedName();
        qualifiedName2.setPayload("qualifiedName2");
        qualifiedName2.save();
        qualifiedName3 = new QualifiedName();
        qualifiedName3.setPayload("qualifiedName3");
        qualifiedName3.save();
    }

    /**
     * Tests that reading an entry from a table with qualified table name works.
     */
    @Test
    void testRead() throws Exception {
        List<QualifiedName> qualifiedNames = qualifiedNameStore.filter(
                and(
                        qualifiedNameStore.id.equalTo(qualifiedName1.getId()),
                        qualifiedNameStore.payload.equalTo(qualifiedName1.getPayload())
                )
        ).list();

        assertThat(qualifiedNames, hasItems(qualifiedName1));
    }

    /**
     * Tests that sorting entries from a table with qualified table name works.
     */
    @Test void testReadWithSort() throws Exception {
        List<QualifiedName> qualifiedNames = qualifiedNameStore.all().sorted(
                qualifiedNameStore.id.asc()
        ).list();

        assertThat(qualifiedNames, hasItems(qualifiedName1, qualifiedName2, qualifiedName3));
    }

    /**
     * Tests that updating an entry in a table with qualified table name works.
     */
    @Test void testUpdate() throws Exception {
        qualifiedName2.setPayload("qualifiedName2a");
        qualifiedName2.save();

        List<QualifiedName> qualifiedNames = qualifiedNameStore.filter(
                qualifiedNameStore.id.equalTo(qualifiedName2.getId())
        ).list();

        assertEquals(1, qualifiedNames.size());
        assertEquals(qualifiedName2.getId(), qualifiedNames.get(0).getId());
        assertEquals(qualifiedName2.getPayload(),  qualifiedNames.get(0).getPayload());
    }

    /**
     * Tests that deleting an entry from a table with qualified table name works.
     */
    @Test void testDelete() throws Exception {
        qualifiedNameStore.delete(qualifiedName2);
        List<QualifiedName> qualifiedNames = qualifiedNameStore.all().list();

        assertThat(qualifiedNameStore.all().list(), hasItems(qualifiedName1, qualifiedName3));
    }

    /**
     * Tests that the table name in the peer does not contain the schema name.
     */
    @Test
    void testTableNameInPeer() throws Exception{
        // verify
        assertEquals("bookstore.qualified_name", qualifiedNameStore.getName());
    }
 }
