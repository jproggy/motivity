package org.jproggy.motivity.test;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.BookstoreSchema.bookStore;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.jproggy.motivity.test.entities.Author;
import org.jproggy.motivity.test.entities.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests selects using the IN operator.
 *
 * @version $Id: SelectInTest.java 1839288 2018-08-27 09:48:33Z tv $
 */
public class SelectInTest {
    private List<Author> authorList;
    private Author author1;
    private Author author2;

    @BeforeEach
    void setUp() throws Exception {
        Db.setUp();
        Bookstore.clean();
        authorList = Bookstore.insertData();
        author1 = authorList.get(0);
        author2 = authorList.get(1);
    }

    /**
     * Tests "in" query with String list and ignoreCase = false.
     */
    @Test
    void testInWithStringListNoIgnoreCase() throws Exception {
        List<Author> result = authorStore.filter(
                authorStore.name.in("Author 1", "Author 2")
        ).sorted(
                authorStore.name.desc()
        ).list();

        assertThat(result, hasItems(author2, author1));
    }

    /**
     * Tests "in" query with String list and ignoreCase = true.
     */
    @Test
    void testInWithIntegerListNoIgnoreCase() throws Exception {
        List<Integer> idList = new ArrayList<>();
        idList.add(authorList.get(0).getAuthorId());
        idList.add(authorList.get(1).getAuthorId());

        List<Author> result = authorStore.filter(
                authorStore.authorId.in(idList.toArray(new Integer[0]))
        ).sorted(
                authorStore.authorId.desc()
        ).list();
        assertThat(result, hasItems(author2, author1));
    }

    /**
     * Tests "in" query with String list containing null value.
     */
    public void testInWithStringListAndNullValue() throws Exception {
        List<Book> result = bookStore.filter(
                bookStore.isbn.in("ISBN 1 - 1", null)
        ).sorted(
                bookStore.isbn.desc()
        ).list();
        assertEquals(11, result.size());
        assertEquals(authorList.get(0).getBooks().get(0).getBookId(), result.get(0).getBookId());
        assertEquals(null, result.get(1).getIsbn());
    }

    /**
     * Tests "in" query with String list and ignoreCase = true.
    public void testInWithStringListIgnoreCaseTrue() throws Exception {
        List<Author> result = authorStore.filter(
                authorStore.name.in("AuTHor 1", "AuTHor 2").ignoreCase()
        ).sorted(
                authorStore.name.desc()
        ).list();
        assertThat(result, contains(author2, author1));
    }
     */

    /**
     * Tests "in" query with Integer list and ignoreCase = true.
    public void testInWithIntegerListIgnoreCaseTrue() throws Exception {
        Criteria criteria = new Criteria();
        List<Integer> idList = new ArrayList<>();
        idList.add(authorList.get(0).getAuthorId());
        idList.add(authorList.get(1).getAuthorId());
        criteria.where(AuthorPeer.AUTHOR_ID, idList, Criteria.IN);
        criteria.setIgnoreCase(true);
        criteria.addDescendingOrderByColumn(AuthorPeer.AUTHOR_ID);

        List<Author> result = AuthorPeer.doSelect(criteria);
        assertThat(result, contains(author2, author1));
    }
     */
}
