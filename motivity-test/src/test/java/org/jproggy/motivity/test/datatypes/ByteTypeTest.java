package org.jproggy.motivity.test.datatypes;


import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import org.jproggy.motivity.util.Persistent;
import org.jproggy.motivity.sql.Table;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/**
 * Tests byte data types.
 */
abstract class ByteTypeTest<T extends Persistent> {
  private final Table<T> store;

  ByteTypeTest(Table<T> store) {
    this.store = store;
  }

  @BeforeEach
  void prepare() throws Exception {
    store.all().delete();
  }

  /**
   * Check that byte[] columns can be inserted and read correctly.
   */
  @Test
  void testInsertReadByteArray() throws Exception {
    // execute write
    T written = fillAndSave(store.create(null), getByteArraySize());
    check(written);
  }

  /**
   * Check that byte[] columns can be updated and read correctly.
   */
  @Test
  void testUpdateReadByteArray() throws Exception {
    T dbObject = store.create(null);
    fillAndSave(dbObject, getByteArraySize() / 2);

    // execute write
    T written = fillAndSave(dbObject, getByteArraySize());
    check(written);
  }

  /**
   * Check that byte[] columns can be updated and read correctly.
   */
  @Test
  void testInsertReadNullByteArray() throws Exception {
    assumeTrue(canWriteNullBytes());
    T dbObject = store.create(null);
    setBytes(null, dbObject);

    // execute write
    store.insert(dbObject);
    check(dbObject);
  }


  abstract void setBytes(byte[] bytes, T toFill);

  abstract byte[] getBytes(T toRead);

  /**
   * Verify, there's exactly one record containing the data, that was written
   *
   * @param written The data, that was written
   */
  void check(T written) {
    // execute read
    List<T> readList = store.all().list();

    // verify
    assertEquals(1, readList.size());
    T read = readList.get(0);
    assertArrayEquals(getBytes(written), getBytes(read));
  }

  /**
   * Fills the byte value in a dbObject, writes it to to the database and returns it.
   *
   * @param toFill the object to fill and save, not null.
   * @param size   the size of the byte array to set.
   * @return the updated dbObject.
   */
  T fillAndSave(T toFill, int size) throws Exception {
    byte[] bytes = createBytes(size);
    setBytes(bytes, toFill);
    if (toFill.isNew()) {
      store.insert(toFill);
    } else {
      store.update(toFill);
    }
    return toFill;
  }

  byte[] createBytes(int size) {
    byte[] bytes = new byte[size];
    for (int i = 0; i < size; ++i) {
      bytes[i] = (byte) i;
    }
    return bytes;
  }

  /**
   * Can be overwritten to indicate that the database cannot write null
   * values for the selected type.
   *
   * @return true if null byte arrays can be written, false if not;
   */
  boolean canWriteNullBytes() {
    return true;
  }

  int getByteArraySize() {
    return 200000;
  }
}
