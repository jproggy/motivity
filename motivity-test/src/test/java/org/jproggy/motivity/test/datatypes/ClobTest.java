package org.jproggy.motivity.test.datatypes;

import static org.jproggy.motivity.test.stores.TypesSchema.clobTypeStore;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.jproggy.motivity.test.entities.ClobType;
import org.jproggy.motivity.test.stores.ClobTypeStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Tests the data type CLOB.
 *
 * @version $Id: ClobTest.java 1437730 2013-01-23 21:01:22Z tfischer $
 */
class ClobTest {

  ClobTypeStore store = clobTypeStore;

  @BeforeEach
  void prepare() {
    store.all().delete();
  }

  /**
   * Check that clob columns can be created and read.
   */
  @Test
  void testCreateAndReadClobs() throws Exception {
    ClobType clobType = fillAndSaveClobType(
        new ClobType(),
        40000,
        "1234567890abcdefghijklmnopqrstuvwxyz");

    // read the ClobTypes from the database
    // and check the values against the original values
    List<ClobType> clobTypeList = store.all().list();
    assertEquals(1, clobTypeList.size());

    ClobType readClobType = clobTypeList.get(0);
    assertEquals(clobType.getClobValue(), readClobType.getClobValue());
  }

  /**
   * Check that clob columns can be updated and read correctly.
   */
  @Test
  void testUpdateClobs() throws Exception {
    ClobType clobType = fillAndSaveClobType(
        new ClobType(),
        40000,
        "1234567890abcdefghijklmnopqrstuvwxyz");
    ClobType found = store.all().first().get();
    assertEquals(clobType.getClobValue(), found.getClobValue());
    assertEquals(clobType.getClobObjectValue(), found.getClobObjectValue());
    assertEquals(clobType.getId(), found.getId());
    fillAndSaveClobType(
        clobType,
        50000,
        "0987654321abcdefghijklmnopqrstuvwxyz");

    // read the ClobTypes from the database
    // and check the values against the original values
    List<ClobType> clobTypeList = store.all().list();
    assertEquals(1, clobTypeList.size());

    ClobType readClobType = clobTypeList.get(0);
    assertEquals(clobType.getClobValue(), readClobType.getClobValue());
    assertEquals(clobType.getClobObjectValue(), readClobType.getClobObjectValue());
    assertEquals(clobType.getId(), readClobType.getId());
  }

  /**
   * Fills the Clob in a ClobType, writes it to to the database and returns it.
   *
   * @param clobType     the clobType to fill and save, not null.
   * @param size         the size of the clob.
   * @param charTemplate the template to use for filling, not null.
   * @return the changed ClobType.
   */
  private ClobType fillAndSaveClobType(ClobType clobType, int size, String charTemplate) {
    StringBuilder chars = new StringBuilder();
    for (int i = 0; i < size; ++i) {
      chars.append(charTemplate.charAt(i % charTemplate.length()));
    }
    clobType.setClobValue(chars.toString());
    clobType.save();
    return clobType;
  }
}
