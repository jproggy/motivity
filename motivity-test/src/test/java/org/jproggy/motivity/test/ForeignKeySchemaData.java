package org.jproggy.motivity.test;

import static org.junit.Assert.assertEquals;

import org.apache.torque.TorqueException;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.Store;
import org.jproggy.motivity.test.entities.CompIntegerVarcharFk;
import org.jproggy.motivity.test.entities.CompIntegerVarcharPk;
import org.jproggy.motivity.test.entities.CompNonpkFk;
import org.jproggy.motivity.test.entities.CompPkContainsFk;
import org.jproggy.motivity.test.entities.CompPkOtherFk;
import org.jproggy.motivity.test.entities.MultiRef;
import org.jproggy.motivity.test.entities.MultiRefSameTable;
import org.jproggy.motivity.test.entities.NonPkOIntegerFk;
import org.jproggy.motivity.test.entities.NonPkPIntegerFk;
import org.jproggy.motivity.test.entities.NullableOIntegerFk;
import org.jproggy.motivity.test.entities.NullablePIntegerFk;
import org.jproggy.motivity.test.entities.OIntegerPk;
import org.jproggy.motivity.test.entities.PIntegerPk;
import org.jproggy.motivity.test.entities.RequiredOIntegerFk;
import org.jproggy.motivity.test.entities.RequiredPIntegerFk;
import org.jproggy.motivity.test.stores.CompPkContainsFkStore;
import org.jproggy.motivity.test.stores.ForeignKeySchema;
import org.jproggy.motivity.transaction.Transaction;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

/**
 * Contains data (i.e. table records) for the tables in the foreign key schema.
 *
 * @version $Id: ForeignKeySchemaData.java 1517385 2013-08-25 21:19:23Z tfischer $
 */
public class ForeignKeySchemaData {
  private final List<PIntegerPk> pIntegerPkList = new ArrayList<>();
  private final List<NullablePIntegerFk> nullablePIntegerFkList = new ArrayList<>();
  private final List<RequiredPIntegerFk> requiredPIntegerFkList = new ArrayList<>();
  private final List<NonPkPIntegerFk> nonPkPIntegerFkList = new ArrayList<>();
  private final List<OIntegerPk> oIntegerPkList = new ArrayList<>();
  private final List<NullableOIntegerFk> nullableOIntegerFkList = new ArrayList<>();
  private final List<RequiredOIntegerFk> requiredOIntegerFkList = new ArrayList<>();
  private final List<NonPkOIntegerFk> nonPkOIntegerFkList = new ArrayList<>();
  private final List<CompPkOtherFk> compositePkOtherFkList = new ArrayList<>();
  private final List<CompPkContainsFk> compositePkContainsFkList = new ArrayList<>();
  private final List<CompIntegerVarcharPk> compositeIntegerVarcharPkList = new ArrayList<>();
  private final List<CompIntegerVarcharFk> compositeIntegerVarcharFkList = new ArrayList<>();
  private final List<CompNonpkFk> compositeNonpkFkList = new ArrayList<>();
  private final List<MultiRef> multiRefList = new ArrayList<>();
  private final List<MultiRefSameTable> multiRefSameTableList = new ArrayList<>();

  /**
   * Creates the default Test data for the foreign key schema.
   * The test data is filled as follows (p=primitive, o=object):
   * <p>
   * pIntegerPk1
   * - nonPkPIntegerFk1a
   * - nonPkPIntegerFk1b
   * - multiRef111a
   * - multiRef111a
   * - multiRef010
   * <p>
   * pIntegerPk2
   * - nonPkPIntegerFk2
   * - nullablePIntegerFk2
   * - requiredPIntegerFk2
   * - multiRef222
   * <p>
   * PIntegerPk3
   * - nullablePIntegerFk3a
   * - nullablePIntegerFk3b
   * - requiredPIntegerFk3a
   * - requiredPIntegerFk3b
   * <p>
   * oIntegerPk1
   * - nonPkOIntegerFk1a
   * - nonPkOIntegerFk1b
   * - compositePkOtherFk1a
   * - compositePkOtherFk1b
   * - multiRef111a
   * - multiRef111a
   * - multiRef100
   * <p>
   * oIntegerPk2
   * - nullableOIntegerFk2
   * - requiredOIntegerFk2
   * - nonPkOIntegerFk2
   * - compositePkOtherFk2
   * - multiRef222
   * <p>
   * oIntegerPk3
   * - nullableOIntegerFk3a
   * - nullableOIntegerFk3b
   * - requiredOIntegerFk3a
   * - requiredOIntegerFk3b
   * <p>
   * null
   * - nullableOIntegerFk4
   * - nonPkOIntegerFk4
   * <p>
   * compositeIntegerVarcharPk1
   * - compositeNonpkFk1a
   * - compositeNonpkFk1b
   * <p>
   * compositeIntegerVarcharPk2
   * - compositeIntegerVarcharFk2
   * - compositeNonpkFk2
   * <p>
   * compositeIntegerVarcharPk3
   * - compositeIntegerVarcharFk3a
   * - compositeIntegerVarcharFk3b
   * <p>
   * nullableOIntegerFk1
   * - multiRef111a
   * - multiRef111a
   * - multiRef001
   * <p>
   * nullableOIntegerFk2
   * - multiRef222
   * <p>
   * null
   * - compositeIntegerVarcharFk4
   *
   * @return a new instance filled with the default test data.
   * @throws TorqueException should not occur.
   */
  public ForeignKeySchemaData() throws TorqueException {
    pIntegerPkList.add(pIntegerPk("pIntegerPk1", 3));
    pIntegerPkList.add(pIntegerPk("pIntegerPk2", 2));
    pIntegerPkList.add(pIntegerPk("pIntegerPk3", 1));

    nullablePIntegerFkList.add(nullablePIntegerPks("nullablePIntegerFk2", pIntegerPkList.get(1)));
    nullablePIntegerFkList.add(nullablePIntegerPks("nullablePIntegerFk3a", pIntegerPkList.get(2)));
    nullablePIntegerFkList.add(nullablePIntegerPks("nullablePIntegerFk3b", pIntegerPkList.get(2)));

    requiredPIntegerFkList.add(requiredPIntegerFks("requiredPIntegerFk2", pIntegerPkList.get(1)));
    requiredPIntegerFkList.add(requiredPIntegerFks("requiredPIntegerFk3a", pIntegerPkList.get(2)));
    requiredPIntegerFkList.add(requiredPIntegerFks("requiredPIntegerFk3b", pIntegerPkList.get(2)));

    nonPkPIntegerFkList.add(nonPkIntegerFks("nonPkPIntegerFk1a", pIntegerPkList.get(0)));
    nonPkPIntegerFkList.add(nonPkIntegerFks("nonPkPIntegerFk1b", pIntegerPkList.get(0)));
    nonPkPIntegerFkList.add(nonPkIntegerFks("nonPkPIntegerFk2", pIntegerPkList.get(1)));

    fillOIntegerPks(this);
    fillNullableOIntegerFks(this);
    fillRequiredOIntegerFks(this);
    fillNonPkOIntegerFks(this);
    fillCompositePkOtherFks(this);
    fillCompositePkContainsFks(this);

    fillCompositeIntegerVarcharPks(this);
    fillCompositeIntegerVarcharFks(this);
    fillCompositeNonpkFks(this);

    fillMultiRefs(this);
    fillMultiRefSameTables(this);
  }

  private static PIntegerPk pIntegerPk(String name, int col) {
    PIntegerPk pIntegerPk1 = new PIntegerPk();
    pIntegerPk1.setName(name);
    pIntegerPk1.setIntegerColumn(col);
    return pIntegerPk1;
  }

  private static NullablePIntegerFk nullablePIntegerPks(String name, PIntegerPk o) {
    NullablePIntegerFk nullablePIntegerFk2 = new NullablePIntegerFk();
    nullablePIntegerFk2.setName(name);
    nullablePIntegerFk2.setPIntegerPk(o);
    return nullablePIntegerFk2;
  }

  private static RequiredPIntegerFk requiredPIntegerFks(String name, PIntegerPk target) {
    RequiredPIntegerFk requiredPIntegerFk2 = new RequiredPIntegerFk();
    requiredPIntegerFk2.setName(name);
    target.addRequiredPIntegerFk(requiredPIntegerFk2);
    return requiredPIntegerFk2;
  }

  private static NonPkPIntegerFk nonPkIntegerFks(String name, PIntegerPk target) {
    NonPkPIntegerFk nonPkPIntegerFk1a = new NonPkPIntegerFk();
    nonPkPIntegerFk1a.setName(name);
    target.addNonPkPIntegerFk(nonPkPIntegerFk1a);
    return nonPkPIntegerFk1a;
  }

  private static void fillOIntegerPks(final ForeignKeySchemaData result) {
    OIntegerPk oIntegerPk1 = new OIntegerPk();
    oIntegerPk1.setName("oIntegerPk1");
    oIntegerPk1.setIntegerColumn(3);
    result.getOIntegerPkList().add(oIntegerPk1);

    OIntegerPk oIntegerPk2 = new OIntegerPk();
    oIntegerPk2.setName("oIntegerPk2");
    oIntegerPk2.setIntegerColumn(2);
    result.getOIntegerPkList().add(oIntegerPk2);

    OIntegerPk oIntegerPk3 = new OIntegerPk();
    oIntegerPk3.setName("oIntegerPk3");
    oIntegerPk3.setIntegerColumn(1);
    result.getOIntegerPkList().add(oIntegerPk3);
  }

  private static void fillNullableOIntegerFks(final ForeignKeySchemaData result)
      throws TorqueException {
    NullableOIntegerFk nullableOIntegerFk2
        = new NullableOIntegerFk();
    nullableOIntegerFk2.setName(
        "nullableOIntegerFk2");
    result.getOIntegerPkList().get(1).addNullableOIntegerFk(
        nullableOIntegerFk2);
    result.getNullableOIntegerFkList().add(
        nullableOIntegerFk2);

    NullableOIntegerFk nullableOIntegerFk3a
        = new NullableOIntegerFk();
    nullableOIntegerFk3a.setName(
        "nullableOIntegerFk3a");
    result.getOIntegerPkList().get(2).addNullableOIntegerFk(
        nullableOIntegerFk3a);
    result.getNullableOIntegerFkList().add(
        nullableOIntegerFk3a);

    NullableOIntegerFk nullableOIntegerFk3b
        = new NullableOIntegerFk();
    nullableOIntegerFk3b.setName(
        "nullableOIntegerFk3b");
    result.getOIntegerPkList().get(2).addNullableOIntegerFk(
        nullableOIntegerFk3b);
    result.getNullableOIntegerFkList().add(
        nullableOIntegerFk3b);

    NullableOIntegerFk nullableOIntegerFk4
        = new NullableOIntegerFk();
    nullableOIntegerFk4.setName(
        "nullableOIntegerFk4");
    result.getNullableOIntegerFkList().add(
        nullableOIntegerFk4);
  }

  private static void fillRequiredOIntegerFks(final ForeignKeySchemaData result)
      throws TorqueException {
    RequiredOIntegerFk requiredOIntegerFk2
        = new RequiredOIntegerFk();
    requiredOIntegerFk2.setName(
        "requiredOIntegerFk2");
    result.getOIntegerPkList().get(1).addRequiredOIntegerFk(
        requiredOIntegerFk2);
    result.getRequiredOIntegerFkList().add(
        requiredOIntegerFk2);

    RequiredOIntegerFk requiredOIntegerFk3a
        = new RequiredOIntegerFk();
    requiredOIntegerFk3a.setName(
        "requiredOIntegerFk3a");
    result.getOIntegerPkList().get(2).addRequiredOIntegerFk(
        requiredOIntegerFk3a);
    result.getRequiredOIntegerFkList().add(
        requiredOIntegerFk3a);

    RequiredOIntegerFk requiredOIntegerFk3b
        = new RequiredOIntegerFk();
    requiredOIntegerFk3b.setName(
        "requiredOIntegerFk3b");
    result.getOIntegerPkList().get(2).addRequiredOIntegerFk(
        requiredOIntegerFk3b);
    result.getRequiredOIntegerFkList().add(
        requiredOIntegerFk3b);
  }

  private static void fillNonPkOIntegerFks(final ForeignKeySchemaData result)
      throws TorqueException {
    NonPkOIntegerFk nonPkOIntegerFk1a
        = new NonPkOIntegerFk();
    nonPkOIntegerFk1a.setName(
        "nonPkOIntegerFk1a");
    result.getOIntegerPkList().get(0).addNonPkOIntegerFk(
        nonPkOIntegerFk1a);
    result.getNonPkOIntegerFkList().add(
        nonPkOIntegerFk1a);

    NonPkOIntegerFk nonPkOIntegerFk1b
        = new NonPkOIntegerFk();
    nonPkOIntegerFk1b.setName(
        "nonPkOIntegerFk1b");
    result.getOIntegerPkList().get(0).addNonPkOIntegerFk(
        nonPkOIntegerFk1b);
    result.getNonPkOIntegerFkList().add(
        nonPkOIntegerFk1b);

    NonPkOIntegerFk nonPkOIntegerFk2
        = new NonPkOIntegerFk();
    nonPkOIntegerFk2.setName(
        "nonPkOIntegerFk2");
    result.getOIntegerPkList().get(1).addNonPkOIntegerFk(
        nonPkOIntegerFk2);
    result.getNonPkOIntegerFkList().add(
        nonPkOIntegerFk2);

    NonPkOIntegerFk nonPkOIntegerFk4
        = new NonPkOIntegerFk();
    nonPkOIntegerFk4.setName(
        "nonPkOIntegerFk4");
    result.getNonPkOIntegerFkList().add(
        nonPkOIntegerFk4);
  }

  private static void fillCompositePkOtherFks(final ForeignKeySchemaData result)
      throws TorqueException {
    CompPkOtherFk compositePkOtherFk1a
        = new CompPkOtherFk();
    compositePkOtherFk1a.setName(
        "compositePkOtherFk1a");
    compositePkOtherFk1a.setId1(1);
    compositePkOtherFk1a.setId2("1a");
    result.getOIntegerPkList().get(0).addCompPkOtherFk(
        compositePkOtherFk1a);
    result.getCompositePkOtherFkList().add(
        compositePkOtherFk1a);

    CompPkOtherFk compositePkOtherFk1b
        = new CompPkOtherFk();
    compositePkOtherFk1b.setName(
        "compositePkOtherFk1b");
    compositePkOtherFk1b.setId1(1);
    compositePkOtherFk1b.setId2("1b");
    result.getOIntegerPkList().get(0).addCompPkOtherFk(
        compositePkOtherFk1b);
    result.getCompositePkOtherFkList().add(
        compositePkOtherFk1b);

    CompPkOtherFk compositePkOtherFk2
        = new CompPkOtherFk();
    compositePkOtherFk2.setName(
        "compositePkOtherFk22");
    compositePkOtherFk2.setId1(2);
    compositePkOtherFk2.setId2("2");
    result.getOIntegerPkList().get(1).addCompPkOtherFk(
        compositePkOtherFk2);
    result.getCompositePkOtherFkList().add(
        compositePkOtherFk2);
  }

  private static void fillCompositePkContainsFks(final ForeignKeySchemaData result)
      throws TorqueException {
    CompPkContainsFk compositePkContainsFk1a
        = new CompPkContainsFk();
    compositePkContainsFk1a.setName(
        "compositePkContainsFk1a");
    compositePkContainsFk1a.setId2("1a");
    result.getOIntegerPkList().get(0).addCompPkContainsFk(
        compositePkContainsFk1a);
    result.getCompositePkContainsFkList().add(
        compositePkContainsFk1a);

    CompPkContainsFk compositePkContainsFk1b
        = new CompPkContainsFk();
    compositePkContainsFk1b.setName(
        "compositePkOtherFk1b");
    compositePkContainsFk1b.setId2("1b");
    result.getOIntegerPkList().get(0).addCompPkContainsFk(
        compositePkContainsFk1b);
    result.getCompositePkContainsFkList().add(
        compositePkContainsFk1b);

    CompPkContainsFk compositePkContainsFk2
        = new CompPkContainsFk();
    compositePkContainsFk2.setName(
        "compositePkOtherFk22");
    compositePkContainsFk2.setId1(2);
    compositePkContainsFk2.setId2("2");
    result.getOIntegerPkList().get(1).addCompPkContainsFk(
        compositePkContainsFk2);
    result.getCompositePkContainsFkList().add(
        compositePkContainsFk2);
  }

  private static void fillCompositeIntegerVarcharPks(
      final ForeignKeySchemaData result) {
    CompIntegerVarcharPk compositeIntegerVarcharPk1
        = new CompIntegerVarcharPk();
    compositeIntegerVarcharPk1.setName("compositeIntegerVarcharPk1");
    compositeIntegerVarcharPk1.setId1(10);
    compositeIntegerVarcharPk1.setId2("x");
    compositeIntegerVarcharPk1.setIntegerColumn(100);
    compositeIntegerVarcharPk1.setVarcharColumn("a");
    result.getCompositeIntegerVarcharPkList().add(
        compositeIntegerVarcharPk1);

    CompIntegerVarcharPk compositeIntegerVarcharPk2
        = new CompIntegerVarcharPk();
    compositeIntegerVarcharPk2.setName("compositeIntegerVarcharPk2");
    compositeIntegerVarcharPk2.setId1(10);
    compositeIntegerVarcharPk2.setId2("y");
    compositeIntegerVarcharPk2.setIntegerColumn(100);
    compositeIntegerVarcharPk2.setVarcharColumn("b");
    result.getCompositeIntegerVarcharPkList().add(
        compositeIntegerVarcharPk2);

    CompIntegerVarcharPk compositeIntegerVarcharPk3
        = new CompIntegerVarcharPk();
    compositeIntegerVarcharPk3.setName("compositeIntegerVarcharPk3");
    compositeIntegerVarcharPk3.setId1(11);
    compositeIntegerVarcharPk3.setId2("x");
    compositeIntegerVarcharPk3.setIntegerColumn(200);
    compositeIntegerVarcharPk3.setVarcharColumn("a");
    result.getCompositeIntegerVarcharPkList().add(
        compositeIntegerVarcharPk3);
  }

  private static void fillCompositeIntegerVarcharFks(
      final ForeignKeySchemaData result)
      throws TorqueException {
    CompIntegerVarcharFk compositeIntegerVarcharFk2
        = new CompIntegerVarcharFk();
    compositeIntegerVarcharFk2.setName(
        "compositeIntegerVarcharFk2");
    result.getCompositeIntegerVarcharPkList().get(1)
        .addCompIntegerVarcharFk(
            compositeIntegerVarcharFk2);
    result.getCompositeIntegerVarcharFkList().add(
        compositeIntegerVarcharFk2);

    CompIntegerVarcharFk compositeIntegerVarcharFk3a
        = new CompIntegerVarcharFk();
    compositeIntegerVarcharFk3a.setName(
        "compositeIntegerVarcharFk3a");
    result.getCompositeIntegerVarcharPkList().get(2)
        .addCompIntegerVarcharFk(
            compositeIntegerVarcharFk3a);
    result.getCompositeIntegerVarcharFkList().add(
        compositeIntegerVarcharFk3a);

    CompIntegerVarcharFk compositeIntegerVarcharFk3b
        = new CompIntegerVarcharFk();
    compositeIntegerVarcharFk3b.setName(
        "compositeIntegerVarcharFk3b");
    result.getCompositeIntegerVarcharPkList().get(2)
        .addCompIntegerVarcharFk(
            compositeIntegerVarcharFk3b);
    result.getCompositeIntegerVarcharFkList().add(
        compositeIntegerVarcharFk3b);

    CompIntegerVarcharFk compositeIntegerVarcharFk4
        = new CompIntegerVarcharFk();
    compositeIntegerVarcharFk4.setName(
        "compositeIntegerVarcharFk4");
    result.getCompositeIntegerVarcharFkList().add(
        compositeIntegerVarcharFk4);
  }

  private static void fillCompositeNonpkFks(
      final ForeignKeySchemaData result)
      throws TorqueException {
    CompNonpkFk compositeNonpkFk1a
        = new CompNonpkFk();
    compositeNonpkFk1a.setName(
        "compositeNonpkFk1a");
    result.getCompositeIntegerVarcharPkList().get(0)
        .addCompNonpkFk(compositeNonpkFk1a);
    result.getCompositeNonpkFkList().add(compositeNonpkFk1a);

    CompNonpkFk compositeNonpkFk1b
        = new CompNonpkFk();
    compositeNonpkFk1b.setName(
        "compositeNonpkFk1b");
    result.getCompositeIntegerVarcharPkList().get(0)
        .addCompNonpkFk(compositeNonpkFk1b);
    result.getCompositeNonpkFkList().add(compositeNonpkFk1b);

    CompNonpkFk compositeNonpkFk2
        = new CompNonpkFk();
    compositeNonpkFk2.setName(
        "compositeNonpkFk2");
    result.getCompositeIntegerVarcharPkList().get(1)
        .addCompNonpkFk(compositeNonpkFk2);
    result.getCompositeNonpkFkList().add(compositeNonpkFk2);
  }

  private static void fillMultiRefs(
      final ForeignKeySchemaData result)
      throws TorqueException {
    {
      MultiRef multiRef111a = new MultiRef();
      multiRef111a.setName("multiRef111a");
      multiRef111a.setOIntegerPk(result.getOIntegerPkList().get(0));
      multiRef111a.setPIntegerPk(result.getPIntegerPkList().get(0));
      multiRef111a.setNullableOIntegerFk(result.getNullableOIntegerFkList().get(0));
      result.getMultiRefList().add(multiRef111a);
    }

    {
      MultiRef multiRef111b = new MultiRef();
      multiRef111b.setName("multiRef111b");
      multiRef111b.setOIntegerPk(result.getOIntegerPkList().get(0));
      multiRef111b.setPIntegerPk(result.getPIntegerPkList().get(0));
      multiRef111b.setNullableOIntegerFk(result.getNullableOIntegerFkList().get(0));
      result.getMultiRefList().add(multiRef111b);
    }

    {
      MultiRef multiRef100 = new MultiRef();
      multiRef100.setName("multiRef100");
      multiRef100.setOIntegerPk(result.getOIntegerPkList().get(0));
      result.getMultiRefList().add(multiRef100);
    }

    {
      MultiRef multiRef010 = new MultiRef();
      multiRef010.setName("multiRef010");
      multiRef010.setPIntegerPk(result.getPIntegerPkList().get(0));
      result.getMultiRefList().add(multiRef010);
    }

    {
      MultiRef multiRef001 = new MultiRef();
      multiRef001.setName("multiRef001");
      multiRef001.setNullableOIntegerFk(result.getNullableOIntegerFkList().get(0));
      result.getMultiRefList().add(multiRef001);
    }

    {
      MultiRef multiRef120 = new MultiRef();
      multiRef120.setName("multiRef120");
      multiRef120.setOIntegerPk(result.getOIntegerPkList().get(0));
      multiRef120.setPIntegerPk(result.getPIntegerPkList().get(1));
      result.getMultiRefList().add(multiRef120);
    }

    {
      MultiRef multiRef201 = new MultiRef();
      multiRef201.setName("multiRef201");
      multiRef201.setOIntegerPk(result.getOIntegerPkList().get(1));
      multiRef201.setNullableOIntegerFk(result.getNullableOIntegerFkList().get(0));
      result.getMultiRefList().add(multiRef201);
    }

    {
      MultiRef multiRef012 = new MultiRef();
      multiRef012.setName("multiRef012");
      multiRef012.setPIntegerPk(result.getPIntegerPkList().get(0));
      multiRef012.setNullableOIntegerFk(result.getNullableOIntegerFkList().get(1));
      result.getMultiRefList().add(multiRef012);
    }

    {
      MultiRef multiRef222 = new MultiRef();
      multiRef222.setName("multiRef222");
      multiRef222.setOIntegerPk(result.getOIntegerPkList().get(1));
      multiRef222.setPIntegerPk(result.getPIntegerPkList().get(1));
      multiRef222.setNullableOIntegerFk(result.getNullableOIntegerFkList().get(1));
      result.getMultiRefList().add(multiRef222);
    }
  }

  private static void fillMultiRefSameTables(
      final ForeignKeySchemaData result)
      throws TorqueException {
    {
      MultiRefSameTable multiRefSameTable111a = new MultiRefSameTable();
      multiRefSameTable111a.setName("multiRefSameTable111a");
      multiRefSameTable111a.setOIntegerPkRelatedByReference1(result.getOIntegerPkList().get(0));
      multiRefSameTable111a.setOIntegerPkRelatedByReference2(result.getOIntegerPkList().get(0));
      multiRefSameTable111a.setOIntegerPkRelatedByReference3(result.getOIntegerPkList().get(0));
      result.getMultiRefSameTableList().add(multiRefSameTable111a);
    }

    {
      MultiRefSameTable multiRefSameTable111b = new MultiRefSameTable();
      multiRefSameTable111b.setName("multiRefSameTable111b");
      multiRefSameTable111b.setOIntegerPkRelatedByReference1(result.getOIntegerPkList().get(0));
      multiRefSameTable111b.setOIntegerPkRelatedByReference2(result.getOIntegerPkList().get(0));
      multiRefSameTable111b.setOIntegerPkRelatedByReference3(result.getOIntegerPkList().get(0));
      result.getMultiRefSameTableList().add(multiRefSameTable111b);
    }

    {
      MultiRefSameTable multiRefSameTable100 = new MultiRefSameTable();
      multiRefSameTable100.setName("multiRefSameTable100");
      multiRefSameTable100.setOIntegerPkRelatedByReference1(result.getOIntegerPkList().get(0));
      result.getMultiRefSameTableList().add(multiRefSameTable100);
    }

    {
      MultiRefSameTable multiRefSameTable010 = new MultiRefSameTable();
      multiRefSameTable010.setName("multiRefSameTable010");
      multiRefSameTable010.setOIntegerPkRelatedByReference2(result.getOIntegerPkList().get(0));
      result.getMultiRefSameTableList().add(multiRefSameTable010);
    }

    {
      MultiRefSameTable multiRefSameTable001 = new MultiRefSameTable();
      multiRefSameTable001.setName("multiRefSameTable001");
      multiRefSameTable001.setOIntegerPkRelatedByReference3(result.getOIntegerPkList().get(0));
      result.getMultiRefSameTableList().add(multiRefSameTable001);
    }

    {
      MultiRefSameTable multiRefSameTable120 = new MultiRefSameTable();
      multiRefSameTable120.setName("multiRefSameTable120");
      multiRefSameTable120.setOIntegerPkRelatedByReference1(result.getOIntegerPkList().get(0));
      multiRefSameTable120.setOIntegerPkRelatedByReference2(result.getOIntegerPkList().get(1));
      result.getMultiRefSameTableList().add(multiRefSameTable120);
    }

    {
      MultiRefSameTable multiRefSameTable201 = new MultiRefSameTable();
      multiRefSameTable201.setName("multiRefSameTable201");
      multiRefSameTable201.setOIntegerPkRelatedByReference1(result.getOIntegerPkList().get(1));
      multiRefSameTable201.setOIntegerPkRelatedByReference3(result.getOIntegerPkList().get(0));
      result.getMultiRefSameTableList().add(multiRefSameTable201);
    }

    {
      MultiRefSameTable multiRefSameTable012 = new MultiRefSameTable();
      multiRefSameTable012.setName("multiRefSameTable012");
      multiRefSameTable012.setOIntegerPkRelatedByReference2(result.getOIntegerPkList().get(0));
      multiRefSameTable012.setOIntegerPkRelatedByReference3(result.getOIntegerPkList().get(1));
      result.getMultiRefSameTableList().add(multiRefSameTable012);
    }

    {
      MultiRefSameTable multiRefSameTable222 = new MultiRefSameTable();
      multiRefSameTable222.setName("multiRefSameTable222");
      multiRefSameTable222.setOIntegerPkRelatedByReference1(result.getOIntegerPkList().get(1));
      multiRefSameTable222.setOIntegerPkRelatedByReference2(result.getOIntegerPkList().get(1));
      multiRefSameTable222.setOIntegerPkRelatedByReference3(result.getOIntegerPkList().get(1));
      result.getMultiRefSameTableList().add(multiRefSameTable222);
    }
  }

  public List<PIntegerPk> getPIntegerPkList() {
    return pIntegerPkList;
  }

  public List<NullablePIntegerFk> getNullablePIntegerFkList() {
    return nullablePIntegerFkList;
  }

  public List<RequiredPIntegerFk> getRequiredPIntegerFkList() {
    return requiredPIntegerFkList;
  }


  public List<NonPkPIntegerFk> getNonPkPIntegerFkList() {
    return nonPkPIntegerFkList;
  }

  public List<OIntegerPk> getOIntegerPkList() {
    return oIntegerPkList;
  }

  public List<NullableOIntegerFk> getNullableOIntegerFkList() {
    return nullableOIntegerFkList;
  }

  public List<RequiredOIntegerFk> getRequiredOIntegerFkList() {
    return requiredOIntegerFkList;
  }

  public List<NonPkOIntegerFk> getNonPkOIntegerFkList() {
    return nonPkOIntegerFkList;
  }

  public List<CompPkOtherFk> getCompositePkOtherFkList() {
    return compositePkOtherFkList;
  }

  public List<CompPkContainsFk> getCompositePkContainsFkList() {
    return compositePkContainsFkList;
  }

  public List<CompIntegerVarcharPk> getCompositeIntegerVarcharPkList() {
    return compositeIntegerVarcharPkList;
  }

  public List<CompIntegerVarcharFk> getCompositeIntegerVarcharFkList() {
    return compositeIntegerVarcharFkList;
  }

  public List<CompNonpkFk> getCompositeNonpkFkList() {
    return compositeNonpkFkList;
  }

  public List<MultiRef> getMultiRefList() {
    return multiRefList;
  }

  public List<MultiRefSameTable> getMultiRefSameTableList() {
    return multiRefSameTableList;
  }

  /**
   * Saves all contained data if the data is new or was changed
   * after the last save.
   *
   * @throws TorqueException If saving fails.
   */
  public void save() throws TorqueException {
    for (PIntegerPk pIntegerPk: pIntegerPkList) {
      pIntegerPk.save();
    }
    for (NullablePIntegerFk nullablePIntegerFk: nullablePIntegerFkList) {
      nullablePIntegerFk.save();
    }
    for (OIntegerPk oIntegerPk: oIntegerPkList) {
      oIntegerPk.save();
    }
    for (NullableOIntegerFk nullableOIntegerFk: nullableOIntegerFkList) {
      nullableOIntegerFk.save();
    }
    for (NonPkOIntegerFk nonPkOIntegerFk: nonPkOIntegerFkList) {
      nonPkOIntegerFk.save();
    }
    for (CompIntegerVarcharPk compositeIntegerVarcharPk
        : compositeIntegerVarcharPkList) {
      compositeIntegerVarcharPk.save();
    }
    for (CompIntegerVarcharFk compositeIntegerVarcharFk
        : compositeIntegerVarcharFkList) {
      compositeIntegerVarcharFk.save();
    }
    for (MultiRef multiRef: multiRefList) {
      // refresh ids
      multiRef.setOIntegerPk(multiRef.getOIntegerPk());
      multiRef.setPIntegerPk(multiRef.getPIntegerPk());
      multiRef.setNullableOIntegerFk(multiRef.getNullableOIntegerFk());
      multiRef.save();
    }
    for (MultiRefSameTable multiRefSameTable: multiRefSameTableList) {
      // refresh ids
      multiRefSameTable.setOIntegerPkRelatedByReference1(
          multiRefSameTable.getOIntegerPkRelatedByReference1());
      multiRefSameTable.setOIntegerPkRelatedByReference2(
          multiRefSameTable.getOIntegerPkRelatedByReference2());
      multiRefSameTable.setOIntegerPkRelatedByReference3(
          multiRefSameTable.getOIntegerPkRelatedByReference3());
      multiRefSameTable.save();
    }
  }

  /**
   * Deletes all records in the foreign-key-schema's tables.
   *
   * @throws TorqueException if the tables could not be cleaned
   */
  public static void clearTablesInDatabase() throws TorqueException {
    try (Transaction tra = Transactions.open()) {
      ForeignKeySchema.compPkContainsFkStore.all().delete();
      ForeignKeySchema.stores().forEach(s -> s.all().delete());
      tra.commit();
    }
  }

  /**
   * Prints all records in the foreign-key-schema's tables to stdout.
   */
  public static void dumpStore(Store<?> s) {
    System.out.println("contents of table " + s.getName());
    System.out.println(s.all().list());
  }

  /**
   * Prints all records in the foreign-key-schema's tables to stdout.
   */
  public static void dumpDatabase() {
    ForeignKeySchema.stores().forEach(ForeignKeySchemaData::dumpStore);
  }

  /**
   * Checks that the table Comp_O_Integer_Fk in the database
   * contains only the passed objects.
   *
   * @param expected the expected content of the table, not null.
   */
  public static <T> void assertFksInDatabaseEquals(
      final List<T> expected, Store<T> test, Function<T, Condition> pk)
      throws TorqueException {
    assertEquals(expected.size(), test.all().count());
    for (T fk: expected) {
      assertEquals("Expected but not found : " + fk, 1, test.filter(pk.apply(fk)).count());
    }
  }
}
