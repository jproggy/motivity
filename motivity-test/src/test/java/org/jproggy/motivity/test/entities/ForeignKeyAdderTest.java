package org.jproggy.motivity.test.entities;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertSame;

import org.junit.jupiter.api.Test;

/**
 * Tests the foreign key adder methods in the generated data object classes.
 *
 * @version $Id: ForeignKeyAdderTest.java 1395238 2012-10-07 07:30:25Z tfischer $
 */
class ForeignKeyAdderTest {
  /**
   * Tests that the add method works if referenced and referencing are
   * related by primitive ints and the referenced object has an id of zero.
   */
  @Test
  void testAddPrimitiveIntFkZero() throws Exception {
    PIntegerPk pIntegerPk = new PIntegerPk();
    NullablePIntegerFk nullablePIntegerFk = new NullablePIntegerFk();

    pIntegerPk.addNullablePIntegerFk(nullablePIntegerFk);

    assertEquals(1, pIntegerPk.getNullablePIntegerFks().size());
    assertSame(nullablePIntegerFk, pIntegerPk.getNullablePIntegerFks().get(0));

    assertEquals(0, nullablePIntegerFk.getFk());
    assertSame(pIntegerPk, nullablePIntegerFk.getPIntegerPk());
  }

  /**
   * Tests that the add method works if referenced and referencing are
   * related by primitive ints and the referenced object has an id
   * which is not equal to zero.
   */
  @Test
  void testAddPrimitiveIntFkNonZero() throws Exception {
    PIntegerPk pIntegerPk = new PIntegerPk();
    pIntegerPk.setId(23);
    NullablePIntegerFk nullablePIntegerFk = new NullablePIntegerFk();

    pIntegerPk.addNullablePIntegerFk(nullablePIntegerFk);

    assertEquals(1, pIntegerPk.getNullablePIntegerFks().size());
    assertSame(nullablePIntegerFk, pIntegerPk.getNullablePIntegerFks().get(0));

    assertEquals(23, nullablePIntegerFk.getFk());
    assertSame(pIntegerPk, nullablePIntegerFk.getPIntegerPk());
  }

  /**
   * Tests that the add method works if the referenced object has an
   * primitive PK but the referencing object has an object FK,
   * and the referenced object has an id of null.
   * <p>
   * This case is questionable, because 0 has a different meaning in both
   * cases.
   */
  @Test
  void testAddObjectToPrimitiveFkNull() throws Exception {
    PIntegerPk pIntegerPk = new PIntegerPk();
    OIntegerFkToPPk oIntegerFkToPPk = new OIntegerFkToPPk();

    pIntegerPk.addOIntegerFkToPPk(oIntegerFkToPPk);

    assertEquals(1, pIntegerPk.getOIntegerFkToPPks().size());
    assertSame(oIntegerFkToPPk, pIntegerPk.getOIntegerFkToPPks().get(0));

    assertEquals(0, oIntegerFkToPPk.getFk());
    assertSame(pIntegerPk, oIntegerFkToPPk.getPIntegerPk());
  }


  /**
   * Tests that the add method works if the referenced object has an
   * primitive PK but the referencing object has an object FK,
   * which is not equal to zero.
   */
  @Test
  void testAddObjectToPrimitiveFkNonZero() throws Exception {
    PIntegerPk pIntegerPk = new PIntegerPk();
    pIntegerPk.setId(4);
    OIntegerFkToPPk oIntegerFkToPPk = new OIntegerFkToPPk();

    pIntegerPk.addOIntegerFkToPPk(oIntegerFkToPPk);

    assertEquals(1, pIntegerPk.getOIntegerFkToPPks().size());
    assertSame(oIntegerFkToPPk, pIntegerPk.getOIntegerFkToPPks().get(0));

    assertEquals(4, oIntegerFkToPPk.getFk());
    assertSame(pIntegerPk, oIntegerFkToPPk.getPIntegerPk());
  }

  /**
   * Tests that the add method works if referenced and referencing are
   * related by object integers and the referenced object has an id of null.
   */
  @Test
  void testAddObjectIntegerFkNull() throws Exception {
    OIntegerPk oIntegerPk = new OIntegerPk();
    NullableOIntegerFk nullableOIntegerFk = new NullableOIntegerFk();

    oIntegerPk.addNullableOIntegerFk(nullableOIntegerFk);

    assertEquals(1, oIntegerPk.getNullableOIntegerFks().size());
    assertSame(nullableOIntegerFk, oIntegerPk.getNullableOIntegerFks().get(0));

    assertNull(nullableOIntegerFk.getFk());
    assertSame(oIntegerPk, nullableOIntegerFk.getOIntegerPk());
  }

  /**
   * Tests that the add method works if referenced and referencing are
   * related by object integers and the referenced object has an id of zero.
   */
  @Test
  void testAddObjectIntegerFkZero() throws Exception {
    OIntegerPk oIntegerPk = new OIntegerPk();
    oIntegerPk.setId(0);
    NullableOIntegerFk nullableOIntegerFk = new NullableOIntegerFk();

    oIntegerPk.addNullableOIntegerFk(nullableOIntegerFk);

    assertEquals(1, oIntegerPk.getNullableOIntegerFks().size());
    assertSame(nullableOIntegerFk, oIntegerPk.getNullableOIntegerFks().get(0));

    assertEquals(0, nullableOIntegerFk.getFk());
    assertSame(oIntegerPk, nullableOIntegerFk.getOIntegerPk());
  }

  /**
   * Tests that the add method works if referenced and referencing are
   * related by object integers and the referenced object has an id
   * which is not equal to zero.
   */
  @Test
  void testAddObjectIntegerFkNonZero() throws Exception {
    OIntegerPk oIntegerPk = new OIntegerPk();
    oIntegerPk.setId(13);
    NullableOIntegerFk nullableOIntegerFk = new NullableOIntegerFk();

    oIntegerPk.addNullableOIntegerFk(nullableOIntegerFk);

    assertEquals(1, oIntegerPk.getNullableOIntegerFks().size());
    assertSame(nullableOIntegerFk, oIntegerPk.getNullableOIntegerFks().get(0));

    assertEquals(13, nullableOIntegerFk.getFk());
    assertSame(oIntegerPk, nullableOIntegerFk.getOIntegerPk());
  }

  /**
   * Tests that the add method works if the referenced object has an
   * Object PK but the referencing object has a primitive FK,
   * and the referenced object has an id of null.
   * <p>
   * This case is questionable, because the related fields in referencing
   * and referenced object are different.
   * However, both values mean that the final id does not yet exist.
   */
  @Test
  void testAddPrimitiveToObjectFkNull() throws Exception {
    OIntegerPk oIntegerPk = new OIntegerPk();
    PIntegerFkToOPk pIntegerFkToOPk = new PIntegerFkToOPk();

    oIntegerPk.addPIntegerFkToOPk(pIntegerFkToOPk);

    assertEquals(1, oIntegerPk.getPIntegerFkToOPks().size());
    assertSame( pIntegerFkToOPk, oIntegerPk.getPIntegerFkToOPks().get(0));

    assertEquals(0, pIntegerFkToOPk.getFk());
    assertSame(oIntegerPk, pIntegerFkToOPk.getOIntegerPk());
  }

  /**
   * Tests that the add method works if the referenced object has an
   * Object PK but the referencing object has a primitive FK,
   * and the referenced object has an id of zero.
   * <p>
   * This case is questionable, because 0 means that the id is not yet
   * determined for a primitive key. But in practice ids will start at 1
   * so this should not happen.
   */
  @Test
  void testAddPrimitiveToObjectFkZero() throws Exception {
    OIntegerPk oIntegerPk = new OIntegerPk();
    oIntegerPk.setId(0);
    PIntegerFkToOPk pIntegerFkToOPk = new PIntegerFkToOPk();

    oIntegerPk.addPIntegerFkToOPk(pIntegerFkToOPk);

    assertEquals(1, oIntegerPk.getPIntegerFkToOPks().size());
    assertSame(pIntegerFkToOPk, oIntegerPk.getPIntegerFkToOPks().get(0));

    assertEquals(0, pIntegerFkToOPk.getFk());
    assertSame(oIntegerPk, pIntegerFkToOPk.getOIntegerPk());
  }

  /**
   * Tests that the add method works if referenced and referencing are
   * related by primitive ints and the referenced object has an id
   * which is not equal to zero.
   */
  @Test
  void testAddPrimitiveToObjectFkNonZero() throws Exception {
    OIntegerPk oIntegerPk = new OIntegerPk();
    oIntegerPk.setId(4);
    PIntegerFkToOPk pIntegerFkToOPk = new PIntegerFkToOPk();

    oIntegerPk.addPIntegerFkToOPk(pIntegerFkToOPk);

    assertEquals(1, oIntegerPk.getPIntegerFkToOPks().size());
    assertSame(pIntegerFkToOPk, oIntegerPk.getPIntegerFkToOPks().get(0));

    assertEquals(4, pIntegerFkToOPk.getFk());
    assertSame(oIntegerPk, pIntegerFkToOPk.getOIntegerPk());
  }
}
