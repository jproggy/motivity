package org.jproggy.motivity.test;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import static java.util.Arrays.asList;
import static org.jproggy.motivity.Motivity.and;
import static org.jproggy.motivity.test.Bookstore.author;
import static org.jproggy.motivity.test.Bookstore.book;
import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.BookstoreSchema.bookStore;
import static org.jproggy.motivity.test.stores.ForeignKeySchema.compPkContainsFkStore;
import static org.jproggy.motivity.test.stores.PkSchema.multiPkStore;
import static org.jproggy.motivity.test.stores.PkSchema.nopkStore;
import static org.jproggy.motivity.test.stores.TestSchema.ifcTableStore;
import static org.jproggy.motivity.test.stores.TestSchema.localIfcTableStore;
import static org.jproggy.motivity.test.stores.TypesSchema.bigintTypeStore;
import static org.jproggy.motivity.test.stores.TypesSchema.integerTypeStore;
import static org.jproggy.motivity.test.stores.TypesSchema.varcharTypeStore;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.IntStream;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.adapter.MssqlAdapter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.failure.NoRowsException;
import org.jproggy.motivity.query.failure.TooManyRowsException;
import org.jproggy.motivity.test.entities.Author;
import org.jproggy.motivity.test.entities.BigintType;
import org.jproggy.motivity.test.entities.Book;
import org.jproggy.motivity.test.entities.CompPkContainsFk;
import org.jproggy.motivity.test.entities.IfcTable;
import org.jproggy.motivity.test.entities.IntegerType;
import org.jproggy.motivity.test.entities.LocalIfcTable;
import org.jproggy.motivity.test.entities.MultiPk;
import org.jproggy.motivity.test.entities.Nopk;
import org.jproggy.motivity.test.entities.OIntegerPk;
import org.jproggy.motivity.test.entities.VarcharType;
import org.jproggy.motivity.transaction.Transaction;

/**
 * Runtime tests.
 *
 * @author <a href="mailto:seade@backstagetech.com.au">Scott Eade</a>
 * @author <a href="mailto:mpoeschl@marmot.at">Martin Poeschl</a>
 * @author <a href="mailto:fischer@seitenbau.de">Thomas Fischer</a>
 * @author <a href="mailto:patrick.carl@web.de">Patrick Carl</a>
 * @author <a href="mailto:tv@apache.org">Thomas Vandahl</a>
 * @version $Id: DataTest.java 1484256 2013-05-19 10:03:24Z tfischer $
 */
class DataTest {
  private static Log log = LogFactory.getLog(DataTest.class);

  @BeforeEach
  void setUp() throws Exception {
    Db.setUp();
    Bookstore.clean();
    multiPkStore.all().delete();
  }
  /**
   * multiple pk test (TRQ12)
   */
  @Test
  void testMultiplePk() {
    MultiPk mpk = new MultiPk();
    mpk.setPk1("varchar");
    mpk.setPk2(5);
    mpk.setPk3("char ");
    mpk.setPk4(3);
    mpk.setPk5((byte)-42);
    mpk.setPk6((short)3);
    mpk.setPk7(4L);
    mpk.setPk8(5.0);
    mpk.setPk9(6.0);
    mpk.setPk10(new Date(9999999999L));
    mpk.save();
    MultiPk multiPk = multiPkStore.all().one();
    assertEquals(new Date(9999999999L), multiPk.getPk10());
    assertEquals(-42, (int)multiPk.getPk5());
  }

  private static final Set<String> validTitles =  new HashSet<>(asList(
      "Book 6 - Author 4", "Book 6 - Author 5", "Book 6 - Author 6",
      "Book 6 - Author 7", "Book 6 - Author 8",
      "Book 7 - Author 4", "Book 7 - Author 5", "Book 7 - Author 6",
      "Book 7 - Author 7", "Book 7 - Author 8"
  ));

  /**
   * test limit/offset
   */
  @Test
  void testLimitOffset() {
    Bookstore.insertData();

    List<Book> books = bookStore.filter(
        and(
            bookStore.title.greaterOrEqual("Book 6 - Author 1"),
            bookStore.title.lessOrEqual("Book 8 - Author 3")
        )
    ).sorted(
        bookStore.bookId.desc()
    ).page(5, 10).list();

    assertEquals(10, books.size());
    for (Book book : books)
    {
      String title = book.getTitle();
      assertTrue(validTitles.contains(title), "Incorrect title: " + title);
    }

    // Test limit of zero works
    assertEquals(0, bookStore.all().limit(0).list().size());

    // check that skip also works without limit
    // skip is not in scope now.
    //assertEquals(95, bookStore.all().skip(5).list().size());

    // Check that limiting also works if a table with an equal column name
    // is joined. This is problematic for oracle, see TORQUE-10.
    // pre-populating dependencies no yet in scope
    //assertEquals(10, bookStore.all().page(5, 10).include(bookstore.author()).list().size());
  }

  /**
   * Checks whether the setSingleRecord() method in criteria works
   */
  @Test void testSingleRecord() throws Exception  {
    Bookstore.insertData();
    assertNotNull(bookStore.all().page(5, 1).one());
    assertThrows(TooManyRowsException.class, () -> bookStore.all().limit(2).one());
    assertThrows(NoRowsException.class, () -> bookStore.filter(bookStore.bookId.isNull()).one());
  }

  /**
   * Tests whether selects work correctly if the value <code>null</code>
   * is used.
   */
  @Test void testNullSelects() throws Exception {
    // clean table
    varcharTypeStore.all().delete();
    integerTypeStore.all().delete();

    // add test data
    VarcharType varcharType = new VarcharType();
    varcharType.setId("text2");
    varcharType.setVarcharValue("text2");
    varcharType.save();
    varcharType = new VarcharType();
    varcharType.setId("text");
    varcharType.save();

    IntegerType integerTypeNotNull = new IntegerType();
    integerTypeNotNull.setIntegerObjectValue(1);
    integerTypeNotNull.save();
    IntegerType integerTypeNull = new IntegerType();
    integerTypeNull.save();

    // check for comparison NOT_EQUAL and value null
    List<VarcharType> varcharResult = varcharTypeStore.filter(
        and(
            varcharTypeStore.id.isNotNull(),
            varcharTypeStore.varcharValue.isNotNull()
        )
    ).list();
    assertEquals(1, varcharResult.size());
    assertEquals("text2", varcharResult.get(0).getId());

    List<IntegerType> integerResult = integerTypeStore.filter(
        and(
            integerTypeStore.id.isNotNull(),
            integerTypeStore.integerObjectValue.isNotNull()
        )
    ).list();
    assertEquals(1, integerResult.size());
    assertEquals(integerTypeNotNull.getId(), integerResult.get(0).getId());

    // check for comparison EQUAL and value null
    varcharResult = varcharTypeStore.filter(varcharTypeStore.varcharValue.isNull()).list();
    assertEquals(1, varcharResult.size());
    assertEquals("text", varcharResult.get(0).getId());

    integerResult = integerTypeStore.filter(integerTypeStore.integerObjectValue.isNull()).list();
    assertEquals(1, integerResult.size());
    assertEquals(integerTypeNull.getId(), integerResult.get(0).getId());
  }

  /**
   * Test whether an update works and whether it only affects the
   * specified record.
   */
  @Test void testUpdate() throws Exception {
    author("OtherName");
    Author author = author("Name");

    // Test doUpdate methods in Peer explicitly
    author.setName("NewName2");
    authorStore.update(author);

    List<Author> authors = authorStore.all().sorted(authorStore.name.asc()).list();
    assertEquals(2, authors.size());
    assertEquals("NewName2", authors.get(0).getName());
    assertEquals("OtherName", authors.get(1).getName());

    author.setName("NewName3");
    authorStore.update(author);

    authors = authorStore.all().sorted(authorStore.name.asc()).list();
    assertEquals(2, authors.size());
    assertEquals("NewName3", authors.get(0).getName());
    assertEquals("OtherName", authors.get(1).getName());

    Nopk nopk = new Nopk();
    nopk.setName("name");
    nopk.save();

    // check the doPupdate Peer methods throw exceptions on a modified
    // object without primary keys
    assertThrows(UnsupportedOperationException.class, () -> nopkStore.update(nopk));

    try (Transaction tra = Transactions.open()) {
      assertThrows(UnsupportedOperationException.class, () -> nopkStore.update(new Nopk()));
      tra.rollback();
    }
  }

  /**
   * test special cases in the select clause
   */
  @Test void testSelectClause() throws Exception {
    Bookstore.insertData();

    // test double functions in select columns
    List<Integer> ids = bookStore.select(bookStore.bookId).list();
    assertEquals(100, ids.size());

    // test qualifiers in function in select columns
    // count( distinct column) is not supported
    List<Long> rowCount = bookStore.select(bookStore.bookId.count().as("c")).list();
    assertEquals(1, rowCount.size());
    assertEquals(100L, rowCount.get(0));
  }

  /**
   * test the order by, especially in joins and with aliases
   */
  @Test void testOrderBy() throws Exception {
    // insert test data
    Author firstAuthor = author("Author 1");
    book(firstAuthor, "Book 1");

    Author secondAuthor = author("Author 2");
    IntStream.of(2,3,4).forEach(bookNr ->  book(secondAuthor, "Book " + bookNr));

    // test simple ascending order by
    List<Book> bookList = bookStore.all().sorted(bookStore.title.asc()).list();
    assertEquals(4, bookList.size());
    assertEquals("Book 1", bookList.get(0).getTitle());
    assertEquals("Book 4", bookList.get(3).getTitle());

    // test simple descending order by
    bookList = bookStore.all().sorted(bookStore.title.desc()).list();
    assertEquals(4, bookList.size());
    assertEquals("Book 4", bookList.get(0).getTitle());
    assertEquals("Book 1", bookList.get(3).getTitle());

    /* no supported. What's the value?
    // the retrieved columns are
    // author    book   b
    // author1  book1   book1
    // author2  book4   book2
    // author2  book3   book2
    // author2  book2   book2
    // author2  book4   book3
    // ...
    bookList = bookStore.all().sorted(bookStore.author().books().as("b").title.asc(),
        bookStore.title.desc()).list();
    assertEquals(10, bookList.size());
    assertEquals("Book 4", bookList.get(1).getTitle());
    assertEquals("Book 3", bookList.get(2).getTitle());

    // test usage of Expressions in order by
    criteria = new Criteria();
    criteria.addAscendingOrderByColumn(
            new ColumnImpl("UPPER(" + bookStore.title + ")"));
    criteria.setIgnoreCase(true);
    bookStore.doSelect(criteria);
     */
  }

  /**
   * Tests whether ignoreCase works correctly
   */
   /* @Test void testIgnoreCase() throws Exception {
        // check ignore case in selects
        Author author = author("AuTHor");

        Criteria criteria = new Criteria();
        criteria.where(AuthorPeer.NAME, author.getName().toLowerCase());
        criteria.setIgnoreCase(true);
        List<Author> result = AuthorPeer.doSelect(criteria);
        assertTrue("Size of result is not 1, but " + result.size(),
                result.size() == 1);

        // LIKE treatment might be different (e.g. postgres), so check extra
        criteria = new Criteria();
        criteria.where(
                AuthorPeer.NAME,
                author.getName().toLowerCase().replace('r', '%'),
                Criteria.LIKE);
        criteria.setIgnoreCase(true);
        result = AuthorPeer.doSelect(criteria);
        assertTrue("Size of result is not 1, but " + result.size(),
                result.size() == 1);

        // Test ignore case in criterion
        criteria = new Criteria();
        Criterion criterion1 = new Criterion(
                AuthorPeer.NAME,
                author.getName().toLowerCase(),
                Criteria.EQUAL);
        criterion1.setIgnoreCase(true);
        Criterion criterion2 = new Criterion(
                AuthorPeer.AUTHOR_ID, null, Criteria.NOT_EQUAL);
        criterion1.and(criterion2);

        result = AuthorPeer.doSelect(criteria);

        // ignore case should not be set either in Criteria
        // nor in other criterions
        assertFalse(criteria.isIgnoreCase());
        assertFalse(criterion2.isIgnoreCase());
        assertTrue("Size of result is not 1, but " + result.size(),
                result.size() == 1);


        // Test ignore case in attached criterion
        criteria = new Criteria();
        criterion1 = new Criterion(
                AuthorPeer.AUTHOR_ID, null, Criteria.NOT_EQUAL);
        criterion2 = new Criterion(
                AuthorPeer.NAME,
                author.getName().toLowerCase(),
                Criteria.EQUAL);
        criterion2.setIgnoreCase(true);
        criterion1.and(criterion2);

        result = AuthorPeer.doSelect(criteria);

        // ignore case should not be set either in Criteria
        // nor in other criterions
        assertFalse(criteria.isIgnoreCase());
        assertFalse(criterion1.isIgnoreCase());

        assertTrue("Size of result is not 1, but " + result.size(),
                result.size() == 1);

        // ignore case in "in" query
        {
            criteria = new Criteria();
            Set<String> names = new HashSet<String>();
            names.add(author.getName().toLowerCase());
            criteria.where(AuthorPeer.NAME, names, Criteria.IN);
            criteria.setIgnoreCase(true);

            result = AuthorPeer.doSelect(criteria);
            assertEquals("Expected result of size 1 but got " + result.size(),
                    result.size(),
                    1);
        }

        // Check that case is not ignored if ignoreCase is not set
        // This is known not to work for mysql
        author = new Author();
        author.setName("author");
        author.save();

        Adapter adapter = Torque.getAdapter(Torque.getDefaultDB());
        if (adapter instanceof MysqlAdapter
                || adapter instanceof MssqlAdapter)
        {
            log.error("testIgnoreCase(): "
                    + "Case sensitive comparisons are known not to work"
                    + " with Mysql and MSSQL");
            // failing is "expected", so bypass without error
        }
        else
        {
            criteria = new Criteria();
            criteria.where(AuthorPeer.NAME, author.getName());
            result = AuthorPeer.doSelect(criteria);
            assertTrue("Size of result is not 1, but " + result.size(),
                    result.size() == 1);

            // again check LIKE treatment
            criteria = new Criteria();
            criteria.where(
                    AuthorPeer.NAME,
                    author.getName().replace('r', '%'),
                    Criteria.LIKE);
            result = AuthorPeer.doSelect(criteria);
            assertTrue("Size of result is not 1, but " + result.size(),
                    result.size() == 1);

            // Test different ignore cases in criterions
            criteria = new Criteria();
            criterion1 = new Criterion(
                    AuthorPeer.NAME,
                    author.getName().toLowerCase(),
                    Criteria.NOT_EQUAL);
            criterion2 = new Criterion(
                    AuthorPeer.NAME,
                    author.getName().toLowerCase(),
                    Criteria.EQUAL);
            criterion2.setIgnoreCase(true);
            criterion1.and(criterion2);
            criteria.where(criterion1);

            result = AuthorPeer.doSelect(criteria);
            assertTrue("Size of result is not 1, but " + result.size(),
                    result.size() == 1);

            // ignore case in "in" query
            {
                criteria = new Criteria();
                Set<String> names = new HashSet<String>();
                names.add(author.getName());
                criteria.where(AuthorPeer.NAME, names, Criteria.IN);

                result = AuthorPeer.doSelect(criteria);
                assertEquals("Expected result of size 1 but got " + result.size(),
                        result.size(),
                        1);
            }
        }

        cleanBookstore();
        author = new Author();
        author.setName("AA");
        author.save();
        author = new Author();
        author.setName("BB");
        author.save();
        author = new Author();
        author.setName("ba");
        author.save();
        author = new Author();
        author.setName("ab");
        author.save();

        // check ignoreCase in Criteria
        criteria = new Criteria();
        criteria.setIgnoreCase(true);
        criteria.addAscendingOrderByColumn(AuthorPeer.NAME);
        result = AuthorPeer.doSelect(criteria);
        assertTrue("Size of result is not 4, but " + result.size(),
                result.size() == 4);
        assertEquals(result.get(0).getName(), "AA");
        assertEquals(result.get(1).getName(), "ab");
        assertEquals(result.get(2).getName(), "ba");
        assertEquals(result.get(3).getName(), "BB");

        // check ignoreCase in orderBy
        criteria = new Criteria();
        criteria.addAscendingOrderByColumn(AuthorPeer.NAME, true);
        result = AuthorPeer.doSelect(criteria);
        assertTrue("Size of result is not 4, but " + result.size(),
                result.size() == 4);
        assertEquals(result.get(0).getName(), "AA");
        assertEquals(result.get(1).getName(), "ab");
        assertEquals(result.get(2).getName(), "ba");
        assertEquals(result.get(3).getName(), "BB");
    } */

  /**
   * tests whether AsColumns produce valid SQL code
   */
  @Test void testAsColumn() throws Exception {
    author("Author 1");
    author("Author 2");

    List<Object[]> data = authorStore.select(
        authorStore.name.as("ALIASNAME"),
        authorStore.authorId
    ).filter(
        authorStore.name.equalTo("Author 1")
    ).list();
    assertEquals(1, data.size());
    assertEquals(2, data.get(0).length);
    assertEquals("Author 1", data.get(0)[0]);
  }

  /**
   * Test whether same column name in different tables
   * are handled correctly
   */
    /*@Test void testSameColumnName() throws Exception {
        cleanBookstore();
        author("Name");
        Author author = author("NotCorrespondingName");
        Book book = book(author, "Name");

        Criteria criteria = new Criteria();
        criteria.addJoin(bookStore.title, AuthorPeer.NAME);
        bookStore.addSelectColumns(criteria);
        AuthorPeer.addSelectColumns(criteria);
        // basically a BasebookStore.setDbName(criteria);
        // and BasePeer.doSelect(criteria);
        CompositeMapper mapper = new CompositeMapper();
        mapper.addMapper(new BookRecordMapper(), 0);
        mapper.addMapper(
                new AuthorRecordMapper(),
                bookStore.numColumns);

        List<List<Object>> queryResult
                = bookStore.doSelect(criteria, mapper);
        List<Object> mappedRow = queryResult.get(0);
        book = (Book) mappedRow.get(0);
        author = (Author) mappedRow.get(1);

        if (book.getAuthorId() == author.getAuthorId())
        {
            fail("wrong Ids read");
        }
    }*/

  /**
   * tests whether large primary keys are inserted and read correctly
   */
  @Test void testLargePk() throws Exception {
    assumeFalse(Db.defaultAdapter instanceof MssqlAdapter,
        "MSSQL does not support inserting defined PK values");
    bigintTypeStore.all().delete();

    long longId = 8771507845873286l;
    BigintType bigintType = new BigintType();
    bigintType.setId(longId);
    bigintType.save();

    BigintType readBigintType = bigintTypeStore.all().one();
    assertEquals(bigintType.getId(), readBigintType.getId());
    assertEquals(longId, readBigintType.getId());
  }

  /**
   * tests whether large bigint values are inserted and read correctly
   */
  @Test void testLargeValue() throws Exception {
    bigintTypeStore.all().delete();

    long longValue = 8771507845873286l;
    BigintType bigintType = new BigintType();
    bigintType.setBigintValue(longValue);
    bigintType.save();

    BigintType readBigintType = bigintTypeStore.all().one();
    assertEquals(bigintType.getId(), readBigintType.getId());
    assertEquals(longValue, readBigintType.getBigintValue());
  }

  /**
   * Tests the CountHelper class
   */
  @Test void testCountHelper() throws Exception {
    author("Name");
    author("Name2");
    author("Name");

    assertEquals(3L, authorStore.all().count());
    assertEquals(2L, authorStore.select(authorStore.name).distinct().count());
    assertEquals(1L, authorStore.filter(authorStore.name.equalTo("Name2")).count());
  }


  /**
   * Tests whether we can handle multiple primary keys some of which are
   * also foreign keys
   */
  @Test void testMultiplePrimaryForeignKey() throws Exception {
    ForeignKeySchemaData.clearTablesInDatabase();

    OIntegerPk oIntegerPk = new OIntegerPk();
    oIntegerPk.save();
    CompPkContainsFk compPkContainsFk = new CompPkContainsFk();
    compPkContainsFk.setId1(oIntegerPk.getId());
    compPkContainsFk.setId2("test");
    compPkContainsFk.save();

    CompPkContainsFk selected = compPkContainsFkStore.all().one();
    assertEquals(oIntegerPk.getId(), selected.getId1());
    assertEquals("test", selected.getId2());
  }

  /**
   * Tests inserting single quotes in Strings.
   * This may not crash now, but in a later task like datasql,
   * so the data has to be inserted in a table which does not get cleaned
   * during the runtime test.
   */
  @Test void testSingleQuotes() throws Exception {
    assertNotNull(author("has Single ' Quote"));
  }

  /**
   * Test whether equals() is working correctly
   */
  @Test void testEquals() throws Exception {
    Author author = new Author();
    author.setAuthorId(1000);

    Book book = new Book();
    book.setBookId(1000);

    Book bookNotEqual = new Book();
    bookNotEqual.setBookId(2000);

    Book bookEqual = new Book();
    bookEqual.setBookId(1000);

    assertNotEquals(author, book);
    assertEquals(book, book);
    assertEquals(book, bookEqual);
    assertNotEquals(book, bookNotEqual);
  }

  /**
   * Tests whether a table implementing an interface actually
   * returns an instance of this interface
   */
  @Test void testInterface() throws Exception {
    ifcTableStore.filter(ifcTableStore.id.notEqual(-1)).delete();

    IfcTable ifc = new IfcTable();

    // don't see value of interface feature.
    //assertTrue(ifc instanceof TestInterface, "IfcTable should be an instance of TestInterface");

    ifc.setId(1);
    ifc.setName("John Doe");
    ifc.save();

    List<IfcTable> results = ifcTableStore.all().list();
    assertEquals(1, results.size());

    LocalIfcTable localIfc = new LocalIfcTable();

    // don't see value of interface feature.
    //assertTrue("LocalIfcTable should be an instance of LocalTestInterface", localIfc instanceof LocalTestInterface);

    List<LocalIfcTable> results2 = localIfcTableStore.all().list();
    assertEquals(0, results2.size());
  }

    /*@Test void testInheritanceWithKeys() throws Exception {
        // make sure that the InheritanceTest table is empty before the test
        inheritanceTestStore.filter(
                inheritanceTestStore.inheritanceTest.isNotNull()).delete();
        long count = inheritanceTestStore.filter(
            inheritanceTestStore.inheritanceTest.isNotNull()).count();
        assertEquals(0L, count);

        // create & save test data
        InheritanceTest inheritanceTest = new InheritanceTest();
        inheritanceTest.setPayload("payload1");
        inheritanceTest.save();
        InheritanceChildB inheritanceChildB = new InheritanceChildB();
        inheritanceChildB.setPayload("payload 2");
        inheritanceChildB.save();
        InheritanceChildC inheritanceChildC = new InheritanceChildC();
        inheritanceChildC.setPayload("payload 3");
        inheritanceChildC.save();
        InheritanceChildD inheritanceChildD = new InheritanceChildD();
        inheritanceChildD.setPayload("payload 4");
        inheritanceChildD.save();

        // Check that all objects are saved into the InheritanceTest table
        criteria = new Criteria();
        criteria.where(
                InheritanceTestPeer.INHERITANCE_TEST,
                null,
                Criteria.ISNOTNULL);
        assertEquals("InheritanceTestTable should contain 4 rows",
                4,
                new CountHelper().count(criteria));
        criteria = new Criteria();
        criteria.addAscendingOrderByColumn(
                InheritanceTestPeer.INHERITANCE_TEST);

        // Check that the class of the object is retained when loading
        List<InheritanceTest> inheritanceObjects
                = InheritanceTestPeer.doSelect(criteria);
        assertEquals(
                InheritanceTest.class,
                inheritanceObjects.get(0).getClass());
        assertEquals(
                InheritanceChildB.class,
                inheritanceObjects.get(1).getClass());
        assertEquals(
                InheritanceChildC.class,
                inheritanceObjects.get(2).getClass());
        assertEquals(
                InheritanceChildD.class,
                inheritanceObjects.get(3).getClass());
    }

    @Test void testInheritanceWithClassname() throws Exception {
        // make sure that the InheritanceTest table is empty before the test
        Criteria criteria = new Criteria();
        InheritanceClassnameTestPeer.doDelete(criteria);
        criteria = new Criteria();
        criteria.where(
                InheritanceClassnameTestPeer.INHERITANCE_TEST,
                null,
                Criteria.ISNOTNULL);
        assertEquals(0,
                new CountHelper().count(criteria));

        // create & save test data
        InheritanceClassnameTest inheritanceClassnameTest
                = new InheritanceClassnameTest();
        inheritanceClassnameTest.setPayload("0 parent");
        inheritanceClassnameTest.save();
        InheritanceClassnameTestChild1 inheritanceClassnameChild1
                = new InheritanceClassnameTestChild1();
        inheritanceClassnameChild1.setPayload("1 child");
        inheritanceClassnameChild1.save();
        InheritanceClassnameTestChild2 inheritanceClassnameChild2
                = new InheritanceClassnameTestChild2();
        inheritanceClassnameChild2.setPayload("2 child");
        inheritanceClassnameChild2.save();

        // Check that all objects are saved into the InheritanceTest table
        criteria = new Criteria();
        criteria.where(
                InheritanceClassnameTestPeer.INHERITANCE_TEST,
                null,
                Criteria.ISNOTNULL);
        assertEquals("InheritanceClassnameTest table should contain 3 rows",
                3,
                new CountHelper().count(criteria));
        criteria = new Criteria();
        criteria.addAscendingOrderByColumn(
                InheritanceClassnameTestPeer.PAYLOAD);

        // Check that the class of the object is retained when loading
        List<InheritanceClassnameTest> inheritanceObjects
                = InheritanceClassnameTestPeer.doSelect(criteria);
        assertEquals(
                InheritanceClassnameTest.class,
                inheritanceObjects.get(0).getClass());
        assertEquals("0 parent", inheritanceObjects.get(0).getPayload());
        assertEquals(
                InheritanceClassnameTestChild1.class,
                inheritanceObjects.get(1).getClass());
        assertEquals("1 child", inheritanceObjects.get(1).getPayload());
        assertEquals(
                InheritanceClassnameTestChild2.class,
                inheritanceObjects.get(2).getClass());
        assertEquals("2 child", inheritanceObjects.get(2).getPayload());
    }*/

  @Test void testLikeClauseEscaping() throws Exception {
    List<String> authorNames = asList("abc", "bbc", "a_c", "a%c", "a\\c", "a\"c", "a'c", "a?c", "a*c");
    // Save authors
    authorNames.forEach(Bookstore::author);
    assertEquals(authorNames.size(), authorStore.all().count());

    // Check authors are in the database
    for (String aName: authorNames) {
      try {
        Author author = authorStore.filter(
            authorStore.name.equalTo(aName)
        ).one();
        assertEquals(aName, author.getName());
      } catch (Exception e) {
        throw new Exception("Name: " + aName, e);
      }
    }

    for (Map.Entry<String, String> likeResult : Bookstore.likeResults.entrySet()) {
      try {
        Author author = authorStore.filter(
            authorStore.name.like(likeResult.getKey())
        ).one();
        assertEquals(likeResult.getValue(), author.getName());
      } catch (Exception e) {
        throw new Exception("Key: " + likeResult.getKey(), e);
      }
    }
/*
        // check that case insensitivity is maintained if
        // a like is replaced with an equals (no wildcard present)
        // This might be a problem for databases which use ILIKE
        Criteria criteria = new Criteria();
        criteria.where(AuthorPeer.NAME, "AbC", Criteria.LIKE);
        criteria.setIgnoreCase(true);
        List<Author> authorList = AuthorPeer.doSelect(criteria);
        assertEquals(
                "AuthorList should contain one author",
                1,
                authorList.size());
        Author author = authorList.get(0);
        assertEquals("Name of author should be abc",
                "abc",
                author.getName());

        // check that the escape clause (where needed) also works
        // with limit, offset and order by
        criteria = new Criteria();
        Criterion criterion1 = new Criterion(
                AuthorPeer.NAME,
                "b%",
                Criteria.LIKE);
        Criterion criterion2 = new Criterion(
                AuthorPeer.NAME,
                "a\\%%",
                Criteria.LIKE);
        Criterion criterion3 = new Criterion(
                AuthorPeer.NAME,
                "cbc",
                Criteria.LIKE);
        criteria.where(criterion1.or(criterion2).or(criterion3));
        criteria.addAscendingOrderByColumn(AuthorPeer.NAME);
        criteria.setOffset(1);
        criteria.setLimit(1);
        authorList = AuthorPeer.doSelect(criteria);
        assertEquals(
                "AuthorList should contain one author",
                1,
                authorList.size());
        author = authorList.get(0);
        assertEquals("Name of author should be bbc",
                "bbc",
                author.getName());*/
  }


  /**
   * Strips the schema and table name from a fully qualified colum name
   * This is useful for creating Query with aliases, as the constants
   * for the colum names in the data objects are fully qualified.
   * @param fullyQualifiedColumnName the fully qualified column name, not null
   * @return the column name stripped from the table (and schema) prefixes
   */
  public static String getRawColumnName(final String fullyQualifiedColumnName) {
    int dotPosition = fullyQualifiedColumnName.lastIndexOf(".");
    if (dotPosition == -1) {
      return fullyQualifiedColumnName;
    }
    return fullyQualifiedColumnName.substring(dotPosition + 1 );
  }
}
