package org.jproggy.motivity.test;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.jproggy.motivity.test.Bookstore.author;
import static org.jproggy.motivity.test.Bookstore.book;
import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.BookstoreSchema.bookStore;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Column;
import org.jproggy.motivity.query.Select;
import org.jproggy.motivity.test.entities.Author;
import org.jproggy.motivity.test.entities.Book;
import org.jproggy.motivity.transaction.Transaction;

/**
 * Tests sub-selects in the where clause.
 */
class SelectTest {
  Author author1;
  Author author2;
  Author author2b;
  Author author3;
  Book book1a;
  Book book1b;
  Book book1c;
  Book book3;

  @BeforeEach void setUp() throws Exception {
    Db.setUp();
    Bookstore.clean();
    try (Transaction tra = Transactions.open()) {
      author1 = author("author1");
      author2 = author("author2");
      author2b = author("author2");
      author3 = author("author3");
      book1a = book(author1, "Book a from author 1");
      book1b = book(author1, "Book b from author 1");
      book1c = book(author1, "Book c from author 1");
      book3 = book(author3, "Book from author 3");
      tra.commit();
    }
  }

  /**
   * Tests whether we can execute sub-selects
   */
  @Test void subSelect() throws Exception {
    Select<Integer> query = authorStore.select(
        authorStore.authorId
    ).filter(
        authorStore.name.equalTo(author1.getName())
    );
    List<Author> result = authorStore.filter(
        authorStore.authorId.in(query)
    ).sorted(
        authorStore.authorId.desc()
    ).list();

    assertThat(result, hasItems(author1));
  }

  /**
   * Tests whether we can execute sub-selects that take parameters from a list
   */
  @Test void subSelectParameterized() throws Exception {
    Select<Integer> subSelect = authorStore.select(authorStore.authorId).filter(
        authorStore.name.in(author1.getName(), author2.getName())
    );
    List<Author> result = authorStore.filter(
        authorStore.authorId.in(subSelect)
    ).sorted(
        authorStore.authorId.desc()
    ).list();

    assertThat(result, hasItems(author2b, author2, author1));
  }

  /**
   * Tests whether we can execute a simple select
   */
  @Test void simpleSelect() throws Exception {
    List<Integer> result = authorStore.select(
        authorStore.authorId
    ).filter(
        authorStore.name.notLike("%1")
    ).sorted(
        authorStore.authorId.desc()
    ).list();

    assertEquals(3, result.size());
    assertEquals(author3.getAuthorId(), result.get(0));
    assertEquals(author2b.getAuthorId(), result.get(1));
    assertEquals(author2.getAuthorId(), result.get(2));
  }

  /**
   * Tests whether we can execute a simple select with aliases
   */
  @Test
  public void selectAlias() throws Exception {
    Select<Object[]> query = authorStore.select(
        authorStore.authorId.as("id"),
        authorStore.name.as("author")
    );
    List<Object[]> list = query.filter(authorStore.name.equalTo(author1.getName())).list();
    assertEquals(1, list.size());
    assertEquals("author1", list.get(0)[1]);
  }

  /**
   * Load some data with join
   */
  @Test void joinColumnsOnly() throws Exception {
    List<Object[]> result = bookStore.select(
        bookStore.title.as("book"),
        bookStore.author().name.as("author")
    ).sorted(
        bookStore.title.asc()
    ).list();
    assertEquals(4, result.size());
    assertEquals(book1a.getTitle(), result.get(0)[0]);
    assertEquals(author3.getName(), result.get(3)[1]);
  }

  /**
   * Load some data with join
   */
  @Test void joinSortOnly() throws Exception {
    List<String> result = bookStore.select(
        bookStore.title.as("book")
    ).sorted(
        bookStore.author().name.desc(),
        bookStore.title.desc()
    ).list();
    assertEquals(4, result.size());
    assertEquals(book3.getTitle(), result.get(0));
    assertEquals(book1a.getTitle(), result.get(3));
  }

  /**
   * Load some data with join
   */
  @Test void join() throws Exception {
    Select<String> query = authorStore.select(
        authorStore.name
    ).filter(
        authorStore.books().title.greaterThen(book1a.getTitle())
    );
    try (Stream<String> stream = query.distinct().stream()) {
      List<String> result = stream.collect(Collectors.toList());
      assertEquals(2, result.size());
      assertTrue(result.contains(author3.getName()));
    }
  }

  /**
   * All the books from the author of book1a
   */
  @Test void doubleJoin() throws Exception {
    List<Book> result = bookStore.filter(
        bookStore.author().books("b2").title.equalTo(book1a.getTitle())
    ).sorted(bookStore.title.desc()).list();
    assertEquals(3, result.size());
    assertEquals(book1c, result.get(0));
    assertEquals(book1b.getBookId(), result.get(1).getBookId());
    assertEquals(book1a.getTitle(), result.get(2).getTitle());
  }

  /**
   * All the books from the author of book1a
   */
  @Test void doubleJoinParameterized() throws Exception {
    Book result = bookStore.filter(
        bookStore.author().books("b2").title.notLike("%1")
    ).one();
    assertEquals(book3, result);
  }

  /**
   * Load some data via select
   */
  @Test void group() throws Exception {
    Column<Long> bookCount = bookStore.bookId.count().as("bookCount");
    List<Object[]> result = bookStore.select(
        bookStore.authorId,
        bookCount
    ).group(
        bookStore.authorId
    ).having(
        bookStore.bookId.count().greaterThen(0L)
    ).sorted(
        bookCount.desc()
    ).list();

    assertEquals(2, result.size());
    assertEquals(author1.getAuthorId(), result.get(0)[0]);
    assertEquals(3L, result.get(0)[1]);
    assertEquals(author3.getAuthorId(), result.get(1)[0]);
    assertEquals(1L, result.get(1)[1]);
  }
}
