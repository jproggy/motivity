package org.jproggy.motivity.test;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.jproggy.motivity.test.Bookstore.author;
import static org.jproggy.motivity.test.Bookstore.book;
import static org.jproggy.motivity.test.stores.BookstoreSchema.authorStore;
import static org.jproggy.motivity.test.stores.BookstoreSchema.bookStore;

import java.util.List;

import org.jproggy.motivity.query.Select;
import org.jproggy.motivity.test.entities.Author;
import org.jproggy.motivity.test.entities.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests sub-selects in the where clause.
 *
 * @version $Id: WhereClauseSubSelectTest.java 1839288 2018-08-27 09:48:33Z tv $
 */
class WhereClauseSubSelectTest {
    Author author1;
    Author author2;
    Author author2b;
    Author author3;
    Book book1;
    Book book3;

    @BeforeEach
    void setUp() throws Exception {
        Db.setUp();
        Bookstore.clean();
        author1 = author("author1");
        author2 = author("author2");
        author2b = author("author2");
        author3 = author("author3");
        book1 = book(author1, "Book from author 1");
        book3 = book(author3, "Book from author 3");
    }

    /**
     * Tests whether we can execute sub-selects using an in clause with
     * integer values.
     */
    @Test
    void testSubSelect() {
        Select<Integer> subQuery = authorStore.select(authorStore.authorId).filter(
                authorStore.name.in(author1.getName(), author2.getName())
        );
        List<Author> result = authorStore.filter(
                authorStore.authorId.in(subQuery)
        ).sorted(
                authorStore.authorId.desc()
        ).list();
        assertThat(result, hasItems(author2b, author2, author1));
    }

    /**
     * Tests whether we can execute sub-selects using an in clause with
     * integer values.
     */
    @Test
    void testSubSelectDifferentTable() {
        Select<Integer> subQuery = bookStore.select(bookStore.authorId).filter(
                bookStore.title.like("Book%")
        );
        List<Author> result = authorStore.filter(
                authorStore.authorId.in(subQuery)
        ).sorted(
                authorStore.authorId.asc()
        ).list();
        assertThat(result, hasItems(author1, author3));
    }

    /**
     * Tests whether we can execute subselects using an equals comparison with
     * integer values, with the subselects as left hand side of the comparison.
     */
    @Test
    void testExists() {
        List<Author> result = authorStore.filter(
                bookStore.exists(bookStore.authorId.equalTo(authorStore.authorId))
        ).sorted(
                authorStore.authorId.asc()
        ).list();

        assertThat(result, hasItems(author1, author3));
    }

    /**
     * Tests whether we can execute sub-queries using in with Strings.
     */
    @Test
    void testSubSelectUsingInWithStrings() {
        Select<String> subQuery = authorStore.select(authorStore.name).filter(
                authorStore.authorId.in(author1.getAuthorId(), author2.getAuthorId(), author2b.getAuthorId())
        );
        List<Author> result = authorStore.filter(
                authorStore.name.in(subQuery)
        ).sorted(
                authorStore.authorId.desc()
        ).list();

        assertThat(result, hasItems(author2b, author2, author1));
    }

    /**
     * Tests whether we can execute sub-selects which reference the outer select
     * in the sub-select.
     */
    @Test
    void testSubSelectWithGroup() {
        Select<String> subQuery = authorStore.select(authorStore.name).group(
                authorStore.name
        ).having(
                authorStore.authorId.count().greaterThen(1L)
        );
        List<Author> result = authorStore.filter(
                authorStore.name.in(subQuery)
        ).sorted(
                authorStore.authorId.desc()
        ).list();

        assertThat(result, hasItems(author2b, author2));
    }
}
