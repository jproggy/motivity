package org.jproggy.motivity.test.datatypes;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import static org.jproggy.motivity.test.stores.TypesSchema.dateTimeTimestampTypeStore;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.torque.TorqueException;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.test.Db;
import org.jproggy.motivity.test.entities.DateTimeTimestampType;
import org.jproggy.motivity.test.stores.DateTimeTimestampTypeStore;
import org.jproggy.snippetory.sql.Cursor;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Stream;

/**
 * Tests behavior of date, time and timestamp fields.
 */
class DateTimeTimestampTest {
  private static final LocalDate BASE_DATE = LocalDate.of(1970, Month.JANUARY, 1);
  DateTimeTimestampTypeStore store = dateTimeTimestampTypeStore;

  private static final Date DATE = toDate(LocalDate.of(2010, Month.JANUARY, 23));
  private static final Date DATE_BEFORE = toDate(BASE_DATE);
  private static final Date DATE_AFTER = new Date();
  private static final LocalTime TIME = LocalTime.of(9, 47, 12);
  private static final LocalTime TIME_BEFORE = LocalTime.of(9, 46, 12);
  private static final LocalTime TIME_AFTER = LocalTime.of(9, 48, 12);
  private static final Date DATE_TIME = add(DATE, TIME);

  /**
   * Tests the date behaviour. Date fields should be truncated to the start
   * of day when saved and reloaded. Note that this does not tell
   * anything about how the field is saved in the database, which can differ
   * between databases.
   */
  @Test
  void testRead() {
    DateTimeTimestampType dateTimeTimestamp = fillAndSave(DATE, TIME);

    DateTimeTimestampType loaded = store.filter(store.id.equalTo(dateTimeTimestamp.getId())).one();

    assertEquals(DATE, loaded.getDateValue());
    assertEquals(DATE_TIME, loaded.getTimestampValue());
    assertEquals(TIME, loaded.getTimeValue().truncatedTo(ChronoUnit.SECONDS));
  }

  /**
   * Checks that a select is possible using a java.util.date object
   * in a date field.
   */
  @Test
  void testSelectWithUtilDateOnDate() {
    DateTimeTimestampType dateTimeTimestamp = fillAndSave(DATE, TIME);

    ColumnDefinition<DateTimeTimestampType, Date> column = store.dateValue;

    List<DateTimeTimestampType> result = store.filter(column.equalTo(DATE)).list();

    assertEquals(1, result.size());
    assertEquals(dateTimeTimestamp, result.get(0));

    try (Cursor<DateTimeTimestampType> c = store.filter(column.greaterOrEqual(DATE)).toCursor()) {
      assertTrue(c.iterator().hasNext());
    }
    assertEquals(0, store.filter(column.greaterOrEqual(DATE_AFTER)).count());

    assertEquals(1, store.filter(column.greaterThen(DATE_BEFORE)).count());
    assertEquals(0, store.filter(column.greaterThen(DATE)).list().size());
    try (Stream<DateTimeTimestampType> stream = store.filter(column.greaterThen(DATE_AFTER)).stream()) {
      stream.forEach(x -> fail());
    }

    DateTimeTimestampType found = store.filter(column.lessThen(DATE_AFTER)).one();

    assertEquals(DATE, found.getDateValue());
    assertEquals(DATE_TIME, found.getTimestampValue());

    found = store.filter(column.between(DATE_BEFORE, DATE_AFTER)).first().get();

    assertEquals(DATE, found.getDateValue());
    assertEquals(DATE_TIME, found.getTimestampValue());
  }

  /**
   * Checks that a select is possible using a java.util.date object
   * in a time field.
   */
  @Test
  void testSelectWithUtilDateOnTime() {
    DateTimeTimestampType dateTimeTimestamp = fillAndSave(DATE, TIME);

    ColumnDefinition<DateTimeTimestampType, LocalTime> column = store.timeValue;
    List<DateTimeTimestampType> result = store.filter(column.equalTo(TIME)).list();
    assertEquals(1, result.size());
    assertEquals(dateTimeTimestamp, result.get(0));

    DateTimeTimestampType found = store.filter(column.greaterOrEqual(TIME)).one();
    assertEquals(dateTimeTimestamp, found);

    assertFalse(store.filter(column.lessOrEqual(TIME_BEFORE)).first().isPresent());
  }

  /**
   * Checks that a select is possible using a java.sql.time object
   * in a time field.
   */
  @Test
  void testSelectWithSqlTimeOnTime() {
    DateTimeTimestampType dateTimeTimestamp = fillAndSave(DATE, TIME);

    List<DateTimeTimestampType> result = store.filter(store.timeValue.between(TIME_BEFORE, TIME_AFTER)).list();

    assertEquals(1, result.size());
    assertEquals(dateTimeTimestamp, result.get(0));

    assertTrue(store.filter(store.timeValue.equalTo(TIME)).first().isPresent());
  }

  /**
   * Checks that a select is possible using a java.util.date object
   * in a timestamp field.
   */
  @Test
  void testSelectWithUtilDateOnTimestamp() {
    DateTimeTimestampType dateTimeTimestamp = fillAndSave(DATE, TIME);

    ColumnDefinition<DateTimeTimestampType, Date> column = store.timestampValue;
    List<DateTimeTimestampType> result = store.filter(column.equalTo(DATE_TIME)).list();

    assertEquals(1, result.size());
    assertEquals(dateTimeTimestamp, result.get(0));

    assertEquals(0, store.filter(column.greaterThen(DATE_AFTER)).count());

    DateTimeTimestampType found = store.filter(column.lessThen(DATE_AFTER)).one();

    assertEquals(DATE, found.getDateValue());
    assertEquals(DATE_TIME, found.getTimestampValue());

    found = store.filter(column.between(toDate(BASE_DATE), DATE_AFTER)).first().get();

    assertEquals(DATE, found.getDateValue());
    assertEquals(DATE_TIME, found.getTimestampValue());
  }

  /**
   * Checks that a select is possible using a java.sql.timestamp object
   * in a timestamp field.
   */
  @Test
  void testSelectWithSqlTimestampOnTimestamp() throws TorqueException {
    DateTimeTimestampType dateTimeTimestamp = fillAndSave(DATE, TIME);

    ColumnDefinition<DateTimeTimestampType, Date> column = store.timestampValue;
    List<DateTimeTimestampType> result = store.filter(column.equalTo(ts(add(DATE, TIME)))).list();

    assertEquals(1, result.size());
    assertEquals(dateTimeTimestamp, result.get(0));
  }

  static Timestamp ts(Date date) {
    return new Timestamp(date.getTime());
  }

  /**
   * Cleans the DateTimeTimestamp table.
   */
  @BeforeEach
  void setUp() throws Exception {
    Db.setUp();
    store.all().delete();
  }

  DateTimeTimestampType fillAndSave(Date toDay, LocalTime time) {
    DateTimeTimestampType dateTimeTimestamp = new DateTimeTimestampType();
    dateTimeTimestamp.setDateValue(toDay);
    dateTimeTimestamp.setTimeValue(time);
    dateTimeTimestamp.setTimestampValue(add(toDay, time));
    dateTimeTimestamp.save();
    return dateTimeTimestamp;
  }

  static Date toDate(LocalDate ld) {
    return toDate(ld.atStartOfDay());
  }

  static Date toDate(LocalDateTime ldt) {
    return Date.from(ldt.atZone(ZoneId.systemDefault()).toInstant());
  }

  static Date add(Date day, LocalTime time) {
    return new Date(DATE.getTime() + (TIME.toNanoOfDay() / 1000_000) + 3_600_000);
  }
}
