package org.jproggy.motivity.test;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

import static org.jproggy.motivity.test.stores.TestSchema.summarize1Store;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import org.jproggy.motivity.test.entities.Summarize1;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Tests selects using functions.
 *
 * @version $Id: SelectFunctionTest.java 1839288 2018-08-27 09:48:33Z tv $
 */
public class SelectFunctionTest {
    private static final int[] INT_VALUES  = {
            1, 1, 1, 5
    };

    @BeforeEach
    void setUp() throws Exception
    {
        Db.setUp();
        summarize1Store.all().delete();

        // Create some test data
        for (int v: INT_VALUES) {
            Summarize1 rec = new Summarize1();
            rec.setInt1(v);
            rec.save();
        }
    }

    /**
     * Tests a select using the Avg function.
     @Test
    public void testSelectAvg() throws Exception {
     assertEquals(2, summarize1Store.select(summarize1Store.int1.avg()).one());
     assertEquals(3, summarize1Store.select(summarize1Store.int1.distinct().avg()).one());
    }
     */

    /**
     * Tests a select using the count function.
     */
    @Test
    void testSelectCount() throws Exception {
        assertEquals(4, summarize1Store.all().count());
        assertEquals(2, summarize1Store.select(summarize1Store.int1).distinct().count());
    }

    /**
     * Tests a select using the min function.
     */
    @Test
    public void testSelectAggregates() throws Exception {
        assertEquals(1, summarize1Store.select(summarize1Store.int1.min()).one());
        assertEquals(5, summarize1Store.select(summarize1Store.int1.max().as("v")).one());
        assertEquals(8, summarize1Store.select(summarize1Store.int1.sum().as("v")).one());
        assertNull(summarize1Store.select(summarize1Store.int1.sum()).filter(
            summarize1Store.id.equalTo(35)  // doesn't exist
        ).one());
//        assertEquals(6, summarize1Store.select(summarize1Store.int1.distinct().sum()).one());
    }
}
