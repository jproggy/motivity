package org.jproggy.motivity.test.datatypes;

import org.jproggy.motivity.test.entities.BinaryType;
import org.jproggy.motivity.test.stores.TypesSchema;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */


class BinaryTest extends ByteTypeTest<BinaryType> {

  public BinaryTest() {
    super(TypesSchema.binaryTypeStore);
  }

  @Override
  protected void setBytes(byte[] bytes, BinaryType toFill) {
    toFill.setBinaryValue(bytes);
    toFill.setBinaryObjectValue(bytes);
  }

  @Override
  byte[] getBytes(BinaryType toRead) {
    return toRead.getBinaryValue();
  }

  @Override
  int getByteArraySize() {
    return 100;
  }

}
