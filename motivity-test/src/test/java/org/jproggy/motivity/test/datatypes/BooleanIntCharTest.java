package org.jproggy.motivity.test.datatypes;

import static org.jproggy.motivity.Motivity.and;
import static org.jproggy.motivity.test.stores.TypesSchema.bintBcharTypeStore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.torque.TorqueException;
import org.jproggy.motivity.test.entities.BintBcharType;
import org.jproggy.motivity.test.stores.BintBcharTypeStore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Tests the data types BOOLEANINT and BOOLEANCHAR.
 */
class BooleanIntCharTest {
  private static final BintBcharTypeStore store = bintBcharTypeStore;

  /**
   * Checks whether we can read boolean true values.
   */
  @Test
  void testReadBooleanIntCharTrueValue() throws Exception {
    BintBcharType bc = store.filter(
        store.id.equalTo("t1")
    ).first().get();
    assertTrue(bc.getBintValue());
    assertTrue(bc.getBcharValue());
    assertEquals(Boolean.TRUE, bc.getBintObjectValue());
    assertEquals(Boolean.TRUE, bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can read boolean false values.
   */
  @Test
  void testReadBooleanIntCharFalseValue() throws Exception {
    BintBcharType bc = store.filter(
        bintBcharTypeStore.id.equalTo("f1")
    ).first().get();
    assertFalse(bc.getBintValue());
    assertFalse(bc.getBcharValue());
    assertEquals(Boolean.FALSE, bc.getBintObjectValue());
    assertEquals(Boolean.FALSE, bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can read Boolean null values.
   */
  @Test
  void testReadBooleanIntCharNullValue() throws Exception {
    BintBcharType bc = store.filter(
        store.bintObjectValue.isNull()
    ).first().get();
    assertNull(bc.getBintObjectValue());
    assertNull(bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can insert boolean true values.
   */
  @Test
  void testInsertBooleanIntCharTrueValue() throws Exception {
    BintBcharType bc = new BintBcharType();
    bc.setId("new");
    bc.setBintValue(true);
    bc.setBcharValue(true);
    bc.setBintObjectValue(Boolean.TRUE);
    bc.setBcharObjectValue(Boolean.TRUE);
    bc.save();

    bc = store.filter(store.id.equalTo("new")).first().get();
    assertTrue(bc.getBintValue());
    assertTrue(bc.getBcharValue());
    assertEquals(Boolean.TRUE, bc.getBintObjectValue());
    assertEquals(Boolean.TRUE, bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can insert boolean false values.
   */
  @Test
  void testInsertBooleanIntCharFalseValue() throws Exception {
    BintBcharType bc = new BintBcharType();
    bc.setId("new");
    bc.setBintValue(false);
    bc.setBcharValue(false);
    bc.setBintObjectValue(Boolean.FALSE);
    bc.setBcharObjectValue(Boolean.FALSE);
    bc.save();

    bc = store.filter(store.id.equalTo("new")).first().get();
    assertFalse(bc.getBintValue());
    assertFalse(bc.getBcharValue());
    assertEquals(Boolean.FALSE, bc.getBintObjectValue());
    assertEquals(Boolean.FALSE, bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can insert Boolean null values.
   */
  @Test
  void testInserteBooleanIntCharNullValue() throws Exception {
    BintBcharType bc = new BintBcharType();
    bc.setId("new");
    bc.setBintObjectValue(null);
    bc.setBcharObjectValue(null);
    bc.save();

    bc = store.filter(
        store.id.equalTo("new")
    ).one();
    assertNull(bc.getBintObjectValue());
    assertNull(bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can update values to boolean true.
   */
  @Test
  void testUpdateBooleanIntCharTrueValue() throws Exception {
    BintBcharType bc = store.filter(
        store.id.equalTo("f1")
    ).first().get();
    bc.setBintValue(true);
    bc.setBcharValue(true);
    bc.setBintObjectValue(Boolean.TRUE);
    bc.setBcharObjectValue(Boolean.TRUE);
    bc.save();

    bc = store.filter(
        store.id.equalTo("f1")
    ).first().get();
    assertTrue(bc.getBintValue());
    assertTrue(bc.getBcharValue());
    assertEquals(Boolean.TRUE, bc.getBintObjectValue());
    assertEquals(Boolean.TRUE, bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can update values to boolean false.
   */
  @Test
  void testWriteBooleanIntCharFalseValue() throws Exception {
    BintBcharType bc = store.filter(
        store.id.equalTo("t1")
    ).one();
    bc.setBintValue(false);
    bc.setBcharValue(false);
    bc.setBintObjectValue(Boolean.FALSE);
    bc.setBcharObjectValue(Boolean.FALSE);
    bc.save();

    bc = store.filter(
        store.id.equalTo("t1")
    ).one();
    assertFalse(bc.getBintValue());
    assertFalse(bc.getBcharValue());
    assertEquals(Boolean.FALSE, bc.getBintObjectValue());
    assertEquals(Boolean.FALSE, bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can update values to Boolean null.
   */
  @Test
  void testUpdateBooleanIntCharNullValue() throws Exception {
    BintBcharType bc = store.filter(
        store.id.equalTo("t1")
    ).first().get();

    bc.setBintObjectValue(null);
    bc.setBcharObjectValue(null);
    bc.save();

    bc = store.filter(
        store.id.equalTo("t1")
    ).first().get();
    assertNull(bc.getBintObjectValue());
    assertNull(bc.getBcharObjectValue());
  }

  /**
   * Check whether we can impose the condition Boolean True to
   * booleanint/booleanchar columns.
   */
  @Test
  void testBooleanTrueSelect() throws Exception {
    List<BintBcharType> selectedList = store.filter(
            and(
                    store.bcharValue.equalTo(true),
                    store.bintValue.equalTo(true),
                    store.bcharObjectValue.equalTo(true),
                    store.bintObjectValue.equalTo(true)
            )
    ).list();
    assertEquals(1, selectedList.size());
    BintBcharType bintBcharType = selectedList.get(0);
    assertEquals("t1", bintBcharType.getId());
  }

  /**
   * Check whether we can impose the condition Boolean False to
   * booleanint/booleanchar columns.
   */
  @Test
  void testBooleanFalseSelect() throws Exception {
    List<BintBcharType> selectedList = store.filter(
            and(
                    store.bcharValue.equalTo(false),
                    store.bintValue.equalTo(false),
                    store.bcharObjectValue.equalTo(false),
                    store.bintObjectValue.equalTo(false)
            )
    ).list();
    assertEquals(1, selectedList.size());
    BintBcharType bintBcharType = selectedList.get(0);
    assertEquals("f1", bintBcharType.getId());
  }

  /**
   * Check whether we can impose the condition Boolean Null to
   * booleanint/booleanchar columns and get objects where the columns
   * are null.
   */
  @Test
  void testBooleanObjectNullSelect() throws Exception {
    List<BintBcharType> selectedList = store.filter(
        and(
            store.bcharObjectValue.isNull(),
            store.bintObjectValue.isNull()
        )
    ).list();
    assertEquals(1, selectedList.size());
    assertEquals("null", selectedList.get(0).getId());
  }

  /**
   * Check whether we can impose the condition Boolean Null to
   * booleanint/booleanchar primitive columns and get no hit.
   */
  @Test
  void testPrimitiveNullSelect() throws Exception {
    List<BintBcharType> selectedList = store.filter(
        and(
            store.bcharValue.isNull(),
            store.bintValue.isNull()
        )
    ).list();
    assertEquals(0, selectedList.size());
  }

  /**
   * Check whether we can impose a Boolean condition to booleanint/booleanchar
   * columns via joins.
   */
  @Test
  void testBooleanSelectViaJoins() throws Exception {
    BintBcharTypeStore bc = store.as("bc");
    List<BintBcharType> selectedList = bc.filter(
        and(
            bc.bcharValue.equalTo(false),
            bc.bintValue.equalTo(false),
            bc.bcharObjectValue.equalTo(false),
            bc.bintObjectValue.equalTo(false)
        )
    ).list();
    assertEquals(1, selectedList.size());
    BintBcharType bintBcharType = selectedList.get(0);
    assertEquals("f1", bintBcharType.getId());
  }


  /**
   * Checks whether we can pass boolean true values to doUpdate.
   */
  @Test
  void testDoUpdateBooleanTrueValue() throws Exception {
    store.filter(
        store.id.equalTo("f1")
    ).update(
        store.bintValue.to(true),
        store.bcharValue.to(true),
        store.bintObjectValue.to(true),
        store.bcharObjectValue.to(true)
    );

    BintBcharType bc = store.filter(
        bintBcharTypeStore.id.equalTo("f1")
    ).first().get();
    assertTrue(bc.getBintValue());
    assertTrue(bc.getBcharValue());
    assertTrue(bc.getBintObjectValue());
    assertTrue(bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can pass boolean false values to doUpdate.
   */
  @Test
  void testDoUpdateBooleanFalseValue() throws Exception {
    store.filter(
        store.id.equalTo("t1")
    ).update(
        store.bintValue.to(false),
        store.bcharValue.to(false),
        store.bintObjectValue.to(false),
        store.bcharObjectValue.to(false)
    );

    BintBcharType bc = store.filter(
        bintBcharTypeStore.id.equalTo("t1")
    ).first().get();
    assertFalse(bc.getBintValue());
    assertFalse(bc.getBcharValue());
    assertFalse(bc.getBintObjectValue());
    assertFalse(bc.getBcharObjectValue());
  }

  /**
   * Checks whether we can pass Boolean null values to doUpdate.
   */
  @Test
  void testDoUpdateBooleanNullValue() throws Exception {
    long changes = store.filter(
        store.id.equalTo("t1")
    ).update(
        store.bintObjectValue.to(null),
        store.bcharObjectValue.to(null)
    );

    BintBcharType bc = store.filter(
        bintBcharTypeStore.id.equalTo("t1")
    ).one();
    assertEquals(1, changes);
    assertTrue(bc.getBintValue());
    assertTrue(bc.getBcharValue());
    assertNull(bc.getBintObjectValue());
    assertNull(bc.getBcharObjectValue());
  }

  /**
   * Delete all previous data from the tested tables
   * and re-inserts test data.
   */
  @BeforeEach
  void fillTables() throws TorqueException {
    store.all().delete();

    BintBcharType bc = new BintBcharType();
    bc.setId("t1");
    bc.setBintValue(true);
    bc.setBcharValue(true);
    bc.setBintObjectValue(Boolean.TRUE);
    bc.setBcharObjectValue(Boolean.TRUE);
    bc.save();
    bc = new BintBcharType();
    bc.setId("f1");
    bc.setBintValue(false);
    bc.setBcharValue(false);
    bc.setBintObjectValue(Boolean.FALSE);
    bc.setBcharObjectValue(Boolean.FALSE);
    bc.save();
    bc = new BintBcharType();
    bc.setId("null");
    bc.setBintValue(false);
    bc.setBcharValue(true);
    bc.setBintObjectValue(null);
    bc.setBcharObjectValue(null);
    bc.save();
  }
}
