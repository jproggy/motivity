package org.jproggy.motivity.test.datatypes;

import static org.jproggy.motivity.Motivity.and;
import static org.jproggy.motivity.test.stores.TypesSchema.bitTypeStore;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.torque.adapter.OracleAdapter;
import org.jproggy.motivity.test.Db;
import org.jproggy.motivity.test.entities.BitCompositePk;
import org.jproggy.motivity.test.entities.BitType;
import org.jproggy.motivity.test.stores.BitCompositePkStore;
import org.jproggy.motivity.test.stores.PkSchema;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

/**
 * Tests the data types BIT.
 *
 * @version $Id: BitTest.java 1439295 2013-01-28 08:21:41Z tfischer $
 */
class BitTest {
  private static final BitCompositePkStore pks = PkSchema.bitCompositePkStore;
  private static Log log = LogFactory.getLog(BitTest.class);

  /**
   * Test whether we can insert into Tables with BIT PKs and BIT values.
   */
  @Test
  void checkCount() throws Exception {
    assertEquals(3, pks.all().count());
    assertEquals(2, bitTypeStore.all().count());
  }

  /**
   * Test whether we can select values using PK values
   */
  @Test
  void testSelectUsingBitPk() throws Exception {
    // check we get correct result when pks match
    List<BitCompositePk> result = pks.filter(
            and(
                    pks.pk1.equalTo("false value"),
                    pks.pk2.equalTo(false)
            )
    ).list();
    assertEquals(1, result.size());
    assertEquals("false payload", result.get(0).getPayload());

    result = pks.filter(pks.pk1.equalTo("true value").and(pks.pk2.equalTo(true))).list();
    assertEquals(1, result.size());
    assertEquals("true payload", result.get(0).getPayload());
  }

  /**
   * Test we can update values in a table where a BIT pk is used.
   */
  @Test
  void testUpdateWithBitPk() throws Exception {
    // check updating works
    BitCompositePk selectResult = pks.filter(
            and(
                    pks.pk1.equalTo("true value"),
                    pks.pk2.equalTo(true)
            )
    ).first().get();
    selectResult.setPayload("true updated payload");
    selectResult.save();

    List<BitCompositePk> result = pks.filter(pks.pk1.equalTo("true value").and(pks.pk2.equalTo(true))).list();
    assertEquals(1, result.size());
    assertEquals("true updated payload", result.get(0).getPayload());
  }

  /**
   * Tests whether column type BIT can be written and read correctly
   * and works in criteria as expected
   */
  @Test
  void testReadBitValue() throws Exception {
    // read data
    BitType bitType = bitTypeStore.filter(bitTypeStore.id.equalTo("t1")).one();
    assertTrue(bitType.getBitValue());

    bitType = bitTypeStore.filter(bitTypeStore.id.equalTo("f1")).first().get();
    assertFalse(bitType.getBitValue());
  }

  /**
   * Tests whether column type BIT works if set to true in Criteria.
   */
  @Test
  void testSelectBitTypeByTrueValue() throws Exception {
    // query data
    List<BitType> bitTypeList = bitTypeStore.filter(bitTypeStore.bitValue.equalTo(true)).list();
    assertEquals(1, bitTypeList.size());
    BitType bitType = bitTypeList.get(0);
    // use trim() for testkey because some databases will return the
    // testkey filled up with blanks, as it is defined as char(10)
    assertEquals("t1", bitType.getId());
  }

  /**
   * Tests whether column type BIT works if set to false in Criteria.
   */
  @Test
  void testSelectBitTypeByFalseValue() throws Exception {
    List<BitType> bitTypeList = bitTypeStore.filter(bitTypeStore.bitValue.equalTo(false)).list();
    assertEquals(1, bitTypeList.size());
    BitType bitValue = bitTypeList.get(0);
    assertEquals("f1", bitValue.getId());
  }

  /**
   * Checks whether the BIT type is supported in the database.
   * If not, a message is logged in the log file.
   */
  private boolean isBitSupported() {
    if (Db.defaultAdapter instanceof OracleAdapter) {
      log.warn("isBitSupported(): "
          + "BIT is known not to work with "
          + "Oracle and Derby");
      return false;
    }
    return true;
  }

  /**
   * Delete all previous data from the tested tables
   * and re-inserts test data.
   */
  @BeforeEach
  void setUp() throws Exception {
    Db.setUp();
    org.junit.Assume.assumeTrue(isBitSupported());
    pks.all().delete();
    bitTypeStore.all().delete();

    BitCompositePk bitCompositePk = new BitCompositePk();
    bitCompositePk.setPk1("false value");
    bitCompositePk.setPk2(Boolean.FALSE);
    bitCompositePk.setPayload("false payload");
    bitCompositePk.save();

    bitCompositePk = new BitCompositePk();
    bitCompositePk.setPk1("true value");
    bitCompositePk.setPk2(Boolean.TRUE);
    bitCompositePk.setPayload("true payload");
    bitCompositePk.save();

    bitCompositePk = new BitCompositePk();
    bitCompositePk.setPk1("value");
    bitCompositePk.setPk2(Boolean.TRUE);
    bitCompositePk.setPayload("payload");
    bitCompositePk.save();

    BitType bitType = new BitType();
    bitType.setId("t1");
    bitType.setBitValue(true);
    bitType.save();

    bitType = new BitType();
    bitType.setId("f1");
    bitType.setBitValue(false);
    bitType.save();
  }
}
