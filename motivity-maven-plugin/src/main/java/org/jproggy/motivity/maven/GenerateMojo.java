package org.jproggy.motivity.maven;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.io.IOException;
import javax.xml.bind.JAXBException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;

import org.jproggy.motivity.generator.TemplateSet;
import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.renderer.Renderer;

@Mojo(name = "generate", executionStrategy = "always")
public class GenerateMojo extends SchemaMojo {

  @Override
  public void execute() throws MojoExecutionException {
    if (project == null) {
      throw new MojoExecutionException("No project assigned");
    }
    if (config.isEmpty()) {
      getLog().warn("motivity:generate not configured - ignoring call");
      return;
    }
    ConfigParser parser = ConfigParser.of(config);
    for (File schemeFile : schemaFiles()) {
      try {
        Configuration scheme = parser.load(schemeFile.toURI().toURL());
        project.addCompileSourceRoot(scheme.getFolders().getBaseFolder().getAbsolutePath());
        getLog().info("processing " + schemeFile);
        TemplateSet.of(scheme).renderers().forEach(Renderer::execute);
      } catch (IOException | JAXBException | RuntimeException e) {
        throw new MojoExecutionException(schemeFile.getAbsolutePath(), e);
      }
    }
  }
}
