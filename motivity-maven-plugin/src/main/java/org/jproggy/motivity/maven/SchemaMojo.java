package org.jproggy.motivity.maven;

import org.apache.commons.io.filefilter.AndFileFilter;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.NotFileFilter;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;

import java.io.File;
import java.io.FileFilter;
import java.util.Properties;

public abstract class SchemaMojo extends AbstractMojo {
    @Parameter()
    protected Properties config;

    @Parameter(defaultValue = "${basedir}/src/main/schema")
    private File schemesDir;

    @Parameter(defaultValue = "${basedir}/src/main/resources/Torque.properties", readonly = true, required = true)
    protected File torqueProperties;

    @Parameter(defaultValue = "*-schema.xml")
    private String includes;

    @Parameter
    private String excludes;

    @Parameter(defaultValue = "${project}", readonly = true, required = true)
    protected MavenProject project;

    protected FileFilter fileFilter() {
        WildcardFileFilter includeFilter = WildcardFileFilter.builder().setWildcards(includes).get();
        if (excludes == null || excludes.trim().isEmpty()) {
            return includeFilter;
        }
        return new AndFileFilter(
                includeFilter,
                new NotFileFilter(
                    WildcardFileFilter.builder().setWildcards(excludes.split(",")).get()
                )
        );
    }

    protected File[] schemaFiles() {
        return schemesDir.listFiles(fileFilter());
    }
}
