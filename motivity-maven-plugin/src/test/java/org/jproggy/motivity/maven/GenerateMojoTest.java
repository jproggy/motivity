package org.jproggy.motivity.maven;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

public class GenerateMojoTest {

  @Rule
  public MojoRule rule = new MojoRule()
  {
  };

  /**
   * @throws Exception if any
   */
  @Test
  @Ignore
  public void testSomething()
      throws Exception
  {
      File pom = getTestFile( "src/test/resources/unit/project-to-test/pom.xml" );
      assertNotNull( pom );
      assertTrue( pom.exists() );

      GenerateMojo myMojo = (GenerateMojo) rule.lookupMojo( "touch", pom );
      assertNotNull( myMojo );
      myMojo.execute();

  }

  private File getTestFile(String string) {
    // TODO Auto-generated method stub
    return null;
  }
}
