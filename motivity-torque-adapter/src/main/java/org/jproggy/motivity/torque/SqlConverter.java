package org.jproggy.motivity.torque;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalTime;
import java.util.Date;
import org.apache.torque.util.JdbcTypedValue;

import org.jproggy.motivity.sql.map.Converters;

public class SqlConverter {
  public static final BoolReader<Integer> BOOLEANINT = new BoolReader<Integer>() {
    @Override
    public boolean readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return 1 == s.getInt(columnIndex);
    }
    @Override
    public Integer toSqlVal(Boolean val) {
      return Boolean.TRUE.equals(val) ? 1 : 0;
    }
    @Override
    public JdbcTypedValue toSql(Boolean val) {
      return new JdbcTypedValue(toSqlVal(val), Types.INTEGER);
    }
  };
  public static final BoolReader<String> BOOLEANCHAR = new BoolReader<String>() {
    @Override
    public boolean readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return "Y".equals(s.getString(columnIndex));
    }
    @Override
    public String toSqlVal(Boolean val) {
      return Boolean.TRUE.equals(val) ? "Y" : "N";
    }
    @Override
    public JdbcTypedValue toSql(Boolean val) {
      return new JdbcTypedValue(toSqlVal(val), Types.CHAR);
    }
  };
  public static final BoolReader<Boolean> BIT = new BoolReader<Boolean>() {
    @Override
    public boolean readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getBoolean(columnIndex);
    }
    @Override
    public Boolean toSqlVal(Boolean val) {
      return val;
    }
    @Override
    public JdbcTypedValue toSql(Boolean val) {
      return new JdbcTypedValue(toSqlVal(val), Types.BIT);
    }
  };
  public static final ByteReader TINYINT = new ByteReader(Types.TINYINT);
  public static final ShortReader SMALLINT = new ShortReader(Types.SMALLINT);
  public static final IntReader INTEGER = new IntReader(Types.INTEGER);
  public static final LongReader BIGINT = new LongReader(Types.BIGINT);
  public static final FloatReader REAL = new FloatReader(Types.REAL);
  public static final DoubleReader DOUBLE = new DoubleReader(Types.DOUBLE);
  public static final DoubleReader FLOAT = new DoubleReader(Types.FLOAT);
  public static final DecimalReader NUMERIC = new DecimalReader(Types.NUMERIC);
  public static final DecimalReader DECIMAL = new DecimalReader(Types.DECIMAL);
  public static final DateReader DATE = new DateReader() {
    @Override
    public Date readObject(ResultSet s, int columnIndex) throws SQLException {
      return toDate(s.getDate(columnIndex));
    }

    @Override
    public JdbcTypedValue toSql(Date val) {
      java.sql.Date result = (val == null) ? null : new java.sql.Date(val.getTime());
      return new JdbcTypedValue(result, Types.DATE);
    }
  };
  public static final DateReader TIME = new DateReader() {
    @Override
    public Date readObject(ResultSet s, int columnIndex) throws SQLException {
      return toDate(s.getTime(columnIndex));
    }

    @Override
    public JdbcTypedValue toSql(Date val) {
      Time result = (val == null) ? null : new Time(val.getTime());
      return new JdbcTypedValue(result, Types.TIME);
    }
  };
  public static final DateReader TIMESTAMP = new DateReader() {
    @Override
    public Date readObject(ResultSet s, int columnIndex) throws SQLException {
      return toDate(s.getTimestamp(columnIndex));
    }

    @Override
    public JdbcTypedValue toSql(Date val) {
      Timestamp result = (val == null) ? null : new Timestamp(val.getTime());
      return new JdbcTypedValue(result, Types.TIMESTAMP);
    }
  };
  public static final StringReader CHAR = new StringReader(Types.CHAR);
  public static final StringReader VARCHAR = new StringReader(Types.VARCHAR);
  public static final StringReader LONGVARCHAR = new StringReader(Types.LONGVARCHAR);
  public static final StringReader CLOB = new StringReader(Types.CLOB);
  public static final BinReader BINARY = new BinReader(Types.BINARY);
  public static final BinReader VARBINARY = new BinReader(Types.VARBINARY);
  public static final BinReader LONGVARBINARY = new BinReader(Types.LONGVARBINARY);
  public static final BinReader BLOB = new BinReader(Types.BLOB);

  private static <T> T handleNull(ResultSet s, T value) throws SQLException {
    if (s.wasNull()) return null;
    return value;
  }

  private static Date toDate(Date read) {
    if (read == null) return null;
    return new Date(read.getTime());
  }

  public static class BinReader extends Converter {
    public BinReader(int type) {
      super(type);
    }

    public byte[] readObject(ResultSet s, int columnIndex) throws SQLException {
      return s.getBytes(columnIndex);
    }
  }

  public static class StringReader extends Converter {
    public StringReader(int type) {
      super(type);
    }

    public String readObject(ResultSet s, int columnIndex) throws SQLException {
      return s.getString(columnIndex);
    }
  }

  public interface DateReader {
    Date readObject(ResultSet s, int columnIndex) throws SQLException;
    JdbcTypedValue toSql(Date val);
  }

  public static class TimeReader extends Converter {
    public TimeReader(int type) {
      super(type);
    }

    public LocalTime readObject(ResultSet rs, int columnIndex) throws SQLException {
      return Converters.TIME.readValue(rs, columnIndex);
    }
  }

  public static class DecimalReader extends Converter {
    public DecimalReader(int type) {
      super(type);
    }

    public BigDecimal readObject(ResultSet s, int columnIndex) throws SQLException {
      return s.getBigDecimal(columnIndex);
    }
  }

  public static class DoubleReader extends Converter {
    public DoubleReader(int type) {
      super(type);
    }

    public double readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getDouble(columnIndex);
    }

    public Double readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getDouble(columnIndex));
    }
  }

  public static class FloatReader extends Converter {
    public FloatReader(int type) {
      super(type);
    }

    public float readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getFloat(columnIndex);
    }

    public Float readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getFloat(columnIndex));
    }
  }

  public static class LongReader extends Converter {
    public LongReader(int type) {
      super(type);
    }

    public long readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getInt(columnIndex);
    }

    public Long readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getLong(columnIndex));
    }
  }

  public static class IntReader extends Converter {
    public IntReader(int type) {
      super(type);
    }

    public int readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getInt(columnIndex);
    }

    public Integer readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getInt(columnIndex));
    }
  }

  public static class ShortReader extends Converter {
    public ShortReader(int type) {
      super(type);
    }

    public short readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getShort(columnIndex);
    }

    public Short readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getShort(columnIndex));
    }
  }

  public static class ByteReader extends Converter {
    public ByteReader(int type) {
      super(type);
    }

    public byte readPrimitive(ResultSet s, int columnIndex) throws SQLException {
      return s.getByte(columnIndex);
    }

    public Byte readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getByte(columnIndex));
    }
  }

  public abstract static class BoolReader<T> {
    public abstract boolean readPrimitive(ResultSet s, int columnIndex) throws SQLException;
    public abstract T toSqlVal(Boolean val);
    public abstract JdbcTypedValue toSql(Boolean val);

    public Boolean readObject(ResultSet s, int columnIndex) throws SQLException {
      return handleNull(s, s.getBoolean(columnIndex));
    }
  }

  public static class Converter {
    private final int type;

    public Converter(int type) {
      this.type = type;
    }

    public JdbcTypedValue toSql(Object val) {
      return new JdbcTypedValue(val, type);
    }
  }
}
