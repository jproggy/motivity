package org.jproggy.motivity.torque;

import java.math.BigDecimal;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import org.apache.torque.om.BooleanKey;
import org.apache.torque.om.ComboKey;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;

public abstract class KeyReader {
  public static KeyReader read(ObjectKey key) {
    if (key instanceof ComboKey) {
      return new ComboReader((ComboKey)key);
    }
    return new AtomReader((SimpleKey)key);
  }

  public static KeyReader read(String data, int elements) {
    if (elements > 1) {
      return new ComboReader(new ComboKey(data));
    }
    if (elements == 1) {
      return new ParsingReader(data);
    }
    throw new UnsupportedOperationException();
  }

  public Object read() {
    return next().getValue();
  }

  public Long readLong() {
    return ((NumberKey)next()).longValue();
  }

  public Integer readInteger() {
    return ((NumberKey)next()).intValue();
  }

  public Short readShort() {
    return ((NumberKey)next()).shortValue();
  }

  public BigDecimal readBigDecimal() {
    return ((NumberKey)next()).getBigDecimal();
  }

  public Double readDouble() {
    return ((NumberKey)next()).doubleValue();
  }

  public Float readFloat() {
    return ((NumberKey)next()).floatValue();
  }

  public Date readDate() {
    return ((DateKey)next()).getDate();
  }

  public String readString() {
    return next().toString();
  }

  public Boolean readBoolean() {
    return ((BooleanKey)next()).getBoolean();
  }

  public Byte readByte() {
    return ((NumberKey)next()).byteValue();
  }
  
  public abstract boolean hasValue();

  public LocalTime readLocalTime() {
    return LocalTime.MIDNIGHT.plus(readDate().getTime(), ChronoUnit.MILLIS);
  }

  protected abstract SimpleKey next();

  private static class ComboReader extends KeyReader {
    private final SimpleKey[] container;
    private int index;

    public ComboReader(ComboKey container) {
      this.container = (SimpleKey[])container.getValue();
    }

    @Override
    protected SimpleKey next() {
      return container[index++];
    }

    @Override
    public boolean hasValue() {
      for (SimpleKey key: container) {
        if (KeyReader.read(key).hasValue()) return true;
      }
      return false;
    }
  }

  private static class AtomReader extends KeyReader {
    private final SimpleKey atom;

    public AtomReader(SimpleKey atom) {
      super();
      this.atom = atom;
    }

    @Override
    protected SimpleKey next() {
      return atom;
    }
    
    @Override
    public boolean hasValue() {
      if (atom == null || atom.getValue() == null) return false;
      if (atom instanceof NumberKey && ((NumberKey)atom).intValue() == 0) return false;
      return !(atom instanceof BooleanKey && Boolean.FALSE.equals(((BooleanKey)atom).getBoolean()));
    }
  }

  private static class ParsingReader extends KeyReader {
    private final String data;

    public ParsingReader(String data) {
      this.data = data;
    }

    @Override
    protected SimpleKey next() {
      throw new UnsupportedOperationException();
    }

    @Override
    public boolean hasValue() {
      return data != null && !data.isEmpty();
    }

    @Override
    public Integer readInteger() {
      return Integer.valueOf(data);
    }

    @Override
    public Short readShort() {
      return Short.valueOf(data);
    }

    @Override
    public BigDecimal readBigDecimal() {
      return new BigDecimal(data);
    }

    @Override
    public Double readDouble() {
      return Double.valueOf(data);
    }

    @Override
    public Float readFloat() {
      return Float.valueOf(data);
    }

    @Override
    public Date readDate() {
      return new Date(Long.parseLong(data));
    }

    @Override
    public String readString() {
      return data;
    }

    @Override
    public Boolean readBoolean() {
      return "true".equalsIgnoreCase(data);
    }

    @Override
    public Byte readByte() {
      return Byte.valueOf(data);
    }
  }
}
