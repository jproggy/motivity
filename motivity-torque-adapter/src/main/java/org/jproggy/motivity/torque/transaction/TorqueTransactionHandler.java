package org.jproggy.motivity.torque.transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import javax.sql.DataSource;
import org.apache.commons.configuration.Configuration;
import org.apache.torque.Database;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;
import org.apache.torque.dsfactory.DataSourceFactory;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.torque.TorqueDbConnector;
import org.jproggy.motivity.transaction.DatasourceWrapper;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.transaction.Transaction.Isolation;
import org.jproggy.motivity.transaction.spi.TransactionHandler;

import static java.util.Arrays.asList;

public class TorqueTransactionHandler implements TransactionHandler {
  private final ThreadLocal<Deque<SqlTransaction>> states = new ThreadLocal<>();
  final Map<String, DataSourceFactory> fabs = new ConcurrentHashMap<>();
  private final String[] dbNames;

  private TorqueTransactionHandler(String[] dbNames) {
    this.dbNames = dbNames;
  }

  public static TorqueTransactionHandler onDatabases(String... dbNames) {
    TorqueTransactionHandler handler = new TorqueTransactionHandler(dbNames);
    Transactions.setHandler(handler);
    asList(dbNames).forEach(TorqueDbConnector::getInstance);
    return handler;
  }

  @Override
  public Transaction openNew(Isolation iso, boolean readonly) {
    if (stack().isEmpty()) registerDataSourceFactories();
    TorqueTransaction newTra = new TorqueTransaction(this, iso, readonly);
    stack().push(newTra);
    return newTra;
  }

  private void registerDataSourceFactories() {
    try {
      for (String name: dbNames) {
        Database database = Torque.getDatabase(name);
        fabs.put(name, database.getDataSourceFactory());
        database.setDataSourceFactory(new DSFab(name));
      }
    } catch (TorqueException e) {
      throw new MotivityException(e);
    }
  }

  void unregisterDataSourceFactories() {
    try {
      for (String name: fabs.keySet()) {
        Database database = Torque.getDatabase(name);
        database.setDataSourceFactory(fabs.remove(name));
      }
    } catch (TorqueException e) {
      throw new MotivityException(e);
    }
  }

  @Override
  public boolean isTransactionOpen() {
    return top() != null;
  }

  public int depth() {
    if (states.get() == null) return 0;
    return stack().size();
  }

  @Override
  public Transaction top() {
    if (depth() == 0) return null;
    return peek();
  }

  private SqlTransaction peek() {
    return stack().getLast();
  }

  Deque<SqlTransaction> stack() {
    Deque<SqlTransaction> stack = states.get();
    if (stack == null) {
      stack = new ArrayDeque<>();
      states.set(stack);
    }
    return stack;
  }

  Connection createConnection(String name) {
    try {
      return fabs.get(name).getDataSource().getConnection();
    } catch (SQLException | TorqueException e) {
      throw new MotivityException(e);
    }
  }

  public void close() {
    unregisterDataSourceFactories();
  }

  public void transactionClosed(TorqueTransaction transaction) {
    stack().remove(transaction);
    if (stack().isEmpty()) {
      unregisterDataSourceFactories();
      states.remove();
    }
  }

  private class DSFab extends DatasourceWrapper implements DataSourceFactory {
    private final String name;

    public DSFab(String name) {
      this.name = name;
    }

    @Override
    public void close() throws TorqueException {
      fabs.get(name).close();
      fabs.remove(name);
    }

    @Override
    public DataSource getDataSource() {
      return this;
    }

    @Override
    public void initialize(Configuration arg0) {
      // can't be configured directly
    }

    @Override
    public Connection getConnection() {
      return peek().getManagedConnection(name);
    }

    @Override
    protected DataSource wrapped() {
      try {
        return fabs.get(name).getDataSource();
      } catch (TorqueException e) {
        throw new MotivityException(e);
      }
    }
  }
}
