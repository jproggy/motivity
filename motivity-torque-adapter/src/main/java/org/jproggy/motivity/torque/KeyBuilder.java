package org.jproggy.motivity.torque;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.torque.om.BooleanKey;
import org.apache.torque.om.ComboKey;
import org.apache.torque.om.DateKey;
import org.apache.torque.om.NumberKey;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.SimpleKey;
import org.apache.torque.om.StringKey;

public class KeyBuilder {
  private List<SimpleKey> container = new ArrayList<>();

  public void add(Number v) {
    container.add(new NumberKey(v));
  }

  public void add(Date v) {
    container.add(new DateKey(v));
  }

  public void add(String v) {
    container.add(new StringKey(v));
  }

  public void add(Boolean v) {
    container.add(new BooleanKey(v));
  }

  public void add(LocalTime v) {
    container.add(new DateKey(new Date(v.toNanoOfDay() / 1000_000)));
  }

  public ObjectKey build() {
    if (container.size() == 1) {
      return container.get(0);
    }
    if (container.isEmpty()) {
      return null;
    }
    return new ComboKey(container.toArray(new SimpleKey[container.size()]));
  }
}