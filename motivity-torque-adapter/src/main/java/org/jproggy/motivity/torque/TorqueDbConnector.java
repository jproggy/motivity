package org.jproggy.motivity.torque;

import java.util.HashMap;
import java.util.Map;
import org.apache.torque.Torque;
import org.apache.torque.TorqueException;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.snippetory.sql.spi.ConnectionProvider;

public class TorqueDbConnector {

    private static String variant(String name) {
        try {
            String adapterClassName = Torque.getDatabase(name).getAdapter().getClass().getSimpleName();
            if (adapterClassName.startsWith("Postgres")) return "postgresql";
            return adapterClassName.substring(0, adapterClassName.length() - "Adapter".length()).toLowerCase();
        } catch (TorqueException e) {
            throw new MotivityException(e);
        }
    }


    public static synchronized DbConnector getInstance(String name) {
        return DbConnector.getInstance(name, toConnectionProvider(name), variant(name));
    }

    private static ConnectionProvider toConnectionProvider(String name) {
        return () -> {
            try {
                return Torque.getConnection(name);
            } catch (TorqueException e) {
                throw new MotivityException(e);
            }
        };
    }
}
