package org.jproggy.motivity.torque.transaction;

import java.sql.Connection;

import org.jproggy.motivity.transaction.Transaction;

public interface SqlTransaction extends Transaction {

  Connection getManagedConnection(String name);

  int connectionClosed(DependendConnection con);
}