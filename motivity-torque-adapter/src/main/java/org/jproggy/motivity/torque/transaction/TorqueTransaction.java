package org.jproggy.motivity.torque.transaction;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jproggy.motivity.MotivityException;

class TorqueTransaction implements SqlTransaction {
  // TODO need reversed iteration order here.
  private final Map<String, Connection> connections = new LinkedHashMap<>();
  protected boolean running = true;
  private boolean begun;
  private boolean rollbackOnly;
  private final boolean readonly;
  private final Isolation iso;
  private final TorqueTransactionHandler handler;
  private final Map<Connection, Integer> counts = new HashMap<>();

  public TorqueTransaction(TorqueTransactionHandler handler, Isolation iso, boolean readonly) {
    this.readonly = readonly;
    this.iso = iso;
    this.handler = handler;
  }

  @Override
  public void begin() {
    if (running && begun) {
      throw new IllegalStateException("is already running");
    }
    running = true;
    begun = true;
  }

  @Override
  public void commit() {
    if (!running) {
      throw new IllegalStateException("no transaction to commit");
    }
    if (rollbackOnly) {
      rollback();
    } else {
      try {
        for (Connection conn : connections.values()) {
          conn.commit();
        }
      } catch (SQLException e) {
        try {
          rollback();
        } catch (Exception e1) {
          e.addSuppressed(e1);
        }
        throw new MotivityException("commit failed partitially - switched to rollback", e);
      } finally {
        running = false;
      }
    }
  }

  @Override
  public void rollback() {
    if (!running) {
      throw new IllegalStateException("no transaction to rollback");
    }
    running = false;
    MotivityException err = null;
    for (Connection conn : connections.values()) {
      try {
        conn.rollback();
      } catch (SQLException e) {
        if (err == null) err = new MotivityException("rollback failed partitially");
        err.addSuppressed(e);
      }
    }
    if (err != null) throw err;
  }

  @Override
  public void close() {
    try {
      if (running) {
        rollback();
      }
      if (!counts.isEmpty()) {
        throw new IllegalStateException(counts.toString());
      }
      MotivityException err = null;
      Iterator<Entry<String, Connection>> i = connections.entrySet().iterator();
      while (i.hasNext()) {
        try {
          i.next().getValue().close();
        } catch (SQLException e) {
          if (err == null) err = new MotivityException();
          err.addSuppressed(e);
        }
        i.remove();
      }
      if (err != null) throw err;
    } finally {
      handler.transactionClosed(this);
    }
  }

  @Override
  public void setRollbackOnly() {
    rollbackOnly = true;
  }

  @Override
  public Isolation getIsolation() {
    return iso;
  }

  @Override
  public boolean isReadOnly() {
    return readonly;
  }

  @Override
  public Connection getManagedConnection(String name) {
    Connection connection = connections.computeIfAbsent(name, this::createConnection);
    return new DependendConnection(this, connection, inc(connection));
  }

  private int inc(Connection connection) {
    return counts.merge(connection, 1, (x, y) -> x + 1);
  }

  Connection createConnection(String key) {
    try {
      Connection con = handler.createConnection(key);
      if (iso != null) con.setTransactionIsolation(iso.asSql());
      if (readonly) con.setReadOnly(true);
      return con;
    } catch (SQLException e) {
      throw new MotivityException(e);
    }
  }

  @Override
  public int connectionClosed(DependendConnection con) {
    int id = counts.merge(con.unwrap(), 0, (x, y) -> x - 1);
    if (id == 0) counts.remove(con.unwrap());
    return id + 1;
  }
}