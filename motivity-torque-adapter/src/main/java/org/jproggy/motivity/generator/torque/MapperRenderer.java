package org.jproggy.motivity.generator.torque;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.renderer.PairRenderer;
import org.jproggy.snippetory.Template;

public class MapperRenderer extends PairRenderer {

  public MapperRenderer(Configuration config, Template paths) {
    super(config, config.getNamingStrategy().getMapper(), paths);
  }

  @Override
  public Template getBaseItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("base").toString());
    bindMapper(template, store);
    return template;
  }

  @Override
  public Template getOverwriteItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("overwrite").toString());
    bindBasics(template, store);
    return template;
  }

  private void bindMapper(Template template, DbStore store) {
    bindBasics(template, store);
    template.set("identifierDb", store.getName());
    template.set("dbName", store.getConfig().getConnectionName());
    template.set("colCount", store.getColumns().size());

    renderFields(template.get("readPlain"), store);
    renderFields(template.get("readCriteria"), store);
    renderFields(template.get("methods"), store);
  }

  private void renderFields(Template template, DbStore store) {
    int index = 0;
    for (Column c: store.getColumns()) {
      template.set("index", ++index);
      bindColumn(template, c).render();
    }
  }
}
