package org.jproggy.motivity.generator.torque;

import java.util.Collection;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.motivity.generator.config.TableColumn;
import org.jproggy.motivity.generator.renderer.PairRenderer;
import org.jproggy.snippetory.Template;

public class PeerImplRenderer extends PairRenderer {

  public PeerImplRenderer(Configuration config, Template paths) {
    super(config, config.getNamingStrategy().getPeerImpl(), paths);
  }

  @Override
  public Template getBaseItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("base").toString());
    bindPeerImpl(template, store);
    return template;
  }

  @Override
  public Template getOverwriteItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("overwrite").toString());
    bindBasics(template, store);
    return template;
  }

  private void bindPeerImpl(Template template, DbStore store) {
    bindBasics(template, store);
    template.set("identifierDb", store.getName());
    template.set("dbName", store.getConfig().getName());
    template.set("colCount", store.getColumns().size());
    if (store.isAbstract()) {
      bindBasics(template.get("factory", "abstract"), store).render(template, "factory");
    } else {
      bindBasics(template.get("factory"), store).render();
    }
    for (Column col: store.getColumns()) {
      Template buildColVal = template.get("buildColValue");
      if (col.getType().checkForPreset(col)) {
        buildColVal = buildColVal.get("checkForPreset");
      }
      bindColumn(buildColVal, col).render(template, "buildColValue");
    }
    renderPks(template, store);
    for (Reference ref: store.getOutgoingRefs()) {
      renderOutgoingRef(template.get("fk"), ref);
    }
  }

  private void renderPks(Template template, DbStore store) {
    Collection<TableColumn> pks = store.getPkColumns();
    if (pks.isEmpty()) return;
    if (pks.size() == 1) {
      final Column pk = pks.iterator().next();
      bindColumn(template.get("pkBuilders"), pk).render();
    } else {
      Template pksTbl = ctx.getTemplate("$entities$/multipartPK.txt").get("peerImpl");
      bindBasics(pksTbl, store);
      int index = 0;
      for (Column pk: pks) {
        renderField(pksTbl.get("doc1"), pk);
        renderField(pksTbl.get("doc2"), pk);
        renderField(pksTbl.get("arg1"), pk);
        renderField(pksTbl.get("arg2"), pk);
        renderField(pksTbl.get("call1"), pk);
        renderField(pksTbl.get("call2"), pk);
        String region = index == 0 ? "pkCritStart" : "pkCritContinue";
        bindColumn(pksTbl.get(region), pk).set("index", index).render();
        ++index;
      }
      template.set("pkBuilders", pksTbl);
    }
    for (Column pk: pks) {
      renderField(template.get("buildCrit"), pk);
      renderField(template.get("doUpdate"), pk);
    }
  }

  private void renderField(Template template, Column col) {
    bindColumn(template, col).render();
  }
}
