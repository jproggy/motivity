package org.jproggy.motivity.generator.torque;

import static org.jproggy.snippetory.toolyng.letter.CaseFormat.UPPER_CAMEL;

import java.util.Collection;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.motivity.generator.config.TableColumn;
import org.jproggy.motivity.generator.renderer.PairRenderer;
import org.jproggy.snippetory.Template;

public class PeerRenderer extends PairRenderer {

  public PeerRenderer(Configuration config, Template paths) {
    super(config, config.getNamingStrategy().getPeer(), paths);
  }

  @Override
  public Template getBaseItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("base").toString());
    bindPeer(template, store);
    return template;
  }

  @Override
  public Template getOverwriteItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("overwrite").toString());
    bindBasics(template, store);
    return template;
  }

  public void bindPeer(Template template, DbStore store) {
    bindBasics(template, store);
    template.set("identifierDb", store.getName());
    template.set("dbName", store.getConfig().getConnectionName());
    template.set("colCount", store.getColumns().size());
    int i = 1;
    for (Column col: store.getColumns()) {
      renderField(template.get("colMapDef"), col);
      renderColMap(template.get("colMap"), col, i);
      i++;
    }
    for (Reference ref: store.getOutgoingRefs()) {
      renderOutgoingRef(template.get("fkMap"), ref);
      renderOutgoingRef(template.get("fk"), ref);
    }
    renderPks(template, store);
  }

  private void renderPks(Template template, DbStore store) {
    Collection<TableColumn> pks = store.getPkColumns();
    if (pks.isEmpty()) return;
    Template pksTbl;
    if (pks.size() == 1) {
      pksTbl = template.get("pkBuilders");
      pksTbl.set("type", pks.iterator().next().getType().getDefinition());
    } else {
      pksTbl = ctx.getTemplate("$entities$/multipartPK.txt").get("peer");
      for (TableColumn pk: pks) {
        renderField(pksTbl.get("doc1"), pk);
        renderField(pksTbl.get("doc2"), pk);
        renderField(pksTbl.get("arg1"), pk);
        renderField(pksTbl.get("arg2"), pk);
        renderField(pksTbl.get("call1"), pk);
        renderField(pksTbl.get("call2"), pk);
      }
    }
    pksTbl.set("Identifier", names.getEntityName(store, UPPER_CAMEL));
    template.set("pkBuilders", pksTbl);
  }

  public void renderColMap(Template template, Column col, int index) {
    template.set("preset", col.getType().getPreset(col));
    template.set("schemaType", col.getSchemaType());
    template.set("usePrimitive", col.getType().isPrimitive());
    template.set("pk", col.isPrimaryKey());
    template.set("notNull", col.isRequired());
    template.set("autoIncrement", col.isAutoIncrement());
    template.set("protected", col.isProtected());
    template.set("javaType", col.getType().getDefinition());
    renderOptional(template.get("default"), col.getDefault(), col);
    renderOptional(template.get("size"), col.getSize(), col);
    renderOptional(template.get("scale"), col.getScale(), col);
    template.set("index", index);

    renderField(template, col);
  }

  public void renderOptional(Template t, Object optional, Column col) {
    if (optional != null) {
      t.set("optional", optional);
      renderField(t, col);
    }
  }

  public void renderField(Template template, Column col) {
    bindColumn(template, col).render();
  }
}
