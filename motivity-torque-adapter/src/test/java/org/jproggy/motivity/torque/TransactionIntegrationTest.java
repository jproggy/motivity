package org.jproggy.motivity.torque;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.apache.torque.Torque;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.sql.SimpleTable;
import org.jproggy.motivity.query.sql.SimpleTable.Simple;
import org.jproggy.motivity.sql.MultiDbTest;
import org.jproggy.motivity.torque.transaction.TorqueTransactionHandler;
import org.jproggy.motivity.transaction.Transaction;

public class TransactionIntegrationTest extends MultiDbTest {
  SimpleTable store;
  TorqueTransactionHandler handler;

  public TransactionIntegrationTest() throws Exception {
    super(TorqueDbConnector.getInstance("test"), "DbAccessRepo.sql");
  }

  @BeforeClass
  public static void init() throws Exception {
    Torque.init("Torque.properties");
  }

  @Before
  public void setup() throws Exception {
    handler = TorqueTransactionHandler.onDatabases("test");
    Transactions.setHandler(handler);
    store = new SimpleTable(cons);
    if (hasTable(cons, SimpleTable.TABLE_NAME)) {
      store.all().delete();
    } else {
      run("createSimpleTable");
    }
  }

  @After
  public void tearDown() throws Exception {
    handler.close();
  }

  @AfterClass
  public static void shutdown() throws Exception {
    Torque.shutdown();
  }

  @Test
  public void rollbackTransaction() throws Exception {
    long countBefore = store.all().count();
    try (Transaction transaction = Transactions.openNew();) {
      store.insert(getInstance());
      transaction.rollback();
    }
    assertEquals(countBefore, store.all().count());
  }

  @Test
  public void commitTransaction() throws Exception {
    long countBefore = store.all().count();
    try (Transaction transaction = Transactions.openNew()) {
      store.insert(getInstance());
      transaction.commit();
    }
    assertEquals(countBefore + 1, store.all().count());
  }

  private static Simple getInstance() {
    Simple simple = new Simple();
    simple.setExtId("ext1");
    simple.setName("Karl");
    simple.setPrice(BigDecimal.valueOf(100));
    return simple;
  }
}
