package org.jproggy.motivity.torque;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.apache.torque.om.BooleanKey;
import org.junit.jupiter.api.Test;

class KeyReaderTest {

  @Test
  void readFalse() {
    KeyReader reader = KeyReader.read(new BooleanKey(false));
    assertEquals(Boolean.FALSE, reader.readBoolean());
    assertFalse(reader.hasValue());
  }

  @Test
  void readTrue() {
    KeyReader reader = KeyReader.read("true", 1);
    assertEquals(Boolean.TRUE, reader.readBoolean());
    assertTrue(reader.hasValue());
  }

  @Test
  void readEmpty() {
    KeyReader reader = KeyReader.read("", 1);
    assertEquals(Boolean.FALSE, reader.readBoolean());
    assertFalse(reader.hasValue());
  }

  @Test
  void read1() {
    KeyReader reader = KeyReader.read("1", 1);
    assertEquals(1, reader.readInteger());
    assertTrue(reader.hasValue());
  }

  @Test
  void readNull() {
    KeyReader reader = KeyReader.read(null);
    assertFalse(reader.hasValue());
  }
}