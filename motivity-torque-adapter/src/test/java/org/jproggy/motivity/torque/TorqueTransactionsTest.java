package org.jproggy.motivity.torque;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.sql.Connection;
import javax.sql.DataSource;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.torque.Torque;
import org.apache.torque.dsfactory.DataSourceFactory;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.torque.transaction.DependendConnection;
import org.jproggy.motivity.torque.transaction.TorqueTransactionHandler;
import org.jproggy.motivity.transaction.Transaction;

class TorqueTransactionsTest {

  private TorqueTransactionHandler handler;

  @BeforeAll static void init() throws Exception {
    if (!Torque.isInit()) {
      URL url = ClassLoader.getSystemResources("Torque.properties").nextElement();
      Configuration config = new PropertiesConfiguration(url);
      Torque.init(config);
    }
  }

  @BeforeEach void setUp() throws Exception {
    handler = TorqueTransactionHandler.onDatabases("test", "other");
  }

  @AfterEach void tearDown() throws Exception {
    handler.close();
  }

  @Test void test() throws Exception {
    Connection con = org.apache.torque.util.Transaction.begin();
    assertNotNull(con);
    con.close();
    try (Transaction t = Transactions.open()) {
      assertNotNull(t);
      con = org.apache.torque.util.Transaction.begin();
      assertNotNull(con);
      assertInstanceOf(DependendConnection.class, con);
      con.close();
    }
    con = org.apache.torque.util.Transaction.begin();
    assertNotNull(con);
    assertFalse(con instanceof DependendConnection);
    con.close();
    try (Transaction t = Transactions.open()) {
      assertNotNull(t);
      con = org.apache.torque.util.Transaction.begin();
      assertInstanceOf(DependendConnection.class, con);

      Connection con2 = org.apache.torque.util.Transaction.begin();
      assertInstanceOf(DependendConnection.class, con2);
      con2.close();
      con.close();

      con = org.apache.torque.util.Transaction.begin();
      assertInstanceOf(DependendConnection.class, con);
      con2 = org.apache.torque.util.Transaction.begin("other");
      assertNotNull(con2);
      assertInstanceOf(DependendConnection.class, con2);
      con2.close();
      con.close();
    }
  }

  @Test void wrappedCalls() throws Exception {
    try (Transaction t = Transactions.open()){
      DataSourceFactory ds = Torque.getDatabase("test").getDataSourceFactory();
      ds.getDataSource().getLoginTimeout();
      PrintWriter out = new PrintWriter(new ByteArrayOutputStream());
      ds.getDataSource().setLogWriter(out);
      assertEquals(out, ds.getDataSource().getLogWriter());
      assertInstanceOf(DataSource.class, ds);
      t.rollback();
    }
  }

  @Test void noMotivityTransaction() throws Exception {
    Connection con = org.apache.torque.util.Transaction.begin();
    assertNotNull(con);
    assertFalse(con instanceof DependendConnection);
    Connection con2 = org.apache.torque.util.Transaction.begin();
    assertNotNull(con2);
    assertFalse(con2 instanceof DependendConnection);
    assertFalse(con == con2);
    org.apache.torque.util.Transaction.commit(con2);
    org.apache.torque.util.Transaction.rollback(con);
  }

  @Test void dependentConUnclosed4ConInTra() throws Exception {
    assertThrows(IllegalStateException.class, () -> {
        try (Transaction t = Transactions.open()) {
          Connection con = org.apache.torque.util.Transaction.begin();
          assertNotNull(con);
          assertTrue(con instanceof DependendConnection);
          Connection con2 = org.apache.torque.util.Transaction.begin();
          assertNotNull(con2);
          assertTrue(con2 instanceof DependendConnection);
          con.close();
          assertEquals(((DependendConnection)con).unwrap(), ((DependendConnection)con2).unwrap());
        }
    });
  }

  @Test void dependentConUnclosed4Tra() throws Exception {
    Transaction t = Transactions.open();
    Connection con = org.apache.torque.util.Transaction.begin();
    assertNotNull(con);
    assertTrue(con instanceof DependendConnection);
    assertThrows(IllegalStateException.class, t::close);
  }

  @Test
  public void traInCon() throws Exception {
    Connection con = org.apache.torque.util.Transaction.begin();
    assertNotNull(con);
    assertFalse(con instanceof DependendConnection);
    try (Transaction t = Transactions.open()) {
      assertNotNull(t);
      Connection con2 = org.apache.torque.util.Transaction.begin();
      assertNotNull(con2);
      assertTrue(con2 instanceof DependendConnection);
      con2.close();
    }
    con.close();
  }

  @Test void unclosedTraInCon() throws Exception {
    Transaction t = Transactions.open();
    assertNotNull(t);
    Connection con2 = org.apache.torque.util.Transaction.begin();
    assertTrue(con2 instanceof DependendConnection);
    assertThrows(IllegalStateException.class, t::close);
  }
}
