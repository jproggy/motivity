package org.jproggy.motivity.generator.torque;

import com.google.common.truth.Truth;
import com.google.testing.compile.JavaFileObjects;
import com.google.testing.compile.JavaSourcesSubject;
import com.google.testing.compile.JavaSourcesSubjectFactory;
import java.io.File;
import java.util.Arrays;
import java.util.Properties;
import javax.tools.JavaFileObject;
import org.junit.Test;

import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.jaxb.ColumnType;
import org.jproggy.motivity.generator.config.jaxb.DatabaseElement;
import org.jproggy.motivity.generator.config.jaxb.ForeignKeyType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.config.jaxb.JavaReturnType;
import org.jproggy.motivity.generator.config.jaxb.ReferenceType;
import org.jproggy.motivity.generator.config.jaxb.SqlDataType;
import org.jproggy.motivity.generator.config.jaxb.TableType;
import org.jproggy.motivity.generator.renderer.EntityRenderer;
import org.jproggy.motivity.generator.renderer.PairRenderer;

public class CompileTest {
  private Renderers renderers;

  @Test
  public void intPropPeerTest() throws Exception {
    DatabaseElement db = new DatabaseElement();
    db.setName("test");
    TableType intProp = table(db, "intProp");
    ColumnType col = col(intProp, "test", SqlDataType.INTEGER, true);
    col.setIdMethod(IdMethodType.INCREMENT);
    Configuration config = config(db, "org/jproggy/motivity/templates/torque");
    DbStore t = config.getTable("intProp");
    renderers = Renderers.of(config);
    JavaFileObject[] entity = render(EntityRenderer.class, t, "x.BaseIntProp", "x.IntProp");
    JavaFileObject[] peer = render(PeerRenderer.class, t, "x.BaseIntPropPeer", "x.IntPropPeer");
    JavaFileObject[] peerImpl = render(PeerImplRenderer.class, t, "x.BaseIntPropPeerImpl", "x.IntPropPeerImpl");
    JavaFileObject[] map = render(MapperRenderer.class, t, "x.BaseIntPropRecordMapper", "x.IntPropRecordMapper");
    assertThat(entity[0], entity[1], peer[0], peer[1], peerImpl[0], peerImpl[1], map[0], map[1]).compilesWithoutError();
  }

  @Test
  public void strDatePeerTest() throws Exception {
    DatabaseElement db = new DatabaseElement();
    db.setName("test");
    TableType intProp = table(db, "str_date");
    ColumnType col = col(intProp, "test_str", SqlDataType.VARCHAR, false);
    col.setIdMethod(IdMethodType.INCREMENT);
    col(intProp, "test_date", SqlDataType.TIMESTAMP, false);
    Configuration config = config(db, "org/jproggy/motivity/templates/torque");
    renderers = Renderers.of(config);
    DbStore t = config.getTable("str_date");
    JavaFileObject[] entity = render(EntityRenderer.class, t, "x.BaseStrDate", "x.StrDate");
    JavaFileObject[] peer = render(PeerRenderer.class, t, "x.BaseStrDatePeer", "x.StrDatePeer");
    JavaFileObject[] peerImpl = render(PeerImplRenderer.class, t, "x.BaseStrDatePeerImpl", "x.StrDatePeerImpl");
    JavaFileObject[] map = render(MapperRenderer.class, t, "x.BaseStrDateRecordMapper", "x.StrDateRecordMapper");
    assertThat(entity[0], entity[1], peer[0], peer[1], peerImpl[0], peerImpl[1], map[0], map[1]).compilesWithoutError();
  }

  @Test
  public void multiPkPeerTest() throws Exception {
    DatabaseElement db = new DatabaseElement();
    db.setName("test");
    TableType intProp = table(db, "multiPk");
    ColumnType col = col(intProp, "test_dec", SqlDataType.DECIMAL, false);
    col.setIdMethod(IdMethodType.INCREMENT);
    col = col(intProp, "test_bool", SqlDataType.BIT, true);
    col.setIdMethod(IdMethodType.NONE);
    col(intProp, "test_bin", SqlDataType.BLOB, false);
    col(intProp, "test_data", SqlDataType.REAL, false);
    Configuration config = config(db, "org/jproggy/motivity/templates/torque");
    DbStore t = config.getTable("multiPk");
    renderers = Renderers.of(config);
    JavaFileObject[] entity = render(EntityRenderer.class, t, "x.BaseMultiPk", "x.MultiPk");
    JavaFileObject[] peer = render(PeerRenderer.class, t, "x.BaseMultiPkPeer", "x.MultiPkPeer");
    JavaFileObject[] peerImpl = render(PeerImplRenderer.class, t, "x.BaseMultiPkPeerImpl", "x.MultiPkPeerImpl");
    JavaFileObject[] map = render(MapperRenderer.class, t, "x.BaseMultiPkRecordMapper", "x.MultiPkRecordMapper");
    assertThat(entity[0], entity[1], peer[0], peer[1], peerImpl[0], peerImpl[1], map[0], map[1]).compilesWithoutError();
  }

  @Test
  public void refPeerTest() throws Exception {
    DatabaseElement db = new DatabaseElement();
    db.setName("test");
    TableType tblFrom = table(db, "test-from");
    ColumnType col = col(tblFrom, "dataFor", SqlDataType.DATE, false);
    col.setIdMethod(IdMethodType.NONE);
    col = col(tblFrom, "active", SqlDataType.INTEGER, false);
    ref(tblFrom, "testTo", fields("active", "id"));

    TableType tblTo = table(db, "testTo");
    col = col(tblTo, "id", SqlDataType.INTEGER, false);
    col.setIdMethod(IdMethodType.INCREMENT);
    col = col(tblTo, "SUM", SqlDataType.BIGINT, true);

    Configuration config = config(db, "org/jproggy/motivity/templates/torque");
    renderers = Renderers.of(config);
    DbStore tFrom = config.getTable("test-from");
    DbStore tTo = config.getTable("testTo");
    JavaFileObject[] entityFrom = render(EntityRenderer.class, tFrom, "x.BaseTestFrom", "x.TestFrom");
    JavaFileObject[] peerFrom = render(PeerRenderer.class, tFrom, "x.BaseTestFromPeer", "x.TestFromPeer");
    JavaFileObject[] peerImplFrom = render(PeerImplRenderer.class, tFrom, "x.BaseTestFromPeerImpl", "x.TestFromPeerImpl");
    JavaFileObject[] mapFrom = render(MapperRenderer.class, tFrom, "x.BaseTestFromRecordMapper", "x.TestFromRecordMapper");
    JavaFileObject[] entityTo = render(EntityRenderer.class, tTo, "x.BaseTestTo", "x.TestTo");
    JavaFileObject[] peerTo = render(PeerRenderer.class, tTo, "x.BaseTestToPeer", "x.TestToPeer");
    JavaFileObject[] peerImplTo = render(PeerImplRenderer.class, tTo, "x.BaseTestToPeerImpl", "x.TestToPeerImpl");
    JavaFileObject[] mapTo = render(MapperRenderer.class, tTo, "x.BaseTestToRecordMapper", "x.TestToRecordMapper");
    assertThat(entityFrom[0], entityFrom[1], entityTo[0], entityTo[1], peerFrom[0], peerFrom[1], peerTo[0], peerTo[1],
        peerImplFrom[0], peerImplFrom[1], peerImplTo[0], peerImplTo[1], mapFrom[0], mapFrom[1], mapTo[0], mapTo[1]).compilesWithoutError();
  }

  private void ref(TableType tblFrom, String tblTo, ReferenceType... refs) {
    ForeignKeyType ref = new ForeignKeyType();
    ref.setForeignTable(tblTo);
    tblFrom.getForeignKeiesAndIndicesAndUniques().addAll(Arrays.asList(refs));
  }

  private ReferenceType fields(String from, String to) {
    ReferenceType fields = new ReferenceType();
    fields.setForeign(to);
    fields.setLocal(from);
    return fields;
  }

  <T extends PairRenderer> JavaFileObject[] render(Class<T> type, DbStore t, String baseClassname, String classname) {
    PairRenderer r = renderers.byType(type);
    JavaFileObject base = JavaFileObjects.forSourceString(baseClassname, r.getBaseItem(t).toString());
    JavaFileObject overwrite = JavaFileObjects.forSourceString(classname, r.getOverwriteItem(t).toString());
    return new JavaFileObject[] {base, overwrite};
  }

  private JavaSourcesSubject assertThat(JavaFileObject... objects) {
    return Truth.assert_().about(JavaSourcesSubjectFactory.javaSources()).that(Arrays.asList(objects));
  }

  public static Configuration config(DatabaseElement db, String templateSet) throws Exception {
    Properties props = new Properties();
    props.put("templates.path", templateSet);
    props.put("naming.package", "x");
    return new ConfigParser(props).parse(db, new File("./" + db.getName() + "-schema.xml").toURI().toURL());
  }

  public static ColumnType col(TableType t, String name, SqlDataType type, boolean primitive) {
    ColumnType c = new ColumnType();
    t.getColumns().add(c);
    c.setName(name);
    c.setType(type);
    if (primitive) c.setJavaType(JavaReturnType.PRIMITIVE);
    return c;
  }

  public static TableType table(DatabaseElement db, String name) {
    TableType t = new TableType();
    db.getTables().add(t);
    t.setName(name);
    return t;
  }
}
