package org.jproggy.motivity.generator.torque;

import java.util.Properties;
import org.junit.Test;

import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.renderer.EntityRenderer;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.toolyng.test.Normalize;

public class EntityRendererTest {
  @Test
  public void test() throws Exception {
    Configuration config = config();
    EntityRenderer renderer = Renderers.of(config).byType(EntityRenderer.class);
    Template template = renderer.getBaseItem(config.getTable("test"));
    Normalize.assertNormEquals(TestResults.getBaseTest(), template.toString());
  }

  @Test
  public void other() throws Exception {
    Configuration config = config();
    EntityRenderer renderer = Renderers.of(config).byType(EntityRenderer.class);
    Template template = renderer.getBaseItem(config.getTable("other"));
    Normalize.assertNormEquals(TestResults.getBaseOther(), template.toString());
  }

  @Test
  public void otherMap() throws Exception {
    Configuration config = config();
    MapperRenderer renderer = Renderers.of(config).byType(MapperRenderer.class);
    Template template = renderer.getBaseItem(config.getTable("other"));
    Normalize.assertNormEquals(TestResults.getBaseOtherMapper(), template.toString());
  }

  @Test
  public void testMap() throws Exception {
    Configuration config = config();
    MapperRenderer renderer = Renderers.of(config).byType(MapperRenderer.class);
    Template template = renderer.getBaseItem(config.getTable("test"));
    Normalize.assertNormEquals(TestResults.getBaseTestMapper(), template.toString());
  }

  private Configuration config() throws Exception {
    Properties props = new Properties();
    props.put("templates.path", "org/jproggy/motivity/templates/torque");
    props.put("naming.package", "org.jproggy.test");
    return new ConfigParser(props).load(this.getClass().getResource("/moretest.xml"));
  }
}
