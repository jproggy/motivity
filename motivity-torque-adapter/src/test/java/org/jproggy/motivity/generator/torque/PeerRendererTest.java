package org.jproggy.motivity.generator.torque;

import java.util.Properties;
import org.junit.Before;
import org.junit.Test;

import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.toolyng.test.Normalize;

public class PeerRendererTest {
  Configuration config;
  Renderers renderers;
  @Before
  public void init() throws Exception {
    Properties props = new Properties();
    props.put("templates.path", "org/jproggy/motivity/templates/torque");
    props.put("naming.package", "org.jproggy.test");
    config = new ConfigParser(props).load(this.getClass().getResource("/moretest.xml"));
    renderers = Renderers.of(config);
  }

  @Test
  public void test() throws Exception {
    DbStore table = config.getTable("test");
    PeerRenderer renderer = renderers.byType(PeerRenderer.class);
    Template template = renderer.getBaseItem(table);

    Normalize.assertNormEquals(TestResults.getBaseTestPeer(), template.toString());
  }

  @Test
  public void testOther() throws Exception {
    DbStore table = config.getTable("other");
    PeerRenderer renderer = renderers.byType(PeerRenderer.class);
    Template template = renderer.getBaseItem(table);
    Normalize.assertNormEquals(TestResults.getBaseOtherPeer(), template.toString());
  }

  @Test
  public void testImpl() throws Exception {
    DbStore table = config.getTable("test");
    PeerImplRenderer renderer = renderers.byType(PeerImplRenderer.class);
    Template template = renderer.getBaseItem(table);
    Normalize.assertNormEquals(TestResults.getBaseTestPeerImpl(), template.toString());
  }

  @Test
  public void testOtherImpl() throws Exception {
    DbStore table = config.getTable("other");
    PeerImplRenderer renderer = renderers.byType(PeerImplRenderer.class);
    Template template = renderer.getBaseItem(table);
    Normalize.assertNormEquals(TestResults.getBaseOtherPeerImpl(), template.toString());
  }

}
