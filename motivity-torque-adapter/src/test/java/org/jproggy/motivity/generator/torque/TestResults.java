package org.jproggy.motivity.generator.torque;

import org.jproggy.snippetory.Syntaxes;
import org.jproggy.snippetory.TemplateContext;
import org.jproggy.snippetory.UriResolver;

public class TestResults {
  private static final TemplateContext REPO = Syntaxes.FLUYT.context().uriResolver(repo());

  private static UriResolver repo() {
    return UriResolver.resource("org/jproggy/motivity/generator/torque");
  }

  public static String getBaseOtherPeer() {
    return REPO.getTemplate("BaseOtherPeer.java").toString();
  }

  public static String getBaseTestPeer() {
    return REPO.getTemplate("BaseTestPeer.java").toString();
  }

  public static String getBaseOtherPeerImpl() {
    return REPO.getTemplate("BaseOtherPeerImpl.java").toString();
  }

  public static String getBaseTestPeerImpl() {
    return REPO.getTemplate("BaseTestPeerImpl.java").toString();
  }

  public static String getBaseOther() {
    return REPO.getTemplate("BaseOther.java").toString();
  }

  public static String getBaseOtherMulti() {
    return REPO.getTemplate("multi/BaseOther.java").toString();
  }

  public static String getBaseTest() {
    return REPO.getTemplate("BaseTest.java").toString();
  }

  public static String getBaseTestMulti() {
    return REPO.getTemplate("multi/BaseTest.java").toString();
  }

  public static String getBaseOtherMapper() {
    return REPO.getTemplate("BaseOtherRecordMapper.java").toString();
  }

  public static String getBaseTestMapper() {
    return REPO.getTemplate("BaseTestRecordMapper.java").toString();
  }

  public static String getBaseOtherMapperMulti() {
    return REPO.getTemplate("multi/BaseOtherRecordMapper.java").toString();
  }

  public static String getBaseTestMapperMulti() {
    return REPO.getTemplate("multi/BaseTestRecordMapper.java").toString();
  }
}
