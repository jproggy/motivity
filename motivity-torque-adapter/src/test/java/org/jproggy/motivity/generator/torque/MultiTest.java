package org.jproggy.motivity.generator.torque;

import java.util.Properties;
import org.junit.Before;
import org.junit.Test;

import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.renderer.EntityRenderer;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.toolyng.test.Normalize;

public class MultiTest {
  private Configuration config;
  private Renderers renderers;

  @Before
  public void setUp() throws Exception {
    Properties props = new Properties();
    props.put("templates.path", "org/jproggy/motivity/templates/torque");
    props.put("naming.package", "org.jproggy.test");
    config =  new ConfigParser(props).load(this.getClass().getResource("/multitest.xml"));
    renderers = Renderers.of(config);
  }

  @Test
  public void test() throws Exception {
    DbStore table = config.getTable("test");
    EntityRenderer renderer = renderers.byType(EntityRenderer.class);
    Template template = renderer.getBaseItem(table);
    Normalize.assertNormEquals(TestResults.getBaseTestMulti(), template.toString());
  }

  @Test
  public void other() throws Exception {
    DbStore table = config.getTable("other");
    EntityRenderer renderer = renderers.byType(EntityRenderer.class);
    Template template = renderer.getBaseItem(table);
    Normalize.assertNormEquals(TestResults.getBaseOtherMulti(), template.toString());
  }

  @Test
  public void otherMap() {
    MapperRenderer renderer = renderers.byType(MapperRenderer.class);
    Template template = renderer.getBaseItem(config.getTable("other"));
    Normalize.assertNormEquals(TestResults.getBaseOtherMapperMulti(), template.toString());
  }

  @Test
  public void testMap() {
    MapperRenderer renderer = renderers.byType(MapperRenderer.class);
    Template template = renderer.getBaseItem(config.getTable("test"));
    Normalize.assertNormEquals(TestResults.getBaseTestMapperMulti(), template.toString());
  }
}
