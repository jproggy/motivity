package org.jproggy.test;

import org.jproggy.test.Test;
import org.jproggy.test.TestPeer;

import java.io.Serializable;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import org.apache.torque.TorqueException;
import org.apache.torque.criteria.Criteria;
import org.apache.torque.map.TableMap;
import org.apache.torque.om.ObjectKey;
import org.apache.torque.om.Persistent;
import org.apache.torque.util.Transaction;

import org.jproggy.motivity.torque.KeyBuilder;
import org.jproggy.motivity.torque.KeyReader;


/**
 * This class was autogenerated by motivity on:
 * <p>
 * You should not use this class directly.  It should not even be
 * extended; all references should be to Test
 */
public abstract class BaseTest
    implements Persistent, Serializable {
  /** Defines the id field. */
  private Integer id = null;

  /** Defines the text field. */
  private String text = null;

  /** Defines the price field. */
  private java.math.BigDecimal price = new java.math.BigDecimal(0);

  /** Defines the bool field. */
  private boolean bool = false;

  /** Whether this object was modified after loading or after last save. */
  private boolean modified = true;

  /**
   * Whether this object was loaded from the database or already saved
   * (false) or whether it is not yet in the database(true).
   */
  private boolean isNew = true;

  /** Flag which indicates whether this object is currently saving. */
  private boolean saving = false;

  /**
   * Flag which indicates whether this object is currently loaded
   * from the database.
   */
  private boolean loading = false;

  /**
   * Flag which indicates whether this object was deleted from the database.
   * Note that this flags does not always display the current database state,
   * there is no magical connection between this flag and the database.
   */
  private boolean deleted = false;


  /** Defines the collOthersRelatedByTestId field. */
  protected List<org.jproggy.test.Other> collOthersRelatedByTestId = null;

  /** Defines the collOthersRelatedByTextAndCost field. */
  protected List<org.jproggy.test.Other> collOthersRelatedByTextAndCost = null;

  /**
   * Get the value of id.
   *
   * @return Integer
   */
  public Integer getId() {
    return id;
  }

  /**
   * Set the value of id.
   *
   * @param v new value
   */
  public void setId(Integer v) {
    if (!Objects.equals(this.id, v)) {
      setModified(true);
    }

    this.id = v;

    // update associated objects in collOthersRelatedByTestId
    if (collOthersRelatedByTestId != null) {
      for (int i = 0; i < collOthersRelatedByTestId.size(); i++) {
        collOthersRelatedByTestId.get(i).setTestId(v);
      }
    }
  }

  /**
   * Get the value of text.
   *
   * @return String
   */
  public String getText() {
    return text;
  }

  /**
   * Set the value of text.
   *
   * @param v new value
   */
  public void setText(String v) {
    if (!Objects.equals(this.text, v)) {
      setModified(true);
    }

    this.text = v;

    // update associated objects in collOthersRelatedByTextAndCost
    if (collOthersRelatedByTextAndCost != null) {
      for (int i = 0; i < collOthersRelatedByTextAndCost.size(); i++) {
        collOthersRelatedByTextAndCost.get(i).setText(v);
      }
    }
  }

  /**
   * Get the value of price.
   *
   * @return java.math.BigDecimal
   */
  public java.math.BigDecimal getPrice() {
    return price;
  }

  /**
   * Set the value of price.
   *
   * @param v new value
   */
  public void setPrice(java.math.BigDecimal v) {
    if (!Objects.equals(this.price, v)) {
      setModified(true);
    }

    this.price = v;

    // update associated objects in collOthersRelatedByTextAndCost
    if (collOthersRelatedByTextAndCost != null) {
      for (int i = 0; i < collOthersRelatedByTextAndCost.size(); i++) {
        collOthersRelatedByTextAndCost.get(i).setCost(v);
      }
    }
  }

  /**
   * Get the value of bool.
   *
   * @return boolean
   */
  public boolean getBool() {
    return bool;
  }

  /**
   * Set the value of bool.
   *
   * @param v new value
   */
  public void setBool(boolean v) {
    if (this.bool != v) {
      setModified(true);
    }

    this.bool = v;
  }

  /**
   * Returns whether the object has ever been saved.  This will
   * be false, if the object was retrieved from storage or was created
   * and then saved.
   *
   * @return true, if the object has never been persisted.
   */
  @Override
  public boolean isNew() {
    return isNew;
  }

  /**
   * Sets whether the object has ever been saved.
   *
   * @param isNew true if the object has never been saved, false otherwise.
   */
  @Override
  public void setNew(boolean isNew) {
    this.isNew = isNew;
  }

  /**
   * Returns whether the object has been modified.
   *
   * @return True if the object has been modified.
   */
  @Override
  public boolean isModified() {
    return modified;
  }

  /**
   * Sets whether the object has been modified.
   *
   * @param modified true if the object has been modified, false otherwise.
   */
  @Override
  public void setModified(boolean modified) {
    this.modified = modified;
  }

  /**
   * Sets the modified state for the object to be false.
   */
  public void resetModified() {
    modified = false;
  }


  /**
   * Returns whether this object is currently saving.
   *
   * @return true if this object is currently saving, false otherwise.
   */
  public boolean isSaving() {
    return saving;
  }

  /**
   * Sets whether this object is currently saving.
   *
   * @param saving true if this object is currently saving, false otherwise.
   */
  public void setSaving(boolean saving) {
    this.saving = saving;
  }


  /**
   * Returns whether this object is currently being loaded from the database.
   *
   * @return true if this object is currently loading, false otherwise.
   */
  public boolean isLoading() {
    return loading;
  }

  /**
   * Sets whether this object is currently being loaded from the database.
   *
   * @param loading true if this object is currently loading, false otherwise.
   */
  public void setLoading(boolean loading) {
    this.loading = loading;
  }


  /**
   * Returns whether this object was deleted from the database.
   * Note that this getter does not automatically reflect database state,
   * it will be set to true by Torque if doDelete() was called with this
   * object. Bulk deletes and deletes via primary key do not change
   * this flag. Also, if doDelete() was called on an object which does
   * not exist in the database, the deleted flag is set to true even if
   * it was not deleted.
   *
   * @return true if this object was deleted, false otherwise.
   */
  public boolean isDeleted() {
    return deleted;
  }

  /**
   * Sets whether this object was deleted from the database.
   *
   * @param deleted true if this object was deleted, false otherwise.
   */
  public void setDeleted(boolean deleted) {
    this.deleted = deleted;
  }

  /**
   * Initializes the cache collOthersRelatedByTestId for referenced objects.
   * This, means, if collOthersRelatedByTestId is null when this operation is called, it is
   * initialized with an empty collection, otherwise it remains unchanged.
   *
   * @return the (possibly new) content of the field collOthersRelatedByTestId, not null.
   */
  public List<org.jproggy.test.Other> initOthersRelatedByTestId() {
    if (collOthersRelatedByTestId == null) {
      collOthersRelatedByTestId = new ArrayList<>();
    }
    return collOthersRelatedByTestId;
  }

  /**
   * Checks whether the cache collOthersRelatedByTestId for referenced objects has either been
   * loaded from the database or has been manually initialized.
   */
  public boolean isOthersRelatedByTestIdInitialized() {
    return (collOthersRelatedByTestId != null);
  }


  /**
   * Method called to associate a Other object to this object
   * through the collOthersRelatedByTestId foreign key attribute.
   * If the associated objects were not retrieved before
   * and this object is not new, the associated objects are retrieved
   * from the database before adding the <code>toAdd</code> object.
   *
   * @param toAdd the object to add to the associated objects, not null.
   * @throws TorqueException      if retrieval of the associated objects fails.
   * @throws NullPointerException if toAdd is null.
   */
  public void addOtherRelatedByTestId(org.jproggy.test.Other toAdd)
      throws TorqueException {
    toAdd.setTestRelatedByTestId((Test)this);
    getOthersRelatedByTestId().add(toAdd);
  }

  /**
   * Method called to associate a Other object to this object
   * through the collOthersRelatedByTestId foreign key attribute using connection.
   *
   * @param toAdd Other
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public void addOtherRelatedByTestId(org.jproggy.test.Other toAdd, Connection con) throws TorqueException {
    getOthersRelatedByTestId(con).add(toAdd);
    toAdd.setTestRelatedByTestId((Test)this);
  }

  /**
   * The criteria used to select the current contents of collOthers
   */
  private Criteria lastOthersRelatedByTestIdCriteria = null;

  /**
   * If this collection has already been initialized, returns
   * the collection. Otherwise returns the results of
   * getOthersRelatedByTestId(new Criteria())
   *
   * @return the collection of associated objects
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTestId()
      throws TorqueException {
    return getOthersRelatedByTestId(new Criteria());
  }

  /**
   * If this collection has already been initialized with
   * an identical criteria, it returns the collection.
   * Otherwise if this BaseTest has previously
   * been saved, it will retrieve related collOthersRelatedByTestId from storage.
   * If this BaseTest is new, it will return
   * an empty collection or the current collection, the criteria
   * is ignored on a new object.
   *
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTestId(Criteria criteria) throws TorqueException {
    if (collOthersRelatedByTestId == null) {
      if (isNew()) {
        initOthersRelatedByTestId();
      } else {
        criteria.and(org.jproggy.test.OtherPeer.TEST_ID, getId());
        collOthersRelatedByTestId = org.jproggy.test.OtherPeer.doSelect(criteria);
      }
    } else {
      // criteria has no effect for a new object
      if (!isNew()) {
        // the following code is to determine if a new query is
        // called for.  If the criteria is the same as the last
        // one, just return the collection.
        criteria.and(org.jproggy.test.OtherPeer.TEST_ID, getId());
        if (lastOthersRelatedByTestIdCriteria == null
            || !lastOthersRelatedByTestIdCriteria.equals(criteria)) {
          collOthersRelatedByTestId = org.jproggy.test.OtherPeer.doSelect(criteria);
        }
      }
    }
    lastOthersRelatedByTestIdCriteria = criteria;

    return collOthersRelatedByTestId;
  }

  /**
   * If this collection has already been initialized, returns
   * the collection. Otherwise returns the results of
   * getOthersRelatedByTestId(new Criteria(),Connection)
   * This method takes in the Connection also as input so that
   * referenced objects can also be obtained using a Connection
   * that is taken as input
   *
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTestId(Connection con) throws TorqueException {
    return getOthersRelatedByTestId(new Criteria(), con);
  }

  /**
   * If this collection has already been initialized with
   * an identical criteria, it returns the collection.
   * Otherwise if this BaseTest has previously
   * been saved, it will retrieve the related Other Objects
   * from storage.
   * If this BaseTest is new, it will return
   * an empty collection or the current collection, the criteria
   * is ignored on a new object.
   * This method takes in the Connection also as input so that
   * referenced objects can also be obtained using a Connection
   * that is taken as input
   *
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTestId(Criteria criteria, Connection con) throws TorqueException {
    if (collOthersRelatedByTestId == null) {
      if (isNew()) {
        initOthersRelatedByTestId();
      } else {
        criteria.and(org.jproggy.test.OtherPeer.TEST_ID, getId());
        collOthersRelatedByTestId = org.jproggy.test.OtherPeer.doSelect(criteria, con);
      }
    } else {
      // criteria has no effect for a new object
      if (!isNew()) {
        // the following code is to determine if a new query is
        // called for.  If the criteria is the same as the last
        // one, just return the collection.
        criteria.and(org.jproggy.test.OtherPeer.TEST_ID, getId());
        if (!lastOthersRelatedByTestIdCriteria.equals(criteria)) {
          collOthersRelatedByTestId = org.jproggy.test.OtherPeer.doSelect(criteria, con);
        }
      }
    }
    lastOthersRelatedByTestIdCriteria = criteria;

    return collOthersRelatedByTestId;
  }


  /**
   * Initializes the cache collOthersRelatedByTextAndCost for referenced objects.
   * This, means, if collOthersRelatedByTextAndCost is null when this operation is called, it is
   * initialized with an empty collection, otherwise it remains unchanged.
   *
   * @return the (possibly new) content of the field collOthersRelatedByTextAndCost, not null.
   */
  public List<org.jproggy.test.Other> initOthersRelatedByTextAndCost() {
    if (collOthersRelatedByTextAndCost == null) {
      collOthersRelatedByTextAndCost = new ArrayList<>();
    }
    return collOthersRelatedByTextAndCost;
  }

  /**
   * Checks whether the cache collOthersRelatedByTextAndCost for referenced objects has either been
   * loaded from the database or has been manually initialized.
   */
  public boolean isOthersRelatedByTextAndCostInitialized() {
    return (collOthersRelatedByTextAndCost != null);
  }


  /**
   * Method called to associate a Other object to this object
   * through the collOthersRelatedByTextAndCost foreign key attribute.
   * If the associated objects were not retrieved before
   * and this object is not new, the associated objects are retrieved
   * from the database before adding the <code>toAdd</code> object.
   *
   * @param toAdd the object to add to the associated objects, not null.
   * @throws TorqueException      if retrieval of the associated objects fails.
   * @throws NullPointerException if toAdd is null.
   */
  public void addOtherRelatedByTextAndCost(org.jproggy.test.Other toAdd)
      throws TorqueException {
    toAdd.setTestRelatedByTextAndCost((Test)this);
    getOthersRelatedByTextAndCost().add(toAdd);
  }

  /**
   * Method called to associate a Other object to this object
   * through the collOthersRelatedByTextAndCost foreign key attribute using connection.
   *
   * @param toAdd Other
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public void addOtherRelatedByTextAndCost(org.jproggy.test.Other toAdd, Connection con) throws TorqueException {
    getOthersRelatedByTextAndCost(con).add(toAdd);
    toAdd.setTestRelatedByTextAndCost((Test)this);
  }

  /**
   * The criteria used to select the current contents of collOthers
   */
  private Criteria lastOthersRelatedByTextAndCostCriteria = null;

  /**
   * If this collection has already been initialized, returns
   * the collection. Otherwise returns the results of
   * getOthersRelatedByTextAndCost(new Criteria())
   *
   * @return the collection of associated objects
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTextAndCost()
      throws TorqueException {
    return getOthersRelatedByTextAndCost(new Criteria());
  }

  /**
   * If this collection has already been initialized with
   * an identical criteria, it returns the collection.
   * Otherwise if this BaseTest has previously
   * been saved, it will retrieve related collOthersRelatedByTextAndCost from storage.
   * If this BaseTest is new, it will return
   * an empty collection or the current collection, the criteria
   * is ignored on a new object.
   *
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTextAndCost(Criteria criteria) throws TorqueException {
    if (collOthersRelatedByTextAndCost == null) {
      if (isNew()) {
        initOthersRelatedByTextAndCost();
      } else {
        criteria.and(org.jproggy.test.OtherPeer.TEXT, getText());
        criteria.and(org.jproggy.test.OtherPeer.COST, getPrice());
        collOthersRelatedByTextAndCost = org.jproggy.test.OtherPeer.doSelect(criteria);
      }
    } else {
      // criteria has no effect for a new object
      if (!isNew()) {
        // the following code is to determine if a new query is
        // called for.  If the criteria is the same as the last
        // one, just return the collection.
        criteria.and(org.jproggy.test.OtherPeer.TEXT, getText());
        criteria.and(org.jproggy.test.OtherPeer.COST, getPrice());
        if (lastOthersRelatedByTextAndCostCriteria == null
            || !lastOthersRelatedByTextAndCostCriteria.equals(criteria)) {
          collOthersRelatedByTextAndCost = org.jproggy.test.OtherPeer.doSelect(criteria);
        }
      }
    }
    lastOthersRelatedByTextAndCostCriteria = criteria;

    return collOthersRelatedByTextAndCost;
  }

  /**
   * If this collection has already been initialized, returns
   * the collection. Otherwise returns the results of
   * getOthersRelatedByTextAndCost(new Criteria(),Connection)
   * This method takes in the Connection also as input so that
   * referenced objects can also be obtained using a Connection
   * that is taken as input
   *
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTextAndCost(Connection con) throws TorqueException {
    return getOthersRelatedByTextAndCost(new Criteria(), con);
  }

  /**
   * If this collection has already been initialized with
   * an identical criteria, it returns the collection.
   * Otherwise if this BaseTest has previously
   * been saved, it will retrieve the related Other Objects
   * from storage.
   * If this BaseTest is new, it will return
   * an empty collection or the current collection, the criteria
   * is ignored on a new object.
   * This method takes in the Connection also as input so that
   * referenced objects can also be obtained using a Connection
   * that is taken as input
   *
   * @throws TorqueException if retrieval of the associated objects fails.
   */
  public List<org.jproggy.test.Other> getOthersRelatedByTextAndCost(Criteria criteria, Connection con) throws TorqueException {
    if (collOthersRelatedByTextAndCost == null) {
      if (isNew()) {
        initOthersRelatedByTextAndCost();
      } else {
        criteria.and(org.jproggy.test.OtherPeer.TEXT, getText());
        criteria.and(org.jproggy.test.OtherPeer.COST, getPrice());
        collOthersRelatedByTextAndCost = org.jproggy.test.OtherPeer.doSelect(criteria, con);
      }
    } else {
      // criteria has no effect for a new object
      if (!isNew()) {
        // the following code is to determine if a new query is
        // called for.  If the criteria is the same as the last
        // one, just return the collection.
        criteria.and(org.jproggy.test.OtherPeer.TEXT, getText());
        criteria.and(org.jproggy.test.OtherPeer.COST, getPrice());
        if (!lastOthersRelatedByTextAndCostCriteria.equals(criteria)) {
          collOthersRelatedByTextAndCost = org.jproggy.test.OtherPeer.doSelect(criteria, con);
        }
      }
    }
    lastOthersRelatedByTextAndCostCriteria = criteria;

    return collOthersRelatedByTextAndCost;
  }


  /**
   * Stores an object in the database.  If the object is new,
   * it is inserted; otherwise an update is performed.
   *
   * @throws TorqueException if an error occurs during saving.
   */
  @Override
  public void save() throws TorqueException {
    save(TestPeer.DATABASE_NAME);
  }

  /**
   * Stores an object in the database.  If the object is new,
   * it is inserted; otherwise an update is performed.
   *
   * @param dbName the name of the database to which the object
   *               should be saved.
   * @throws TorqueException if an error occurs during saving.
   */
  @Override
  public void save(String dbName)
      throws TorqueException {
    Connection con = null;
    try {
      con = Transaction.begin(dbName);
      save(con);
      Transaction.commit(con);
    } catch (TorqueException e) {
      Transaction.safeRollback(con);
      throw e;
    }
  }

  /**
   * Stores an object in the database.  If the object is new,
   * it is inserted; otherwise an update is performed.  This method
   * is meant to be used as part of a transaction, otherwise use
   * the save() method and the connection details will be handled
   * internally.
   *
   * @param con the connection to use for saving the object, not null.
   * @throws TorqueException if an error occurs during saving.
   */
  @Override
  public void save(Connection con)
      throws TorqueException {
    if (isSaving()) {
      return;
    }
    try {
      setSaving(true);
      // If this object has been modified, then save it to the database.
      if (isModified()) {
        if (isNew()) {
          TestPeer.doInsert((Test)this, con);
          setNew(false);
        } else {
          TestPeer.doUpdate((Test)this, con);
        }
      }

      if (isOthersRelatedByTestIdInitialized()) {
        for (org.jproggy.test.Other collOthers: getOthersRelatedByTestId()) {
          collOthers.save(con);
        }
      }
      if (isOthersRelatedByTextAndCostInitialized()) {
        for (org.jproggy.test.Other collOthers: getOthersRelatedByTextAndCost()) {
          collOthers.save(con);
        }
      }
    } finally {
      setSaving(false);
    }
  }


  /**
   * Set the PrimaryKey using ObjectKey.
   */
  @Override
  public void setPrimaryKey(ObjectKey key) {
    KeyReader reader = KeyReader.read(key);
    setId(reader.readInteger());
  }

  /**
   * Set the PrimaryKey using a String.
   */
  @Override
  public void setPrimaryKey(String key) {
    KeyReader reader = KeyReader.read(key, 1);
    setId(reader.readInteger());
  }


  /**
   * returns an id that differentiates this object from others
   * of its class.
   */
  @Override
  public ObjectKey getPrimaryKey() {
    KeyBuilder builder = new KeyBuilder();
    builder.add(getId());
    return builder.build();
  }


  /**
   * Retrieves the TableMap object related to this Table data without
   * compiler warnings of using getPeer().getTableMap().
   *
   * @return The associated TableMap object.
   */
  public TableMap getTableMap() throws TorqueException {
    return org.jproggy.test.TestPeer.getTableMap();
  }


  @Override
  public String toString() {
    StringBuffer str = new StringBuffer();
    str.append("Test:\n");
    str.append("id = ")
        .append(getId())
        .append("\n");
    str.append("text = ")
        .append(getText())
        .append("\n");
    str.append("price = ")
        .append(getPrice())
        .append("\n");
    str.append("bool = ")
        .append(getBool())
        .append("\n");
    return (str.toString());
  }

  /**
   * Compares the primary key of this instance with the key of another.
   *
   * @param toCompare The object to compare to.
   * @return Whether the primary keys are equal and the object have the
   * same class.
   */
  @Override
  public boolean equals(Object toCompare) {
    if (toCompare == null) {
      return false;
    }
    if (this == toCompare) {
      return true;
    }
    if (!getClass().equals(toCompare.getClass())) {
      return false;
    }
    Test other = (Test)toCompare;
    if (getPrimaryKey() == null || other.getPrimaryKey() == null) {
      return false;
    }
    return getPrimaryKey().equals(other.getPrimaryKey());
  }

  /**
   * If the primary key is not <code>null</code>, return the hashcode of the
   * primary key.  Otherwise calls <code>Object.hashCode()</code>.
   *
   * @return an <code>int</code> value
   */
  @Override
  public int hashCode() {
    ObjectKey ok = getPrimaryKey();
    if (ok == null) {
      return super.hashCode();
    }

    return ok.hashCode();
  }
}
