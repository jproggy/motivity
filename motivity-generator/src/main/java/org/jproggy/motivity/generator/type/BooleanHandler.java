package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.ConfigFailure;

public class BooleanHandler extends TypeHandler {

  public BooleanHandler(Class<?> type) {
    super (type);
  }

  @Override
  public String getNullReplacement() {
    if (isPrimitive()) {
      return "false";
    }
    return super.getNullReplacement();
  }

  @Override
  public String getPreset(Column col) {
    if ("true".equalsIgnoreCase(col.getDefault())) {
      return "true";
    }
    if ("false".equalsIgnoreCase(col.getDefault())) {
      return "false";
    }
    if ("1".equals(col.getDefault())) {
      return "true";
    }
    if ("0".equals(col.getDefault())) {
      return "false";
    }
    if ("y".equalsIgnoreCase(col.getDefault())) {
      return "true";
    }
    if ("n".equalsIgnoreCase(col.getDefault())) {
      return "false";
    }
    if (col.getDefault() == null) {
      return getNullReplacement();
    }
    throw new ConfigFailure(col.getDefault() + " is not a valid default for boolean");
  }

  @Override
  public String getPrototyper() {
    return "false";
  }
}
