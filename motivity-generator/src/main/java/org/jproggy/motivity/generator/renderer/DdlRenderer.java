package org.jproggy.motivity.generator.renderer;

import java.sql.Time;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.ColumnReference;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.Index;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.motivity.generator.config.Table;
import org.jproggy.motivity.generator.config.TableColumn;
import org.jproggy.motivity.generator.config.UniqueKey;
import org.jproggy.motivity.generator.config.View;
import org.jproggy.motivity.generator.config.View.ViewColumn;
import org.jproggy.motivity.generator.config.jaxb.CascadeType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.type.DateHandler;
import org.jproggy.motivity.generator.type.SchemaType;
import org.jproggy.motivity.generator.util.VariantResolver;
import org.jproggy.snippetory.Template;

public class DdlRenderer extends Renderer implements StatementBuilder {
  private final Template ddl;

  public DdlRenderer(Configuration config, Template paths) {
    super(config);
    ddl = new VariantResolver(ctx.getTemplate(paths.get("base").toString()), config.getDbVariant());
  }

  @Override
  public void execute() {
    writeTo(names.getDdlPath(config), getDdl());
  }

  @Override
  public List<Template> buildStatements() {
    List<Template> result = new ArrayList<>();
    List<Template> fks = new ArrayList<>();
    Template template = ddl.get();
    for (String schema: config.getSchemas()) {
      result.add(template.get("createSchema").set("schema", schema));
    }
    for (Table t: config.getTables()) {
      if (t.isSkipSql()) continue;
      result.addAll(getStatements(template.get("createTable"), t));
      int n = 1;
      for (Reference r: t.getOutgoingRefs()) {
        String name = r.getName();
        if (name == null) name = r.getSourceTable().getName() + "_FK_" + n;
        fks.add(bindReference(template.get("createFK"), r, name));
        n++;
      }
    }
    result.addAll(fks);
    for (View v: config.getViews()) {
      if (!v.isSkipSql()) result.add(bindView(template.get("createView"), v));
    }
    return result;
  }

  private List<Template> getStatements(Template tplTable, Table table) {
    List<Template> result = new ArrayList<>();
    Template parts = tplTable.get("part");
    for (TableColumn c: table.getColumns()) {
      bindColumn(parts.get("col"), c).render(tplTable, "part");
    }
    if (!table.getPkColumns().isEmpty()) {
      bindPk(parts.get("pk"), table).render(tplTable, "part");
    }
    for (UniqueKey key: table.getUniqueKeys()) {
      Template tplKey = parts.get("unique").set("name", key.getName());
      key.getColumns().forEach(c -> tplKey.append("field", c.getName()));
      tplKey.render(tplTable, "part");
    }
    result.add(bindBasics(tplTable, table));
    for (Index key: table.getIndices()) {
      Template tplKey1 = parts.get("index").set("name", key.getName());
      key.getColumns().forEach(c -> tplKey1.append("field", c.getName()));
      tplKey1.render(tplTable, "part");
      Template tplKey2 = tplTable.get("index").set("name", key.getName()).set("table", table.getName());
      key.getColumns().forEach(c -> tplKey2.append("field", c.getName()));
      if (tplKey2.isPresent()) result.add(tplKey2);
    }
    if (table.getIdMethod() == IdMethodType.SEQUENCE && table.getIdParameter() != null) {
      Template seq = tplTable.get("sequence").set("idData", table.getIdParameter());
      if (seq.isPresent()) result.add(seq);
    }
    return result;
  }

  protected Template getDdl() {
    Template template = ddl.get();
    for (String schema: config.getSchemas()) {
      template.get("dropSchema").set("schema", schema).render();
      template.get("createSchema").set("schema", schema).render();
    }
    for (Table t: config.getTables()) {
      if (t.isSkipSql()) continue;
      bindTable(template.get("dropTable"), t).render();
      bindTable(template.get("createTable"), t).render();
      int n = 1;
      for (Reference r: t.getOutgoingRefs()) {
        String name = r.getName();
        if (name == null) name = r.getSourceTable().getName() + "_FK_" + n;
        bindReference(template.get("dropFK"), r, name).render();
        bindReference(template.get("createFK"), r, name).render();
        n++;
      }
    }
    for (View v: config.getViews()) {
      if (!v.isSkipSql()) bindView(template.get("createView"), v).render();
    }
    return bindBasics(template);
  }

  private Template bindReference(Template tblFk, Reference r, String name) {
    tblFk.set("destTable",  r.getTargetTable().getName())
        .set("deleteAction", format(r.getOnDelete()))
        .set("name", name);
    if (!("derby".equalsIgnoreCase(config.getDbVariant()) &&
        (r.getOnUpdate() == CascadeType.CASCADE || r.getOnUpdate() == CascadeType.SETNULL))) {
      tblFk.set("updateAction", format(r.getOnUpdate()));
    }

    for (ColumnReference col: r.getColumns()) {
      tblFk.append("field", col.getSource().getName());
      tblFk.append("destField", col.getTarget().getName());
    }
    return bindBasics(tblFk, r.getSourceTable());
  }

  private String format(CascadeType type) {
    if (type == null) return null;
    if (type == CascadeType.SETNULL) return "SET NULL";
    return type.name();
  }

  @Override
  protected Template bindBasics(Template template) {
    template.set("db", config.getConnectionName());
    template.set("dbVendor", config.getDbVariant());
    return template;
  }

  protected Template bindBasics(Template tplTable, Table table) {
    tplTable.set("table", table.getName());
    return bindBasics(tplTable);
  }

  private Template bindView(Template tplView, View view) {
    tplView.set("view", view.getName()).set("sqlSuffix", view.getSqlSuffix());
    for (ViewColumn c: view.getColumns()) {
      tplView.get("col").set("sql", c.getSelect()).set("alias", c.getName()).render();
    }
    return bindBasics(tplView);
  }

  private Template bindTable(Template tplTable, Table table) {
    Template parts = tplTable.get("part");
    for (TableColumn c: table.getColumns()) {
      bindColumn(parts.get("col"), c).render(tplTable, "part");
    }
    if (!table.getPkColumns().isEmpty()) {
      bindPk(parts.get("pk"), table).render(tplTable, "part");
    }
    for (UniqueKey key: table.getUniqueKeys()) {
      Template tplKey = parts.get("unique").set("name", key.getName());
      key.getColumns().forEach(c -> tplKey.append("field", c.getName()));
      tplKey.render(tplTable, "part");
    }
    for (Index key: table.getIndices()) {
      Template tplKey1 = parts.get("index").set("name", key.getName());
      key.getColumns().forEach(c -> tplKey1.append("field", c.getName()));
      tplKey1.render(tplTable, "part");
      Template tplKey2 = tplTable.get("index").set("name", key.getName()).set("table", table.getName());
      key.getColumns().forEach(c -> tplKey2.append("field", c.getName()));
      tplKey2.render();
    }
    if (table.getIdMethod() == IdMethodType.SEQUENCE && table.getIdParameter() != null) {
      tplTable.get("sequence").set("idData", table.getIdParameter()).render();
    }
    return bindBasics(tplTable, table);
  }

  private Template bindPk(Template tplPk, Table table) {
    for(Column pk : table.getPkColumns()) {
      tplPk.append("field", pk.getName());
    }
    return tplPk;
  }

  protected Template bindColumn(Template template, TableColumn col) {
    template.set("field", col.getName());
    template.set("type", config.type(col));
    template.set("defValue", defaultVal(col));
    if (col.isRequired() || col.isPrimaryKey())  template.get("required").render();
    if (col.isAutoIncrement()) template.get("autoInc").render();
    return template;
  }

  private String defaultVal(TableColumn col) {
    String val = col.getDefault();
    if (val == null) {
      return null;
    }
    SchemaType type = col.getTypeDefinition();
    if (type == SchemaType.DATE || type == SchemaType.TIMESTAMP) {
      if (val.startsWith("CURRENT_")) return null;
      Date parsed = new Date(DateHandler.parse(val));
      return config.getFormat(type).format(parsed);
    }
    if (type == SchemaType.TIME) {
      if ("CURRENT_TIME".equalsIgnoreCase(val)) return null;
      Date parsed = Time.valueOf(LocalTime.parse(val));
      return config.getFormat(type).format(parsed);
    }
    if (type.isQuoted()) {
      return "'" + val.replace("'", "''") + "'";
    }
    return val;
  }
}
