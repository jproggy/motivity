package org.jproggy.motivity.generator.util;

import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.util.TemplateWrapper;

public class VariantResolver  extends TemplateWrapper {
  private final String variant;

  public VariantResolver(Template template, String variant) {
    super(template);
    this.variant = variant;
  }

  @Override
  protected Template wrap(Template toBeWrapped) {
    if (toBeWrapped == null) return null;
    return new VariantResolver(toBeWrapped, variant);
  }

  @Override
  public Template get(String... names) {
    if (names.length == 1) {
      Template t = super.get(names);
      if (variant != null && t.regionNames().contains(variant)) {
        return new Bridge(t.get(variant), this, names[0]);
      }
      return t;
    }
    return super.get(names);
  }

  private static class Bridge extends TemplateWrapper {
    private final Template parent;
    private final String name;
    public Bridge(Template t, Template parent, String name) {
      super(t);
      this.parent = parent;
      this.name = name;
    }
    @Override
    protected Template wrap(Template toBeWrapped) {
      return toBeWrapped;
    }

    @Override
    public void render() {
      render(parent, name);
    }

    @Override
    public void render(String siblingName) {
      render(parent, siblingName);
    }
  }
}
