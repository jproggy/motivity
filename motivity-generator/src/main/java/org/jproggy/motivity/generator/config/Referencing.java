package org.jproggy.motivity.generator.config;

public class Referencing {
  public final Reference ref;
  public final Column col;

  public Referencing(Reference ref, Column col) {
    super();
    this.ref = ref;
    this.col = col;
  }

  Reference getReference() {
    return ref;
  }

  Column getColumn() {
    return col;
  }
  
}
