package org.jproggy.motivity.generator.config;

import java.util.Collection;
import java.util.List;

import org.jproggy.motivity.generator.config.jaxb.DomainType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.config.jaxb.InheritanceType;
import org.jproggy.motivity.generator.config.jaxb.JavaReturnType;
import org.jproggy.motivity.generator.type.SchemaType;
import org.jproggy.motivity.generator.type.TypeHandler;

public interface Column extends Named {
  List<InheritanceType> getInheritances();

  String getSchemaType();

  SchemaType getTypeDefinition();

  default TypeHandler getType() {
    SchemaType schemaType = getTypeDefinition();
    if (getJavaType() == JavaReturnType.PRIMITIVE) return schemaType.getPrimitiveType();
    return schemaType.getObjectType();
  }

  default Integer getJdbcType() {
    return getTypeDefinition().getJdbcType();
  }

  Integer getSize();

  Integer getScale();

  IdMethodType getIdMethod();

  boolean isPrimaryKey();

  boolean isAutoIncrement();

  boolean isRequired();

  JavaReturnType getJavaType();

  DomainType getDomain();

  boolean isProtected();

  String getDescription();

  DbStore getStore();

  Collection<Referencing> getIncomingRefs();

  Collection<Referencing> getOutgoingRefs();

  String getOption(String key);

  default String getDefault() { return null; }

  default boolean isUseDatabaseDefaultValue() { return false; }

  boolean isNoopMethod();
}
