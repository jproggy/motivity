package org.jproggy.motivity.generator.renderer;

import java.util.Date;

import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.snippetory.Syntaxes;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.TemplateContext;
import org.jproggy.snippetory.UriResolver;

public final class RenderContext extends TemplateContext {
  public RenderContext(Configuration config) {
    if ("folder".equalsIgnoreCase(config.getProp("templates.packaging"))) {
      uriResolver(UriResolver.directories(config.getProp("templates.path")));
    } else {
      uriResolver(UriResolver.resource(config.getProp("templates.path")));
    }
    syntax(Syntaxes.FLUYT_CC);
  }

  @Override
  public Template getTemplate(String uri) {
    Template template = super.getTemplate(uri);
    template.set("date", new Date());
    return template;
  }
}
