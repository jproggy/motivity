package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;

import java.math.BigDecimal;

public class BigDecimalHandler extends TypeHandler {

  public BigDecimalHandler() {
    super(BigDecimal.class);
  }

  @Override
  public String getPreset(Column col) {
    String def = col.getDefault();
    if (def == null) def = "0";
    return "new " + getDefinition() + '(' + def + ')';
  }

  @Override
  public boolean checkForPreset(Column col) {
    return col.isUseDatabaseDefaultValue();
  }

  @Override
  public String getPrototyper() {
    return getDefinition() + ".ZERO";
  }
}
