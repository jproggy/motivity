package org.jproggy.motivity.generator.config;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.config.jaxb.TableType;
import org.jproggy.snippetory.toolyng.letter.CaseFormat;
import org.jproggy.snippetory.toolyng.letter.CaseHelper;

public final class Table extends HasOptions implements DbStore {
  private final Configuration config;
  final TableType def;
  private List<Reference> incomingRefs = new ArrayList<>();
  private List<Reference> outgoingRefs = new ArrayList<>();
  private List<UniqueKey> uniqueKeys = new ArrayList<>();
  private List<Index> indices = new ArrayList<>();
  private String idParameter;
  // high access privileges necessary to resolve the cyclic reference between columns and table
  Map<String, TableColumn> columns;

  public Table(TableType t, Configuration config) {
    super(t.getOptions());
    def = t;
    this.config =  config;
  }

  @Override
  public Configuration getConfig() {
    return config;
  }

  @Override
  public String getName() {
    return def.getName();
  }

  @Override
  public String getInterface() {
    return def.getInterface();
  }

  @Override
  public String getBaseClass() {
    return def.getBaseClass();
  }

  @Override
  public String getBasePeer() {
    return def.getBasePeer();
  }

  @Override
  public IdMethodType getIdMethod() {
    return getPkColumns().stream().map(Column::getIdMethod)
            .filter(m -> m != null && m!= IdMethodType.NONE)
            .findAny()
            .orElse(null);
  }

  @Override
  public boolean isAbstract() {
    return def.isAbstract();
  }

  private static final Pattern SPLITTER = Pattern.compile(Pattern.quote("."));

  @Override
  public String getJavaName(CaseFormat charCase) {
    String val = def.getJavaName();
    if (val == null) {
      val = def.getName();
      String[] parts = SPLITTER.split(val, 0);
      if (parts.length > 0) {
        val = parts[parts.length - 1];
      }
    }
    return CaseHelper.convert(charCase, val);
  }

  @Override
  public boolean isSkipSql() {
    return def.isSkipSql();
  }

  @Override
  public String getDescription() {
    return def.getDescription();
  }

  @Override
  public List<Reference> getIncomingRefs() {
    return incomingRefs;
  }

  @Override
  public List<Reference> getOutgoingRefs() {
    return outgoingRefs;
  }

  public List<UniqueKey> getUniqueKeys() {
    return uniqueKeys;
  }

  @Override
  public Collection<TableColumn> getColumns() {
    return columns.values();
  }

  @Override
  public Collection<TableColumn> getPkColumns() {
    return  getColumns().stream().filter(Column::isPrimaryKey).collect(Collectors.toList());
  }

  @Override
  public TableColumn getColumn(String name) {
    TableColumn column = columns.get(name);
    if (column == null) throw new IllegalArgumentException(getName() + '.' + name + " not found");
    return column;
  }

  @Override
  public Optional<TableColumn> getIdColumn() {
    return  getColumns().stream().filter(
            c -> c.getIdMethod() != null && c.getIdMethod() != IdMethodType.NONE
    ).findAny();
  }

  @Override
  public String toString() {
    return def.getName();
  }

  @Override
  public int getReferenceCount(DbStore counted, RefType dir) {
    List<Reference> refs = (dir == RefType.IN) ? getOutgoingRefs() : getIncomingRefs();
    int count = 0;
    for (Reference r : refs) {
      DbStore compare = (dir == RefType.IN) ? r.getTargetTable() : r.getSourceTable();
      if (compare.equals(counted)) count++;
    }
    return count;
  }

  public List<Index> getIndices() {
    return indices;
  }

  public void setIdParameter(String idParameter) {
    this.idParameter = idParameter;
  }

  @Override
  public String getIdParameter() {
    return idParameter;
  }
}
