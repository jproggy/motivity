package org.jproggy.motivity.generator.renderer;

import org.jproggy.snippetory.Template;

import java.util.List;

public interface StatementBuilder {
  List<Template> buildStatements();
}
