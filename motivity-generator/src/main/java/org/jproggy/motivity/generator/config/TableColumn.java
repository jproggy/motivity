package org.jproggy.motivity.generator.config;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.jproggy.motivity.generator.config.jaxb.ColumnType;
import org.jproggy.motivity.generator.config.jaxb.DomainType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.config.jaxb.InheritanceAttrType;
import org.jproggy.motivity.generator.config.jaxb.InheritanceType;
import org.jproggy.motivity.generator.config.jaxb.JavaReturnType;
import org.jproggy.motivity.generator.config.jaxb.SqlDataType;
import org.jproggy.motivity.generator.type.SchemaType;
import org.jproggy.snippetory.toolyng.letter.CaseFormat;
import org.jproggy.snippetory.toolyng.letter.CaseHelper;

public class TableColumn extends HasOptions implements Column {
  private final ColumnType def;
  private final DbStore table;
  private List<Referencing> incomingRefs = new ArrayList<>();
  private List<Referencing> outgoingRefs = new ArrayList<>();

  public TableColumn(ColumnType c, DbStore table) {
    super(c.getOptions());
    this.def = c;
    this.table =  table;
  }

  @Override
  public List<InheritanceType> getInheritances() {
    return def.getInheritances();
  }

  @Override
  public String getName() {
    return def.getName();
  }

  @Override
  public String getSchemaType() {
    SqlDataType type = def.getType();
    if (type == null && getDomain() != null) {
      type = getDomain().getType();
    }
    if (type == null) {
      throw new ConfigFailure("Can't calculate sql type of " + this);
    }
    return type.name();
  }

  @Override
  public SchemaType getTypeDefinition() {
    return SchemaType.valueOf(getSchemaType());
  }

  @Override
  public Integer getSize() {
    BigDecimal val = def.getSize();
    if (val != null) return val.intValue();
    if (getDomain() != null) {
      val = getDomain().getSize();
      if (val != null) return val.intValue();
    }
    return null;
  }

  @Override
  public Integer getScale() {
    Integer val = calculateScale(def.getSize(), def.getScale());
    if (val == null && getDomain() != null) {
      return calculateScale(getDomain().getSize(), getDomain().getScale());
    }
    return val;
  }

  @Override
  public String getDefault() {
    String val = def.getDefault();
    if (val == null && getDomain() != null) {
      return getDomain().getDefault();
    }
    return val;
  }

  @Override
  public boolean isUseDatabaseDefaultValue() {
    Boolean useDbDefault = def.isUseDatabaseDefaultValue();
    return Boolean.TRUE.equals(useDbDefault);
  }

  @Override
  public IdMethodType getIdMethod() {
    if (def.getIdMethod() == IdMethodType.AUTO) {
      return table.getConfig().getPreferredIdMethod();
    }
    return def.getIdMethod();
  }

  @Override
  public boolean isPrimaryKey() {
    return def.getIdMethod() != null;
  }

  @Override
  public boolean isAutoIncrement() {
    return getIdMethod() == IdMethodType.INCREMENT;
  }

  @Override
  public boolean isNoopMethod() {
    return isAutoIncrement() || getIdMethod() == IdMethodType.NONE;
  }

  @Override
  public boolean isRequired() {
    return def.isRequired();
  }

  @Override
  public String getJavaName(CaseFormat charCase) {
    String val = def.getJavaName();
    if (val == null) val = def.getName();
    return CaseHelper.convert(charCase, val);
  }

  @Override
  public JavaReturnType getJavaType() {
    return def.getJavaType();
  }

  @Override
  public DomainType getDomain() {
    return getStore().getConfig().getDomain(def.getDomain());
  }

  public InheritanceAttrType getInheritance() {
    return def.getInheritance();
  }

  @Override
  public boolean isProtected() {
    return def.isProtected();
  }

  public boolean isVersion() {
    return def.isVersion();
  }

  @Override
  public String getDescription() {
    return def.getDescription();
  }

  @Override
  public DbStore getStore() {
    return table;
  }

  @Override
  public Collection<Referencing> getIncomingRefs() {
    return Collections.unmodifiableCollection(incomingRefs);
  }

  void addIncomingRef(Referencing incomingRef) {
    this.incomingRefs.add(incomingRef);
  }

  @Override
  public Collection<Referencing> getOutgoingRefs() {
    return Collections.unmodifiableCollection(outgoingRefs);
  }

  void addOutgoingRef(Referencing outgoingRef) {
    this.outgoingRefs.add(outgoingRef);
  }

  @Override
  public String toString() {
    return getStore().getName() + '.' + getName();
  }

  private Integer calculateScale(BigDecimal size, BigInteger scale) {
    if (scale != null) return scale.intValue();
    if (size == null) return null;
    String[] parts = size.toString().split("\\.");
    if (parts.length > 1) return Integer.valueOf(parts[1]);
    return null;
  }
}
