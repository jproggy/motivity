package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;

public class NumberHandler extends TypeHandler {
  private final String typeMarker;
  public NumberHandler(Class<?> type, String typeMarker) {
    super(type);
    this.typeMarker = typeMarker;
  }

  public NumberHandler(Class<?> type) {
    this(type, "");
  }

  @Override
  public String getDefinition() {
    return type.getSimpleName();
  }

  @Override
  public String getNullReplacement() {
    if (isPrimitive()) {
      return "0" + typeMarker;
    }
    return super.getNullReplacement();
  }

  @Override
  public String getPreset(Column col) {
    if (col.getDefault() == null) {
      return getNullReplacement();
    }
    return col.getDefault() + typeMarker;
  }

  @Override
  public boolean checkForPreset(Column col) {
    return col.isUseDatabaseDefaultValue() || col.isNoopMethod();
  }

  @Override
  public String getPrototyper() {
    return getDefinition() + ".valueOf((byte)0)";
  }
}
