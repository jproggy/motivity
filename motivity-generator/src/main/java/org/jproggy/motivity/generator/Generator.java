package org.jproggy.motivity.generator;

import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.renderer.Renderer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class Generator {
  public static void main(String[] args) throws Exception {
    if (args.length != 2) {
      System.out.println("Usage: <propertiesFile> <schemaFile>");
      return;
    }
    Configuration configuration = ConfigParser.of(readProperties(args[0])).load(toUrl(args[1]));
    TemplateSet.of(configuration).renderers().forEach(Renderer::execute);
  }

  private static URL toUrl(String path) throws IOException {
    return new File(".", path).toURI().toURL();
  }

  private static Properties readProperties(String configFile)
      throws IOException {
    try (Reader configData = new InputStreamReader(new FileInputStream(configFile), StandardCharsets.UTF_8)) {
      Properties config = new Properties();
      config.load(configData);
      return config;
    }
  }
}
