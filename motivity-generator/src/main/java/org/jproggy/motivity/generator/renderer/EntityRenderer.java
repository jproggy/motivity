package org.jproggy.motivity.generator.renderer;

import static org.jproggy.snippetory.toolyng.letter.CaseFormat.LOWER_CAMEL;
import static org.jproggy.snippetory.toolyng.letter.CaseFormat.UPPER_CAMEL;

import java.util.ArrayList;
import java.util.List;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.motivity.generator.config.Referencing;
import org.jproggy.motivity.generator.config.View;
import org.jproggy.motivity.generator.type.BinaryHandler;
import org.jproggy.snippetory.Template;


public class EntityRenderer extends PairRenderer {

  public EntityRenderer(Configuration config, Template paths) {
    super(config, config.getNamingStrategy().getEntity(), paths);
  }

  @Override
  public Template getOverwriteItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("overwrite").toString());
    bindBasics(template, store);
    return template;
  }

  @Override
  public Template getBaseItem(DbStore store) {
    String uri = store instanceof View ?
        paths.get("base", "view").toString() :
        paths.get("base", "table").toString();
    Template template = ctx.getTemplate(uri);
    bindEntity(template, store);
    return template;
  }

  protected void bindEntity(Template template, DbStore store) {
    bindBasics(template, store);
    List<Column> pks = new ArrayList<>();
    for (Column col: store.getColumns()) {
      if (col.isPrimaryKey()) {
        pks.add(col);
      }
      bindColumn(template.get("fieldDef"), col).render();
      renderFieldAccessors(template.get("fieldAccessors"), col);
      Template toString = template.get("toString");
      if (col.getType() instanceof BinaryHandler) {
        toString = toString.get("binary");
      }
      bindColumn(toString, col).render(template, "toString");
    }
    renderPKs(template, pks);
    for (Reference ref: store.getOutgoingRefs()) {
      renderOutgoingRef(template.get("referredDef"), ref);
      renderOutgoingRef(template.get("accessReferred"), ref);
      renderOutgoingRef(template.get("saveReferred"), ref);
    }
    for (Reference ref: store.getIncomingRefs()) {
      renderIncomingRef(template.get("referringDef"), ref);
      renderIncomingRef(template.get("accessReferring"), ref);
      renderIncomingRef(template.get("saveReferring"), ref);
    }
  }

  private void renderPKs(Template template, List<Column> pks) {
    if (!pks.isEmpty()) template.set("pkCount", pks.size());
    for (Column pk : pks) {
      bindColumn(template.get("fromKey"), pk).render();
      bindColumn(template.get("fromString"), pk).render();
      bindColumn(template.get("toKey"), pk).render();
    }
  }

  protected void renderFieldAccessors(Template template, Column col) {
    for (Referencing ref: col.getIncomingRefs()) {
      Template referring = template.get("maintainReferring");
      referring.set("ReferringField", names.getPropertyName(ref.col, UPPER_CAMEL));
      referring.set("ReferenceList", names.getLocalRefName(ref.ref, UPPER_CAMEL));
      referring.set("ReferringType", names.getEntityName(ref.col.getStore(), UPPER_CAMEL));
      referring.render();
    }
    for (Referencing ref: col.getOutgoingRefs()) {
      Template referred = template.get("maintainReferred");
      referred.set("ReferredType", names.getEntityName(ref.col.getStore(), UPPER_CAMEL));
      referred.set("ReferredItem", names.getForeignRefName(ref.ref, UPPER_CAMEL));
      referred.set("ReferredPk", names.getPropertyName(ref.col, UPPER_CAMEL));
      String compareReg = col.getType().isPrimitive() ? "primitiveCompare" : "objectCompare";
      Template compare = referred.get(compareReg);
      compare.set("ReferredType", names.getEntityName(ref.col.getStore(), UPPER_CAMEL));
      compare.set("ReferredItem", names.getForeignRefName(ref.ref, UPPER_CAMEL));
      compare.set("ReferredPk", names.getPropertyName(ref.col, UPPER_CAMEL));
      compare.render();
      referred.render();
    }
    if (col.getType().isPrimitive()) {
      template.get("primitiveCompare").set("field", names.getPropertyName(col, LOWER_CAMEL)).render();
    } else {
      template.get("objectCompare").set("field", names.getPropertyName(col, LOWER_CAMEL)).render();
    }
    bindColumn(template, col).render();
  }
}
