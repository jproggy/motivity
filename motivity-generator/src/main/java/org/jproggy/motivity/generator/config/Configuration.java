package org.jproggy.motivity.generator.config;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;

import org.jproggy.conftreaty.PropertiesTransferer;
import org.jproggy.motivity.generator.config.jaxb.DatabaseElement;
import org.jproggy.motivity.generator.config.jaxb.DomainType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.properties.DefaultNamingStrategy;
import org.jproggy.motivity.generator.properties.OutputFolders;
import org.jproggy.motivity.generator.type.SchemaType;
import org.jproggy.motivity.generator.util.VariantResolver;
import org.jproggy.snippetory.Syntaxes;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.TemplateContext;
import org.jproggy.snippetory.UriResolver;
import org.jproggy.snippetory.toolyng.letter.CaseFormat;
import org.jproggy.snippetory.toolyng.letter.CaseHelper;

public class Configuration extends HasOptions implements Named {
  private final DatabaseElement db;
  private final Properties options;
  private final Map<String, DomainType> domains = new LinkedHashMap<>();
  final Map<String, Table> tables = new LinkedHashMap<>();
  final Map<String, View> views = new LinkedHashMap<>();
  final List<String> schemas = new ArrayList<>();
  private final NamingStrategy names;
  private final OutputFolders folders;
  private final String name;
  private final Template properties;
  private final String dbVariant;

  public Configuration(DatabaseElement db, Properties options, String fileName) {
    super(db.getOptions());
    this.db = db;
    this.options = options;
    for (DomainType d: db.getDomains()) {
      domains.put(d.getName(), d);
    }
    dbVariant = options.getProperty("database.variant");
    PropertiesTransferer transferer = new PropertiesTransferer(options);
    names = transferer.transfer(DefaultNamingStrategy.class);
    folders = transferer.transfer(OutputFolders.class);
    name = fileName;
    TemplateContext ctx = new TemplateContext()
            .uriResolver(UriResolver.resource("org/jproggy/motivity"))
            .syntax(Syntaxes.FLUYT);
    properties = new VariantResolver(ctx.getTemplate("dbProperties.snip"), getDbVariant());
  }

  public String getProp(String name) {
    return options.getProperty(name);
  }

  public boolean isProp(String name) {
    return "true".equals(getProp(name));
  }

  public Collection<Table> getTables() {
    return tables.values();
  }

  public Table getTable(String name) {
    return tables.get(name);
  }

  public Collection<View> getViews() {
    return views.values();
  }

  public View getView(String name) {
    return views.get(name);
  }

  public List<String> getSchemas() {
    return new ArrayList<>(schemas);
  }

  public NamingStrategy getNamingStrategy() {
    return names;
  }

  public OutputFolders getFolders() {
    return folders;
  }

  public DomainType getDomain(String name) {
    return domains.get(name);
  }

  public String getConnectionName() {
    return db.getName();
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public String getJavaName(CaseFormat charCase) {
    return CaseHelper.convert(charCase, getName());
  }

  public String getDbVariant() {
    return dbVariant;
  }


  public IdMethodType getPreferredIdMethod() {
    String method = properties.get("preferredIdMethod").toString().trim();
    return IdMethodType.valueOf(method);
  }

  public SimpleDateFormat getFormat(SchemaType type) {
    SimpleDateFormat dateFormat = new SimpleDateFormat(properties.get("formats", type.name()).toString());
    dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
    return dateFormat;  }

  public Template type(TableColumn col) {
    return properties.get("types", col.getSchemaType()).set("size", col.getSize()).set("scale", col.getScale());
  }
  public boolean hasIndependentSchemas() {
    return Boolean.valueOf(properties.get("independentSchema").toString().trim()) == Boolean.TRUE;
  }
}
