package org.jproggy.motivity.generator.config;

public class ConfigFailure extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public ConfigFailure(String message) {
    super(message);
  }

  public ConfigFailure(String message, Throwable cause) {
    super(message, cause);
  }
}
