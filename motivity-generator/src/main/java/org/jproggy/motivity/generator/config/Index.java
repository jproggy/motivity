package org.jproggy.motivity.generator.config;

import org.jproggy.motivity.generator.config.jaxb.IndexType;

import java.util.List;

public class Index extends HasOptions {
  private final IndexType def;
  private final List<TableColumn> columns;

  public Index(IndexType def, List<TableColumn> columns) {
    super(def.getOptions());
    this.def = def;
    this.columns = columns;
  }

  public String getName() {
    return def.getName();
  }

  public List<TableColumn> getColumns() {
    return columns;
  }
}
