package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;

public class BinaryHandler extends TypeHandler {

  public BinaryHandler() {
    super(byte[].class);
  }

  @Override
  public String getPreset(Column col) {
    return "null";
  }

  @Override
  public String getDefinition() {
    return "byte[]";
  }

  @Override
  public String getPrototyper() {
    return "new byte[0]";
  }
}
