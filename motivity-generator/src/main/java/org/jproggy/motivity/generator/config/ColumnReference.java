package org.jproggy.motivity.generator.config;

import java.util.Objects;

public class ColumnReference {
  private final Column source;
  private final Column target;

  public ColumnReference(Column source, Column target) {
    super();
    this.source = Objects.requireNonNull(source);
    this.target = Objects.requireNonNull(target);
  }

  public Column getSource() {
    return source;
  }

  public Column getTarget() {
    return target;
  }

  @Override
  public int hashCode() {
    return source.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (obj == null) return false;
    if (getClass() != obj.getClass()) return false;
    ColumnReference other = (ColumnReference)obj;
    if (!Objects.equals(source, other.source)) return false;
    if (!Objects.equals(target, other.target)) return false;
    return true;
  }
}
