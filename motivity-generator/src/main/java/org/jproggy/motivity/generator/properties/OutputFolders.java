package org.jproggy.motivity.generator.properties;

import java.io.File;

import org.jproggy.conftreaty.Path;

@Path("output")
public class OutputFolders {
    private File overwrites;
    private File base;

    public File getBaseFolder() {
        return base;
    }

    public File getOverwritesFolder() {
        return overwrites;
    }
}
