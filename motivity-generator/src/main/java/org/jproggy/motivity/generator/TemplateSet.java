package org.jproggy.motivity.generator;

import static java.util.stream.Collectors.toList;

import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.renderer.RenderContext;
import org.jproggy.motivity.generator.renderer.Renderer;
import org.jproggy.motivity.generator.renderer.StatementBuilder;
import org.jproggy.snippetory.Syntaxes;
import org.jproggy.snippetory.Template;

public class TemplateSet {
  private final Configuration config;
  private final Template manifest;

  public TemplateSet(Configuration config) {
    this.config = config;
    RenderContext ctx = new RenderContext(config);
    ctx.syntax(Syntaxes.FLUYT);
    manifest = ctx.getTemplate("manifest.snip");
  }

  public static TemplateSet of(Configuration config) {
    return new TemplateSet(config);
  }

  public List<Renderer> renderers() {
    Template renderers = manifest.get("Renderers");
    return renderers.regionNames().stream().map(n -> newInstance(n, config, renderers)).collect(toList());
  }

  public void updateDb(Connection conn) throws SQLException {
    try (Statement statement = conn.createStatement()) {
      for (Renderer r: renderers() ) {
        if (r instanceof  StatementBuilder) {
          List<Template> statements = ((StatementBuilder)r).buildStatements();
          for (Template stmt: statements) {
            String sql = stmt.toString();
            int semiColon = sql.lastIndexOf(';');
            if (semiColon > 0) sql = sql.substring(0, semiColon);
            System.out.println(sql);
            statement.executeUpdate(sql);
            System.out.println("---");
          }
        }
      }
    }
  }

  private Renderer newInstance(String name, Configuration config, Template manifest) {
    try {
      return (Renderer) Class.forName(name)
              .getConstructor(Configuration.class, Template.class)
              .newInstance(config, manifest.get(name));
    } catch (InstantiationException | IllegalAccessException | InvocationTargetException |
        NoSuchMethodException | ClassNotFoundException e) {
      throw new IllegalArgumentException(name + " not a valid Renderer", e);
    }
  }

}
