package org.jproggy.motivity.generator.renderer;

import static org.jproggy.snippetory.toolyng.letter.CaseFormat.LOWER_CAMEL;
import static org.jproggy.snippetory.toolyng.letter.CaseFormat.UPPER_CAMEL;

import java.io.File;

import org.jproggy.motivity.generator.config.ColumnReference;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.NamingStrategy.ItemNaming;
import org.jproggy.motivity.generator.config.NamingStrategy.PairNaming;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.snippetory.Template;

public abstract class PairRenderer extends Renderer {
  protected final ItemNaming<DbStore> baseNames;
  protected final ItemNaming<DbStore> overwriteNames;
  protected final Template paths;

  protected PairRenderer(Configuration config, PairNaming pairNames, Template paths) {
    super(config);
    this.baseNames = pairNames.getBase();
    this.overwriteNames = pairNames.getOverwrite();
    this.paths = paths;
  }

  public abstract Template getOverwriteItem(DbStore store);
  public abstract Template getBaseItem(DbStore store);

  public void writeStore(DbStore store) {
    writeTo(baseNames.getPath(store, config.getFolders().getBaseFolder()), getBaseItem(store));
    File derived = overwriteNames.getPath(store, config.getFolders().getOverwritesFolder());
    if (!derived.exists()) {
      writeTo(derived, getOverwriteItem(store));
    }
  }

  @Override
  public void execute() {
    for (DbStore s: config.getTables()) {
      writeStore(s);
    }
    for (DbStore s: config.getViews()) {
      writeStore(s);
    }
  }

  public void renderIncomingRef(Template template, Reference ref) {
    bindBasics(template, ref.getTargetTable());
    template.set("ReferringType", names.getEntityName(ref.getSourceTable(), UPPER_CAMEL));
    template.set("referringType", names.getEntityName(ref.getSourceTable(), LOWER_CAMEL));
    template.set("ReferenceList", names.getLocalRefName(ref, UPPER_CAMEL));
    template.set("reference", names.getLocalRefName(ref, LOWER_CAMEL));
    template.set("addToReferenceList", names.getAddToRefListName(ref));
    template.set("referringItem", names.getLocalRefName(ref, LOWER_CAMEL));
    template.set("ReferredItem", names.getForeignRefName(ref, UPPER_CAMEL));

    for (String startRegion : template.regionNames()) {
      String contRegion = startRegion;
      if (startRegion.endsWith("Continue")) continue;
      if (startRegion.endsWith("Start")) {
        contRegion = startRegion.substring(0, startRegion.length() - 5) + "Continue";
      }
      boolean first = true;
      for (ColumnReference colRef: ref.getColumns()) {
        Template colRefTpl = template.get(first ? startRegion : contRegion);
        first = false;
        colRefTpl.set("ReferringType", names.getEntityName(ref.getSourceTable(), UPPER_CAMEL));
        colRefTpl.set("referringType", names.getEntityName(ref.getSourceTable(), LOWER_CAMEL));
        colRefTpl.set("ReferringField", names.getPropertyName(colRef.getSource(), UPPER_CAMEL));
        colRefTpl.set("referringField", names.getPropertyName(colRef.getSource(), LOWER_CAMEL));
        colRefTpl.set("referringFieldDb", colRef.getSource().getName());
        colRefTpl.set("REFERENCE_FIELD", names.fieldConstant(colRef.getSource()));
        colRefTpl.set("ReferenceList", names.getLocalRefName(ref, UPPER_CAMEL));
        colRefTpl.set("reference", names.getLocalRefName(ref, LOWER_CAMEL));
        colRefTpl.set("referringItem", names.getLocalRefName(ref, LOWER_CAMEL));
        colRefTpl.set("ReferredItem", names.getForeignRefName(ref, UPPER_CAMEL));
        bindColumn(colRefTpl, colRef.getTarget());
        colRefTpl.render();
      }
    }

    template.render();
  }

  public void renderOutgoingRef(Template template, Reference ref) {
    template.set("ReferredType", names.getEntityName(ref.getTargetTable(), UPPER_CAMEL));
    template.set("ReferredItem", names.getForeignRefName(ref, UPPER_CAMEL));
    template.set("referredItem", names.getForeignRefName(ref, LOWER_CAMEL));
    template.set("referredType", names.getEntityName(ref.getTargetTable(), LOWER_CAMEL));
    template.set("referredTable", ref.getTargetTable().getName());
    bindBasics(template, ref.getSourceTable());

    for (String startRegion : template.regionNames()) {
      String contRegion = startRegion;
      if (startRegion.endsWith("Continue")) continue;
      if (startRegion.endsWith("Start")) {
        contRegion = startRegion.substring(0, startRegion.length() - 5) + "Continue";
      }
      boolean first = true;
      for (ColumnReference colRef: ref.getColumns()) {
        Template colRefTpl = template.get(first ? startRegion : contRegion);
        first = false;
        bindColumn(colRefTpl, colRef.getSource());
        colRefTpl.set("ReferredPk", names.getPropertyName(colRef.getTarget(), UPPER_CAMEL));
        colRefTpl.set("REFERED_PK", names.fieldConstant(colRef.getTarget()));
        colRefTpl.set("referenceField", names.getPropertyName(colRef.getTarget(), LOWER_CAMEL));
        colRefTpl.set("referredPkDb", colRef.getTarget().getName());
        colRefTpl.set("referredType", names.getEntityName(ref.getTargetTable(), LOWER_CAMEL));
        colRefTpl.set("ReferredType", names.getEntityName(ref.getTargetTable(), UPPER_CAMEL));
        if (colRef.getSource().getType().isPrimitive() && !colRef.getTarget().getType().isPrimitive()) {
          colRefTpl.get("nullCheck").set("ReferredPk", names.getPropertyName(colRef.getTarget(), UPPER_CAMEL)).render();
        }
        colRefTpl.render();
      }
    }
    template.render();
  }

}
