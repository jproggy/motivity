package org.jproggy.motivity.generator.config;


import java.io.File;

import org.jproggy.snippetory.toolyng.letter.CaseFormat;

public interface NamingStrategy {

  String getEntityName(DbStore t, CaseFormat charCase);

  String getDbName(Configuration config, CaseFormat charCase);

  String getPropertyName(Column col, CaseFormat charCase);

  String getLocalRefName(Reference ref, CaseFormat charCase);
  String getAddToRefListName(Reference ref);
  String getForeignRefName(Reference ref, CaseFormat charCase);

  String fieldConstant(Column col);

  PairNaming getEntity();
  PairNaming getPeer();
  PairNaming getPeerImpl();
  PairNaming getMapper();
  PairNaming getStore();

  ItemNaming<Configuration> getSchema();
  File getDdlPath(Configuration config);

  String getGeneratorId();

  interface ItemNaming<T extends Named> {
    File getPath(T t, File dir);
    String getPackage();
    String getClassName(T t);
  }

  interface PairNaming {
    ItemNaming<DbStore> getBase();
    ItemNaming<DbStore> getOverwrite();
  }
}
