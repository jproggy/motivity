package org.jproggy.motivity.generator.properties;

import java.io.File;

import org.jproggy.conftreaty.Path;
import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.ColumnReference;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.DbStore.RefType;
import org.jproggy.motivity.generator.config.Named;
import org.jproggy.motivity.generator.config.NamingStrategy;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.snippetory.Syntaxes;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.toolyng.letter.CaseFormat;
import org.jproggy.snippetory.toolyng.letter.CaseHelper;

@Path("naming")
public class DefaultNamingStrategy implements NamingStrategy {
  private static final Template PATH_TPL = Syntaxes.FLUYT.parse("$package$/$name$.java");
  private static final Template REF_TPL = Syntaxes.FLUYT.parse("$type$RelatedBy$field(delimiter='And')$");
  private static final Template ADD_TO_REF_TPL = Syntaxes.FLUYT.parse("add$type$RelatedBy$field(delimiter='And')$");
  @Path("naming.package")
  private String basePackage = "";
  private String dbObjectPackageSuffix = "";
  private String peerPackageSuffix = "";
  private String peerImplPackageSuffix = "";
  private String recordMapperPackageSuffix = "";
  private String storePackageSuffix = "";
  private String baseDbObjectPackageSuffix = "";
  private String basePeerPackageSuffix = "";
  private String basePeerImplPackageSuffix = "";
  private String baseRecordMapperPackageSuffix = "";
  private String baseStorePackageSuffix = "";

  public DefaultNamingStrategy() {
    super();
  }

  @Override
  public String getPropertyName(Column col, CaseFormat charCase) {
    return col.getJavaName(charCase);
  }

  @Override
  public String getAddToRefListName(Reference ref) {
    if (ref.getName() != null) {
      return "add" + ref.getJavaName(CaseFormat.UPPER_CAMEL);
    }
    if (ref.getTargetTable().getReferenceCount(ref.getSourceTable(), RefType.OUT) > 1) {
      Template tpl = ADD_TO_REF_TPL.get().set("type", getEntityName(ref.getSourceTable(), CaseFormat.UPPER_CAMEL));
      for (ColumnReference col : ref.getColumns()) {
        tpl.append("field", getPropertyName(col.getSource(), CaseFormat.UPPER_CAMEL));
      }
      return tpl.toString();
    }
    return "add" + getEntityName(ref.getSourceTable(), CaseFormat.UPPER_CAMEL);
  }

  @Override
  public String getForeignRefName(Reference ref, CaseFormat charCase) {
    if (ref.getSourceTable().getReferenceCount(ref.getTargetTable(), RefType.IN) > 1) {
      Template tpl = REF_TPL.get().set("type", getEntityName(ref.getTargetTable(), charCase));
      for (ColumnReference col : ref.getColumns()) {
        tpl.append("field", getPropertyName(col.getSource(), CaseFormat.UPPER_CAMEL));
      }
      return tpl.toString();
    }
    return getEntityName(ref.getTargetTable(), charCase);
  }

  @Override
  public String getLocalRefName(Reference ref, CaseFormat charCase) {
    if (ref.getName() != null) {
      return ref.getJavaName(charCase) + 's';
    }
    if (ref.getTargetTable().getReferenceCount(ref.getSourceTable(), RefType.OUT) > 1) {
      Template tpl = REF_TPL.get().set("type", getEntityName(ref.getSourceTable(), charCase) + 's');
      for (ColumnReference col : ref.getColumns()) {
        tpl.append("field", getPropertyName(col.getSource(), CaseFormat.UPPER_CAMEL));
      }
      return tpl.toString();
    }
    return getEntityName(ref.getSourceTable(), charCase) + 's';
  }

  @Override
  public String fieldConstant(Column col) {
    return CaseHelper.convert(CaseFormat.UPPER_UNDERSCORE, col.getName());
  }

  @Override
  public String getEntityName(DbStore t, CaseFormat charCase) {
    return t.getJavaName(charCase);
  }

  @Override
  public String getDbName(Configuration config, CaseFormat charCase) {
    return config.getJavaName(charCase);
  }

  @Override
  public PairNaming getEntity() {
    return new PairNames("", dbObjectPackageSuffix, baseDbObjectPackageSuffix);
  }

  @Override
  public PairNaming getPeer() {
    return new PairNames("Peer", peerPackageSuffix, basePeerPackageSuffix);
  }

  @Override
  public PairNaming getPeerImpl() {
    return new PairNames("PeerImpl", peerImplPackageSuffix, basePeerImplPackageSuffix);
  }

  @Override
  public PairNaming getMapper() {
    return new PairNames("RecordMapper", recordMapperPackageSuffix, baseRecordMapperPackageSuffix);
  }

  @Override
  public PairNaming getStore() {
    return new PairNames("Store", storePackageSuffix, baseStorePackageSuffix);
  }

  @Override
  public ItemNaming<Configuration> getSchema() {
    return new ItemNames<>(storePackageSuffix, "");
  }

  @Override
  public File getDdlPath(Configuration config) {
    return new File(config.getFolders().getBaseFolder(), config.getName() + ".sql");
  }

  @Override
  public String getGeneratorId() {
    var version = getClass().getPackage().getImplementationVersion();
    var tool = getClass().getPackage().getImplementationTitle();
    return "org.jproggy:" + tool + ":" + version;
  }

  public String getBasePackage() {
    return basePackage;
  }

  private class PairNames implements PairNaming {
    private final ItemNames<DbStore> base;
    private final ItemNames<DbStore> overwite;

    public PairNames(String classSuffix, String ovPckSuf, String basePckSuf) {
      this.base = new ItemNames<>(true, basePckSuf, classSuffix);
      this.overwite = new ItemNames<>(false, ovPckSuf, classSuffix);
    }

    @Override
    public ItemNaming<DbStore> getBase() {
      return base;
    }

    @Override
    public ItemNaming<DbStore> getOverwrite() {
      return overwite;
    }
  }

  private class ItemNames<T extends Named> implements ItemNaming<T> {
    private final String pckSuff;
    private final String classPre;
    private final String classSuff;

    ItemNames(boolean base, String pckSuff, String classSuff) {
      this.pckSuff = pckSuff;
      this.classPre = base ? "Base" : "";
      this.classSuff = classSuff;
    }

    ItemNames(String pckSuff, String classSuff) {
      this.pckSuff = pckSuff;
      this.classPre = "";
      this.classSuff = classSuff;
    }

    @Override
    public File getPath(T t, File dir) {
      String path = PATH_TPL.get().set("package", toFile(getPackage())).set("name", getClassName(t)).toString();
      return new File(dir, path);
    }

    @Override
    public String getClassName(T t) {
      return classPre + t.getJavaName(CaseFormat.UPPER_CAMEL) + classSuff;
    }

    @Override
    public String getPackage() {
      return packageName(pckSuff);
    }

    private Object toFile(String pkg) {
      return pkg.replace(".", "/");
    }

    private String packageName(String pkg) {
      if (pkg == null || pkg.isEmpty()) {
        return getBasePackage();
      }
      if (pkg.charAt(0) == '.') {
        return getBasePackage() + pkg;
      }
      return getBasePackage() + '.' + pkg;
    }
  }
}
