package org.jproggy.motivity.generator.config;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;

import org.jproggy.motivity.generator.config.jaxb.DomainType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.motivity.generator.config.jaxb.InheritanceType;
import org.jproggy.motivity.generator.config.jaxb.JavaReturnType;
import org.jproggy.motivity.generator.config.jaxb.ViewColumnType;
import org.jproggy.motivity.generator.config.jaxb.ViewType;
import org.jproggy.motivity.generator.type.SchemaType;
import org.jproggy.snippetory.toolyng.letter.CaseFormat;
import org.jproggy.snippetory.toolyng.letter.CaseHelper;

public class View extends HasOptions implements DbStore {
  private final Configuration config;
  final ViewType def;
  Map<String, ViewColumn> columns;
  private static final Pattern SPLITTER = Pattern.compile(Pattern.quote("."));

  public View(ViewType t, Configuration config) {
    super(t.getOptions());
    def = t;
    this.config =  config;
  }

  public String getSqlSuffix() {
    return def.getSqlSuffix();
  }

  @Override
  public Configuration getConfig() {
    return config;
  }

  @Override
  public String getName() {
    return def.getName();
  }

  @Override
  public String getInterface() {
    return null;
  }

  @Override
  public String getBaseClass() {
    return def.getBaseClass();
  }

  @Override
  public String getBasePeer() {
    return def.getBasePeer();
  }

  @Override
  public IdMethodType getIdMethod() {
    return null;
  }

  @Override
  public boolean isAbstract() {
    return def.isAbstract();
  }

  @Override
  public String getJavaName(CaseFormat charCase) {
    String val = def.getJavaName();
    if (val == null) {
      val = def.getName();
      String[] parts = SPLITTER.split(val, 0);
      if (parts.length > 0) {
        val = parts[parts.length - 1];
      }
    }
    return CaseHelper.convert(charCase, val);
  }

  @Override
  public boolean isSkipSql() {
    return def.isSkipSql();
  }

  @Override
  public String getDescription() {
    return def.getDescription();
  }

  @Override
  public List<Reference> getIncomingRefs() {
    return Collections.emptyList();
  }

  @Override
  public List<Reference> getOutgoingRefs() {
    return Collections.emptyList();
  }

  @Override
  public Collection<ViewColumn> getColumns() {
    return columns.values();
  }

  @Override
  public Collection<TableColumn> getPkColumns() {
    return Collections.emptyList();
  }

  @Override
  public Column getColumn(String name) {
    return columns.get(name);
  }

  @Override
  public <T extends Column> Optional<T> getIdColumn() {
    return Optional.empty();
  }

  @Override
  public int getReferenceCount(DbStore counted, RefType dir) {
    return 0;
  }

  public class ViewColumn extends HasOptions implements Column{
    final ViewColumnType def;

    public ViewColumn(ViewColumnType def) {
      super(def.getOptions());
      this.def = def;
    }

    public String getSelect() {
      return def.getSelect();
    }

    @Override
    public List<InheritanceType> getInheritances() {
      return def.getInheritances();
    }

    @Override
    public String getName() {
      return def.getName();
    }

    @Override
    public String getSchemaType() {
      return def.getType().name();
    }

    @Override
    public SchemaType getTypeDefinition() {
      return SchemaType.valueOf(getSchemaType());
    }

    @Override
    public Integer getSize() {
      BigDecimal val = def.getSize();
      if (val != null) return val.intValue();
      if (getDomain() != null) {
        val = getDomain().getSize();
        if (val != null) return val.intValue();
      }
      return null;
    }

    @Override
    public Integer getScale() {
      Integer val = calculateScale(def.getSize(), def.getScale());
      if (val == null && getDomain() != null) {
        return calculateScale(getDomain().getSize(), getDomain().getScale());
      }
      return val;
    }

    @Override
    public IdMethodType getIdMethod() {
      return null;
    }

    @Override
    public boolean isPrimaryKey() {
      return false;
    }

    @Override
    public boolean isAutoIncrement() {
      return false;
    }

    @Override
    public boolean isNoopMethod() {
      return false;
    }

    @Override
    public boolean isRequired() {
      return false;
    }

    @Override
    public String getJavaName(CaseFormat charCase) {
      String val = def.getJavaName();
      if (val == null) val = def.getName();
      return CaseHelper.convert(charCase, val);
    }

    @Override
    public JavaReturnType getJavaType() {
      return JavaReturnType.OBJECT;
    }

    @Override
    public DomainType getDomain() {
      return config.getDomain(def.getDomain());
    }

    @Override
    public boolean isProtected() {
      return def.isProtected();
    }

    @Override
    public String getDescription() {
      return def.getDescription();
    }

    @Override
    public DbStore getStore() {
      return View.this;
    }

    @Override
    public Collection<Referencing> getIncomingRefs() {
      return Collections.emptyList();
    }

    @Override
    public Collection<Referencing> getOutgoingRefs() {
      return Collections.emptyList();
    }

    private Integer calculateScale(BigDecimal size, BigInteger scale) {
      if (scale != null) return scale.intValue();
      if (size == null) return null;
      String[] parts = size.toString().split("\\.");
      if (parts.length > 1) return Integer.valueOf(parts[1]);
      return null;
    }
  }
}
