package org.jproggy.motivity.generator.config;

import org.jproggy.motivity.generator.config.jaxb.OptionType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class HasOptions {
  private final Map<String, String> options = new HashMap<>();

  protected HasOptions(List<OptionType> options) {
    for (OptionType o: options) {
      this.options.put(o.getKey(), o.getValue());
    }
  }

  public String getOption(String key) {
    return options.get(key);
  }
}
