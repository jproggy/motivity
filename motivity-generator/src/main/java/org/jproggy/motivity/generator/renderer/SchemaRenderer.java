package org.jproggy.motivity.generator.renderer;

import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.snippetory.Template;

public class SchemaRenderer extends Renderer {
  private final Template paths;
  public SchemaRenderer(Configuration config, Template paths) {
    super(config);
    this.paths = paths;
  }

  public Template getSchema() {
    Template template = ctx.getTemplate(paths.get("base").toString());
    bindBasics(template);
    for (DbStore t : config.getTables()) {
      bindBasics(template.get("table-access"), t).render();
      bindBasics(template.get("table-array"), t).render();
    }
    for (DbStore t : config.getViews()) {
      bindBasics(template.get("table-access"), t).render();
      bindBasics(template.get("table-array"), t).render();
    }
    return template;
  }

  @Override
  public void execute() {
    writeTo(names.getSchema().getPath(config, config.getFolders().getBaseFolder()), getSchema());
  }

}
