package org.jproggy.motivity.generator.config;

import org.jproggy.motivity.generator.config.jaxb.CascadeType;
import org.jproggy.motivity.generator.config.jaxb.ForeignKeyType;
import org.jproggy.snippetory.toolyng.letter.CaseHelper;

import org.jproggy.snippetory.toolyng.letter.CaseFormat;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Pattern;

public class Reference extends HasOptions implements Named {
  private final List<ColumnReference> cols = new ArrayList<>();
  private final ForeignKeyType def;
  private final Table source;
  private final Table target;

  public Reference(ForeignKeyType fk, Table source, Table target) {
    super(fk.getOptions());
    this.source = source;
    this.target = target;
    def = fk;
  }

  public List<ColumnReference> getColumns() {
    return Collections.unmodifiableList(cols);
  }
  
  public ColumnReference addColumn(Column source, Column target) {
    ColumnReference col = new ColumnReference(source, target);
    cols.add(col);
    return col;
  }
  
  public Table getSourceTable() {
    return source;
  }
  public Table getTargetTable() {
    return target;
  }

  private static final Pattern SPLITTER = Pattern.compile(Pattern.quote("."));

  @Override
  public String getJavaName(CaseFormat charCase) {
    String val = def.getName();
    String[] parts = SPLITTER.split(val, 0);
    if (parts.length > 0) {
      val = parts[parts.length - 1];
    }
    return CaseHelper.convert(charCase, val);
  }

  public String getName() {
    return def.getName();
  }

  public void setName(String value) {
    def.setName(value);
  }

  public CascadeType getOnDelete() {
    return def.getOnDelete();
  }

  public void setOnDelete(CascadeType value) {
    def.setOnDelete(value);
  }

  public CascadeType getOnUpdate() {
    return def.getOnUpdate();
  }

  public void setOnUpdate(CascadeType value) {
    def.setOnUpdate(value);
  }
}
