package org.jproggy.motivity.generator.config;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.snippetory.toolyng.letter.CaseFormat;

public interface DbStore extends Named {
  enum RefType {IN, OUT}

  Configuration getConfig();

  @Override
  String getName();

  String getInterface();

  String getBaseClass();

  String getBasePeer();

  IdMethodType getIdMethod();

  boolean isAbstract();

  @Override
  String getJavaName(CaseFormat charCase);

  boolean isSkipSql();

  String getDescription();

  List<Reference> getIncomingRefs();

  List<Reference> getOutgoingRefs();

  Collection<? extends Column> getColumns();

  Collection<TableColumn> getPkColumns();

  Column getColumn(String name);

  <T extends Column> Optional<T> getIdColumn();

  int getReferenceCount(DbStore counted, RefType dir);

  default String getIdParameter() { return "";}
}
