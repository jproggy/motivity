package org.jproggy.motivity.generator.config;

import java.util.List;

public class UniqueKey {
  private final String name;
  private final List<TableColumn> columns;

  public UniqueKey(String name, List<TableColumn> columns) {
    this.name = name;
    this.columns = columns;
  }

  public String getName() {
    return name;
  }

  public List<TableColumn> getColumns() {
    return columns;
  }
}
