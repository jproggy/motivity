package org.jproggy.motivity.generator.type;

import java.sql.Types;

/**
 * Types from the schema XML file
 */
public enum SchemaType {
  BOOLEANINT(Types.TINYINT, new BooleanHandler(Boolean.class), new BooleanHandler(Boolean.TYPE)),
  BOOLEANCHAR(Types.CHAR, new BooleanHandler(Boolean.class), new BooleanHandler(Boolean.TYPE)),
  BIT(Types.BIT, new BooleanHandler(Boolean.class), new BooleanHandler(Boolean.TYPE)),
  TINYINT(Types.TINYINT, new NumberHandler(Byte.class), new NumberHandler(Byte.TYPE)),
  SMALLINT(Types.SMALLINT, new NumberHandler(Short.class), new NumberHandler(Short.TYPE)),
  INTEGER(Types.INTEGER, new NumberHandler(Integer.class), new NumberHandler(Integer.TYPE)),
  BIGINT(Types.BIGINT, new NumberHandler(Long.class, "L"), new NumberHandler(Long.TYPE, "L")),
  REAL(Types.REAL, new NumberHandler(Float.class, "f"), new NumberHandler(Float.TYPE, "f")),
  FLOAT(Types.FLOAT, new NumberHandler(Double.class, "d"), new NumberHandler(Double.TYPE, "d")),
  DOUBLE(Types.DOUBLE, new NumberHandler(Double.class, "d"), new NumberHandler(Double.TYPE, "d")),
  NUMERIC(Types.NUMERIC, new BigDecimalHandler()),
  DECIMAL(Types.DECIMAL, new BigDecimalHandler()),
  DATE(Types.DATE, new DateHandler()),
  TIME(Types.TIME, new TimeHandler()),
  TIMESTAMP(Types.TIMESTAMP, new DateHandler()),
  CHAR(Types.CHAR, new StringHandler()),
  VARCHAR(Types.VARCHAR, new StringHandler()),
  LONGVARCHAR(Types.LONGVARCHAR, new StringHandler()),
  CLOB(Types.CLOB, new StringHandler()),
  BINARY(Types.BINARY, new BinaryHandler()),
  VARBINARY(Types.VARBINARY, new BinaryHandler()),
  LONGVARBINARY(Types.LONGVARBINARY, new BinaryHandler()),
  BLOB(Types.BLOB, new BinaryHandler()),
  JAVA_OBJECT(Types.JAVA_OBJECT, null);

  private final Integer jdbcType;
  private final TypeHandler objectType;
  private final TypeHandler primitiveType;

  SchemaType(Integer jdbcType, TypeHandler objectType, TypeHandler primitiveType) {
    this.jdbcType = jdbcType;
    this.objectType = objectType;
    this.primitiveType = primitiveType;
  }

  SchemaType(Integer jdbcType, TypeHandler javaType) {
    this(jdbcType, javaType, javaType);
  }

  public Integer getJdbcType() {
    return jdbcType;
  }

  public TypeHandler getObjectType() {
    return objectType;
  }

  public TypeHandler getPrimitiveType() {
    return primitiveType;
  }

  public boolean isQuoted() {
    return getObjectType() instanceof StringHandler || getObjectType() instanceof DateHandler || this == BOOLEANCHAR;
  }
}
