package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;

public abstract class TypeHandler {
  protected final Class<?> type;

  public TypeHandler(Class<?> type) {
    this.type = type;
  }
  public String getNullReplacement() {
    return "null";
  }
  public String getDefinition() {
    return type.getName();
  }
  public boolean isPrimitive() {
    return type.isPrimitive();
  }
  public abstract String getPreset(Column col);

  public abstract String getPrototyper();

  public String getShortName() {
    return type.getSimpleName();
  }

  public boolean checkForPreset(Column col) {
    return false;
  }
}
