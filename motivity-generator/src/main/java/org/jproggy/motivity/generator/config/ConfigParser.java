package org.jproggy.motivity.generator.config;

import java.io.IOException;
import java.net.URL;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Collectors;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.jproggy.motivity.generator.config.View.ViewColumn;
import org.jproggy.motivity.generator.config.jaxb.ColumnType;
import org.jproggy.motivity.generator.config.jaxb.DatabaseElement;
import org.jproggy.motivity.generator.config.jaxb.ExternalSchemaType;
import org.jproggy.motivity.generator.config.jaxb.ForeignKeyType;
import org.jproggy.motivity.generator.config.jaxb.IdMethodParameterType;
import org.jproggy.motivity.generator.config.jaxb.IncludeSchemaType;
import org.jproggy.motivity.generator.config.jaxb.IndexType;
import org.jproggy.motivity.generator.config.jaxb.JavaReturnType;
import org.jproggy.motivity.generator.config.jaxb.ReferenceType;
import org.jproggy.motivity.generator.config.jaxb.TableType;
import org.jproggy.motivity.generator.config.jaxb.UniqueType;
import org.jproggy.motivity.generator.config.jaxb.ViewColumnType;
import org.jproggy.motivity.generator.config.jaxb.ViewType;

public class ConfigParser {
  private final Properties props;

  public static ConfigParser of(Properties props) throws IllegalArgumentException {
    String genClassName = props.getProperty("configParser.class");
    if (genClassName != null && !genClassName.isEmpty()) {
      try {
        return (ConfigParser)Class.forName(genClassName).getConstructor(Properties.class).newInstance(props);
      } catch (Exception e) {
        throw new IllegalArgumentException(genClassName + " is expected to extend " + ConfigParser.class.getName()
            + " and provide a public constructor taking Properties as an argument.", e);
      }
    } else {
      return new ConfigParser(props);
    }
  }


  public ConfigParser(Properties props) {
    this.props = props;
  }

  public Configuration load(URL target) throws JAXBException, IOException {
    return parse(unmarshalDatabase(target), target);
  }

  public Configuration parse(DatabaseElement db, URL context) throws JAXBException, IOException {
    Configuration configuration = configuration(db, context);
    Map<String, Table> tables = buildTables(configuration, db, context);
    initRefs(tables);
    if (configuration.hasIndependentSchemas()) {
      configuration.schemas.addAll(getSchemas(tables));
    }
    configuration.tables.putAll(tables);
    configuration.views.putAll(buildViews(configuration, db));
    return configuration;
  }

  protected DatabaseElement unmarshalDatabase(URL target) throws JAXBException {
    JAXBContext jaxbContext = JAXBContext.newInstance(DatabaseElement.class.getPackage().getName());
    Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
    return (DatabaseElement)jaxbUnmarshaller.unmarshal(target);
  }

  protected Map<String, Table> buildTables(Configuration configuration, DatabaseElement db, URL context)
      throws IOException, JAXBException {
    Map<String, Table> tables = new LinkedHashMap<>();
    for (ExternalSchemaType scheme : db.getExternalSchemas()) {
      DatabaseElement subDb = unmarshalDatabase(new URL(context, scheme.getFilename()));
      tables.putAll(buildTables(configuration, subDb, context));
    }
    for (TableType t: db.getTables()) {
      tables.put(t.getName(), buildTable(t, db.getDefaultJavaType(), configuration));
    }
    for (IncludeSchemaType scheme : db.getIncludeSchemas()) {
      DatabaseElement subDb = unmarshalDatabase(new URL(context, scheme.getFilename()));
      tables.putAll(buildTables(configuration, subDb, context));
    }
    return tables;
  }

  private Collection<String> getSchemas(Map<String, Table> tables) {
    return tables.keySet().stream()
            .map(n -> n.split("\\."))
            .filter(p -> p.length == 2)
            .map(p -> p[0])
            .collect(Collectors.toSet());
  }

  private void initRefs(Map<String, Table> tables) {
    for (Table table: tables.values()) {
      String idParameter = table.getName();
      for (Object x: table.def.getForeignKeiesAndIndicesAndUniques()) {
        if (x instanceof ForeignKeyType) {
          ForeignKeyType fk = (ForeignKeyType)x;
          buildRef(fk, table, tables.computeIfAbsent(fk.getForeignTable(),
                  k -> { throw new IllegalArgumentException("Table " + fk.getForeignTable() + " not found"); }));
        }
        if (x instanceof UniqueType) {
          UniqueType key = (UniqueType)x;
          List<TableColumn> columns = key.getUniqueColumns().stream()
              .map(u -> table.getColumn(u.getName()))
              .collect(Collectors.toList());
          table.getUniqueKeys().add(new UniqueKey(key.getName(), columns));
        }
        if (x instanceof  IndexType) {
          IndexType key = (IndexType)x;
          List<TableColumn> columns = key.getIndexColumns().stream()
              .map(u -> table.getColumn(u.getName()))
              .collect(Collectors.toList());
          table.getIndices().add(new Index(key, columns));
        }
        if (x instanceof IdMethodParameterType) {
          idParameter = ((IdMethodParameterType) x).getValue();
        }
      }
      table.setIdParameter(idParameter);
    }
  }

  private Reference buildRef(ForeignKeyType fk, Table local, Table foreign) {
    Reference ref = reference(fk, local, foreign);
    for (ReferenceType refType : fk.getReferences()) {
      TableColumn foreignCol = foreign.getColumn(refType.getForeign());
      TableColumn localCol = local.getColumn(refType.getLocal());
      ref.addColumn(localCol, foreignCol);
      localCol.addOutgoingRef(referencing(ref, foreignCol));
      foreignCol.addIncomingRef(referencing(ref, localCol));
    }
    foreign.getIncomingRefs().add(ref);
    local.getOutgoingRefs().add(ref);
    return ref;
  }

  protected Table buildTable(TableType t, JavaReturnType defaultJavaType, Configuration config) {
    Table table = table(t, config);
    Map<String, TableColumn> columns = new LinkedHashMap<>();
    for (ColumnType c: t.getColumns()) {
      if (c.getJavaType() == null) {
        c.setJavaType(defaultJavaType);
      }
      columns.put(c.getName(), column(table, c));
    }
    table.columns = columns;
    return table;
  }

  protected Map<String, View> buildViews(Configuration configuration, DatabaseElement db) {
    Map<String, View> views = new LinkedHashMap<>();
    for (ViewType t: db.getViews()) {
      views.put(t.getName(), buildView(t, configuration));
    }
    return views;
  }

  protected View buildView(ViewType v, Configuration config) {
    View view = view(v, config);
    Map<String, ViewColumn> columns = new LinkedHashMap<>();
    for (ViewColumnType c: v.getColumns()) {
      columns.put(c.getName(), column(view, c));
    }
    view.columns = columns;
    return view;
  }

  protected Configuration configuration(DatabaseElement db, URL context) {
    return new Configuration(db, props, removeType(name(context.getPath())));
  }

  private String name(String file) {
    int slash = file.lastIndexOf('/');
    if (slash < 0) return file;
    return file.substring(slash);
  }

  private String removeType(String file) {
    int dot = file.lastIndexOf('.');
    if (dot < 0) return file;
    if (dot == 0) return file.substring(1);
    return file.substring(1, dot);
  }

  protected TableColumn column(Table table, ColumnType c) {
    return new TableColumn(c, table);
  }

  protected ViewColumn column(View view, ViewColumnType c) {
    return view.new ViewColumn(c);
  }

  protected Referencing referencing(Reference ref, Column foreignCol) {
    return new Referencing(ref, foreignCol);
  }

  protected Reference reference(ForeignKeyType fk, Table local, Table foreign) {
    return new Reference(fk, local, foreign);
  }

  protected Table table(TableType t, Configuration config) {
    return new Table(t, config);
  }

  protected View view(ViewType v, Configuration config) {
    return new View(v, config);
  }
}
