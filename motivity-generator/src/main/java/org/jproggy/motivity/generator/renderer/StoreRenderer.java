package org.jproggy.motivity.generator.renderer;

import java.util.Collection;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.Reference;
import org.jproggy.motivity.generator.config.View;
import org.jproggy.motivity.generator.config.jaxb.IdMethodType;
import org.jproggy.snippetory.Template;

public class StoreRenderer extends PairRenderer {

  public StoreRenderer(Configuration config, Template paths) {
    super(config, config.getNamingStrategy().getStore(), paths);
  }

  @Override
  public Template getOverwriteItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("overwrite").toString());
    bindBasics(template, store);
    return template;
  }

  @Override
  public Template getBaseItem(DbStore store) {
    Template template = ctx.getTemplate(paths.get("base").toString());
    bindStore(template, store);
    String tplName = store.getPkColumns().isEmpty() ? "noKey" : "hasKey";
    bindStore(template.get(tplName), store).render();
    tplName = store instanceof View ? "noAdd" : "canAdd";
    bindStore(template.get(tplName), store).render();
    return template;
  }

  public Template bindStore(Template template, DbStore store) {
    bindBasics(template, store);
    template.set("identifierDb", store.getName());
    template.set("dbName", store.getConfig().getConnectionName());
    template.set("idData", store.getIdParameter());
    template.set("idMethod", format(store.getIdMethod()));
    for (Column col: store.getColumns()) {
      bindColumn(template.get("columnsDef"), col).render();
      bindColumn(template.get("columnsInit"), col).render();
      Template insertTpl = template.get("insert-column");
      if (col.isNoopMethod()) {
        bindColumn(insertTpl.get("noopCheck"), col).render();
      }
      bindColumn(insertTpl, col).render();
      bindColumn(template.get("update-column"), col).render();
    }
    renderPKs(template, store.getPkColumns());

    store.getIdColumn().ifPresent(c -> {
      bindColumn(template.get("idGen"), c).render();
      bindColumn(template.get("idFetch"), c).render();
    });

    for (Reference ref: store.getIncomingRefs()) {
      renderIncomingRef(template.get("refSources"), ref);
    }
    for (Reference ref: store.getOutgoingRefs()) {
      renderOutgoingRef(template.get("refTargets"), ref);
    }
    return template;
  }

  private String format(IdMethodType idMethod) {
    if (idMethod == IdMethodType.INCREMENT) return "INCREMENT";
    if (idMethod == IdMethodType.TABLE) return "TABLE";
    if (idMethod == IdMethodType.SEQUENCE) return "SEQUENCE";
    return "NONE";
  }

  private <T extends Column> void renderPKs(Template template, Collection<T> pks) {
    for (Column pk : pks) {
      bindColumn(template.get("pk-cond"), pk).render();
    }
  }}
