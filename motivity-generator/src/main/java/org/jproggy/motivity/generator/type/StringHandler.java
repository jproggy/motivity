package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;

public class StringHandler extends TypeHandler {

  public StringHandler() {
    super(String.class);
  }

  @Override
  public String getDefinition() {
    return type.getSimpleName();
  }

  @Override
  public String getPreset(Column col) {
    if (col.getDefault() == null) return "null";
    return '"' + col.getDefault() + '"';
  }

  @Override
  public boolean checkForPreset(Column col) {
    return col.isUseDatabaseDefaultValue() || col.isNoopMethod();
  }

  @Override
  public String getPrototyper() {
    return "\"\"";
  }
}
