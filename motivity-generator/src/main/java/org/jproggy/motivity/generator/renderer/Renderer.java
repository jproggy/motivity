package org.jproggy.motivity.generator.renderer;

import static org.jproggy.snippetory.toolyng.letter.CaseFormat.LOWER_CAMEL;
import static org.jproggy.snippetory.toolyng.letter.CaseFormat.UPPER_CAMEL;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.config.DbStore;
import org.jproggy.motivity.generator.config.NamingStrategy;
import org.jproggy.snippetory.Template;

public abstract class Renderer {
  protected final RenderContext ctx;
  protected final Configuration config;
  protected final NamingStrategy names;

  public Renderer(Configuration config) {
    ctx = new RenderContext(config);
    this.config = config;
    this.names = config.getNamingStrategy();
  }

  public abstract void execute();

  protected void writeTo(File path, Template tpl) {
    try {
      File parentFile = path.getParentFile();
      if (parentFile != null) {
        parentFile.mkdirs();
      }
      try (Writer out = new OutputStreamWriter(new FileOutputStream(path) , StandardCharsets.UTF_8)) {
        tpl.render(out);
      }
    } catch (IOException e) {
      throw new IllegalArgumentException("Can't access " + path.getAbsolutePath(), e);
    }
  }

  protected Template bindBasics(Template template, DbStore store) {
    template.set("identifier", names.getEntityName(store, LOWER_CAMEL));
    template.set("Identifier", names.getEntityName(store, UPPER_CAMEL));
    template.set("View", names.getEntityName(store, UPPER_CAMEL));
    return bindBasics(template);
  }

  protected Template bindBasics(Template template) {
    template.set("entities", names.getEntity().getOverwrite().getPackage());
    template.set("base_entities", names.getEntity().getBase().getPackage());
    template.set("peers", names.getPeer().getOverwrite().getPackage());
    template.set("base_peers", names.getPeer().getBase().getPackage());
    template.set("peer_impls", names.getPeerImpl().getOverwrite().getPackage());
    template.set("base_peer_impls", names.getPeerImpl().getBase().getPackage());
    template.set("mappers", names.getMapper().getOverwrite().getPackage());
    template.set("base_mappers", names.getMapper().getBase().getPackage());
    template.set("stores", names.getStore().getOverwrite().getPackage());
    template.set("base_stores", names.getStore().getBase().getPackage());
    template.set("Schema", names.getDbName(config, UPPER_CAMEL));
    template.set("generator", names.getGeneratorId());
    return template;
  }

  protected Template bindColumn(Template template, Column col) {
    bindBasics(template, col.getStore());
    template.set("fieldDb", col.getName());
    template.set("field", names.getPropertyName(col, LOWER_CAMEL));
    template.set("Field", names.getPropertyName(col, UPPER_CAMEL));
    template.set("FIELD", names.fieldConstant(col));
    template.set("preset", col.getType().getPreset(col));
    template.set("proto", col.getTypeDefinition().getObjectType().getPrototyper());
    template.set("type", col.getType().getDefinition());
    template.set("Type", col.getTypeDefinition().getObjectType().getDefinition());
    template.set("SimpleType", col.getTypeDefinition().getObjectType().getShortName());
    template.set("jdbcType", col.getJdbcType());
    template.set("schemaType", col.getTypeDefinition().name());
    template.set("javaType", col.getType().isPrimitive() ? "Primitive" : "Object");
    template.set("nullreplacement", col.getType().getNullReplacement());
    template.set("accessModifier", col.isProtected() ? "protected" : "public");
    return template;
  }
}
