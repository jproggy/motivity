package org.jproggy.motivity.generator.config;


import org.jproggy.snippetory.toolyng.letter.CaseFormat;

public interface Named {
  String getName();
  String getJavaName(CaseFormat charCase);
}
