package org.jproggy.motivity.generator.type;

import org.jproggy.motivity.generator.config.Column;

import java.time.LocalTime;

public class TimeHandler extends TypeHandler  {
  public TimeHandler() {
    super(LocalTime.class);
  }

  @Override
  public String getPreset(Column col) {
    if (col.isUseDatabaseDefaultValue()) {
      // if we provide default here db will not when saving
      // and time of saving might not be same as creation
      return "null";
    }
    String defaultValue = col.getDefault();
    if ("CURRENT_TIME".equalsIgnoreCase(defaultValue)) {
      return "java.time.LocalTime.now()";
    } else if (defaultValue != null) {
      return "java.time.LocalTime.parse(\"" + defaultValue + "\")";
    } else {
      return "null";
    }
  }

  @Override
  public boolean checkForPreset(Column col) {
    return col.isUseDatabaseDefaultValue();
  }

  @Override
  public String getPrototyper() {
    return "java.time.LocalTime.of(0, 0)";
  }
}
