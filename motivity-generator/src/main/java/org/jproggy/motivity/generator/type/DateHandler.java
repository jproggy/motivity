package org.jproggy.motivity.generator.type;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

import org.jproggy.motivity.generator.config.Column;
import org.jproggy.motivity.generator.config.ConfigFailure;

public class DateHandler extends TypeHandler {
  private static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.S";

  public DateHandler() {
    super(Date.class);
  }

  @Override
  public String getPreset(Column col) {
    if (col.isUseDatabaseDefaultValue()) {
      // if we provide default here db will not when saving
      // and time of saving might not be same as creation
      return "null";
    }
    String defaultValue = col.getDefault();
    if ("CURRENT_DATE".equalsIgnoreCase(defaultValue)) {
      return "org.jproggy.motivity.util.Dates.today()";
    } else if ("CURRENT_TIMESTAMP".equalsIgnoreCase(defaultValue)) {
      return "new Date()";
    } else if (defaultValue != null) {
      return "new Date(" + parse(defaultValue) + "L)";
    } else {
      return "null";
    }
  }

  public static long parse(String defaultValue) {
    try {
      SimpleDateFormat dateFormat = new SimpleDateFormat(DEFAULT_DATE_FORMAT);
      dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
      return dateFormat.parse(defaultValue).getTime();
    } catch (ParseException e) {
      throw new ConfigFailure("The default value " + defaultValue + " does not match the format "
          + DEFAULT_DATE_FORMAT + " for dates", e);
    }
  }

  @Override
  public boolean checkForPreset(Column col) {
    return col.isUseDatabaseDefaultValue() || col.isNoopMethod();
  }

  @Override
  public String getPrototyper() {
    return "new " +  getDefinition() + "()";
  }
}
