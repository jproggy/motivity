package $stores$;

import $entities$.$ReferredType$;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;

public class $ReferredType$Store extends Table<$ReferredType$> {

  public $ReferredType$Store() {
    super("", null);
  }

  public final ColumnDefinition<$ReferredType$, /*$Type(*/Integer/*)*/> $referenceField$ = null;

  @Override
  public $ReferredType$ create(ResultSet rs) throws SQLException {
    return null;
  }

  @Override
  public $ReferredType$Store as(String name) {
    return ($ReferredType$Store) super.as(name);
  }

  @Override
  protected Condition pk($ReferredType$ instance) {
    return null;
  }

  @Override
  public boolean delete($ReferredType$ instance) {
    return false;
  }

  @Override
  public boolean update($ReferredType$ instance) {
    return false;
  }

  @Override
  public boolean insert($ReferredType$ instance) {
    return false;
  }
}
