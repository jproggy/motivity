package $entities$;

import java.sql.Connection;

import org.jproggy.motivity.util.Persistent;

public abstract class $ReferringType$ implements Persistent {

  public void set$ReferringField$(int v) {
  }

  public void save() {
  }

  public void set$ReferredItem$($Identifier$ $Identifier$) {
  }

  public void save(Connection con) {
    
  }
}
