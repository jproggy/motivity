package $stores$;

import java.util.stream.Stream;

import org.jproggy.motivity.Store;

import javax.annotation.processing.Generated;

@Generated(value = "$generator", date = "$date(date='sql_sql')")
public class $Schema$ {
// $table-access{
  public static $Identifier$Store $identifier$Store = new $Identifier$Store();
//   $mock{
  public static $ReferredType$Store $referredType$Store = new $ReferredType$Store();
  public static $ReferringType$Store $referringType$Store = new $ReferringType$Store();
//   }$
//}$
  private static Store<?>[] stores = new Store<?>[] {
// $table-array(delimiter=",\n"){
      $identifier$Store// }$
  };
  
  public static Stream<Store<?>> stores() {
    return Stream.of(stores);
  }
}
