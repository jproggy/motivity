package $base_entities$;

import static $stores$.$Schema$.$identifier$Store;
import static org.jproggy.motivity.Motivity.and;

import $entities$.$Identifier$;
import $stores$.$Schema$;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import javax.annotation.processing.Generated;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.util.Persistent;

/**
 * You should not use this class directly.  It should not even be
 * extended; all references should be to $Identifier$
 */
@Generated(value = "$generator", date = "$date(date='sql_sql')")
public abstract class Base$Identifier$
    implements Serializable, Persistent {
  /**
   * Serial version
   */
  private static final long serialVersionUID = /*$date(date="yyyyMMddHHmmss'L'" */1386359556869L/*)*/;

// $fieldDef{
  /**
   * Defines the $field$ field.
   */
  private /*$type(*/ int/*)*/ $field$ = /*$preset(*/0/*)*/;

// }$
  /**
   * Whether this object was modified after loading or after last save.
   */
  private boolean modified = true;

  // $dummy{
  @SuppressWarnings("unused")
  private Date d = new Date();
// }$

  /**
   * Whether this object was loaded from the database or already saved
   * (false) or whether it is not yet in the database(true).
   */
  private boolean isNew = true;

  /**
   * Flag which indicates whether this object is currently saving.
   */
  private boolean saving = false;
// $referredDef{

  private $entities$.$ReferredType$ a$ReferredItem$ = null;
// }$
// $referringDef{

  protected List<$entities$.$ReferringType$> coll$ReferenceList$ = null;
// }$

  // $fieldAccessors{
  /*$accessModifier(*/
  public/*)*/ /*$type(*/int/*)*/ get$Field$() {
    return $field$;
  }

  /*$accessModifier(*/
  public/*)*/ void set$Field$(/*$type(*/int/*)*/ v) {
/* $objectCompare{
   if (!Objects.equals(this.$field$, v)) {
// }$ */
// $primitiveCompare{
    if (this.$field$ != v) {
// }$
      modified();
    }

    this.$field$ = v;
/// for pk columuns
// $maintainReferring{

    // update associated objects in coll$ReferenceList$
    if (coll$ReferenceList$ != null) {
      coll$ReferenceList$.forEach(x -> x.set$ReferringField$(v));
    }
// }$
// $maintainReferred{
/*   $objectCompare{
   if (a$ReferredItem$ != null && !Objects.equals(a$ReferredItem$.get$ReferredPk$(), v)) {
//   }$ */
//   $primitiveCompare{
    if (a$ReferredItem$ != null && a$ReferredItem$.get$ReferredPk$() != v) {
//   }$
      a$ReferredItem$ = null;
    }
// }$
  }

// }$

  /**
   * Returns whether the object has ever been saved. This will be false, if the object was retrieved from storage
   * or was created and then saved.
   *
   * @return true, if the object has never been persisted.
   */
  @Override
  public boolean isNew() {
    return isNew;
  }

  /**
   * Returns whether the object has been modified.
   *
   * @return True if the object has been modified.
   */
  @Override
  public boolean isModified() {
    return modified;
  }

  @Override
  public void inSync() {
    isNew = false;
    modified = false;
  }

  public void modified() {
    modified = true;
  }


// $accessReferred{

  /**
   * Returns the associated $ReferredType$ object. If it was not retrieved before, the object is retrieved from
   * the database
   *
   * @return the associated $ReferredType$ object
   */
  public $entities$.$ReferredType$ get$ReferredItem$() {
    if (a$ReferredItem$ == null/*$check{*/ && this.$field$ != /*$nullreplacement(*/0/*)*//*}$*/) {
      $stores$.$ReferredType$Store store = $Schema$.$referredType$Store;
      Condition data = and(//   $filter(delimiter=","){
              store.$referenceField$.equalTo(get$Field$())//   }$
      );
      a$ReferredItem$ = store.filter(data).first().orElse(null);
    }
    return a$ReferredItem$;
  }

  /**
   * Declares an association between this object and a $ReferredType$ object
   *
   * @param v $ReferredType$
   */
  public void set$ReferredItem$($entities$.$ReferredType$ v) {
//   $fields{
    if (v == null/*$nullCheck{ || v.get$ReferredPk$() == null}$*/) {
      set$Field$(/*$nullreplacement(*/0/*)*/);
    } else {
      set$Field$(v.get$ReferredPk$());
    }
//   }$
    a$ReferredItem$ = v;
  }

  // }accessReferred$
  // $accessReferring{

  /**
   * If this collection has already been initialized, it returns the collection. Otherwise if this Base$Identifier
   * has previously been saved, it will retrieve the related $ReferringType$ Objects from storage.
   * If this Base$Identifier is new, it will return an empty collection or the current collection.
   */
  public List<$entities$.$ReferringType$> get$ReferenceList$() {
    if (coll$ReferenceList$ == null) {
      if (isNew()) {
        coll$ReferenceList$ = new ArrayList<>();
      } else {
        $stores$.$ReferringType$Store store = $Schema$.$referringType$Store;
        Condition data = and(// $filter(delimiter=","){
                store.$referringField$.equalTo(get$Field$())// }$
        );
        coll$ReferenceList$ = store.filter(data).list();
        coll$ReferenceList$.forEach(it -> it.set$ReferredItem$(($Identifier$) this));
      }
    }

    return coll$ReferenceList$;
  }

  /**
   * Method called to associate a $ReferringType$ object to this object
   * through the coll$ReferenceList$ foreign key attribute.
   * If the associated objects were not retrieved before
   * and this object is not new, the associated objects are retrieved
   * from the database before adding the <code>toAdd</code> object.
   *
   * @param toAdd the object to add to the associated objects, not null.
   * @throws NullPointerException if toAdd is null.
   */
  public void $addToReferenceList$($entities$.$ReferringType$ toAdd) {
    toAdd.set$ReferredItem$(($Identifier$) this);
    get$ReferenceList$().add(toAdd);
  }

// }accessReferring$

  /**
   * Stores an object in the database.
   */
  public void save() {
    if (saving) {
      return;
    }
    try {
      saving = true;
      // If this object has been modified, then save it to the database.
      if (isModified()) {
        if (isNew) {
          $identifier$Store.insert(($Identifier$) this);
        } else {
          $identifier$Store.update(($Identifier$) this);
        }
      }

// $saveReferring{
      if (coll$ReferenceList$ != null) {
        for ($entities$.$ReferringType$ $referringType$ : coll$ReferenceList$) {
          $referringType$.save();
        }
      }
// }$
    } finally {
      saving = false;
    }
  }

  @Override
  public String toString() {
    StringBuilder str = new StringBuilder();
    str.append("$Identifier$:");
// $toString{
    str.append("\n$field$ = ").append(get$Field$());
    // $binary{
    str.append("\n$field$ = <binary>");
    // }$
// }$
    return (str.toString());
  }

  @Override
  public boolean equals(Object toCompare) {
    if (toCompare == null) {
      return false;
    }
    if (this == toCompare) {
      return true;
    }
    if (!getClass().equals(toCompare.getClass())) {
      return false;
    }
// $(default='    return super.equals(toCompare);'){
//   $fromKey{
    if (!Objects.equals($field$, ((Base$Identifier$) toCompare).$field$)) {
      return false;
    }
//   }$
    return true;
// }$
  }

  @Override
  public int hashCode() {
// $(default='    return super.hashCode();'){
    return Objects.hash(
//   $toKey(delimiter=','){
            $field$// }$
    );
// }$
  }
}
