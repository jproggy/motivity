package $stores$;

import $entities$.$ReferringType$;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;

public class $ReferringType$Store extends Table<$ReferringType$> {

  public $ReferringType$Store() {
    super("", null);
  }

  public ColumnDefinition<$ReferringType$, Integer> $referringField$;

  @Override
  public $ReferringType$ create(ResultSet rs) throws SQLException {
    return null;
  }

  @Override
  protected Condition pk($ReferringType$ instance) {
    return null;
  }

  @Override
  public $ReferringType$Store as(String name) {
    return ($ReferringType$Store)super.as(name);
  }

  @Override
  public boolean delete($ReferringType$ instance) {
    return false;
  }

  @Override
  public boolean update($ReferringType$ instance) {
    return false;
  }

  @Override
  public boolean insert($ReferringType$ instance) {
    return false;
  }
}
