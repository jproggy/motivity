package $base_stores$;

import static org.jproggy.motivity.Motivity.and;

import $entities$.$Identifier$;
import $stores$.$Identifier$Store;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.id.IdData;
import org.jproggy.motivity.sql.id.IdProvider;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.id.IdGenerator.Strategy;
import org.jproggy.motivity.sql.map.Converters;

import javax.annotation.processing.Generated;

/**
 * You should not use this class directly.  It should not even be
 * extended; all references should be to $Identifier$Store
 */
@Generated(value = "$generator", date = "$date(date='sql_sql')")
public class Base$Identifier$Store extends Table<$Identifier$> {
  /** The default database name for this class. */
  public static final String DATABASE_NAME = "$dbName$";
  /** The table name for this class. */
  public static final String TABLE_NAME = "$identifierDb$";
// ${
  private final IdData idData = new IdData("$idData", Strategy.SEQUENCE);// $idMethod(backward="SEQUENCE")
// }$
// $columnsDef{

  /** The column for the $fieldDb$ field */
  public final ColumnDefinition<$Identifier$, /*$Type(*/Integer/*)*/> $field$;
// }$

  protected Base$Identifier$Store() {
    super(TABLE_NAME, DbConnector.getInstance(DATABASE_NAME));
// $columnsInit{
    $field$ = column("$fieldDb$", $Identifier$::set$Field$, $Identifier$::get$Field$, Converters.INTEGER);// $schemaType(backward='INTEGER')
// }$
  }

  @Override
  public $Identifier$ create(ResultSet rs) throws SQLException {
    return new $Identifier$();
  }

  @Override
  public $Identifier$Store as(String alias) {
    return ($Identifier$Store)super.as(alias);
  }

  // $refTargets{
  public $stores$.$ReferredType$Store $referredItem$() {
    $stores$.$ReferredType$Store tbl = new $stores$.$ReferredType$Store();

    Condition cond = and(//   $cond(delimiter=","){
        tbl.$referenceField$.equalTo($field$)//   }$
    );
    return join(tbl, cond);
  }

  public $stores$.$ReferredType$Store $referredItem$(String alias) {
    $stores$.$ReferredType$Store tbl = new $stores$.$ReferredType$Store().as(alias);

    Condition cond = and(//   $condAs(delimiter=","){
        tbl.$referenceField$.equalTo($field$)//   }$
    );
    return join(tbl, cond);
  }

  // }$
// $refSources{
  public $stores$.$ReferringType$Store $referringItem$() {
    $stores$.$ReferringType$Store tbl = new $stores$.$ReferringType$Store();

    Condition cond = and(//  $cond(delimiter=","){
        tbl.$referringField$.equalTo($field$)//  }$
    );

    return join(tbl, cond);
  }

  public $stores$.$ReferringType$Store $referringItem$(String alias) {
    $stores$.$ReferringType$Store tbl = new $stores$.$ReferringType$Store().as(alias);

    Condition cond = and(//  $condAs(delimiter=","){
        tbl.$referringField$.equalTo($field$)//  }$
    );

    return join(tbl, cond);
  }

  // }$
// $hasKey{
  @Override
  public boolean update($Identifier$ item) {
    long rows = filter(pk(item)).update(
// $update-column(delimiter=',\n'){
        $field$.to(item.get$Field$())// }$
    );
    item.inSync();
    return rows == 1;
  }

  @Override
  protected Condition pk($Identifier$ instance) {
    return and(// $pk-cond(delimiter=","){
            $field$.equalTo(instance.get$Field$())// }$
    );
  }

// }$
// $canAdd{
  @Override
  public boolean insert($Identifier$ instance) {
    var idGen = /*$idGen{*/instance.get$Field$() == /*$preset(*/0/*)*/ ?
        getDbConnector().getIdGenerator(idData, $field$) : /*}$*/IdProvider.noop();
    return insert(instance, idGen);
  }

  protected List<ColumnValue<?>> getColumnValues($Identifier$ instance) {
    List<ColumnValue<?>> values = new ArrayList<>();
// $insert-column{
    /*$noopCheck{*/if (instance.get$Field$() != 0) /* $preset(backward="0")}$*/values.add($field$.to(instance.get$Field$()));
// }insert-column$
    return values;
  }

// }canAdd$
/* $noKey{

	@Override
	protected Condition pk($Identifier$ instance) {
		throw new UnsupportedOperationException("This table has no primary key");
	}

	@Override
	public boolean update($Identifier$ instance) {
		throw new UnsupportedOperationException("This table has no primary key");
	}
// }$
// $noAdd{

	@Override
	public boolean insert($Identifier$ instance) {
		throw new UnsupportedOperationException("This table has no primary key");
	}
// }$ */
}
