-- Syntax:sql
-- -----------------------------------------------------------------------
-- $db SQL script for schema moretest
-- -----------------------------------------------------------------------

ALTER TABLE other
    DROP CONSTRAINT other_FK_1;


ALTER TABLE other
    DROP CONSTRAINT other_FK_2;

-- $dropTbl{
DROP TABLE if exists test;
DROP TABLE if exists other;
DROP SEQUENCE other_seq;
-- $postgresql{
DROP TABLE if exists test CASCADE;
DROP TABLE if exists other CASCADE;
DROP SEQUENCE other_seq;
-- }$
-- $mysql{
DROP TABLE if exists test;
DROP TABLE if exists other;
-- }$
-- }$


-- -----------------------------------------------------------------------
-- test
-- -----------------------------------------------------------------------
CREATE TABLE test
(
    test_id INTEGER NOT NULL GENERATED BY DEFAULT AS IDENTITY ,
    text VARCHAR(2048),
    price DECIMAL(10,2) default 0 NOT NULL,
    bool SMALLINT default 0,
    PRIMARY KEY(test_id)
);

-- -----------------------------------------------------------------------
-- other
-- -----------------------------------------------------------------------
CREATE TABLE other
(
    other_id DATE default '2020-02-20' NOT NULL,
    data SMALLINT default 1,
    test_id INTEGER,
    text VARCHAR(2048),
    cost DECIMAL(10,2) default 0 NOT NULL,
    PRIMARY KEY(other_id)
);
-- $seq{
CREATE SEQUENCE other_seq INCREMENT BY 1 START WITH 1 NO MAXVALUE NO CYCLE;
-- $mysql{
-- }$
-- }$

ALTER TABLE other
    ADD CONSTRAINT other_FK_1
    FOREIGN KEY (test_id)
    REFERENCES test (test_id);

ALTER TABLE other
    ADD CONSTRAINT other_FK_2
    FOREIGN KEY (text, cost)
    REFERENCES test (text, price);
