package org.jproggy.motivity.torque;

import org.jproggy.motivity.sql.connect.DbConnector;

/**
 * Dummy version until having own connection handling
 */
public class TorqueDbConnector {
    public static synchronized DbConnector getInstance(String name) {
        return null;
    }
}
