package org.jproggy.motivity.generator.config;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.net.URL;
import java.util.Collection;
import java.util.Properties;
import org.junit.Test;

public class ConfigurationTest {
  @Test
  public void test() throws Exception {
    Configuration config = new ConfigParser(properties()).load(this.getClass().getResource("/shop-schema.xml"));
    Collection<Table> tables = config.getTables();
    assertEquals(8, tables.size());
  }
  @Test
  public void test2() throws Exception {
    Configuration config = new ConfigParser(properties()).load(this.getClass().getResource("/onetable.xml"));
    Collection<Table> tables = config.getTables();
    assertEquals(1, tables.size());
    DbStore table = config.getTable("test");
    assertEquals(null, table.getBaseClass());
    assertEquals(4, table.getColumns().size());
  }

  // @Test
  public void testUrl() throws Exception {
    URL base = new File("c:data/").toURI().toURL();
    assertEquals("file:/c://data/", base.toString());
    assertEquals("file:/c://data/test", new URL(base, "test").toString());
    assertEquals("file:/c://data/test/other", new URL(base, "test\\other").toString());
    assertEquals("file:/test", new URL(base, "/test").toString());
    assertEquals("file:/j:/test", new URL(base, "/j:/test").toString());
    assertEquals("http://jproggy.org", new URL(base, "http://jproggy.org").toString());
    base = new File("c:\\data").toURI().toURL();
    assertEquals("file:/c:/data/test", new URL(base, "test").toString());
    base = new File("c:\\base").toURI().toURL();
    assertEquals("file:/c:/test", new URL(base, "test").toString());
    base = new File("c:\\data\\base").toURI().toURL();
    assertEquals("file:/c:/data/test", new URL(base, "test").toString());
    base = new File("c:\\base\\data").toURI().toURL();
    assertEquals("file:/c:/base/test", new URL(base, "test").toString());
  }

  Properties properties() {
    Properties properties = new Properties();
    properties.put("torque.useClasspath", "true");
    properties.put("database.variant", "h2");
    properties.put("configPackage", "org/jproggy/motivity/templates/store");
    return properties;
  }
}
