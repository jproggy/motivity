package org.jproggy.motivity.generator.renderer;

import java.util.List;

import org.jproggy.motivity.generator.TemplateSet;
import org.jproggy.motivity.generator.config.Configuration;

public class Renderers {
    private final List<Renderer> entries;

    private Renderers(List<Renderer> entries) {
        this.entries = entries;
    }

    public static Renderers of(Configuration configuration) {
        return new Renderers(TemplateSet.of(configuration).renderers());
    }

    public  <T> T byType(Class<T> type) {
        for (Renderer r : entries) {
            if (type.isInstance(r)) return type.cast(r);
        }
        throw new IllegalArgumentException("no renderer of type" + type.getName() + " found in " + entries);
    }
}
