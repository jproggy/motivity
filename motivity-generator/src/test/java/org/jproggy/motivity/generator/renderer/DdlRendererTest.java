package org.jproggy.motivity.generator.renderer;


import java.util.Properties;
import org.junit.Test;

import org.jproggy.motivity.generator.config.ConfigParser;
import org.jproggy.motivity.generator.config.Configuration;
import org.jproggy.motivity.generator.util.VariantResolver;
import org.jproggy.snippetory.Syntaxes;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.TemplateContext;
import org.jproggy.snippetory.UriResolver;
import org.jproggy.snippetory.toolyng.test.Normalize;

public class DdlRendererTest {
    private static final TemplateContext REPO = Syntaxes.FLUYT.context().uriResolver(repo());
    @Test
    public void derby() throws Exception {
        DdlRenderer renderer = Renderers.of(config("derby")).byType(DdlRenderer.class);
        Template template = renderer.getDdl();
        Normalize.assertNormEquals(getDdl("derby"), template.toString());
    }

    @Test
    public void postgres() throws Exception {
        DdlRenderer renderer = Renderers.of(config("postgresql")).byType(DdlRenderer.class);
        Template template = renderer.getDdl();
        Normalize.assertNormEquals(getDdl("postgresql"), template.toString());
    }

    private Configuration config(String db) throws Exception {
        Properties props = new Properties();
        props.put("templates.path", "org/jproggy/motivity/templates/ddl");
        props.put("database.variant", db);
        return new ConfigParser(props).load(this.getClass().getResource("/multitest.xml"));
    }

    private static UriResolver repo() {
        return UriResolver.resource("org/jproggy/motivity/renderer");
    }

    private String getDdl(String db) {
        VariantResolver tpl = new VariantResolver(REPO.getTemplate("multi/ddl.sql"), db);
        tpl.regionNames().forEach(n -> tpl.get(n).render());
        return tpl.set("db", db).toString();
    }
}