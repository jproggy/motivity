package org.jproggy.motivity.sql.id;

import static org.jproggy.motivity.Motivity.and;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.logging.Logger;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.InsertStatement;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.map.Converter;
import org.jproggy.motivity.sql.map.Converters;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.util.Persistent;

public class TableGenerator implements IdGenerator {
    public static final String TABLE_NAME = "ID_TABLE";
    /**
     * The cached IDs for each table.
     *
     * Key: String table name.
     * Value: List of Integer IDs.
     */
    private final Map<String, AtomicLong> ids = new ConcurrentHashMap<>();

    /**
     * The cached IDs for each table.
     *
     * Key: String table name.
     * Value: List of Integer IDs.
     */
    private final Map<String, Long> maxIds = new ConcurrentHashMap<>();

    private final IDTable table;

    /** the log */
    private final Logger log = Logger.getLogger(TableGenerator.class.getName());

    /**
     * Constructs an IdBroker for the given Database.
     *
     * @param db the database connector where this IdBroker is running in.
     */
    public TableGenerator(DbConnector db) {
        this.table = new IDTable(db);
    }


    @Override
    public boolean generates() {
        return true;
    }

    /**
     * This method returns x number of ids for the given table.
     *
     * @param tableName The name of the table for which we want an id.
     * @return A long.
     */
    @Override
    public synchronized <T> T getNextId(Converter<T> conv, String tableName) {
        if (tableName == null) {
            throw new MotivityException("getNextIds(): tableName == null");
        }

        AtomicLong nextId = ids.get(tableName);

        if (nextId == null) {
            try (Transaction tra = Transactions.openNew()) {
                Optional<Sequence> sequence = table.filter(
                        table.tableName.equalTo(tableName)
                ).first();
                if (sequence.isEmpty()) {
                    log.finest(() -> "Creating new entry for " + tableName);
                    table.insert(new Sequence(tableName, getQuantity()));
                    tra.commit();
                    ids.put(tableName, new AtomicLong(1L));
                    maxIds.put(tableName, getQuantity());
                    return convert(conv, ids.get(tableName).getAndIncrement());
                }
                log.finest(() -> "Loading entry for " + tableName);
                nextId = new AtomicLong(sequence.get().getNextId());
                ids.put(tableName, nextId);
                long maxId = nextId.get() + getQuantity();
                maxIds.put(tableName, maxId);
                log.finest(() -> "Reserve new block for " + tableName + " -> " + maxIds);
                table.update(new Sequence(tableName, maxId));
                tra.commit();
                return convert(conv, nextId.getAndIncrement());
            }
        }
        long id = nextId.getAndIncrement();
        if (id > maxIds.get(tableName)) {
            long maxId = maxIds.get(tableName) + getQuantity();
            log.finest(() -> "Reserve new block for {}" + tableName + " -> " + maxId);
            maxIds.put(tableName, maxId);
            try (Transaction tra = Transactions.openNew()) {
                table.update(new Sequence(tableName, maxId));
                tra.commit();
            }
        }
        return convert(conv, id);
    }

    private static <T> T convert(Converter<T> conv, long value) {
        if (conv == Converters.VARCHAR) {
            return (T) Long.toString(value);
        }
        if (conv == Converters.INTEGER) {
            return (T) Integer.valueOf((int) value);
        }
        if (conv == Converters.BIGINT) {
            return (T) Long.valueOf(value);
        }
        throw new MotivityException("Unsupported converter: " + conv);
    }

    /**
     *
     * @return An int with the number of ids cached in memory.
     */
    private long getQuantity() {
        return Short.MAX_VALUE;
    }

    private static class Sequence implements Persistent {
        private boolean modified = true;
        private boolean isNew = true;
        private String tableName;
        private Long nextId;

        public Sequence() { }

        public Sequence(String tableName, Long nextId) {
            this.tableName = tableName;
            this.nextId = nextId;
        }

        @Override
        public void inSync() {
            isNew = false;
            modified = false;
        }

        @Override
        public boolean isNew() {
            return isNew;
        }

        @Override
        public boolean isModified() {
            return modified;
        }

        public String getTableName() {
            return tableName;
        }

        public void setTableName(String tableName) {
            this.tableName = tableName;
        }

        public Long getNextId() {
            return nextId;
        }

        public void setNextId(Long nextId) {
            this.nextId = nextId;
        }
    }

    private static class IDTable extends Table<Sequence> {
        final ColumnDefinition<Sequence, String> tableName;
        final ColumnDefinition<Sequence, Long> nextId;
        public IDTable(DbConnector db) {
            super(TABLE_NAME, db);
            tableName = column("TABLE_NAME", Sequence::setTableName, Sequence::getTableName,
                    Converters.VARCHAR);
            nextId = column("NEXT_ID", Sequence::setNextId, Sequence::getNextId,
                    Converters.BIGINT);
        }

        @Override
        public boolean update(Sequence instance) {
            return  filter(pk(instance)).update(
                    tableName.to(tableName.getValue(instance)),
                    nextId.to(instance.getNextId())
            ) == 1;
        }

        @Override
        protected Condition pk(Sequence instance) {
            return and(
                    tableName.equalTo(instance.getTableName())
            );
        }

        @Override
        public boolean insert(Sequence instance) {
            try (Connection con = getDbConnector().getConnection()) {
                List<ColumnValue<?>> values = new ArrayList<>();
                values.add(tableName.to(instance.getTableName()));
                values.add(nextId.to(instance.getNextId()));
                try (PreparedStatement stmt = new InsertStatement(this, values).toStatement().getStatement(con)) {
                    long rows = stmt.executeUpdate();
                    instance.inSync();
                    return rows == 1;
                }
            } catch (SQLException e) {
                throw new MotivityException(e);
            }
        }

        @Override
        public Sequence create(ResultSet rs) throws SQLException {
            return new Sequence();
        }
    }
 }
