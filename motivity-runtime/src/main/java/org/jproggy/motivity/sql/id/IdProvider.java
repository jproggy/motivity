package org.jproggy.motivity.sql.id;

import java.sql.PreparedStatement;

import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.util.Internal;
import org.jproggy.motivity.util.Persistent;

public class IdProvider<R extends Persistent, T> {
  private final IdGenerator keyGen;
  private final IdData data;
  private final ColumnDefinition<R, T> col;

  public static <R extends Persistent, T> IdProvider noop() {
    return new IdProvider<>(null, null, null);
  }

  public IdProvider(IdGenerator gen, IdData data, ColumnDefinition<R, T> column) {
    this.keyGen = gen;
    this.data = data;
    this.col = column;
  }

  private boolean generatesBefore() {
    if (keyGen == null) return false;
    return keyGen.generates();
  }

  private boolean fetchesAfterwards() {
    if (keyGen == null) return false;
    return keyGen.fetches();
  }

  public void afterwards(R instance, PreparedStatement accessor) {
    if (fetchesAfterwards()) {
      col.setValue(instance, keyGen.fetch(col.getConverter(), accessor));
    }
  }

  public void before(R instance) {
    if (generatesBefore()) {
      col.setValue(instance, keyGen.getNextId(col.getConverter(), data.getTable()));
    }
  }


  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, e -> e.data, e -> e.keyGen);
  }

  @Override
  public int hashCode() {
    return keyGen.hashCode();
  }

  @Override
  public String toString() {
    return "IdProvider [generator=" + keyGen + ", data=" + data + "]";
  }
}
