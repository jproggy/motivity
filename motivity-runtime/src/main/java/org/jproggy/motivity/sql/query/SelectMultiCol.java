package org.jproggy.motivity.sql.query;

import static java.util.Collections.singletonList;

import org.jproggy.motivity.query.Expression;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.map.ConverterResolver;
import org.jproggy.motivity.util.Internal;
import org.jproggy.motivity.util.Property;
import org.jproggy.snippetory.sql.spi.RowTransformer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SelectMultiCol extends SelectImpl<Object[]> {
  private final List<Expression<?>> selectColumns;

  @SuppressWarnings({ "rawtypes" })
  public SelectMultiCol(Table from, Expression<?>... columns) {
    super(from);
    selectColumns = Arrays.asList(columns);
  }

  @Override
  protected List<Expression<?>> selectColumns() {
    return selectColumns;
  }

  @Override
  protected RowTransformer<Object[]> getTransformer() {
    ConverterResolver conv = new ConverterResolver();
    return new RowTransformer<Object[]>() {
      @Override
      public Object[] transformRow(ResultSet rs) throws SQLException {
        Object[] result = new Object[selectColumns.size()];
        for (int i = 0; i < selectColumns.size(); i++) {
          result[i] = conv.of(selectColumns.get(i)).readValue(rs, i + 1);
        }
        return result;
      }
    };
  }

  @Override
  public List<Property<Object[], ?>> properties() {
    List<Property<Object[], ?>> result = new ArrayList<>();
    for (int i = 0; i < selectColumns.size(); i++) {
      result.add(new ArrayAccessor(i, selectColumns.get(i).toString()));
    }
    return result;
  }


  @Override
  public boolean equals(Object obj) {
    return Internal.equalsWithSuper(this, obj, super::equals, x -> x.selectColumns);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }

  private static class ArrayAccessor implements Property<Object[], Object> {
    private final int index;
    private final String name;

    private ArrayAccessor(int index, String name) {
      this.index = index;
      this.name = name;
    }

    @Override
      public Object getValue(Object[] item) {
        return item[index];
      }

      @Override
      public String getName() {
        return name;
      }

      @Override
      public void setValue(Object[] item, Object value) {
        throw new UnsupportedOperationException();
      }
    }
}
