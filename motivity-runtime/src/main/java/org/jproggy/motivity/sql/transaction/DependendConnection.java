package org.jproggy.motivity.sql.transaction;

import java.sql.Connection;
import java.sql.SQLException;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.transaction.ConnectionWrapper;

public class DependendConnection extends ConnectionWrapper {
  private final SqlTransactionImpl tra;
  private boolean autoCommit;
  private boolean closed = false;
  private final int depth;

  public DependendConnection(SqlTransactionImpl tra, Connection nativeCon, int depth) {
    super(nativeCon);
    this.tra = tra;
    try {
      autoCommit = wrappedCon.getAutoCommit();
      wrappedCon.setAutoCommit(false);
    } catch (SQLException e) {
      throw new MotivityException(e);
    }
    this.depth = depth;
  }

  @Override
  public void setAutoCommit(boolean autoCommit) throws SQLException {
    verify();
    this.autoCommit = autoCommit;
  }

  void verify() throws SQLException {
    if (closed) throw new SQLException();
  }

  @Override
  public boolean getAutoCommit() throws SQLException {
    verify();
    return autoCommit;
  }

  @Override
  public void commit() throws SQLException {
    // ignore
  }

  @Override
  public void rollback() throws SQLException {
    verify();
    tra.setRollbackOnly();
  }

  @Override
  public void close() throws SQLException {
    if (!closed) {
      closed = true;
    }
    int actual = tra.connectionClosed(this);
    if (actual != depth) {
      throw new IllegalStateException((actual - depth) + " connections still open");
    }
  }

  @Override
  public boolean isClosed() throws SQLException {
    return closed;
  }

  @Override
  public void setReadOnly(boolean readOnly) throws SQLException {
    if (readOnly != wrappedCon.isReadOnly()) throw new UnsupportedOperationException();
  }

  @Override
  public boolean isReadOnly() throws SQLException {
    verify();
    return wrappedCon.isReadOnly();
  }

  @Override
  public void setTransactionIsolation(int level) throws SQLException {
    if (level != wrappedCon.getTransactionIsolation()) throw new UnsupportedOperationException();
  }

  @Override
  public int getTransactionIsolation() throws SQLException {
    verify();
    return wrappedCon.getTransactionIsolation();
  }

  public Connection unwrap() {
    return wrappedCon;
  }
}
