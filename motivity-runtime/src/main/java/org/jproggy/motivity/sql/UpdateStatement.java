package org.jproggy.motivity.sql;

import java.util.Arrays;
import java.util.List;

import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.sql.Vocabulary.UpdateTpl;
import org.jproggy.motivity.sql.query.SqlRenderer;
import org.jproggy.motivity.util.Indent;
import org.jproggy.motivity.util.Internal;
import org.jproggy.snippetory.util.TemplateWrapper;
import org.jproggy.snippetory.sql.SQL;
import org.jproggy.snippetory.sql.Statement;

public class UpdateStatement {
  private final Table<?> to;
  private Condition where = new And();
  private final List<ColumnValue<?>> values;

  public UpdateStatement(Table<?> to, ColumnValue<?>... values) {
    this.to = to;
    this.values = Arrays.asList(values);
  }

  public UpdateStatement where(Condition where) {
    this.where = this.where.and(where);
    return this;
  }

  public Statement toStatement() {
    Vocabulary voc = to.getDbConnector().getVocabulary();
    return (Statement)render(voc, new Indent()).getImplementation();
  }

  private TemplateWrapper render(Vocabulary vocabulary, Indent indent) {
    indent = indent.indent();
    SqlRenderer r = new SqlRenderer(vocabulary);
    UpdateTpl update = vocabulary.getUpdate().setPrimaryTable(to.render(vocabulary, indent));
    update.setWhereExpression(r.render(where, indent));
    for (ColumnValue<?> val : values) {
      update.getColumns()
              .setColumn(SQL.markAsSql(val.getColumn().getName()))
              .setValue(r.render(val.getValue(), indent))
              .render();
    }
    return update;
  }
  @Override
  public int hashCode() {
    return to.hashCode();
  }
  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> x.to, x -> x.values, x -> x.where);
  }

}
