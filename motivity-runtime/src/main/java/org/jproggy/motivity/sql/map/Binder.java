package org.jproggy.motivity.sql.map;

@FunctionalInterface
public interface Binder<R, T> {

  void bindValue(R item, T value);

}
