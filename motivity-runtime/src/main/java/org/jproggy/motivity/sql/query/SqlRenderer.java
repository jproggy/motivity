package org.jproggy.motivity.sql.query;

import org.jproggy.motivity.Store.Exists;
import org.jproggy.motivity.query.Column.SortColumn;
import org.jproggy.motivity.query.ColumnAlias;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.query.Condition.Not;
import org.jproggy.motivity.query.Condition.Or;
import org.jproggy.motivity.query.impl.Function;
import org.jproggy.motivity.query.impl.Literal;
import org.jproggy.motivity.query.impl.Op;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.query.impl.TermVisitor;
import org.jproggy.motivity.query.impl.Tuple;
import org.jproggy.motivity.sql.Vocabulary;
import org.jproggy.motivity.sql.Vocabulary.AndTpl;
import org.jproggy.motivity.sql.Vocabulary.ConcatTpl;
import org.jproggy.motivity.sql.Vocabulary.OrTpl;
import org.jproggy.motivity.sql.Vocabulary.TupleTpl;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.map.Converter;
import org.jproggy.motivity.sql.map.ConverterResolver;
import org.jproggy.motivity.sql.query.JoinImpl.Ref;
import org.jproggy.motivity.util.Indent;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.sql.SQL;

public class SqlRenderer implements TermVisitor<Object, Indent> {
    private final Vocabulary vocabulary;
    private final ConverterResolver converter = new ConverterResolver();

    public SqlRenderer(Vocabulary vocabulary) {
        this.vocabulary = vocabulary;
    }

    public Template renderCondition(Condition filter, Indent param) {
        return (Template) render(filter, param);
    }

    public Object render(Term visitee, Indent i) {
        if (visitee instanceof Ref) return visit((Ref) visitee, i);
        if (visitee instanceof Table) return visit((Table) visitee, i);
        if (visitee instanceof Query) return visit((Query<?>) visitee, i);
        if (visitee instanceof ColumnDefinition) return visit((ColumnDefinition<?, ?>) visitee, i);
        return TermVisitor.dispatch(this, visitee, i);
    }

    public Object visit(Table<?> visitee, Indent param) {
        return visitee.render(vocabulary, param);
    }

    public Object visit(Query<?> visitee, Indent param) {
        return visitee.render(vocabulary, param);
    }

    public Object visit(ColumnDefinition<?, ?> visitee, Indent param) {
        return SQL.markAsSql(visitee.getReference());
    }

    @Override
    public Object visit(And visitee, Indent param) {
        param = param.indent();
        if (visitee.terms.length == 0) return null;
        Object start = render(visitee.terms[0], param);
        if (visitee.terms.length == 1) return start;
        AndTpl result = vocabulary.getAnd().setI(param).setStart(start);
        for (int i = 1; i < visitee.terms.length; i++) {
            result.getMore().setI(param).setX(render(visitee.terms[i], param)).render();
        }
        return result;
    }

    @Override
    public Object visit(Or visitee, Indent param) {
        if (visitee.terms.length == 0) return null;
        Object start = render(visitee.terms[0], param);
        OrTpl result = vocabulary.getOr().setI(param).setStart(start);
        if (visitee.terms.length == 1) return start;
        for (int i = 1; i < visitee.terms.length; i++) {
            result.getMore().setI(param).setX(render(visitee.terms[i], param)).render();
        }
        return result;
    }

    @Override
    public Template visit(Not visitee, Indent i) {
        return vocabulary.getNot().setX(render(visitee.terms[0], i));
    }

    @Override
    public Template visit(Op.Operation visitee, Indent i) {
        Op op = visitee.getOperator();
        Object origin = render(visitee.getOrigin(), i);
        if (op == Op.IsNull) return vocabulary.getIsNull().setOrigin(origin);
        if (op == Op.IsNotNull) return vocabulary.getIsNotNull().setOrigin(origin);
        if (op == Op.Between) return vocabulary.getBetween()
                .setOrigin(origin)
                .setFrom(render(visitee.operand1(), i))
                .setTo(render(visitee.operand2(), i));
        return vocabulary.get(op.name()).set("origin", origin).set("data", render(visitee.operand1(), i));
    }

    @Override
    public Object visit(Exists visitee, Indent param) {
        return vocabulary.getExists().setI(param)
                .setTable(render(visitee.store(), param))
                .setFilter(render(visitee.filter(), param));
    }

    @Override
    public Object visit(ColumnAlias<?> visitee, Indent i) {
        vocabulary.getAs().setOrigin(render(visitee.getDefinition(), i)).setAlias(SQL.markAsSql(visitee.getName()));
        return SQL.markAsSql(visitee.getName());
    }

    @Override
    public Template visit(Function.Call<?> visitee, Indent i) {
        if (visitee.function == Function.Concat) {
            ConcatTpl concatTpl = vocabulary.getConcat().setOrigin(render(visitee.origin, i));
            for (Term t : visitee.params) {
                concatTpl.appendData(render(t, i));
            }
            return concatTpl;
        }
        if (visitee.function == Function.IfNull) {
            return vocabulary.getIfNull()
                    .setOrigin(render(visitee.origin, i))
                    .setReplacement(render(visitee.params[0], i));
        }
        return vocabulary.get(visitee.function.name()).set("column", render(visitee.origin, i));
    }

    public Object visit(Ref v, Indent i) {
        if (v.isLeft) {
            return vocabulary.getSelect().getLeftJoin().setAdditionalTable(render(v.target, i))
                    .setJoinCondition(render(v.condition, i));
        } else {
            return vocabulary.getSelect().getJoin().setAdditionalTable(render(v.target, i))
                    .setJoinCondition(render(v.condition, i));
        }

    }

    @Override
    public Object visit(SortColumn<?> visitee, Indent param) {
        return vocabulary.get(visitee.direction().name())
                .set("col", render(visitee.column(), param));
    }

    @Override
    public Object visit(Literal<?> visitee, Indent param) {
        return render(visitee);
    }

    private <T> Object render(Literal<T> visitee) {
        Converter<? super T> c = converter.of(visitee.parent);
        return toSql(visitee.value, c);
    }

    private <T> Object toSql(T value, Converter<? super T> c) {
        if (c != null) {
            return c.toSQL(value);
        }
        if (value instanceof Boolean) {
            return value == Boolean.TRUE ? 1 : 0;
        }
        return value;
    }

    @Override
    public TupleTpl visit(Tuple<?> visitee, Indent param) {
        return render(visitee);
    }

    private <T> TupleTpl render(Tuple<T> visitee) {
        Converter<? super T> c = converter.of(visitee.parent);
        TupleTpl tuple = vocabulary.getTuple();
        for (T val: visitee.values) {
            tuple.appendData(toSql(val, c));
        }
        return tuple;
    }
}
