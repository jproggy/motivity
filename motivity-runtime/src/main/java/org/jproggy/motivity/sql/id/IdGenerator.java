package org.jproggy.motivity.sql.id;

import java.sql.PreparedStatement;

import org.jproggy.motivity.sql.map.Converter;

public interface IdGenerator {
    enum Strategy {
        INCREMENT, SEQUENCE, TABLE, NONE;
    }

    default boolean generates() {
        return false;
    }

    default <T> T getNextId(Converter<T> conv, String data) {
        throw new UnsupportedOperationException("getNextId may only be called if generates returned true");
    }

    default boolean fetches() {
        return false;
    }

    default <T> T fetch(Converter<T> reader, PreparedStatement accessor) {
        throw new UnsupportedOperationException("Fetch may only be called if fetches returned true");
    }
}
