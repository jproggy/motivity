package org.jproggy.motivity.sql;

import static java.sql.Statement.RETURN_GENERATED_KEYS;
import static java.util.Collections.emptyList;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.Store;
import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Expression;
import org.jproggy.motivity.query.Select;
import org.jproggy.motivity.sql.Vocabulary.DeleteTpl;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.id.IdProvider;
import org.jproggy.motivity.sql.map.Binder;
import org.jproggy.motivity.sql.map.Converter;
import org.jproggy.motivity.sql.map.TableRowTransformer;
import org.jproggy.motivity.sql.query.JoinImpl;
import org.jproggy.motivity.sql.query.Query;
import org.jproggy.motivity.sql.query.SelectMultiCol;
import org.jproggy.motivity.sql.query.SelectOneCol;
import org.jproggy.motivity.sql.query.SqlRenderer;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.util.Getter;
import org.jproggy.motivity.util.Indent;
import org.jproggy.motivity.util.Internal;
import org.jproggy.motivity.util.Persistent;
import org.jproggy.snippetory.spi.EncodedData;
import org.jproggy.snippetory.sql.SQL;
import org.jproggy.snippetory.sql.Statement;
import org.jproggy.snippetory.sql.impl.StatementImpl;
import org.jproggy.snippetory.sql.spi.RowTransformer;

public abstract class Table<R extends Persistent> implements Store<R> {
  private final Map<String, ColumnDefinition<R, ?>> columns = new HashMap<>();
  private final String name;
  private String alias;
  private JoinImpl referenceQueue = new JoinImpl(this);
  private final DbConnector db;

  protected Table(String name, DbConnector db) {
    this.name = name;
    this.db = db;
  }
  
  public abstract R create(ResultSet rs) throws SQLException;

  protected final <T> ColumnDefinition<R, T> column(
          String name, Binder<R, T> binder, Getter<R, T> getter, Converter<T> converter) {
    ColumnDefinition<R, T> col = new ColumnDefinition<>(this, name, binder, getter, converter);
    columns.put(col.getName(), col);
    return col;
  }

  public <T> ColumnDefinition<R, T> getColumn(String name) {
    return Table.cast(columns.get(name));
  }

  public Collection<ColumnDefinition<R,?>> getColumns() {
    return columns.values();
  }

  public String getReference() {
    return alias != null ? alias : name;
  }
  
  @Override
  public String getName()
  {
    return name;
  }

  @Override
  public Store<R> as(String name) {
    Table<R> clone = copy();
    clone.alias = name;
    clone.referenceQueue = referenceQueue.adapt(clone);
    return clone;
  }

  public RowTransformer<R> getTransformer() {
    return new TableRowTransformer<>(this);
  }

  @Override
  public Query<R> all() {
    return new Query<>(this);
  }

  protected <X extends Persistent, T extends Table<X>> T join(T t, Condition c) {
    return t.ref(referenceQueue.leftJoin(t, c));
  }

  @SuppressWarnings("unchecked")
  <T extends Table<R>> T ref(JoinImpl j) {
    Table<R> clone = copy();
    clone.referenceQueue = j;
    return (T)clone;
  }

  @SuppressWarnings("unchecked")
  protected Table<R> copy() {
    try {
      Table<R> copy = getClass().getConstructor().newInstance();
      copy.alias = alias;
      copy.referenceQueue = referenceQueue;
      return copy;
    } catch (ReflectiveOperationException e) {
      throw new MotivityException(e);
    }
  }

  protected PreparedStatement insertStatement(Connection con, List<ColumnValue<?>> values) throws SQLException {
    Transactions.requireTransaction();
    Statement stmt = new InsertStatement(this, values).toStatement();
    PreparedStatement ps =  con.prepareStatement(stmt.toString(), RETURN_GENERATED_KEYS);
    ((StatementImpl) stmt).bindTo(ps, 1);
    return ps;
  }


  @Override
  public Query<R> filter(Condition filter) {
    return all().filter(filter);
  }

  @Override
  public Select<Object[]> select(Expression<?>... columns) {
    return new SelectMultiCol(this, columns);
  }

  @Override
  public <T> Select<T> select(Expression<T> cs) {
    return new SelectOneCol<>(this, cs);
  }

  public JoinImpl getJoins() {
    return referenceQueue;
  }

  public EncodedData render(Vocabulary vocabulary, Indent indent) {
    if (alias == null) return SQL.markAsSql(name);
    return vocabulary.getAs().setOrigin(SQL.markAsSql(name)).setAlias(SQL.markAsSql(alias));
  }

  @SuppressWarnings("unchecked")
  private static<T> T cast(Object x) {
    return (T)x;
  }

  public DbConnector getDbConnector() {
    return db;
  }

  @Override
  public boolean delete(R instance) {
    Vocabulary voc = getDbConnector().getVocabulary();
    SqlRenderer r = new SqlRenderer(voc);
    DeleteTpl delete = voc.getDelete().setPrimaryTable(r.render(this, new Indent()));
    delete.setWhereExpression(r.render(pk(instance), new Indent()));
    try (Transaction tra = Transactions.ensureOpen()) {
      long rows = ((Statement) delete.getImplementation()).executeUpdate();
      tra.commit();
      return rows == 1;
    }
  }

  protected  boolean insert(R instance, IdProvider<R, ?> ids) {
    try (Transaction transaction = Transactions.ensureOpen()) {
      ids.before(instance);
      List<ColumnValue<?>> values = getColumnValues(instance);
      try (Connection con = getDbConnector().getConnection();
           PreparedStatement stmt = insertStatement(con, values)) {
        long rows = stmt.executeUpdate();
        ids.afterwards(instance, stmt);
        instance.inSync();
        transaction.commit();
        return rows == 1;
      }
    } catch (SQLException e) {
      throw new MotivityException(e);
    }
  }

  protected List<ColumnValue<?>> getColumnValues(R instance) {
    return emptyList();
  }

  protected abstract Condition pk(R instance);

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> x.name, x -> x.alias, x -> x.db, x -> x.referenceQueue);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }
}
