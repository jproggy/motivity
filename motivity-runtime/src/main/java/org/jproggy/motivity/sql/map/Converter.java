package org.jproggy.motivity.sql.map;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Converter<T> {

  @SuppressWarnings("unchecked")
  public abstract T readValue(ResultSet rs, int columnIndex) throws SQLException;

  public Object toSQL(T value) {
    return value;
  }

}
