package org.jproggy.motivity.sql.query;

import org.jproggy.motivity.query.ColumnAlias;
import org.jproggy.motivity.sql.Vocabulary;
import org.jproggy.motivity.util.Indent;
import org.jproggy.snippetory.sql.SQL;

public class SelectRenderer extends SqlRenderer {
    private final Vocabulary vocabulary;
    public SelectRenderer(Vocabulary vocabulary) {
        super(vocabulary);
        this.vocabulary = vocabulary;
    }

    @Override
    public Object visit(ColumnAlias<?> visitee, Indent i) {
        return vocabulary.getAs().setOrigin(render(visitee.getDefinition(), i)).setAlias(SQL.markAsSql(visitee.getName()));
    }

}
