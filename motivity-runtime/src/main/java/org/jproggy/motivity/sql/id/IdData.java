package org.jproggy.motivity.sql.id;

import org.jproggy.motivity.sql.id.IdGenerator.Strategy;
import org.jproggy.motivity.util.Internal;

public class IdData {
  private final String table;
  private final Strategy method;

  public IdData(String table, Strategy method) {
    super();
    this.table = table;
    this.method = method;
  }

  public String getTable() {
    return table;
  }
  public Strategy getMethod() {
    return method;
  }

  @Override
  public int hashCode() {
    return method.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> x.table, x -> x.method);
  }

  @Override
  public String toString() {
    return "IdData [data=" + table + ", method=" + method + "]";
  }
}
