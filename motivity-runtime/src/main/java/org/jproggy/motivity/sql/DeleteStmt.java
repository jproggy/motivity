package org.jproggy.motivity.sql;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.sql.Vocabulary.DeleteTpl;
import org.jproggy.motivity.sql.query.SqlRenderer;
import org.jproggy.motivity.util.Indent;
import org.jproggy.motivity.util.Internal;
import org.jproggy.snippetory.util.TemplateWrapper;
import org.jproggy.snippetory.sql.Statement;

public class DeleteStmt {
  private final Table from;
  private Condition where = new And();

  public DeleteStmt(Table from) {
    this.from = from;
  }

  public DeleteStmt where(Condition where) {
    this.where = this.where.and(where);
    return this;
  }

  public Statement toStatement() {
    Vocabulary voc = from.getDbConnector().getVocabulary();
    return (Statement)render(voc, new Indent()).getImplementation();
  }

  private TemplateWrapper render(Vocabulary vocabulary, Indent indent) {
    indent = indent.indent();
    DeleteTpl result = vocabulary.getDelete();
    result.setPrimaryTable(from.render(vocabulary, indent));
    if (where != null) {
      result.setWhereExpression(new SqlRenderer(vocabulary).render(where, indent));
    }
    return result;
  }

  @Override
  public int hashCode() {
    return from.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> from.hashCode());
  }
}
