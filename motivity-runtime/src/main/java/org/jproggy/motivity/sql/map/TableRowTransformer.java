package org.jproggy.motivity.sql.map;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

import org.jproggy.motivity.util.Persistent;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;
import org.jproggy.snippetory.sql.spi.RowTransformer;

@SuppressWarnings("rawtypes")
public class TableRowTransformer<T extends Persistent> implements RowTransformer<T> {
  private final Table<T> store;
  private final Collection<ColumnDefinition<T,?>> cols;

  public TableRowTransformer(Table<T> table) {
    this.store = table;
    this.cols = table.getColumns();
  }

  @SuppressWarnings("unchecked")
  @Override
  public T transformRow(ResultSet rs) throws SQLException {
    T item = store.create(rs);
    int col = 1;
    for (ColumnDefinition c: cols) {
      c.bindValue(item, rs, col);
      ++col;
    }
    item.inSync();
    return item;
  }
}
