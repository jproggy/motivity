package org.jproggy.motivity.sql.query;

import java.util.Collection;
import java.util.List;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.query.Column.SortColumn;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.query.Entities;
import org.jproggy.motivity.query.Expression;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.sql.DeleteStmt;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.UpdateStatement;
import org.jproggy.motivity.sql.Vocabulary;
import org.jproggy.motivity.sql.Vocabulary.DistinctTpl;
import org.jproggy.motivity.sql.Vocabulary.SelectTpl;
import org.jproggy.motivity.sql.query.JoinImpl.Ref;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.util.Indent;
import org.jproggy.motivity.util.Internal;
import org.jproggy.snippetory.sql.Cursor;
import org.jproggy.snippetory.sql.SQL;
import org.jproggy.snippetory.sql.Statement;
import org.jproggy.snippetory.sql.spi.RowTransformer;
import org.jproggy.snippetory.util.TemplateWrapper;

public class Query<R> implements Entities<R> {
  private boolean distinct;
  protected final Table<?> from;
  private Condition where = new And();
  private SortColumn<?>[] order = {};
  private int offset;
  private int count = -1;

  public Query(Table<?> from) {
    this.from = from;
  }

  public Query<R> filter(Condition where) {
    this.where = this.where.and(where);
    return this;
  }

  @Override
  public Entities<R> page(int offset, int count) {
    this.count = count;
    this.offset = offset;
    return this;
  }

  @Override
  public Entities<R> limit(int count) {
    this.count = count;
    return this;
  }

  public Entities<R> distinct() {
    distinct = true;
    return this;
  }

  @Override
  public Entities<R> sorted(SortColumn<?>... order) {
    this.order = order;
    return this;
  }

  @Override
  public long count() {
    Vocabulary voc = from.getDbConnector().getVocabulary();
    TemplateWrapper queryTpl = voc.getCountQuery().setQuery(render(voc, new Indent()));
    Statement query = (Statement)queryTpl.getImplementation();
    List<Long> list = query.list(SQL.asLong());
    return list.get(0);
  }

  @Override
  public long delete() {
    try (Transaction transaction = Transactions.ensureOpen()) {
      long rows = new DeleteStmt(from).where(where).toStatement().executeUpdate();
      transaction.commit();
      return rows;
    }
  }
  
  @Override
  public long update(ColumnValue<?>... values) {
    try (Transaction transaction = Transactions.ensureOpen()) {
      long rows = new UpdateStatement(from, values).where(where).toStatement().executeUpdate();
      transaction.commit();
      return rows;
    }
  }

  public Cursor<R> toCursor() {
    return toStatement().cursor(getTransformer());
  }

  @SuppressWarnings("unchecked")
  protected RowTransformer<R> getTransformer()
  {
    return (RowTransformer<R>) from.getTransformer();
  }

  @Override
  public Stream<R> parallelStream() {
    return stream(true);
  }

  @Override
  public Stream<R> stream()
  {
    return stream(false);
  }
  
  private boolean isSorted() {
    return order.length > 0;
  }

  private Stream<R> stream(boolean parallel) {
    Cursor<R> c = toCursor();
    try {
      return StreamSupport.stream(spliterator(c), parallel).onClose(c::close);
    } catch (RuntimeException e) {
      try {
        c.close();
      } catch (Exception t) {
        e.addSuppressed(t);
      }
      throw e;
    }
  }

  private Spliterator<R> spliterator(Cursor<R> c) {
    int characteristics = Spliterator.IMMUTABLE | Spliterator.NONNULL;
    if (isSorted()) characteristics |= Spliterator.ORDERED;
    if (distinct) characteristics |= Spliterator.DISTINCT;
    return Spliterators.spliteratorUnknownSize(c.iterator(), characteristics);
  }

  protected TemplateWrapper render(Vocabulary vocabulary, Indent indent) {
    indent = indent.indent();
    SelectTpl result = vocabulary.getSelect();
    bind(result, vocabulary, indent);
    if (count >= 0) {
      if (offset <= 0) {
        return vocabulary.getTopQuery().setQuery(result).setCount(count);
      } else {
        return (TemplateWrapper) vocabulary.getWindowQuery().setQuery(result).setStart(offset)
                .set("end", offset + count).set("numRows", count);
      }
    }
    return result;
  }

  public void bind(SelectTpl query, Vocabulary vocabulary, Indent indent) {
    SqlRenderer renderer = new SqlRenderer(vocabulary);
    SelectRenderer select = new SelectRenderer(vocabulary);
    if (distinct) {
      DistinctTpl distinctTpl = vocabulary.getDistinct();
      for (Expression<?> c : selectColumns()) {
        distinctTpl.appendSelectCols(select.render(c, indent));
      }
      query.setSelectCols(distinctTpl);
    }else {
      for (Expression<?> c : selectColumns()) {
        query.appendSelectCols(select.render(c, indent));
      }
    }
    query.setPrimaryTable(from.render(vocabulary, indent));
    for (Ref join : getJoins().getJoins()) {
      query.appendJoin(renderer.render(join, indent));
    }
    if (where != null) {
      query.setWhereExpression(renderer.renderCondition(where, indent));
    }
    for (SortColumn c : order) {
      query.appendOrderCols(renderer.render(c, indent));
    }
  }

  protected JoinImpl getJoins() {
    return JoinBuilder.join(from, order)
            .merge(JoinBuilder.join(from, where))
            .merge(JoinBuilder.join(from, selectColumns().toArray(new Term[0])));
  }

  protected Collection<? extends Expression<?>> selectColumns() {
    return from.getColumns();
  }

  public Statement toStatement() {
    Vocabulary voc = from.getDbConnector().getVocabulary();
    return (Statement)render(voc, new Indent()).getImplementation();
  }

  @Override
  public String toString() {
    return toStatement().toString();
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> x.from, x -> x.distinct, x -> x.where, x -> x.order,
        x -> x.offset, x -> x.count);
  }

  @Override
  public int hashCode() {
    return from.hashCode();
  }
}
