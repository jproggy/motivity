package org.jproggy.motivity.sql.map;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.time.LocalTime;
import java.util.Date;
import java.util.Optional;

public abstract class Converters {
    public static final Converter<Boolean> BOOLEANINT = new Converter<Boolean>() {
      public Boolean readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getInt(columnIndex)).map(x -> x == 1).orElse(null);
      }

      @Override
      public Integer toSQL(Boolean value) {
        if (value == null) return null;
        return value ? 1 : 0;
      }
    };
    public static final Converter<Boolean> BOOLEANCHAR = new Converter<Boolean>() {
      public Boolean readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getString(columnIndex)).map("Y"::equals).orElse(null);
      }

      @Override
      public String toSQL(Boolean value) {
        if (value == null) return null;
        return value ? "Y" : "N";
      }
    };
    public static final Converter<Boolean> BIT = new Converter<Boolean>() {
      public Boolean readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getBoolean(columnIndex)).orElse(null);
      }

      @Override
      public Boolean toSQL(Boolean value) {
        return value;
      }
    };
    public static final Converter<Byte> TINYINT = new Converter<Byte>() {
      @Override
      public Byte readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getByte(columnIndex)).orElse(null);

      }
    };
    public static final Converter<Short> SMALLINT = new Converter<Short>() {
      @Override
      public Short readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getShort(columnIndex)).orElse(null);

      }
    };
    public static final Converter<Integer> INTEGER = new Converter<Integer>() {
      @Override
      public Integer readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getInt(columnIndex)).orElse(null);

      }
    };
    public static final Converter<Long> BIGINT = new Converter<Long>() {
      @Override
      public Long readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getLong(columnIndex)).orElse(null);

      }
    };
    public static final Converter<Float> REAL = new Converter<Float>() {
      @Override
      public Float readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getFloat(columnIndex)).orElse(null);

      }
    };
    public static final Converter<Double> DOUBLE = new DoubleConverter();
    public static final Converter<Double> FLOAT = new DoubleConverter();
    public static final Converter<BigDecimal> NUMERIC = new BigDecimalConverter();
    public static final Converter<BigDecimal> DECIMAL = new BigDecimalConverter();
    public static final Converter<Date> DATE = new Converter<Date>() {
      public Date readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getDate(columnIndex)).map(Converters::toDate).orElse(null);
      }

      @Override
      public Date toSQL(Date value) {
        if (value == null) return null;
        return new java.sql.Date(value.getTime());
      }
    };
    public static final Converter<LocalTime> TIME = new Converter<LocalTime>() {
      public LocalTime readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getTime(columnIndex)).map(Time::toLocalTime).orElse(null);
      }
    };
    public static final Converter<Date> TIMESTAMP = new Converter<Date>() {
      public Date readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getTimestamp(columnIndex)).map(Converters::toDate).orElse(null);
      }

      @Override
      public Date toSQL(Date value) {
        if (value == null) return null;
        return new java.sql.Timestamp(value.getTime());
      }
    };
    public static final Converter<String> CHAR = new StringConverter();
    public static final Converter<String> VARCHAR = new StringConverter();
    public static final Converter<String> LONGVARCHAR = new StringConverter();
    public static final Converter<String> CLOB = new StringConverter();
    public static final Converter<byte[]> BINARY = new BinaryConverter();
    public static final Converter<byte[]> VARBINARY = new BinaryConverter();
    public static final Converter<byte[]> LONGVARBINARY = new BinaryConverter();
    public static final Converter<byte[]> BLOB = new BinaryConverter();
    public static final Converter<Object> DEFAULT = new Converter<Object>() {
      @Override
      public Object toSQL(Object value) {
        if (value instanceof Boolean) {
          return Boolean.TRUE.equals(value) ? 1 : 0;
        }
        return value;
      }
      @Override
      public Object readValue(ResultSet rs, int columnIndex) throws SQLException {
        return rs.getObject(columnIndex);
      }
    };

    @SuppressWarnings("Unchecked")
    public static <RT> Converter<RT> by(RT val) {
      if (val instanceof  String) return (Converter<RT>)VARCHAR;
      if (val instanceof  Byte) return (Converter<RT>)TINYINT;
      if (val instanceof  Short) return (Converter<RT>)SMALLINT;
      if (val instanceof  Integer) return (Converter<RT>)INTEGER;
      if (val instanceof  Long) return (Converter<RT>)BIGINT;
      if (val instanceof  Float) return (Converter<RT>)REAL;
      if (val instanceof  Double) return (Converter<RT>)DOUBLE;
      if (val instanceof  BigDecimal) return (Converter<RT>)DECIMAL;
      if (val instanceof  Date) return (Converter<RT>)TIMESTAMP;
      if (val instanceof  byte[]) return (Converter<RT>)BINARY;
      return (Converter<RT>) DEFAULT;
    }

    private static <R> Optional<R> opt(ResultSet s, R value) throws SQLException {
      if (s.wasNull()) return Optional.empty();
      return Optional.ofNullable(value);
    }

    private static Date toDate(Date read) {
      return new Date(read.getTime());
    }

    private static class BinaryConverter extends  Converter<byte[]> {
      public byte[] readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getBytes(columnIndex)).orElse(null);
      }
    }

    private static class StringConverter extends  Converter<String> {
      public String readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getString(columnIndex)).orElse(null);
      }
    }

    private static class BigDecimalConverter extends  Converter<BigDecimal> {
      public BigDecimal readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getBigDecimal(columnIndex)).orElse(null);
      }
    }

    private static class DoubleConverter extends  Converter<Double> {
      public Double readValue(ResultSet rs, int columnIndex) throws SQLException {
        return opt(rs, rs.getDouble(columnIndex)).orElse(null);
      }
    }
}
