package org.jproggy.motivity.sql.query;

import static java.util.Collections.singletonList;

import java.util.List;

import org.jproggy.motivity.query.Expression;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.map.ConverterResolver;
import org.jproggy.motivity.util.Internal;
import org.jproggy.motivity.util.Property;
import org.jproggy.snippetory.sql.spi.RowTransformer;

public class SelectOneCol<T> extends SelectImpl<T> {
  private final Expression<T> selectColumn;

  @SuppressWarnings({ "rawtypes" })
  public SelectOneCol(Table from, Expression<T> column) {
    super(from);
    selectColumn = column;
  }

  @Override
  protected List<Expression<T>> selectColumns() {
    return singletonList(selectColumn);
  }

  @Override
  protected RowTransformer<T> getTransformer() {
    ConverterResolver conv = new ConverterResolver();
    return rs -> conv.of(selectColumn).readValue(rs, 1);
  }

  @Override
  public List<Property<T, ?>> properties() {
    return singletonList( new Property<T, T>() {
      @Override
      public T getValue(T item) {
        return item;
      }

      @Override
      public String getName() {
        return selectColumn.toString();
      }

      @Override
      public void setValue(T item, T value) {
        throw new UnsupportedOperationException();
      }
    });
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equalsWithSuper(this, obj, super::equals, x -> x.selectColumn);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
