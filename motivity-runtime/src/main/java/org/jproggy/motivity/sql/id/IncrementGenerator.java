package org.jproggy.motivity.sql.id;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.map.Converter;
import org.jproggy.snippetory.sql.Statement;

public class IncrementGenerator implements IdGenerator {
    private final DbConnector db;

    public IncrementGenerator(DbConnector db) {
        this.db = db;
    }
    @Override
    public boolean fetches() {
        return true;
    }

    @Override
    public <T> T fetch(Converter<T> reader, PreparedStatement accessor) {
        try (ResultSet generatedKeys = ((Statement) db.getVocabulary().getFetch().getImplementation())
                .getStatement(accessor.getConnection()).executeQuery()) {
            if (generatedKeys.next()) {
                return reader.readValue(generatedKeys, 1);
            }
            throw new MotivityException("Key couldn't be generated");
        } catch (SQLException e) {
            throw new MotivityException(e);
        }
    }
}
