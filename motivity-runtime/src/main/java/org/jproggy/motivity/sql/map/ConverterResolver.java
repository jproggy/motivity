package org.jproggy.motivity.sql.map;

import org.jproggy.motivity.Store.Exists;
import org.jproggy.motivity.query.Column.SortColumn;
import org.jproggy.motivity.query.ColumnAlias;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.query.Condition.Not;
import org.jproggy.motivity.query.Condition.Or;
import org.jproggy.motivity.query.Expression;
import org.jproggy.motivity.query.impl.Function;
import org.jproggy.motivity.query.impl.Function.Call;
import org.jproggy.motivity.query.impl.Literal;
import org.jproggy.motivity.query.impl.Op;
import org.jproggy.motivity.query.impl.TermVisitor;
import org.jproggy.motivity.query.impl.Tuple;
import org.jproggy.motivity.sql.ColumnDefinition;

public class ConverterResolver implements TermVisitor<Converter<?>, Void> {

    @SuppressWarnings("unchecked")
    public <T> Converter<T> of(Expression<T> v) {
        if (v == null) return (Converter<T>) Converters.DEFAULT;
        if (v instanceof ColumnDefinition) {
            return ((ColumnDefinition<?,T>) v).getConverter();
        }
        return (Converter<T>)TermVisitor.dispatch(this, v, null);
    }

    @Override
    public Converter<?> visit(And visitee, Void param) {
        return null;
    }

    @Override
    public Converter<?> visit(Or visitee, Void param) {
        return null;
    }

    @Override
    public Converter<?> visit(Not visitee, Void param) {
        return null;
    }

    @Override
    public Converter<?> visit(Op.Operation visitee, Void param) {
        return null;
    }

    @Override
    public Converter<?> visit(Exists visitee, Void param) {
        return null;
    }

    @Override
    public Converter<?> visit(ColumnAlias<?> visitee, Void param) {
        return of(visitee.getDefinition());
    }

    @Override
    public Converter<?> visit(Call<?> visitee, Void param) {
        if (visitee.function == Function.Count) {
            return Converters.BIGINT;
        }
        return of(visitee.origin);
    }

    @Override
    public Converter<?> visit(Literal<?> visitee, Void param) {
        return of(visitee.parent);
    }

    @Override
    public Converter<?> visit(Tuple<?> visitee, Void param) {
        return of(visitee.parent);
    }

    @Override
    public Converter<?> visit(SortColumn<?> visitee, Void param) {
        return null;
    }
}
