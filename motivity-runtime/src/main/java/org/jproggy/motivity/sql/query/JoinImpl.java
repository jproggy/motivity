package org.jproggy.motivity.sql.query;

import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.util.Persistent;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.Store;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.util.Internal;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class JoinImpl {
  private final List<Ref> refs;
  private final Table<?> base;

  public JoinImpl(Table<?> base) {
    this.base = base;
    refs = Collections.emptyList();
  }

  public JoinImpl() {
    this(null);
  }

  private JoinImpl(Table<?> base, List<Ref> refs) {
    this.base = base;
    this.refs = refs;
  }

  private JoinImpl(JoinImpl parent, Ref ref) {
    this.base = parent.base;
    List<Ref> refs = new ArrayList<>(parent.refs);
    refs.add(ref);
    this.refs = refs;
  }

  public List<Ref> getJoins() {
    return refs;
  }

  public JoinImpl leftJoin(Store<?> t, Condition c) {
    return new JoinImpl(this, new Ref(t, c, true));
  }

  public JoinImpl join(Store<?> t, Condition c) {
    return new JoinImpl(this, new Ref(t, c, false));
  }

  public JoinImpl merge(JoinImpl other) {
    if (other == null || other.base == null || other == this) return this;
    if (base == null) return other;
    if (!base.equals(other.base) ) {
      throw new IllegalArgumentException("Join based on " + base.getReference() +
              " incompatible with" + other.base.getReference());
    }
    Set<Ref> result = new LinkedHashSet<>(refs);
    result.addAll(other.refs);
    return new JoinImpl(base, new ArrayList<>(result));
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> Optional.ofNullable(x.base).map(Table::getName), x -> x.refs);
  }

  @Override
  public int hashCode() {
    return base.hashCode();
  }

  public <R extends Persistent> JoinImpl adapt(Table<R> clone) {
    return new JoinImpl(clone, refs);
  }

  public static class Ref implements Term {
    final Term target;
    final Condition condition;
    final boolean isLeft;

    public Ref(Term target, Condition condition, boolean isLeft) {
      this.target = target;
      this.condition = condition;
      this.isLeft = isLeft;
    }

    @Override
    public boolean equals(Object obj) {
      return Internal.equals(this, obj, x -> x.target, x -> x.condition, x -> x.isLeft);
    }

    @Override
    public int hashCode() {
      return target.hashCode();
    }
  }
}
