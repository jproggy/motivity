package org.jproggy.motivity.sql.query;

import org.jproggy.motivity.Store.Exists;
import org.jproggy.motivity.query.Column.SortColumn;
import org.jproggy.motivity.query.ColumnAlias;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.query.Condition.Not;
import org.jproggy.motivity.query.Condition.Or;
import org.jproggy.motivity.query.impl.Function;
import org.jproggy.motivity.query.impl.Function.Call;
import org.jproggy.motivity.query.impl.Literal;
import org.jproggy.motivity.query.impl.Op;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.query.impl.TermVisitor;
import org.jproggy.motivity.query.impl.Tuple;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;

public class JoinBuilder implements TermVisitor<JoinImpl, JoinImpl> {

    public static JoinImpl join(Table<?> t, Term... terms) {
        return new JoinBuilder().join(terms, new JoinImpl(t));
    }

    private JoinImpl join(Term[] terms, JoinImpl param) {
        for (Term t : terms) {
            param = param.merge(join(t, param));
        }
        return param;
    }

    private JoinImpl join(Term v, JoinImpl param) {
        if (v == null) return param;
        if (v instanceof ColumnDefinition) return visit((ColumnDefinition<?, ?>) v, param);
        if (v instanceof Op) return visit((Op) v, param);
        if (v instanceof Function) return visit((Function) v, param);
        if (v instanceof SelectImpl) return visit((SelectImpl) v, param);
        return TermVisitor.dispatch(this, v, param);
    }

    public JoinImpl visit(SelectImpl v, JoinImpl param) {
        return param;
    }

    public JoinImpl visit(ColumnDefinition<?, ?> visitee, JoinImpl param) {
        return visitee.getReferences();
    }

    public JoinImpl visit(Op visitee, JoinImpl param) {
        return param;
    }

    public JoinImpl visit(Function visitee, JoinImpl param) {
        return param;
    }

    @Override
    public JoinImpl visit(And visitee, JoinImpl param) {
        return join(visitee.terms, param);
    }

    @Override
    public JoinImpl visit(Or visitee, JoinImpl param) {
        return join(visitee.terms, param);
    }

    @Override
    public JoinImpl visit(Not visitee, JoinImpl param) {
        return join(visitee.terms, param);
    }

    @Override
    public JoinImpl visit(Op.Operation visitee, JoinImpl param) {
        return join(visitee.terms, param);
    }

    @Override
    public JoinImpl visit(Exists visitee, JoinImpl param) {
        return param;
    }

    @Override
    public JoinImpl visit(ColumnAlias<?> visitee, JoinImpl param) {
        return join(visitee.getDefinition(), param);
    }

    @Override
    public JoinImpl visit(Call<?> visitee, JoinImpl param) {
        return join(visitee.origin, param).merge(join(visitee.params, param));
    }

    @Override
    public JoinImpl visit(Literal<?> visitee, JoinImpl param) {
        return param;
    }

    @Override
    public JoinImpl visit(Tuple<?> visitee, JoinImpl param) {
        return param;
    }

    @Override
    public JoinImpl visit(SortColumn<?> visitee, JoinImpl param) {
        return join(visitee.column(), param);
    }
}
