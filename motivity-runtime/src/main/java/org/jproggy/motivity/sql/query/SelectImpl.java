package org.jproggy.motivity.sql.query;

import org.jproggy.motivity.query.Column;
import org.jproggy.motivity.query.Column.SortColumn;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Select;
import org.jproggy.motivity.query.Select.SelectGroup;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.Vocabulary;
import org.jproggy.motivity.sql.Vocabulary.SelectTpl;
import org.jproggy.motivity.util.Indent;
import org.jproggy.motivity.util.Internal;

public abstract class SelectImpl<T> extends Query<T> implements SelectGroup<T> {
  private Column<?>[] groupColumns = {};
  private Condition having;

  @SuppressWarnings({ "rawtypes" })
  public SelectImpl(Table from) {
    super(from);
  }

  @Override
  public SelectImpl<T> filter(Condition where) {
    return (SelectImpl<T>)super.filter(where);
  }

  @Override
  public SelectGroup<T> group(Column<?>... by) {
    groupColumns = by;
    return this;
  }

  @Override
  public Select<T> having(Condition having) {
    this.having = having;
    return this;
  }

  @Override
  public Select<T> sorted(SortColumn<?>... order) {
    return (Select<T>)super.sorted(order);
  }

  @Override
  public Select<T> limit(int count) {
    return (Select<T>)super.limit(count);
  }

  @Override
  public Select<T> page(int offset, int count) {
    return (Select<T>)super.page(offset, count);
  }

  @Override
  public Select<T> distinct() {
    return (Select<T>)super.distinct();
  }

  @Override
  protected JoinImpl getJoins() {
    return super.getJoins()
            .merge(JoinBuilder.join(from, groupColumns))
            .merge(JoinBuilder.join(from, having));
  }

  @Override
  public void bind(SelectTpl result, Vocabulary vocabulary, Indent indent) {
    super.bind(result, vocabulary, indent);
    indent = indent.indent();
    SqlRenderer r = new SqlRenderer(vocabulary);
    for (Column<?> c : groupColumns) {
      result.appendGroupCols(r.render(c, indent));
    }
    if (having != null) {
      result.setHavingExpression(r.render(having, indent));
    }
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equalsWithSuper(this, obj, super::equals, x -> x.groupColumns,
        x -> x.having);
  }

  @Override
  public int hashCode() {
    return super.hashCode();
  }
}
