package org.jproggy.motivity.sql.transaction;

import java.io.Closeable;
import java.sql.Connection;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.transaction.spi.TransactionHandler;
import org.jproggy.snippetory.sql.spi.ConnectionProvider;

public class SqlTransactionHandler implements TransactionHandler, Closeable {
  private final ThreadLocal<Deque<SqlTransactionImpl>> states = new ThreadLocal<>();
  final Map<String, ConnectionProvider> sources = new ConcurrentHashMap<>();
  @Override
  public Transaction openNew(Transaction.Isolation iso, boolean readOnly) {
    SqlTransactionImpl newTra = new SqlTransactionImpl(this, iso, readOnly);
    stack().push(newTra);
    return newTra;
  }

  @Override
  public boolean isTransactionOpen() {
    return depth() > 0;
  }

  @Override
  public Transaction top() {
    if (depth() == 0) return null;
    return peek();
  }

  private SqlTransactionImpl peek() {
    return stack().getLast();
  }

  public int depth() {
    return stack().size();
  }

  Deque<SqlTransactionImpl> stack() {
    Deque<SqlTransactionImpl> stack = states.get();
    if (stack == null) {
      stack = new ArrayDeque<>();
      states.set(stack);
    }
    return stack;
  }

  public synchronized DbConnector registerConnection(String name, ConnectionProvider ds, String variant) {
    sources.put(name, ds);
    ConnectionProvider wrapped = () -> {
      if (depth() == 0) {
        return ds.getConnection();
      }
      return peek().getManagedConnection(name);
    };
    return DbConnector.getInstance(name, wrapped, variant);
  }

  Connection createConnection(String name) {
    return sources.get(name).getConnection();
  }

  public void transactionClosed(SqlTransactionImpl sqlTransaction) {
    if (top() != sqlTransaction) {
      throw new IllegalStateException("Transaction is not current");
    }
    stack().remove(sqlTransaction);
    if (depth() == 0) {
      states.remove();
    }
  }

  public void close() {
    if (depth() > 0) {
      throw new IllegalStateException("Transactions still open");
    }
    states.remove();
  }
}
