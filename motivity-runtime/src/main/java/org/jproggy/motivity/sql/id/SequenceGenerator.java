package org.jproggy.motivity.sql.id;

import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.map.Converter;
import org.jproggy.snippetory.sql.SQL;
import org.jproggy.snippetory.sql.Statement;

public class SequenceGenerator implements IdGenerator {
    private final DbConnector db;

    public SequenceGenerator(DbConnector db) {
        this.db = db;
    }

    @Override
    public boolean generates() {
        return true;
    }

    @Override
    public <T> T getNextId(Converter<T> conv, String data) {
        Statement nextVal = (Statement) db.getVocabulary().getNextVal().setSequence(SQL.markAsSql(data + "_seq")).getImplementation();
        return nextVal.one(rs -> conv.readValue(rs, 1));
    }
}
