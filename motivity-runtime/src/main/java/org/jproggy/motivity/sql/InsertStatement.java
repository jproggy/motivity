package org.jproggy.motivity.sql;

import java.util.List;

import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.sql.query.SqlRenderer;
import org.jproggy.motivity.util.Indent;
import org.jproggy.motivity.util.Internal;
import org.jproggy.snippetory.util.TemplateWrapper;
import org.jproggy.snippetory.sql.SQL;
import org.jproggy.snippetory.sql.Statement;

public class InsertStatement {
  private final Table<?> to;
  private final List<ColumnValue<?>> values;

  public InsertStatement(Table<?> to, List<ColumnValue<?>> values) {
    this.to = to;
    this.values = values;
  }

  public Statement toStatement() {
    Vocabulary voc = to.getDbConnector().getVocabulary();
    return (Statement)render(voc, new Indent()).getImplementation();
  }

  private TemplateWrapper render(Vocabulary vocabulary, Indent indent) {
    indent = indent.indent();
    SqlRenderer r = new SqlRenderer(vocabulary);
    Vocabulary.InsertTpl insert = vocabulary.getInsert().setPrimaryTable(to.render(vocabulary, indent));
    for (ColumnValue<?> val : values) {
      insert.appendColumn(SQL.markAsSql(val.getColumn().getName()))
              .appendValue(r.render(val.getValue(), indent));
    }
    return insert;
  }

  @Override
  public int hashCode() {
    return to.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, x -> x.to, x -> x.values);
  }
}
