package org.jproggy.motivity.sql.connect;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Vocabulary;
import org.jproggy.motivity.sql.id.GetGenKeysGenerator;
import org.jproggy.motivity.sql.id.IdData;
import org.jproggy.motivity.sql.id.IdGenerator;
import org.jproggy.motivity.sql.id.IdGenerator.Strategy;
import org.jproggy.motivity.sql.id.IdProvider;
import org.jproggy.motivity.sql.id.IncrementGenerator;
import org.jproggy.motivity.sql.id.NoopGenerator;
import org.jproggy.motivity.sql.id.SequenceGenerator;
import org.jproggy.motivity.sql.id.TableGenerator;
import org.jproggy.motivity.util.Internal;
import org.jproggy.motivity.util.Persistent;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.UriResolver;
import org.jproggy.snippetory.sql.SqlContext;
import org.jproggy.snippetory.sql.spi.ConnectionProvider;
import org.jproggy.snippetory.sql.spi.VariantResolver;

public class DbConnector implements ConnectionProvider {
  private static final Map<String, ConnectionProvider> instances = new ConcurrentHashMap<>();
  private static final Map<String, String> variants = new ConcurrentHashMap<>();

  private final Map<Strategy, IdGenerator> generators = new ConcurrentHashMap<>();
  private static final Map<String, Vocabulary> vocabularies = new ConcurrentHashMap<>();
  private final String name;

  protected DbConnector(String name) {
    this.name = name;
  }

  public static synchronized DbConnector getInstance(String name, ConnectionProvider fab, String variant) {
    instances.put(name, fab);
    variants.put(name, variant);
    return new DbConnector(name);
  }

  public static synchronized DbConnector getInstance(String name) {
    if (!instances.containsKey(name)) {
      throw new MotivityException("Database with name " + name + " is not registered");
    }
    return new DbConnector(name);
  }

  public static synchronized DbConnector dummy() {
    return getInstance("<dummy>", () -> null, "");
  }

  private Vocabulary initVocabulary(String variant) {
    SqlContext sqlCtx = new SqlContext().uriResolver(UriResolver.resource()).connections(this).postProcessor(VariantResolver.wrap(variant));
    Template t = sqlCtx.getRepository("org/jproggy/motivity/sql/Vocabulary.sql").toTemplate();
    return new Vocabulary(t);
  }

  public Vocabulary getVocabulary() {
    return vocabularies.computeIfAbsent(getVariant(), this::initVocabulary);
  }

  @Override
  public Connection getConnection() {
    return instances.get(name).getConnection();
  }

  public String getVariant() {
    return variants.get(name);
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, e -> e.name);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  public <R extends Persistent, T> IdProvider<R, T> getIdGenerator(IdData data, ColumnDefinition<R, T> column) {
      return new IdProvider<>(generator(data.getMethod()), data, column);
  }

  private synchronized IdGenerator generator(Strategy method) {
    if (!generators.containsKey(method)) {
      generators.put(method, createGenerator(method));
    }
    return generators.get(method);
  }
  private IdGenerator createGenerator(Strategy method) {
    switch (method) {
      case TABLE:
        return new TableGenerator(this);
      case INCREMENT:
        try (Connection con = getConnection()) {
          if (con.getMetaData().supportsGetGeneratedKeys()) {
            return new GetGenKeysGenerator();
          }
          return new IncrementGenerator(this);
        } catch (SQLException e) {
          throw new MotivityException(e);
        }
      case SEQUENCE:
        return new SequenceGenerator(this);
      case NONE:
        return new NoopGenerator();
    }
    return null;
  }
}
