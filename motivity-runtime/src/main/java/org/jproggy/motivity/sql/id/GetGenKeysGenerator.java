package org.jproggy.motivity.sql.id;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.sql.map.Converter;

public class GetGenKeysGenerator implements IdGenerator {
    @Override
    public boolean fetches() {
        return true;
    }

    @Override
    public <T> T fetch(Converter<T> reader, PreparedStatement accessor) {
        try (ResultSet generatedKeys = accessor.getGeneratedKeys()) {
            if (generatedKeys.next()) {
                return reader.readValue(generatedKeys, 1);
            }
            throw new MotivityException("Key couldn't be generated");
        } catch (SQLException e) {
            throw new MotivityException(e);
        }
    }
}
