package org.jproggy.motivity.sql;

import org.jproggy.motivity.sql.query.JoinImpl;
import org.jproggy.motivity.util.Persistent;
import org.jproggy.motivity.query.Column;
import org.jproggy.motivity.sql.map.Binder;
import org.jproggy.motivity.sql.map.Converter;
import org.jproggy.motivity.util.Getter;
import org.jproggy.motivity.util.Internal;
import org.jproggy.motivity.util.Property;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ColumnDefinition<R extends Persistent, T> extends Column<T> implements Property<R, T> {
  private final String name;
  private final Table<R> parent;
  private final Converter<T> converter;
  private final Binder<R, T> binder;
  private final Getter<R, T> getter;

  public ColumnDefinition(Table<R> parent, String name, Binder<R, T> binder,
                          Getter<R, T> getter, Converter<T> converter) {
    this.name = name;
    this.parent = parent;
    this.converter =  converter;
    this.binder =  binder;
    this.getter = getter;
  }

  public void bindValue(R item, ResultSet rs, int columnIndex) throws SQLException  {
    binder.bindValue(item, converter.readValue(rs, columnIndex));
  }

  @Override
  public T getValue(R item) {
    return getter.getValue(item);
  }

  @Override
  public void setValue(R item, T value) {
    binder.bindValue(item, value);
  }

  public Converter<T> getConverter() {
    return converter;
  }

  public String getReference() {
    return parent.getReference() + '.' + name;
  }

  @Override
  public String getName() {
    return name;
  }

  public JoinImpl getReferences() {
    return parent.getJoins();
  }

  @Override
  public ColumnDefinition<R, T> as(String alias) {
    return new Alias<>(alias, this);
  }

  @Override
  public String toString() {
    return getName();
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equals(this, obj, ColumnDefinition::getName, x -> x.parent);
  }

  @Override
  public int hashCode() {
    return name.hashCode();
  }

  private static class Alias<R extends Persistent, T> extends ColumnDefinition<R, T> {
    private final String alias;
    public Alias(String alias, ColumnDefinition<R, T> column) {
      super(column.parent, column.name, column.binder, column.getter, column.converter);
      this.alias = alias;
    }

    @Override
    public String getName() {
      return alias;
    }

    @Override
    public boolean equals(Object obj) {
      return Internal.equalsWithSuper(this, obj, super::equals, x  -> x.alias);
    }

    @Override
    public int hashCode() {
      return alias.hashCode();
    }
  }
}
