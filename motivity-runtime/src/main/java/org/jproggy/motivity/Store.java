package org.jproggy.motivity;

import org.jproggy.motivity.query.Column;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Entities;
import org.jproggy.motivity.query.Expression;
import org.jproggy.motivity.query.Select;
import org.jproggy.motivity.query.impl.Term;

/**
 * Represents some kind of storage, that contains many items of the same type or at least the same
 * base type. These types can be queried based on their properties. Additionally, there are references
 * to items stored in other tables.   
 * 
 * @param <R> Is the type of items contained in this store
 */
public interface Store<R> extends Term {
  boolean delete(R instance);
  boolean update(R instance);
  boolean insert(R instance);

  default Condition exists(Condition test) {
    return new Exists(this, test);
  }


  Store<R> as(String name);
  Entities<R> all();
  
  /** 
   * The conditions used to filter are typically based on Expressions based on
   * generated items.
   * (columns or references to other stores) Usage looks like this:
   * <pre>
   * {@code
   * List<Product> coolX2s = productStore.filter(
   *   and(
   *     productStore.name.like("xx%"),
   *     or(
   *       productStore.merchant().name.in("Cool Store", "Cooler Store", Coolest Store"),
   *       productStore.rating.greaterOrEqual(Rating.COOL)
   *     )
   *   )
   * ).list(); }
   * </pre>
   * <strong>Caution: Generally expressions within a filter have to start in the
   * store the filter is
   * executed on, to make clear how the relation is. Like<br>
   * {@code productStore.mechants().name}<br> 
   * and neither only<br>
   * <strike>{@code mechantStore.name}</strike><br> 
   * nor <br>
   * <strike>{@code orderStore.entry().product().merchant().name}</strike><br>
   * in previous example.
   * <strong>
   * <br>
   * 
   * @param filter
   *          that matches rows within this store one in interested in
   * @return a representation items matching the given filter 
   */
  Entities<R> filter(Condition filter);

  Select<Object[]> select(Expression<?>...cs);

  <T> Select<T> select(Expression<T> cs);

  String getName();

  class Exists extends Condition {
    private Exists(Store<?> t, Condition filter) {
      super(t, filter);
    }
    public Store<?> store() {
      return (Store<?>) terms[0];
    }

    public Condition filter() {
      return (Condition) terms[1];
    }
  }
}
