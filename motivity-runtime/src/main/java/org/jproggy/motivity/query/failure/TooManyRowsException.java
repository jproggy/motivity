package org.jproggy.motivity.query.failure;

import org.jproggy.motivity.MotivityException;

public class TooManyRowsException extends MotivityException {
  private static final long serialVersionUID = 1L;

  public TooManyRowsException() {
    super();
  }

  public TooManyRowsException(String message) {
    super(message);
  }

  public TooManyRowsException(Throwable cause) {
    super(cause);
  }

  public TooManyRowsException(String message, Throwable cause) {
    super(message, cause);
  }

}