package org.jproggy.motivity.query;


import java.util.List;

import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.util.Property;

public interface Select<T> extends Term, Entities<T> {
  Select<T> filter(Condition has);

  @Override
  Select<T> page(int offset, int count);
  @Override
  Select<T> limit(int count);
  Select<T> distinct();
  List<Property<T, ?>> properties();

  /**
   * @deprecated update operations are not supported for selects
   */
  @Override @Deprecated
  default long delete() {
    throw new UnsupportedOperationException();
  }

  /**
   * @deprecated update operations are not supported for selects
   */
  @Override @Deprecated
  default long update(Column.ColumnValue<?>... values) {
    throw new UnsupportedOperationException();
  }

  @Override
  Select<T> sorted(Column.SortColumn<?>... order);
  SelectGroup<T> group(Column<?>...by);

  interface SelectGroup<T> extends Select<T> {
    Select<T> having(Condition having);
  }
}
