package org.jproggy.motivity.query.impl;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Expression;

public enum Op implements Term {
    Equal, NotEqual, Less, GreaterOrEqual, Greater, LessOrEqual, IsNull, IsNotNull, In, NotIn, Like,
    NotLike, Between;

    public <T> Condition on(Expression<T> operand1, Term operand2, Term operand3) {
        return new Operation(operand1, this, operand2, operand3);
    }

    public <T> Condition on(Expression<T> operand1, Term operand2) {
        return new Operation(operand1, this, operand2);
    }

    public <T> Condition on(Expression<T> operand) {
        return new Operation(operand, this);
    }

    public static class Operation extends Condition {
        private Operation(Term...terms) {
            super(terms);
        }
        public Expression<?> getOrigin() {
            return (Expression<?>) terms[0];
        }
        public Op getOperator() {
            return (Op) terms[1];
        }
        public Term operand1() {
            if (terms.length < 3) return null;
            return terms[2];
        }
        public Term operand2() {
            if (terms.length < 4) return null;
            return terms[3];
        }
    }
}
