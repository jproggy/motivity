package org.jproggy.motivity.query;

import static java.util.Arrays.asList;

import org.jproggy.motivity.query.impl.Function;
import org.jproggy.motivity.query.impl.Literal;
import org.jproggy.motivity.query.impl.Op;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.query.impl.Tuple;

public abstract class Expression<T> implements Term {
  @SuppressWarnings("unchecked")
  public Condition in(T... values) {
    return Op.In.on(this, new Tuple<>(asList(values), this));
  }

  public Condition in(Iterable<T> values) {
    return Op.In.on(this, new Tuple<>(values, this));
  }

  public Condition isNull() {
    return Op.IsNull.on(this);
  }

  public Condition isNotNull() {
    return Op.IsNotNull.on(this);
  }

  public Condition equalTo(T value) {
    return Op.Equal.on(this, new Literal<>(value, this));
  }

  public Condition equalTo(Expression<T> x) {
    return Op.Equal.on(this, x);
  }

  public Condition notEqual(T value) {
    return Op.NotEqual.on(this, new Literal<>(value, this));
  }

  public Condition notEqual(Expression<T> x) {
    return Op.NotEqual.on(this, x);
  }

  public Condition lessThen(T value) {
    return Op.Less.on(this, new Literal<>(value, this));
  }

  public Condition lessThen(Expression<T> x) {
    return Op.Less.on(this, x);
  }

  public Condition greaterOrEqual(T value) {
    return Op.GreaterOrEqual.on(this, new Literal<>(value, this));
  }

  public Condition greaterOrEqual(Expression<T> x) {
    return Op.GreaterOrEqual.on(this, x);
  }

  public Condition greaterThen(T value) {
    return Op.Greater.on(this, new Literal<>(value, this));
  }

  public Condition greaterThen(Expression<T> x) {
    return Op.Greater.on(this, x);
  }

  public Condition lessOrEqual(T value) {
    return Op.LessOrEqual.on(this, new Literal<>(value, this));
  }

  public Condition lessOrEqual(Expression<T> x) {
    return Op.LessOrEqual.on(this, x);
  }

  public Condition between(T from, T to) {
    return Op.Between.on(this, new Literal<>(from, this), new Literal<>(to, this));
  }

  public Expression<T> ifNull(T replacement) {
    return Function.IfNull.on(this, new Literal<>(replacement, this));
  }

  @SuppressWarnings("unchecked")
  public Expression<T> concat(Expression<T> other) {
    return Function.Concat.on(this, other);
  }

  @SuppressWarnings("unchecked")
  public Expression<T> concat(T other) {
    return Function.Concat.on(this, new Literal<>(other, this));
  }

  public Column<T> as(String alias) {
    return new ColumnAlias<T>(alias, this);
  }

  public Condition in(Select<T> q) {
    return Op.In.on(this, q);
  }

  public Condition notIn(Select<T> q) {
    return Op.NotIn.on(this, q);
  }

  public Condition like(String pattern) {
    return Op.Like.on(this, new Literal<>(pattern, null));
  }

  public Condition notLike(String pattern) {
    return Op.NotLike.on(this, new Literal<>(pattern, null));
  }
}
