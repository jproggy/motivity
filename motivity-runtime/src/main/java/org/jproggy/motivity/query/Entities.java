package org.jproggy.motivity.query;

import org.jproggy.motivity.query.failure.NoRowsException;
import org.jproggy.motivity.query.failure.TooManyRowsException;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public interface Entities<R> {
  Entities<R> page(int offset, int count);
  Entities<R> limit(int count);
  Entities<R> sorted(Column.SortColumn<?>... order);

  long count();
  long delete();
  long update(Column.ColumnValue<?>... values);

  default Optional<R> first() {
    limit(1);
    try (Stream<R> s = stream()) {
      return s.findFirst();
    }
  }

  default R one() {
    List<R> list = list();
    if (list.isEmpty()) {
      throw new NoRowsException(this.toString());
    }
    if (list.size() > 1) {
      throw new TooManyRowsException(this + " found " + list.size() + " items.");
    }
    return list.get(0);
  }

  /**
   * The entities in the stream  
   *
   * @return a sequential {@code Stream} over the row represented by this {@code Entities} instance.
   */
  Stream<R> stream();

  /**
   * Returns a possibly parallel {@code Stream} containing the rows determined by this object. 
   * It is allowable for this method to return a sequential stream.
   *
   * @return a possibly parallel {@code Stream} over the elements in this
   * collection
   */
  Stream<R>  parallelStream();

  default List<R> list() {
    try (Stream<R> s = stream()) {
      return s.collect(Collectors.toList());
    }
  }
}