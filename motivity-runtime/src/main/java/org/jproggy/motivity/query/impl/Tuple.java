package org.jproggy.motivity.query.impl;

import org.jproggy.motivity.query.Expression;

public class Tuple<T> implements Term {
  public final Iterable<T> values;
  public final Expression<T> parent;
  public Tuple(Iterable<T> values, Expression<T> parent) {
    this.values = values;
    this.parent = parent;
  }
}
