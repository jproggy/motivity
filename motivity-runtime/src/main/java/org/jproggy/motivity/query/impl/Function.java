package org.jproggy.motivity.query.impl;

import org.jproggy.motivity.query.Expression;

public enum Function implements Term {
    Sum, Count, Min, Max,
    Concat, IfNull;

    public <T> Expression<T> on(Expression<?> origin, Term... params) {
        return new Call<T>(this, origin, params);
    }

    public static class Call<T> extends Expression<T> {
        public final Function function;
        public final Expression<?> origin;
        public final Term[] params;

        public Call(Function function, Expression<?> origin, Term[] params) {
            this.function = function;
            this.origin = origin;
            this.params = params;
        }

        @Override
        public String toString() {
            return origin.toString() + '.' + function.name();
        }
    }
}
