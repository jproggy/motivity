package org.jproggy.motivity.query;

import org.jproggy.motivity.util.Internal;

import java.util.Objects;

public class ColumnAlias<T> extends Column<T> {
  private final String alias;
  private final Expression<T> definition;

  public ColumnAlias(String alias, Expression<T> definition) {
    this.alias = Objects.requireNonNull(alias);
    this.definition = Objects.requireNonNull(definition);
  }

  public Expression<T> getDefinition() {
    return definition;
  }

  @Override
  public String getName() {
    return alias;
  }

  @Override
  public String toString() {
    return alias;
  }

  @Override
  public boolean equals(Object obj) {
    return Internal.equalsWithSuper(this, obj, super::equals, x -> x.definition, x -> x.alias);
  }

  @Override
  public int hashCode() {
    return alias.hashCode();
  }
}
