package org.jproggy.motivity.query.impl;

import org.jproggy.motivity.Store.Exists;
import org.jproggy.motivity.query.Column.SortColumn;
import org.jproggy.motivity.query.ColumnAlias;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.query.Condition.Not;
import org.jproggy.motivity.query.Condition.Or;
import org.jproggy.motivity.query.impl.Function.Call;
import org.jproggy.motivity.query.impl.Op.Operation;

public interface TermVisitor<T,P> {
    T visit(And visitee, P param);
    T visit(Or visitee, P param);
    T visit(Not visitee, P param);
    T visit(Operation visitee, P param);
    T visit(Exists visitee, P param);
    T visit(ColumnAlias<?> visitee, P param);
    T visit(Function.Call<?> visitee, P param);
    T visit(Literal<?> visitee, P param);
    T visit(Tuple<?> visitee, P param);
    T visit(SortColumn<?> visitee, P param);

    static <T, P> T dispatch(TermVisitor<T,P> visitor, Term visitee, P param) {
        if (visitee instanceof And) return visitor.visit((And) visitee, param);
        if (visitee instanceof Or) return visitor.visit((Or) visitee, param);
        if (visitee instanceof Not) return visitor.visit((Not) visitee, param);
        if (visitee instanceof Exists) return visitor.visit((Exists) visitee, param);
        if (visitee instanceof Operation) return visitor.visit((Operation) visitee, param);
        if (visitee instanceof ColumnAlias) return visitor.visit((ColumnAlias<?>) visitee, param);
        if (visitee instanceof Call) return visitor.visit((Call<?>) visitee, param);
        if (visitee instanceof Literal) return visitor.visit((Literal<?>) visitee, param);
        if (visitee instanceof Tuple) return visitor.visit((Tuple<?>) visitee, param);
        if (visitee instanceof SortColumn) return visitor.visit((SortColumn<?>) visitee, param);
        throw new IllegalArgumentException("Don't understand " + visitee);
    }

}
