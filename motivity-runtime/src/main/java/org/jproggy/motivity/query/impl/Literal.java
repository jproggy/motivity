package org.jproggy.motivity.query.impl;

import org.jproggy.motivity.query.Expression;

public class Literal<T> implements Term {
    public final T value;
    public final Expression<T> parent;

    public Literal(T value, Expression<T> parent) {
        this.value = value;
        this.parent = parent;
    }
}
