package org.jproggy.motivity.query.failure;

import org.jproggy.motivity.MotivityException;

public class NoRowsException extends MotivityException {
  private static final long serialVersionUID = 1L;

  public NoRowsException() {
    super();
  }

  public NoRowsException(String message) {
    super(message);
  }

  public NoRowsException(Throwable cause) {
    super(cause);
  }

  public NoRowsException(String message, Throwable cause) {
    super(message, cause);
  }

}
