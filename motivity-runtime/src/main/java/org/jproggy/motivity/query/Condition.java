package org.jproggy.motivity.query;

import java.util.Objects;

import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.util.Internal;

public abstract class Condition implements Term {
  public final Term[] terms;

  protected Condition(Term... terms) {
    this.terms = terms;
  }

  public Condition and(Condition x) {
    return new And(this, x);
  }

  public Condition or(Condition x) {
    return new Or(this, x);
  }

  public Condition not() {
    return new Not(this);
  }

  @Override
  public boolean equals(Object o) {
    return Internal.equals(this, o, x -> terms);
  }

  @Override
  public int hashCode() {
    return Objects.hash(terms);
  }

  public static class Not extends Condition {
    public Not(Condition x) {
      super(x);
    }
  }

  public static class And extends Condition {
    public And(Condition... parts) {
      super(parts);
    }
    private And(Term[] terms) {
      super(terms);
    }

    @Override
    public Condition and(Condition x) {
      return new And(Internal.add(terms, x));
    }
  }

  public static class Or extends Condition {
    public Or(Condition... parts) {
      super(parts);
    }
    private Or(Term[] terms) {
      super(terms);
    }

    @Override
    public Condition or(Condition x) {
      return new Or(Internal.add(terms, x));
    }
  }
}