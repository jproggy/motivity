package org.jproggy.motivity.query;

import java.util.Objects;

import org.jproggy.motivity.query.impl.Function;
import org.jproggy.motivity.query.impl.Literal;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.util.Internal;

public abstract class Column<T> extends Expression<T> {
	public Expression<Long> count() {
		return Function.Count.on(this);
	}
	public Expression<T> sum() {
		return Function.Sum.on(this);
	}
	public Expression<T> min() {
		return Function.Min.on(this);
	}
	public Expression<T> max() {
		return Function.Max.on(this);
	}

	public SortColumn<T> asc() {
		return new SortColumn<>(this, Direction.asc);
	}

	public SortColumn<T> desc() {
		return new SortColumn<>(this, Direction.desc);
	}

	public abstract String getName();

	public <X extends T> ColumnValue<T> to(X value) {
		return new ColumnValue<>(this, value);
	}

	public static class SortColumn<T> implements Term {
	  private final Column<T> col;
	  private final Direction dir;

	  public SortColumn(Column<T> col, Direction dir) {
		this.col = col;
		this.dir = dir;
	  }

	  public Column<T> column() {
		return col;
	  }
	  public Direction direction() {
		return dir;
	  }

	  @Override
	  public boolean equals(Object obj) {
		return Internal.equals(this, obj, x -> x.dir, x -> x.col);
	  }

	  @Override
	  public int hashCode() {
		return Objects.hash(col, dir);
	  }
	}

	public static class ColumnValue<T> {
		private final Column<T> column;
		private final T value;

		public ColumnValue(Column<T> column, T value) {
			super();
			this.column = column;
			this.value = value;
		}

		public Column<T> getColumn() {
			return column;
		}

		public Literal<T> getValue() {
			return new Literal<>(value, column);
		}

		@Override
		public boolean equals(Object obj) {
			return Internal.equals(this, obj, ColumnValue::getColumn, ColumnValue::getValue);
		}

		@Override
		public int hashCode() {
			return column.hashCode();
		}
	}
}
