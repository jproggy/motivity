package org.jproggy.motivity;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.Condition.Not;
import org.jproggy.motivity.query.Condition.And;
import org.jproggy.motivity.query.Condition.Or;

public class Motivity {

  public static Condition not(Condition x) {
    return new Not(x);
  }

  public static Condition and(Condition... other) {
    return new And(other);
  }

  public static Condition or(Condition... other) {
    return new Or(other);
  }
}
