package org.jproggy.motivity;

import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.transaction.Transaction.Isolation;
import org.jproggy.motivity.transaction.spi.TransactionHandler;

public class Transactions {
  private Transactions() {
    // intentionally blank
  }
  private static TransactionHandler handler;

  public static TransactionHandler getHandler() {
    return handler;
  }

  public static void setHandler(TransactionHandler handler) {
    Transactions.handler = handler;
  }

  public static Transaction openNew() {
    return getHandler().openNew(null, false);
  }

  public static Transaction openNew(Isolation iso) {
    return getHandler().openNew(iso, false);
  }

  public static Transaction open() {
    return openable().openNew(null, false);
  }

  public static Transaction open(Isolation iso) {
    return openable().openNew(iso, false);
  }

  public static Transaction openNewReadonly() {
    return getHandler().openNew(null, true);
  }

  public static Transaction openNewReadonly(Isolation iso) {
    return getHandler().openNew(iso, true);
  }

  public static Transaction openReadonly() {
    return openable().openNew(null, true);
  }

  public static Transaction openReadonly(Isolation iso) {
    return openable().openNew(iso, true);
  }

  public static Transaction ensureOpen() {
    if (getHandler().isTransactionOpen()) return new InnerTransaction(handler.top());
    return getHandler().openNew(null, false);
  }

  public static void setRollbackonly() {
    handler.top().setRollbackOnly();
  }

  private static TransactionHandler openable() {
    if (getHandler().isTransactionOpen()) throw new IllegalStateException("is already running");
    return getHandler();
  }

  public static void requireTransaction() {
    if (!getHandler().isTransactionOpen()) throw new IllegalStateException("This action reqires an open transaction");
  }

  private static class InnerTransaction implements Transaction {
    private final Transaction wrapped;
    protected boolean         running = true;
    private boolean           begun;

    public InnerTransaction(Transaction wrapped) {
      super();
      this.wrapped = wrapped;
    }

    @Override
    public void commit() {
      running = false;
    }

    @Override
    public void rollback() {
      setRollbackOnly();
      running = false;
    }

    @Override
    public void close() {
      if (running) {
        setRollbackOnly();
      }
      running = false;
    }

    @Override
    public void begin() {
      if (running && begun) {
        throw new IllegalStateException("is already running");
      }
      running = true;
      begun = true;
    }

    @Override
    public void setRollbackOnly() {
      wrapped.setRollbackOnly();
    }

    @Override
    public boolean isReadOnly() {
      return wrapped.isReadOnly();
    }

    @Override
    public Isolation getIsolation() {
      return wrapped.getIsolation();
    }
  }
}
