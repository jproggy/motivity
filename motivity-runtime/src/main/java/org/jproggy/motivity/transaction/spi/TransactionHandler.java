package org.jproggy.motivity.transaction.spi;

import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.transaction.Transaction.Isolation;

public interface TransactionHandler {

  Transaction openNew(Isolation iso, boolean readOnly);

  boolean isTransactionOpen();

  Transaction top();
}
