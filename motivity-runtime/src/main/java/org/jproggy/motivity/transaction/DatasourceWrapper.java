package org.jproggy.motivity.transaction;

import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.logging.Logger;
import javax.sql.DataSource;

public abstract class DatasourceWrapper implements DataSource {

  @Override
  public PrintWriter getLogWriter() throws SQLException {
    return wrapped().getLogWriter();
  }

  protected abstract DataSource wrapped();

  @Override
  public <T> T unwrap(Class<T> iface) throws SQLException {
    return wrapped().unwrap(iface);
  }

  @Override
  public void setLogWriter(PrintWriter out) throws SQLException {
    wrapped().setLogWriter(out);
  }

  @Override
  public boolean isWrapperFor(Class<?> iface) throws SQLException {
    return wrapped().isWrapperFor(iface);
  }

  @Override
  public void setLoginTimeout(int seconds) throws SQLException {
    wrapped().setLoginTimeout(seconds);
  }

  @Override
  public Connection getConnection(String username, String password) throws SQLException {
    throw new UnsupportedOperationException();
  }

  @Override
  public int getLoginTimeout() throws SQLException {
    return wrapped().getLoginTimeout();
  }

  @Override
  public Logger getParentLogger() throws SQLFeatureNotSupportedException {
    return wrapped().getParentLogger();
  }
}
