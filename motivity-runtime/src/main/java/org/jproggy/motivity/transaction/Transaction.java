package org.jproggy.motivity.transaction;

import java.sql.Connection;

public interface Transaction extends AutoCloseable {
  enum Isolation {
    NONE(Connection.TRANSACTION_NONE),
    READ_COMMITTED(Connection.TRANSACTION_READ_COMMITTED),
    REPEATABLE_READ(Connection.TRANSACTION_REPEATABLE_READ),
    SERIALIZABLE (Connection.TRANSACTION_SERIALIZABLE );
    private final int sqlId;
    
    Isolation(int sqlId) {
      this.sqlId = sqlId;
    }
    
    public int asSql() {
      return sqlId;
    }
  }
  
  void begin();

  void commit();

  void rollback();
  
  void setRollbackOnly();
  
  Isolation getIsolation();
  
  boolean isReadOnly();
  
  void close();
}
