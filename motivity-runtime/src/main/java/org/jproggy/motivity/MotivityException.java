package org.jproggy.motivity;

public class MotivityException extends RuntimeException {
  private static final long serialVersionUID = 1L;

  public MotivityException() {
    super();
  }

  public MotivityException(String message) {
    super(message);
  }

  public MotivityException(Throwable cause) {
    super(cause);
  }

  public MotivityException(String message, Throwable cause) {
    super(message, cause);
  }
}
