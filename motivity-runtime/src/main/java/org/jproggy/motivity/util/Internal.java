package org.jproggy.motivity.util;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.Predicate;

public class Internal {
  @SafeVarargs
  @SuppressWarnings("Unchecked")
  public static <T> boolean equals(T one, Object obj, Function<T, ?>... fields) {
    if (one == obj) return true;
    if (obj == null) return false;
    if (one.getClass() != obj.getClass()) return false;
    T other = (T) obj;
    for (Function<T, ?> f: fields) {
      if (!java.util.Objects.equals(f.apply(one), f.apply(other))) {
        return false;
      }
    }
    return true;
  }

  @SafeVarargs
  @SuppressWarnings("Unchecked")
  public static <T> boolean equalsWithSuper(T one, Object obj, Predicate<Object> superCall, Function<T, ?>... fields) {
    if (one == obj) return true;
    if (!superCall.test(obj)) return false;
    T other = (T) obj;
    for (Function<T, ?> f: fields) {
      if (!java.util.Objects.equals(f.apply(one), f.apply(other))) {
        return false;
      }
    }
    return true;
  }

  public static <T> T[] add(T[] org, T x) {
    T[] dest = Arrays.copyOf(org, org.length + 1);
    dest[org.length] = x;
    return dest;
  }
}
