package org.jproggy.motivity.util;

public interface Property<R, T> extends Getter<R, T> {
    void setValue(R item, T value);

    String getName();
}
