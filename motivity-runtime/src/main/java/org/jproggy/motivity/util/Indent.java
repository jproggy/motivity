package org.jproggy.motivity.util;

import org.jproggy.snippetory.spi.EncodedData;
import org.jproggy.snippetory.sql.SQL;

public class Indent implements EncodedData {
  private final int depth;
  private final int count;
  private static String indent = "                 ";

  private static synchronized void ensure(int length) {
    while (indent.length() < length) {
      indent += indent;
    }
  }

  public Indent() {
    this(2);
  }

  public Indent(int depth) {
    this(depth, 1);
  }

  private Indent(int depth, int count) {
    this.depth = depth;
    this.count = count;
    ensure(length());
  }

  public Indent indent() {
    return new Indent(depth, count + 1);
  }

  @Override
  public String toString() {
    return indent.substring(0, length());
  }

  private int length() {
    return depth * count;
  }

  @Override
  public String getEncoding() {
    return SQL.ENCODING.getName();
  }

  @Override
  public CharSequence toCharSequence() {
    return toString();
  }
}
