package org.jproggy.motivity.util;

public interface Persistent {
  void inSync();
  boolean isNew();
  boolean isModified();
}
