package org.jproggy.motivity.util;

import java.util.Date;

public class Dates {
  public static Date today() {
    return Calendars.today().getTime();
  }
}
