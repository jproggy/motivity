package org.jproggy.motivity.util;

import java.util.Calendar;
import java.util.GregorianCalendar;

public class Calendars {
  public static Calendar today() {
    Calendar date = new GregorianCalendar();
    date.set(GregorianCalendar.HOUR_OF_DAY, 0);
    date.set(GregorianCalendar.MINUTE, 0);
    date.set(GregorianCalendar.SECOND, 0);
    date.set(GregorianCalendar.MILLISECOND, 0);
    return date;
  }
}
