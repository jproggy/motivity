package org.jproggy.motivity.util;

@FunctionalInterface
public interface Getter<R, T> {

    T getValue(R item);

}
