-- Syntax:sql

-- $select{
SELECT :selectCols/*delimiter=', '*/
FROM $primaryTable
-- $leftJoin{
LEFT OUTER JOIN $additionalTable ON (:joinCondition)
-- }$
-- $join{
JOIN $additionalTable ON (:joinCondition)
-- }$
-- ${
WHERE :whereExpression
-- }$
-- ${
GROUP BY :groupCols/*delimiter=', '*/
-- }$
-- ${
HAVING :havingExpression
-- }$
-- ${
ORDER BY :orderCols/*delimiter=', '*/
-- }$
-- $forUpdate{
FOR UPDATE
-- }$
-- }$
;
-- $delete{
DELETE FROM $primaryTable
-- ${
WHERE :whereExpression
-- }$
-- }$
;
--$insert{
INSERT INTO $primaryTable /*${*/ (:column/*delimiter=', '*/) /*}$*/
VALUES (:value/*delimiter=', '*/)
-- }$
;
-- $update{
UPDATE $primaryTable
SET /*$columns(delimiter=', '){*/ $column = :value /*}$*/
-- ${
WHERE :whereExpression
-- }$
-- }$
;
$asc{ :col ASC}$
$desc{ :col DESC}$
;
-- $topQuery{
:query
LIMIT :count
-- $derby{
SELECT * FROM (
    SELECT x.*, ROW_NUMBER() OVER() AS rownum
    FROM (
    $query
    ) as x
) AS tmp
WHERE rownum <= :count
-- }derby$
-- }topQuery$
;
-- $windowQuery{
SELECT * FROM (
    SELECT result.*, ROW_NUMBER() OVER() AS rownum
    FROM (
    $query
    ) as result
) AS tmp
WHERE rownum > :start AND rownum <= :end
-- $postgresql{
:query
LIMIT :numRows${ OFFSET :start}$
-- }$
-- $mysql{
:query
LIMIT :numRows${, :start}$
-- }$
-- }windowQuery$

-- $countQuery{
SELECT count(*) 
FROM (
$query
) as x
-- }$

$distinct{
 DISTINCT :selectCols/*delimiter=', '*/
}$

         $Equal{ :origin = :data}$
      $NotEqual{ :origin <> :data}$
          $Less{ :origin < :data}$
$GreaterOrEqual{ :origin >= :data}$
       $Greater{ :origin > :data}$
   $LessOrEqual{ :origin <= :data}$
          $Like{ :origin LIKE :data ESCAPE '\'$mysql{ :origin LIKE :data}$}$
       $NotLike{ :origin NOT LIKE :data ESCAPE '\'$mysql{ :origin NOT LIKE :data}$}$
        $IsNull{ :origin IS NULL}$
     $IsNotNull{ :origin IS NOT NULL}$
            $In{ :origin IN( :data/*delimiter=', '*/ )}$
         $NotIn{ :origin NOT IN( :data/*delimiter=', '*/ )}$
       $Between{ :origin BETWEEN :from AND :to}$
            $as{ :origin AS :alias }$
        $IfNull{ COALESCE(:origin, :replacement)}$
        $Concat{ :origin || :data/*delimiter=' || '*/$mysql{ CONCAT(:origin, :data/*delimiter=', '*/)}$}$
           $Sum{ SUM(:column) }$
           $Min{ MIN(:column) }$
           $Max{ MAX(:column) }$
         $Count{ COUNT(:column) $derby{cast(COUNT(:column) as BIGINT)}$}$

$tuple{:data/*delimiter=', '*/}$

$exists{ EXISTS
/*$i*/(
/*$i*/SELECT * FROM $table
/*$i*/WHERE :filter 
/*$i*/)}$

$and{ 
/*$i*/(:start
$more{
/*$i*/AND
/*$i*/:x}$)}$

$or{
/*$i*/(:start
$more{
/*$i*/OR
/*$i*/:x}$)}$

$not{ NOT :x}$

$fetch{
VALUES IDENTITY_VAL_LOCAL()
}$

$nextVal{
SELECT NEXT VALUE FOR $sequence;
$derby{select t from ( values next value for $sequence ) s( t)}$
    $postgresql{SELECT nextval('$sequence')}$
    $oralce{select $sequence$.nextval from dual}$
}$

