-- Syntax:sql
-- $condition{
--   $orNotExists{
 (a
  OR
   NOT EXISTS
  (
  SELECT * FROM other 
  WHERE  other.simple_id = ?
  ))
--   }$
--   $andExists{
(a
  AND
   ( EXISTS
  (
  SELECT * FROM TestTbl 
  WHERE  ( TestTbl.simple_id > ?) 
  )))
--   }$
-- }$
-- $select{
--   $simple{
SELECT TestTbl.simple_id
FROM TestTbl
--   }$
--   $lessOrEqual{
SELECT TestTbl.simple_id
FROM TestTbl
WHERE TestTbl.simple_id <= ?
--   }$
--   $group{
SELECT TestTbl.simple_id, COUNT(TestTbl.simple_id) AS count
FROM TestTbl
GROUP BY TestTbl.simple_id
--   }$
--   $sort{
SELECT TestTbl.simple_id,   COUNT(TestTbl.simple_id)  AS count 
FROM TestTbl 
WHERE  ( TestTbl.name LIKE ? ESCAPE '\'
    AND
     TestTbl.simple_id BETWEEN ? AND ?)
ORDER BY  count ASC 
--   }$
--   $join{
SELECT other.other_id, TestTbl.simple_id
FROM other 
LEFT OUTER JOIN TestTbl ON  ( TestTbl.simple_id = other.simple_id)
WHERE  TestTbl.name = ?
--   }$
-- }$
-- $query{
-- $columns{TestTbl.ext_id, TestTbl.price, TestTbl.simple_id, TestTbl.name}$
--   $simple{
SELECT :columns
FROM TestTbl 
--   }$
--   $greater{
SELECT :columns
FROM TestTbl
WHERE TestTbl.simple_id > ?
--   }$
--   $sort{
SELECT :columns
FROM TestTbl
ORDER BY TestTbl.simple_id DESC
--   }$
--   $distinct{
SELECT DISTINCT :columns
FROM TestTbl
--   }$
--   $join{
SELECT other.simple_id, other.other_id
FROM other 
LEFT OUTER JOIN TestTbl ON (TestTbl.simple_id = other.simple_id)
WHERE  ( TestTbl.simple_id = ?
    AND
     other.other_id LIKE ? ESCAPE '\')
--   }$
--   $limit{
SELECT :columns
FROM TestTbl
LIMIT ?
--   }$
--   $skipLimit{
SELECT * FROM (
    SELECT result.*, ROW_NUMBER() OVER() AS rownum
    FROM (
    SELECT TestTbl.ext_id, TestTbl.price, TestTbl.simple_id, TestTbl.name
FROM TestTbl

    ) as result
) AS tmp
WHERE rownum > ? AND rownum <= ?
--   }$
-- }$
