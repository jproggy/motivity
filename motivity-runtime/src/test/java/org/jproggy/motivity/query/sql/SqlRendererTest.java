package org.jproggy.motivity.query.sql;

import org.junit.Before;
import org.junit.Test;

import org.jproggy.motivity.query.ColumnAlias;
import org.jproggy.motivity.query.impl.Term;
import org.jproggy.motivity.sql.Vocabulary;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.query.SelectRenderer;
import org.jproggy.motivity.sql.query.SqlRenderer;
import org.jproggy.motivity.util.Indent;
import org.jproggy.snippetory.sql.SQL;
import org.jproggy.snippetory.toolyng.test.Normalize;

public class SqlRendererTest {
  private Vocabulary voc = DbConnector.dummy().getVocabulary();
  private SqlRenderer c = new SqlRenderer(voc);
  private SelectRenderer select = new SelectRenderer(voc);
  private Indent i = new Indent(2);
  ColumnAlias<Integer> a;
  ColumnAlias<Integer> b;
  ColumnAlias<String> d = new ColumnAlias<>("d", TestDb.other.otherId);

  static {
    SQL.SYNTAX.getName();
  }

  @Before
  public void before() {
    a = new ColumnAlias<>("a", TestDb.other.simpleId);
    b = new ColumnAlias<>("b", TestDb.simple.simpleId);
  }

  @Test
  public void def() {
    assertSimilar("a", render(a));
    assertSimilar("other.simple_id AS a ", select.render(a, new Indent(2)).toString());
  }

  @Test
  public void expression() {
    assertSimilar(" a = b", render(a.equalTo(b)));
    assertSimilar(" a = ?", render(a.equalTo(3)));
    assertSimilar(" a IN( ?, ?, ? )", render(a.in(1,2,3)));
    assertSimilar(" a BETWEEN ? AND ?", render(a.between(1,3)));
    assertSimilar(" a < b", render(a.lessThen(b)));
    assertSimilar(" a >= b", render(a.greaterOrEqual(b)));
    assertSimilar(" a IS NULL", render(a.isNull()));
    assertSimilar(" COALESCE(a, ?)", render(a.ifNull(0)));
    assertSimilar(" d || ? || ?", render(d.concat("test").concat("x")));
  }

  @Test
  public void condition() {
    assertSimilar(" ( a > ? AND d IS NOT NULL AND d NOT LIKE ? ESCAPE '\\')", render(
            a.greaterThen(3).and(d.isNotNull()).and(d.notLike("%")))
    );
  }

  private void assertSimilar(String o1, String o2) {
    Normalize.assertNormEquals(o1, o2);
  }

  private String render(Term t) {
    return c.render(t, i).toString();
  }
}
