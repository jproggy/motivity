/// Copyright JProggy
///
/// Licensed under the Apache License, Version 2.0 (the "License");
/// you may not use this file except in compliance with the License.
/// You may obtain a copy of the License at
///
///     http://www.apache.org/licenses/LICENSE-2.0
///
/// Unless required by applicable law or agreed to in writing, software
/// distributed under the License is distributed on an "AS IS" BASIS,
/// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
/// See the License for the specific language governing permissions and
/// limitations under the License.

package org.jproggy.motivity.query.sql;

import static java.util.Arrays.asList;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Column;
import org.jproggy.motivity.sql.DeleteStmt;
import org.jproggy.motivity.sql.InsertStatement;
import org.jproggy.motivity.sql.MultiDbTest;
import org.jproggy.motivity.sql.UpdateStatement;
import org.jproggy.motivity.sql.query.SelectMultiCol;
import org.jproggy.motivity.transaction.Transaction;

@RunWith(Parameterized.class)
public class DBAccessTest extends MultiDbTest {
  SimpleTable store;

  public DBAccessTest(DbHandle con) throws Exception {
    super(con,"DbAccessRepo.sql");
  }

  @Before
  public void init() throws Exception {
    this.store = new SimpleTable(cons);
    if (hasTable(cons, SimpleTable.TABLE_NAME)) {
      run("clearSimpleTable");
    } else {
      run("createSimpleTable");
    }
    run("fillSimpleTable");
  }

  @Test
  public void test() throws Exception {
    List<SimpleTable.Simple> data = store.filter(store.simpleId.greaterThen(1)).list();
    assertThat(data.size(), greaterThanOrEqualTo(4));
    assertEquals(2, store.filter(store.name.like("%o")).count());
    assertEquals("Kuno", store.filter(store.name.like("%o")).first().orElseThrow().getName());
  }

  @Test
  public void group() {
    Column<Long> count = store.simpleId.count().as("counter");
    SelectMultiCol query = new SelectMultiCol(store, store.name, count);
    query.group(store.name);
    List<Object[]> result = query.list();
    assertThat(result.size(), equalTo(5));
  }

  @Test
  public void names() {
    List<String> names = store.select(store.name).filter(store.extId.equalTo("test2")).list();
    assertThat(names, contains("Kuno", "Karl"));
  }

  @Test
  public void groupSortedCount() {
    Column<Long> count = store.simpleId.count().as("counter");
    SelectMultiCol query = new SelectMultiCol(store, store.simpleId, count);
    query.group(store.simpleId).sorted(count.asc());
    List<Object[]> result = query.list();
    assertThat(result.size(), equalTo(5));
    assertThat((Long) result.get(1)[1], greaterThanOrEqualTo((Long) result.get(0)[1]));
  }

  @Test
  public void groupSortedId() {
    Column<Long> count = store.simpleId.count().as("counter");
    SelectMultiCol query = new SelectMultiCol(store, store.simpleId, count);
    List<Object[]> result = query.group(store.simpleId).sorted(store.simpleId.asc()).list();
    assertThat(result.size(), equalTo(5));
    assertThat((Integer) result.get(1)[0], greaterThanOrEqualTo((Integer) result.get(0)[0]));
  }

  @Test
  public void groupHavingCount() {
    Column<Long> count = store.extId.count().as("counter");
    SelectMultiCol query = new SelectMultiCol(store, store.extId, count);
    query.group(store.extId).having(store.extId.count().greaterThen(1L));
    List<Object[]> result = query.list();
    assertThat(result.size(), is(1));
    assertThat(result.get(0)[0], is("test2"));
  }

  @Test
  public void groupHavingId() {
    Column<Long> count = store.extId.count().as("counter");
    SelectMultiCol query = new SelectMultiCol(store, store.extId, count);
    query.group(store.extId).having(store.extId.greaterThen("0"));
    List<Object[]> result = query.list();
    assertThat(result.size(), is(4));
  }

  @Test
  public void update() {
    long result = new UpdateStatement(store, store.extId.to("other"))
            .where(store.name.equalTo("Bruno")).toStatement().executeUpdate();
    assertThat(result, is(1L));
  }

  @Test
  public void insert() {
    long result = new InsertStatement(store, asList(
            store.extId.to("other one"),
            store.name.to("Sert Inn"),
            store.price.to(BigDecimal.TEN))
    ).toStatement().executeUpdate();
    assertThat(result, is(1L));
  }

  @Test
  public void delete() {
    long result = new DeleteStmt(store).where(store.extId.equalTo("other one")).toStatement().executeUpdate();
    assertThat(result, is(0L));
    result = new DeleteStmt(store).where(store.extId.equalTo("test2")).toStatement().executeUpdate();
    assertThat(result, is(2L));
  }

}
