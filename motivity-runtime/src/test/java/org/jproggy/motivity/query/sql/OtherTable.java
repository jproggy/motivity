package org.jproggy.motivity.query.sql;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.sql.OtherTable.Other;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.map.Converters;
import org.jproggy.motivity.util.Persistent;

public class OtherTable extends Table<Other> {
  public final ColumnDefinition<Other, String> otherId;
  public final ColumnDefinition<Other, Integer> simpleId;

  public OtherTable() {
    super("other", DbConnector.dummy());
    otherId = column("other_id", (x, y) -> {;}, x -> "", Converters.VARCHAR);
    simpleId = column("simple_id", (x, y) -> {;}, x -> 0, Converters.INTEGER);
  }

  public SimpleTable simple() {
    return join(TestDb.simple, TestDb.simple.simpleId.equalTo(TestDb.other.simpleId));
  }

  @Override
  public Other create(ResultSet rs) throws SQLException {
    return null;
  }

  @Override
  public Condition pk(Other instance) {
    return null;
  }

  @Override
  public boolean update(Other instance) {
    return false;
  }

  @Override
  public boolean insert(Other instance) {
    return false;
  }

  public static class Other implements Persistent {
    @Override
    public boolean isModified()
    {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isNew()
    {
        // TODO Auto-generated method stub
        return false;
    }
    
    @Override
    public void inSync()
    {
        // TODO Auto-generated method stub
        
    }
  }
}
