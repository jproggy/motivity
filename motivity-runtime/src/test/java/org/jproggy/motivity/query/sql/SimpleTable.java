package org.jproggy.motivity.query.sql;

import static org.jproggy.motivity.Motivity.and;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.Column.ColumnValue;
import org.jproggy.motivity.query.Condition;
import org.jproggy.motivity.query.sql.SimpleTable.Simple;
import org.jproggy.motivity.sql.ColumnDefinition;
import org.jproggy.motivity.sql.Table;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.id.IdData;
import org.jproggy.motivity.sql.id.IdGenerator.Strategy;
import org.jproggy.motivity.sql.id.IdProvider;
import org.jproggy.motivity.sql.map.Converters;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.motivity.util.Persistent;

public class SimpleTable extends Table<Simple> {
  public static final String TABLE_NAME = "TestTbl";
  public final ColumnDefinition<Simple, Integer> simpleId;
  public final ColumnDefinition<Simple, String> name;
  public final ColumnDefinition<Simple, BigDecimal> price;
  public final ColumnDefinition<Simple, String> extId;
  private final IdData idData = new IdData(TABLE_NAME, Strategy.INCREMENT);

  public SimpleTable() {
    this(DbConnector.dummy());
  }

  public SimpleTable(DbConnector cons) {
    super(TABLE_NAME, cons);
    simpleId = column("simple_id", Simple::setId, Simple::getId, Converters.INTEGER);
    name = column("name", Simple::setName, Simple::getName, Converters.VARCHAR);
    price = column("price", Simple::setPrice, Simple::getPrice, Converters.DECIMAL);
    extId = column("ext_id", Simple::setExtId, Simple::getExtId, Converters.VARCHAR);
  }

  @Override
  public Simple create(ResultSet rs) throws SQLException {
    return new Simple();
  }

  @Override
  public Condition pk(Simple instance) {
    return and(
        simpleId.equalTo(instance.getId())
    );
  }

  @Override
  public boolean update(Simple instance) {
    long rows = filter(pk(instance)).update(
        simpleId.to(instance.getId()),
        name.to(instance.getName()),
        price.to(instance.getPrice()),
        extId.to(instance.getExtId())
    );
    instance.inSync();
    return rows == 1;
  }

  @Override
  public boolean insert(Simple instance) {
    return insert(instance, getDbConnector().getIdGenerator(idData, simpleId));
  }

  protected List<ColumnValue<?>> getColumnValues(Simple instance) {
    List<ColumnValue<?>> values = new ArrayList<>();
    if (instance.getId() != 0) values.add(simpleId.to(instance.getId()));
    values.add(name.to(instance.getName()));
    values.add(price.to(instance.getPrice()));
    values.add(extId.to(instance.getExtId()));
    return values;
  }

  public static class Simple implements Persistent {
    private int id;
    private String name;
    private BigDecimal price;
    private String extId;
    private boolean isNew = true;

    public BigDecimal getPrice()
    {
      return price;
    }

    public void setPrice(BigDecimal price)
    {
      this.price = price;
    }

    public String getExtId()
    {
      return extId;
    }

    public void setExtId(String extId)
    {
      this.extId = extId;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public int getId() {
      return id;
    }

    public void setId(int id) {
      this.id = id;
    }
    @Override
    public boolean isModified()
    {
      return false;
    }

    @Override
    public boolean isNew()
    {
      return isNew;
    }

    @Override
    public void inSync()
    {
      isNew = false;
    }
  }
}
