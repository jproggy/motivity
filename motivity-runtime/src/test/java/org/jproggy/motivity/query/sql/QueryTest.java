package org.jproggy.motivity.query.sql;

import static org.jproggy.motivity.Motivity.and;
import static org.jproggy.motivity.query.sql.TestDb.other;
import static org.jproggy.motivity.query.sql.TestDb.simple;

import org.jproggy.motivity.query.sql.SimpleTable.Simple;
import org.jproggy.motivity.sql.query.Query;
import org.jproggy.snippetory.sql.Statement;
import org.jproggy.snippetory.toolyng.test.Normalize;
import org.junit.Test;

public class QueryTest {
  static TestResults results = new TestResults("query");

  @Test
  public void simple() {
    Query<Simple> query = new Query<>(new SimpleTable());
    Normalize.assertNormEquals(results.test("simple"), query.toStatement().toString());
  }

  @Test
  public void greater() {
     Query<SimpleTable.Simple> query = new Query<>(simple);
    query.filter(simple.simpleId.greaterThen(3));
    Normalize.assertNormEquals(results.test("greater"), query.toStatement().toString());
  }

  @Test
  public void sort() {
    Query<SimpleTable.Simple> query = new Query<>(simple);
    query.sorted(simple.simpleId.desc());
    Normalize.assertNormEquals(results.test("sort"), query.toStatement().toString());
  }

  @Test
  public void distinct() {
    Query<SimpleTable.Simple> query = new Query<>(simple);
    query.distinct();
    Normalize.assertNormEquals(results.test("distinct"), query.toStatement().toString());
  }

  @Test
  public void first() {
    Query<SimpleTable.Simple> query = new Query<>(simple);
    query.limit(10);
    Normalize.assertNormEquals(results.test("limit"), query.toStatement().toString());
  }

  @Test
  public void page() {
    Query<SimpleTable.Simple> query = new Query<>(simple);
    query.page(10, 10);
    Normalize.assertNormEquals(results.test("skipLimit"), query.toStatement().toString());
  }

  @Test
  public void joinExplicit() {
    Query<Object> query = new Query<>(other);
    SimpleTable simple = other.simple();
    Statement statement = query.filter(
            and(
                    simple.simpleId.equalTo(5),
                    other.otherId.like("x%")
            )
    ).toStatement();
    Normalize.assertNormEquals(results.test("join"), statement.toString());
  }

  @Test
  public void join() {
    Query<Object> query = new Query<>(other);
    Statement statement = query.filter(
            and(
                    other.simple().simpleId.equalTo(5),
                    other.otherId.like("x%")
            )
    ).toStatement();
    Normalize.assertNormEquals(results.test("join"), statement.toString());
  }
}
