package org.jproggy.motivity.query.sql;

import org.junit.Test;

import org.jproggy.motivity.query.Column;
import org.jproggy.motivity.sql.query.SelectMultiCol;
import org.jproggy.snippetory.Repo;
import org.jproggy.snippetory.Template;
import org.jproggy.snippetory.toolyng.test.Normalize;

public class SelectMultiColTest {
  static Template results = Repo.readResource("org/jproggy/motivity/query/sql/TestResults.sql").parse().get("select");

  @Test
  public void simple() {
    SimpleTable from = new SimpleTable();
    SelectMultiCol query = new SelectMultiCol(from, from.simpleId);
    Normalize.assertNormEquals(results.get("simple").toString(), query.toStatement().toString());
  }

  @Test
  public void notGreater() {
    SimpleTable from = new SimpleTable();
    SelectMultiCol query = new SelectMultiCol(from, from.simpleId);
    query.filter(from.simpleId.lessOrEqual(3));
    Normalize.assertNormEquals(results.get("lessOrEqual").toString(), query.toStatement().toString());
  }

  @Test
  public void group() {
    SimpleTable from = new SimpleTable();
    Column<Long> count = from.simpleId.count().as("count");
    SelectMultiCol query = new SelectMultiCol(from, from.simpleId, count);
    query.group(from.simpleId);
    Normalize.assertNormEquals(results.get("group").toString(), query.toStatement().toString());
  }

  @Test
  public void sort() {
    SimpleTable from = new SimpleTable();
    Column<Long> count = from.simpleId.count().as("count");
    SelectMultiCol query = new SelectMultiCol(from, from.simpleId, count);
    query.filter(from.name.like("snip%").and(from.simpleId.between(3, 3333)));
    query.sorted(count.asc());
    Normalize.assertNormEquals(results.get("sort").toString(), query.toStatement().toString());
  }

  @Test
  public void join() {
    SimpleTable o = new SimpleTable();
    OtherTable from = new OtherTable();
    SelectMultiCol query = new SelectMultiCol(from, from.otherId, from.simple().simpleId);
    query.filter(from.simple().name.equalTo("Ghia"));
    Normalize.assertNormEquals(results.get("join").toString(), query.toStatement().toString());
  }
}
