package org.jproggy.motivity.query.sql;

import org.jproggy.snippetory.Repo;
import org.jproggy.snippetory.Template;

public class TestResults {
  private final Template results;

  public TestResults(String area) {
    results = Repo.readResource("org/jproggy/motivity/query/sql/TestResults.sql").parse().get(area);
  }

  public String test(String name) {
    return results.get(name).set("columns", results.get("columns")).toString();
  }

}
