package org.jproggy.motivity.sql.id;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import org.jproggy.motivity.query.sql.SimpleTable;
import org.jproggy.motivity.sql.MultiDbTest;

@RunWith(Parameterized.class)
public class IncrementGeneratorTest extends MultiDbTest {
  SimpleTable store;

  public IncrementGeneratorTest(DbHandle con) throws Exception {
    super(con, "DbAccessRepo.sql");
  }

  @Before
  public void setUp() throws Exception{
    this.store = new SimpleTable(cons);
    if (hasTable(cons, SimpleTable.TABLE_NAME)) {
      store.all().delete();
    } else {
      run("createSimpleTable");
    }
  }

  @Test
  public void generates() {
    var simple = new SimpleTable.Simple();
    assertEquals(0, simple.getId());
    simple.setName("Egon Vasiliev");
    simple.setPrice(BigDecimal.valueOf(123.45));
    simple.setExtId("aa-123");
    store.insert(simple);
    assertThat(simple.getId(), greaterThan(0));
  }

}