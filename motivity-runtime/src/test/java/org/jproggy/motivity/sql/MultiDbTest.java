package org.jproggy.motivity.sql;

import static java.util.Arrays.asList;

import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.After;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import org.jproggy.motivity.MotivityException;
import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.sql.connect.DbConnector;
import org.jproggy.motivity.sql.transaction.SqlTransactionHandler;
import org.jproggy.motivity.transaction.Transaction;
import org.jproggy.snippetory.UriResolver;
import org.jproggy.snippetory.sql.Repository;
import org.jproggy.snippetory.sql.SqlContext;
import org.jproggy.snippetory.sql.spi.ConnectionProvider;
import org.jproggy.snippetory.sql.spi.VariantResolver;

public abstract class MultiDbTest {
  protected final DbConnector cons;
  protected Repository repo;
  final SqlTransactionHandler handler;

  public MultiDbTest(DbHandle con, String repoName) throws Exception {
    handler = new SqlTransactionHandler();
    cons = handler.registerConnection("Db", con, con.getVariant());
    Transactions.setHandler(handler);
    this.repo = buildRepo(repoName);
  }
  public MultiDbTest(DbConnector con, String repoName) throws Exception {
    cons = con;
    handler = null;
    this.repo = buildRepo(repoName);
  }

  protected long run(String stmtName) {
    try (Transaction tra = Transactions.ensureOpen()) {
      long count = repo.get(stmtName).executeUpdate();
      tra.commit();
      return count;
    }
  }

  @Parameters(name = "{0}")
  public static Iterable<DbHandle[]> connections() {
    return handles().map(c -> new DbHandle[] {c}).collect(Collectors.toList());
  }

  @After
  public void close() {
    if (handler != null) {
      handler.close();
    }
  }

  public static Stream<DbHandle> handles() {
    new File("target/test/db/sqlite").mkdirs();
    List<String[]> result = new ArrayList<>();
    if (System.getProperty("motivity.test.dbUser") != null) {
      result.add(new String[]{
          "mysql",
          "jdbc:mysql://localhost:3306/motivity_test",
          System.getProperty("motivity.test.dbUser"),
          System.getProperty("motivity.test.dbPassword")});
      result.add(new String[]{"postgresql", "jdbc:postgresql://localhost:5432/motivity_test",
          "motivity",
          "m0tivity"});
    }
    result.addAll(asList(
        new String[]{"derby", "jdbc:derby:target/test/db/derby/motivity_test;create=true"},
        new String[]{"derby", "jdbc:derby:memory:motivity_test;create=true"},
        new String[]{"sqlite", "jdbc:sqlite:target/test/db/sqlite/motivity_test.db"},
        new String[]{"hsql", "jdbc:hsqldb:mem:motivity_test"},
        new String[]{"hsql", "jdbc:hsqldb:file:target/test/db/hsql/motivity_test"}
    ));
    return result.stream().map(MultiDbTest::toConnections);
  }

  private static DbHandle toConnections(String[] params) {
    return new DbHandle() {
      @Override
      public String getVariant() {
        return params[0];
      }

      @Override
      public Connection getConnection() {
        try {
          if (params.length == 2) {
            return DriverManager.getConnection(params[1]);
          }
          return DriverManager.getConnection(params[0], params[2], params[3]);
        } catch (Exception e) {
          throw new MotivityException(e);
        }
      }

      @Override
      public String toString() {
        return params[1];
      }
    };
  }

  private Repository buildRepo(String name) {
    SqlContext ctx = new SqlContext().connections(cons);
    ctx.postProcessor(VariantResolver.wrap(cons.getVariant()));
    ctx.uriResolver(UriResolver.resource("org/jproggy/motivity/query/sql"));
    return ctx.getRepository(name);
  }

  public static boolean hasTable(DbConnector cons, String tableName) throws Exception {
    try (ResultSet rs = cons.getConnection().getMetaData().getTables(null, "%", "%", null)) {
      while (rs.next()) {
        if (tableName.equalsIgnoreCase(rs.getString("TABLE_NAME"))) return true;
      }
    }
    return false;
  }
  public interface DbHandle extends ConnectionProvider {

    String getVariant();
  }
}
