package org.jproggy.motivity.sql.transaction;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import org.jproggy.motivity.Transactions;
import org.jproggy.motivity.query.sql.SimpleTable;
import org.jproggy.motivity.query.sql.SimpleTable.Simple;
import org.jproggy.motivity.sql.MultiDbTest;
import org.jproggy.motivity.transaction.Transaction;

@RunWith(Parameterized.class)
public class SqlTransactionHandlerTest extends MultiDbTest {
  SimpleTable store;

  public SqlTransactionHandlerTest(DbHandle con) throws Exception {
    super(con, "DbAccessRepo.sql");
  }

  @Before
  public void setup() throws Exception {
    store = new SimpleTable(cons);
    if (hasTable(cons, SimpleTable.TABLE_NAME)) {
      store.all().delete();
    } else {
      run("createSimpleTable");
    }
  }

  @Test
  public void rollbackTransaction() throws Exception {
    long countBefore = store.all().count();
    try (Transaction transaction = Transactions.openNew();) {
      store.insert(getInstance());
      transaction.rollback();
    }
    assertEquals(countBefore, store.all().count());
  }

  @Test
  public void commitTransaction() throws Exception {
    long countBefore = store.all().count();
    try (Transaction transaction = Transactions.openNew()) {
      store.insert(getInstance());
      transaction.commit();
    }
    assertEquals(countBefore + 1, store.all().count());
  }

  private static Simple getInstance() {
    Simple simple = new Simple();
    simple.setExtId("ext1");
    simple.setName("Karl");
    simple.setPrice(BigDecimal.valueOf(100));
    return simple;
  }
}