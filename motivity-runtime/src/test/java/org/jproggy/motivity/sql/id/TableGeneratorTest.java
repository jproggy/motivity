package org.jproggy.motivity.sql.id;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import org.jproggy.motivity.query.sql.SimpleTable;
import org.jproggy.motivity.query.sql.SimpleTable.Simple;
import org.jproggy.motivity.sql.MultiDbTest;

@RunWith(Parameterized.class)
public class TableGeneratorTest extends MultiDbTest {
  public static final String TABLE_NAME = "IdTableTest";
  private final IdData idData = new IdData(TABLE_NAME, IdGenerator.Strategy.TABLE);

  public TableGeneratorTest(DbHandle con) throws Exception {
    super(con, "TableGenRepo.sql");
    this.store = new SimpleTable(cons);
  }
  final SimpleTable store;

  @Before
  public void setUp() throws Exception{
    if (!hasTable(cons, TableGenerator.TABLE_NAME)) {
      run("createIdTable");
    }
  }

  @Test
  public void getNextId() throws Exception {
    IdProvider<Simple, Integer> ids = cons.getIdGenerator(idData, store.simpleId);
    Simple instance = new Simple();
    ids.afterwards(instance, null);
    assertEquals(0, instance.getId());
    ids.before(instance);
    int id = instance.getId();
    ids.before(instance);
    assertEquals(id + 1, instance.getId());
    cons.getIdGenerator(idData, store.extId).before(instance);
    ids.before(instance);
    assertEquals("" + (id + 2), instance.getExtId());
  }

}