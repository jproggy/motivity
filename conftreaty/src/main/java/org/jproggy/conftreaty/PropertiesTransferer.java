package org.jproggy.conftreaty;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;
import java.util.NavigableMap;
import java.util.Properties;
import java.util.TreeMap;

public class PropertiesTransferer {
  private static final Class<?>[] STRING_ARG = new Class[] { String.class };
  private final NavigableMap<String, String> data = new TreeMap<>();

  public PropertiesTransferer(Properties conf) {
    conf.forEach((k, v) -> data.put(k.toString(), v.toString()));
  }

  public <T> T transfer(Class<T> to) {
    try {
      return transferRoot(to);
    } catch (ReflectiveOperationException e) {
      throw new IllegalArgumentException("Can't use " + to, e);
    }
  }

  private <T> T transferRoot(Class<T> to) throws ReflectiveOperationException {
    Path annotation = to.getAnnotation(Path.class);
    if (annotation == null) {
      return transfer(to, "");
    }
    return transfer(to, annotation.value());
  }

  private <T> T transfer(Class<T> to, String parentPath) throws ReflectiveOperationException {
    T holder = to.getConstructor().newInstance();
    for (Field f : to.getDeclaredFields()) {
      if ((f.getModifiers() & Modifier.FINAL) == 0) {
        transferField(holder, f, parentPath, to.getAnnotation(Path.class));
      }
    }
    return holder;
  }

  private <T> void transferField(T holder, Field f, String parenPath, Path fallback) throws ReflectiveOperationException {
    f.setAccessible(true);
    for (String p : candidates(f, parenPath, fallback)) {
      if (data.containsKey(p)) {
        String property = data.get(p);
        if (property != null) f.set(holder, fromString(f.getType(), property));
        return;
      }
      if (transferChild(holder, f, p)) return;
    }
  }

  private List<String> candidates(Field f, String parenPath, Path fallback) {
    if (fallback == null) {
      if (parenPath.isEmpty()) return singletonList(name(f));
      return singletonList(parenPath + '.' + name(f));
    }
    Path annotation = f.getAnnotation(Path.class);
    if (parenPath.isEmpty()) {
      if (annotation == null) return singletonList(fallback.value() + '.' + f.getName());
      return singletonList(annotation.value());
    }
    if (annotation == null) return asList(parenPath + '.' + f.getName(), fallback.value() + '.' + f.getName());
    return asList(parenPath + '.' + annotation.value(), annotation.value());
  }

  private String name(Field f) {
    Path annotation = f.getAnnotation(Path.class);
    if (annotation == null) return f.getName();
    return annotation.value();
  }

  private <T> boolean transferChild(T holder, Field f, String parentPath) throws ReflectiveOperationException {
    Class<?> type = f.getType();
    if (type.isPrimitive() || getPackageName(type).startsWith("java.")) return false;
    for (Constructor<?> c: type.getConstructors()) {
      if (c.getParameterTypes().length == 0) {
        f.set(holder, transfer(type, parentPath));
        return true;
      }
    }
    return false;
  }

  private String getPackageName(Class<?> type) {
    Package pck = type.getPackage();
    if (pck == null) return "";
    return pck.getName();
  }

  private Object fromString(Class<?> type, String property) {
    if (String.class.isAssignableFrom(type)) {
      return property;
    }
    if (Boolean.class.isAssignableFrom(type)) {
      return Boolean.valueOf(property);
    }
    if (Boolean.TYPE.isAssignableFrom(type)) {
      return Boolean.valueOf(property);
    }
    if (Long.class.isAssignableFrom(type)) {
      return Long.valueOf(property);
    }
    if (Long.TYPE.isAssignableFrom(type)) {
      return Integer.valueOf(property);
    }
    if (Integer.class.isAssignableFrom(type)) {
      return Integer.valueOf(property);
    }
    if (Integer.TYPE.isAssignableFrom(type)) {
      return Integer.valueOf(property);
    }
    try {
      return type.getConstructor(STRING_ARG).newInstance(property);
    } catch (Exception e) {
      throw new IllegalArgumentException("Can't instanciate " + type, e);
    }
  }

}
