package org.jproggy.conftreaty;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.fail;

import java.math.BigDecimal;
import java.util.Locale;
import java.util.Properties;
import java.util.Set;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import org.junit.Test;

public class PropertiesTransfererTest {
  @Test
  public void test0() {
    Properties props = new Properties();
    props.put("v1", "teststr");
    props.put("v2", "35");
    props.put("other", "true");
    PropertiesTransferer pt = new PropertiesTransferer(props);
    TestClass0 test = pt.transfer(TestClass0.class);
    assertEquals("teststr", test.v1);
    assertEquals(35, test.v2);
  }

  @Test
  public void test1() throws Exception {
    Properties props = new Properties();
    props.put("test.testBool", "false");
    props.put("test.testBigDecimal", "3.5");
    props.put("other", "true");
    PropertiesTransferer pt = new PropertiesTransferer(props);
    TestClass1 test = pt.transfer(TestClass1.class);
    assertEquals(false, test.testBool);
    assertEquals(Boolean.TRUE, test.testBool2);
    assertEquals(new BigDecimal("3.5"), test.testBigDecimal);
  }

  @Test
  public void test2() throws Exception {
    Properties props = new Properties();
    props.put("test.testBool", "false");
    props.put("other", "3.5");
    PropertiesTransferer pt = new PropertiesTransferer(props);
    TestClass2 test = pt.transfer(TestClass2.class);
    assertEquals("true", test.test1);
    assertEquals(null, test.test2);
    assertEquals(new BigDecimal("3.5"), test.testBigDecimal);
    Locale.setDefault(Locale.US);
    Set<ConstraintViolation<TestClass2>> validations = Validation.buildDefaultValidatorFactory().getValidator().validate(test);
    assertEquals(2, validations.size());
    for (ConstraintViolation<TestClass2> err: validations) {
      if ("must be greater than or equal to 10".equals(err.getMessage())) {
        assertEquals("{javax.validation.constraints.Min.message}", err.getMessageTemplate());
        assertEquals("testBigDecimal", err.getPropertyPath().toString());
      } else if ("may not be null".equals(err.getMessage())) {
        assertEquals("{javax.validation.constraints.NotNull.message}", err.getMessageTemplate());
        assertEquals("test2", err.getPropertyPath().toString());
      } else {
        fail();
      }
    }
  }

  @Test
  public void test3() throws Exception {
    Properties props = new Properties();
    props.put("test1.testBool", "true");
    props.put("x.other", "3.5");
    props.put("x.test2", "x");
    PropertiesTransferer pt = new PropertiesTransferer(props);
    TestClass3 test = pt.transfer(TestClass3.class);
    assertNotNull(test);
    assertNotNull(test.test1);
    assertNotNull(test.test2);
    assertEquals(new BigDecimal("3.5"), test.test2.testBigDecimal);
    assertEquals("x", test.test2.test2);
    assertEquals(true, test.test1.testBool);
  }

  @Test
  public void db() throws Exception{
    Properties props = new Properties();
    props.put("database.variant", "h2");
    props.put("database.user", "sa");
    props.put("orders.url", "h2://test/orders");
    props.put("orders.password", "$ecr3t!");
    props.put("products.url", "h2:test/products");
    PropertiesTransferer pt = new PropertiesTransferer(props);
    Schemas test = pt.transfer(Schemas.class);
    assertEquals("h2://test/orders", test.orders.url);
    assertEquals("h2:test/products", test.products.url);
    assertEquals("h2", test.products.variant);
  }

  @Path("database")
  public static class Schema {
    private String url;
    private String password;
    private String user;
    private String variant;
  }

  public static class Schemas {
    private Schema orders;
    private Schema products;
  }

  public static class TestClass0 {
    private String v1;
    private int v2;
  }

  @Path("test")
  public static class TestClass1 {
    private boolean testBool = false;
    @Path("other")
    private Boolean testBool2 = false;
    private BigDecimal testBigDecimal;
  }

  public static class TestClass2 {
    private final String test1 = "true";
    @NotNull
    private String test2;
    @Path("other")
    @Min(10)
    private BigDecimal testBigDecimal;
  }

  public static class TestClass3 {
    private TestClass1 test1;
    @Path("x")
    private TestClass2 test2;
  }
}
